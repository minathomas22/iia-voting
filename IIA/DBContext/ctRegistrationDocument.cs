//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IIA.DBContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class ctRegistrationDocument
    {
        public long DocumentId { get; set; }
        public Nullable<long> ProfileId { get; set; }
        public string DocumentUrl { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
    }
}
