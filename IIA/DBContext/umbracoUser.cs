//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IIA.DBContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class umbracoUser
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public umbracoUser()
        {
            this.ctElectionCommittees = new HashSet<ctElectionCommittee>();
            this.umbracoUserGroups = new HashSet<umbracoUserGroup>();
        }
    
        public int id { get; set; }
        public bool userDisabled { get; set; }
        public bool userNoConsole { get; set; }
        public string userName { get; set; }
        public string userLogin { get; set; }
        public string userPassword { get; set; }
        public string passwordConfig { get; set; }
        public string userEmail { get; set; }
        public string userLanguage { get; set; }
        public string securityStampToken { get; set; }
        public Nullable<int> failedLoginAttempts { get; set; }
        public Nullable<System.DateTime> lastLockoutDate { get; set; }
        public Nullable<System.DateTime> lastPasswordChangeDate { get; set; }
        public Nullable<System.DateTime> lastLoginDate { get; set; }
        public Nullable<System.DateTime> emailConfirmedDate { get; set; }
        public Nullable<System.DateTime> invitedDate { get; set; }
        public System.DateTime createDate { get; set; }
        public System.DateTime updateDate { get; set; }
        public string avatar { get; set; }
        public string tourData { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ctElectionCommittee> ctElectionCommittees { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<umbracoUserGroup> umbracoUserGroups { get; set; }
    }
}
