//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IIA.DBContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class ctElection
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ctElection()
        {
            this.ctElectionCommittees = new HashSet<ctElectionCommittee>();
            this.ctCandidates = new HashSet<ctCandidate>();
            this.ctElectionUserMails = new HashSet<ctElectionUserMail>();
            this.ctVotings = new HashSet<ctVoting>();
        }
    
        public long ElectionId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public System.DateTime ApplyStartDate { get; set; }
        public System.DateTime ApplyEndDate { get; set; }
        public int NumberOfWinners { get; set; }
        public System.DateTime WinningAnnouncementDate { get; set; }
        public int Status { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public bool CreatedByRedo { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string TitleAr { get; set; }
        public string ApplyTitle { get; set; }
        public string ApplyTitleAr { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ctElectionCommittee> ctElectionCommittees { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ctCandidate> ctCandidates { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ctElectionUserMail> ctElectionUserMails { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ctVoting> ctVotings { get; set; }
    }
}
