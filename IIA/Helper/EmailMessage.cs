﻿using CMS.Helpers;
using IIA.DBContext;
using IIA.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Runtime.Remoting.Contexts;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Web;
using System.Web.Configuration;
using umbraco.cms.helpers;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace IIA.Helper
{
    public static class EmailMessage
    {
        public static IIAContext db = new IIAContext();        
        public static string GetContactAdminMessage(ContactFormModel model, string lang)
        {
            string retVal = "";            
            if (model.ContactType=="ContactMember")
            {                
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/"+ lang + "/Contact_Member.html")))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_FirstName_%", model.FirstName);
                    retVal = retVal.Replace("%_LastName_%", model.LastName);
                    retVal = retVal.Replace("%_Email_%", model.EmailAddress);
                    retVal = retVal.Replace("%_ContactNo_%", model.ContactNo);
                    retVal = retVal.Replace("%_Message_%", model.Message);
                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                }
            }
            else if (model.ContactType == "ContactAdmin")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/Contact_Admin.html")))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_FirstName_%", model.FirstName);
                    retVal = retVal.Replace("%_LastName_%", model.LastName);
                    retVal = retVal.Replace("%_Email_%", model.EmailAddress);
                    retVal = retVal.Replace("%_ContactNo_%", model.ContactNo);
                    retVal = retVal.Replace("%_Message_%", model.Message);
                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                }
            }
            else if (model.ContactType == "ContactMentor")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/Contact_Mentor.html")))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_FirstName_%", model.FirstName);
                    retVal = retVal.Replace("%_LastName_%", model.LastName);
                    retVal = retVal.Replace("%_Email_%", model.EmailAddress);
                    retVal = retVal.Replace("%_ContactNo_%", model.ContactNo);
                    retVal = retVal.Replace("%_Message_%", model.Message);
                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                }
            }
            else if (model.ContactType == "ContactUs")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/Contact_Us.html")))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_FirstName_%", model.FirstName);
                    retVal = retVal.Replace("%_LastName_%", model.LastName);
                    retVal = retVal.Replace("%_Email_%", model.EmailAddress);
                    retVal = retVal.Replace("%_ContactNo_%", model.ContactNo);
                    retVal = retVal.Replace("%_Message_%", model.Message);
                    retVal = retVal.Replace("%_PointOfInterest_%", model.PageId);
                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                }
            }
            else if (model.ContactType == "RequestCallBack")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/RequestCallBack.html")))
                {
                    var helper = new UmbracoHelper(UmbracoContext.Current);

                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_FirstName_%", model.FirstName);
                    retVal = retVal.Replace("%_LastName_%", model.LastName);
                    retVal = retVal.Replace("%_Email_%", model.EmailAddress);
                    retVal = retVal.Replace("%_ContactNo_%", model.ContactNo);
                    retVal = retVal.Replace("%_Message_%", model.Message);
                    retVal = retVal.Replace("%_Subject_%", helper.TypedContent(model.PageId).GetPropertyValue("title").ToString());
                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                }
            }
            else if (model.ContactType == "DownloadBrochure")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/DownloadBrochure.html")))
                {
                    string Url = WebConfigurationManager.AppSettings["WebUrl"].ToString();
                    var helper = new UmbracoHelper(UmbracoContext.Current);
                    string Id = lang == "en" ? "1304" : "2525";
                    var BrochureList = helper.TypedContent(Id).GetPropertyValue<IEnumerable<IPublishedContent>>("brochureItem");
                    string link = "";
                    foreach (var item in BrochureList)
                    {
                        var pdf = item.GetPropertyValue<IPublishedContent>("pDF");
                        link += "<a href='" + Url + pdf.Url + "'>" + item.GetPropertyValue("title") + "</a><br/>";
                    }

                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", model.FirstName);
                    retVal = retVal.Replace("%_DownloadLink_%", link);                    
                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                }
            }
            else if (model.ContactType == "DownloadExamContent")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/DownloadBrochure.html")))
                {
                    string Url = WebConfigurationManager.AppSettings["WebUrl"].ToString();
                    var helper = new UmbracoHelper(UmbracoContext.Current);
                    var page = helper.TypedContent(model.PageId);
                    var pdf= page.GetPropertyValue<IPublishedContent>("examContentPDF").Url;
                    string link = "";
                    link += "<a href='" + Url + pdf + "'>" + page.GetPropertyValue("examContentTitle") + "</a>";

                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", model.FirstName);
                    retVal = retVal.Replace("%_DownloadLink_%", link);
                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                }
            }
            else if (model.ContactType == "AboutInternalAudition")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/AboutInternalAudition.html")))
                {
                    var helper = new UmbracoHelper(UmbracoContext.Current);

                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_FirstName_%", model.FirstName);
                    retVal = retVal.Replace("%_LastName_%", model.LastName);
                    retVal = retVal.Replace("%_Email_%", model.EmailAddress);
                    retVal = retVal.Replace("%_ContactNo_%", model.ContactNo);
                    retVal = retVal.Replace("%_JobTitle_%", model.Designation);
                    retVal = retVal.Replace("%_Organization_%", model.Company);                   
                    retVal = retVal.Replace("%_Message_%", model.Message);
                    retVal = retVal.Replace("%_Subject_%", model.PageId);
                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                }
            }
            return retVal;
        }

        public static string GetRegistrationMessage(ctRegistration model, string ContactType, string Url, string UserName, string lang)
        {
            string retVal = "";
            if (ContactType == "MemberEnrolled")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/Member_Enrolled.html")))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", lang == "en" ? model.FirstName : model.FirstName_ar);
                    retVal = retVal.Replace("%_FirstName_%", lang == "en" ? model.FirstName : model.FirstName_ar);
                    retVal = retVal.Replace("%_LastName_%", lang == "en" ? model.LastName : model.LastName_ar);
                    retVal = retVal.Replace("%_Email_%", model.EmailAddress);
                    retVal = retVal.Replace("%_ContactNo_%", model.MobileNumber);
                    retVal = retVal.Replace("%_Url_%", Url);
                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                }
            }
            else if (ContactType == "MemberRegistration")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/Member_Registration.html")))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", lang == "en" ? model.FirstName : model.FirstName_ar);
                    retVal = retVal.Replace("%_FirstName_%", lang == "en" ? model.FirstName : model.FirstName_ar);
                    retVal = retVal.Replace("%_MiddleName_%", lang == "en" ? model.MiddleName : model.MiddleName_ar);
                    retVal = retVal.Replace("%_LastName_%", lang == "en" ? model.LastName : model.LastName_ar);
                    retVal = retVal.Replace("%_Email_%", model.EmailAddress);
                    retVal = retVal.Replace("%_MobileNo_%", model.MobileNumber);
                    if (model.MembershipType == "Corporate")
                    {
                        retVal = retVal.Replace("%_MembershipType_%", GetMultilanguageText(model.MembershipType, lang) + ", " + GetMultilanguageText(model.MembershipBadge, lang));
                    }
                    else
                    {
                        retVal = retVal.Replace("%_MembershipType_%", GetMultilanguageText(model.MembershipType, lang));
                    }
                    
                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));

                    long PaymentId = long.Parse(Utility.GetApplicationCookies("payId").Value);                   
                    ctRegistrationPayment objDbRegistrationPayment = db.ctRegistrationPayments.Where(x => x.PaymentId == PaymentId).FirstOrDefault();

                    retVal = retVal.Replace("%_ShippingReference_%", objDbRegistrationPayment.ShippingReference);
                    retVal = retVal.Replace("%_InvoiceNumber_%", objDbRegistrationPayment.InvoiceNumber);
                    retVal = retVal.Replace("%_SubTotal_%", string.Format("{0:0.00}", objDbRegistrationPayment.SubTotal));
                    retVal = retVal.Replace("%_Shipping_%", string.Format("{0:0.00}", objDbRegistrationPayment.Shipping));
                    retVal = retVal.Replace("%_VAT_%", string.Format("{0:0.00}", objDbRegistrationPayment.VAT));
                    retVal = retVal.Replace("%_GrandTotal_%", string.Format("{0:0.00}", objDbRegistrationPayment.GrandTotal));
                    retVal = retVal.Replace("%_Discount_%", string.Format("{0:0.00}", objDbRegistrationPayment.Discount));
                }
            }
            else if (ContactType == "EnrollMemberRegistration")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/Member_Registration_Enroll.html")))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", lang == "en" ? model.FirstName : model.FirstName_ar);
                    retVal = retVal.Replace("%_FirstName_%", lang == "en" ? model.FirstName : model.FirstName_ar);
                    retVal = retVal.Replace("%_MiddleName_%", lang == "en" ? model.MiddleName : model.MiddleName_ar);
                    retVal = retVal.Replace("%_LastName_%", lang == "en" ? model.LastName : model.LastName_ar);
                    retVal = retVal.Replace("%_Email_%", model.EmailAddress);
                    retVal = retVal.Replace("%_MobileNo_%", model.MobileNumber);
                    retVal = retVal.Replace("%_MembershipType_%", GetMultilanguageText(model.MembershipType,lang) + ", " + GetMultilanguageText(model.MembershipBadge, lang));
                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));                   
                }
            }
            else if (ContactType == "NonMemberRegistration")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/Non_Member_Registration.html")))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", lang == "en" ? model.FirstName : model.FirstName_ar);
                    retVal = retVal.Replace("%_FirstName_%", lang == "en" ? model.FirstName : model.FirstName_ar);
                    retVal = retVal.Replace("%_MiddleName_%", lang == "en" ? model.MiddleName : model.MiddleName_ar);
                    retVal = retVal.Replace("%_LastName_%", lang == "en" ? model.LastName : model.LastName_ar);
                    retVal = retVal.Replace("%_Email_%", model.EmailAddress);
                    retVal = retVal.Replace("%_MobileNo_%", model.MobileNumber);                   
                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                }
            }
            else if (ContactType == "ApplyMentoring")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/ApplyMentoring.html")))
                {
                    retVal = reader.ReadToEnd();                    
                    retVal = retVal.Replace("%_FirstName_%", lang == "en" ? model.FirstName : model.FirstName_ar);
                    retVal = retVal.Replace("%_MiddleName_%", lang == "en" ? model.MiddleName : model.MiddleName_ar);
                    retVal = retVal.Replace("%_LastName_%", lang == "en" ? model.LastName : model.LastName_ar);
                    retVal = retVal.Replace("%_Email_%", model.EmailAddress);
                    retVal = retVal.Replace("%_MobileNo_%", model.MobileNumber);
                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                }
            }
            else if (ContactType == "MemberActivation")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/MemberActivation.html")))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", lang == "en" ? model.FirstName : model.FirstName_ar);
                    retVal = retVal.Replace("%_IIAGlobalNumber_%", model.IIAGlobalNumber);
                    if (!string.IsNullOrEmpty(model.MembershipBadge))
                    {
                        retVal = retVal.Replace("%_MembershipType_%", GetMultilanguageText(model.MembershipType, lang) + ", " + GetMultilanguageText(model.MembershipBadge, lang));
                    }
                    else
                    {
                        retVal = retVal.Replace("%_MembershipType_%", GetMultilanguageText(model.MembershipType, lang));
                    }
                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                }
            }
            else if (ContactType == "NonMemberActivation")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/NonMemberActivation.html")))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", lang == "en" ? model.FirstName : model.FirstName_ar); 
                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                }
            }
            else if (ContactType == "ApproveMentoring")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/ApproveMentoring.html")))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", lang == "en" ? model.FirstName : model.FirstName_ar);
                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                }
            }
            else if (ContactType == "ForgotPassword")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/ForgotPassword.html")))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", lang == "en" ? model.FirstName : model.FirstName_ar);                    
                    retVal = retVal.Replace("%_URL_%", Url);
                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                }
            }
            else if (ContactType == "MembershipRenewal")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/Membership_Renewal.html")))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", lang == "en" ? model.FirstName : model.FirstName_ar);
                    retVal = retVal.Replace("%_FirstName_%", lang == "en" ? model.FirstName : model.FirstName_ar);
                    retVal = retVal.Replace("%_MiddleName_%", lang == "en" ? model.MiddleName : model.MiddleName_ar);
                    retVal = retVal.Replace("%_LastName_%", lang == "en" ? model.LastName : model.LastName_ar);
                    retVal = retVal.Replace("%_Email_%", model.EmailAddress);
                    retVal = retVal.Replace("%_MobileNo_%", model.MobileNumber);
                    retVal = retVal.Replace("%_ExpireDate_%", model.MembershipEndDate.Value.ToString("dd MMM yyyy"));
                    if (model.MembershipType == "Corporate")
                    {
                        retVal = retVal.Replace("%_MembershipType_%", GetMultilanguageText(model.MembershipType, lang) + ", " + GetMultilanguageText(model.MembershipBadge, lang));
                    }
                    else
                    {
                        retVal = retVal.Replace("%_MembershipType_%", GetMultilanguageText(model.MembershipType, lang));
                    }

                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));

                    long PaymentId = long.Parse(Utility.GetApplicationCookies("payId").Value);
                    ctRegistrationPayment objDbRegistrationPayment = db.ctRegistrationPayments.Where(x => x.PaymentId == PaymentId).FirstOrDefault();

                    retVal = retVal.Replace("%_ShippingReference_%", objDbRegistrationPayment.ShippingReference);
                    retVal = retVal.Replace("%_InvoiceNumber_%", objDbRegistrationPayment.InvoiceNumber);
                    retVal = retVal.Replace("%_SubTotal_%", string.Format("{0:0.00}", objDbRegistrationPayment.SubTotal));
                    retVal = retVal.Replace("%_Shipping_%", string.Format("{0:0.00}", objDbRegistrationPayment.Shipping));
                    retVal = retVal.Replace("%_VAT_%", string.Format("{0:0.00}", objDbRegistrationPayment.VAT));
                    retVal = retVal.Replace("%_GrandTotal_%", string.Format("{0:0.00}", objDbRegistrationPayment.GrandTotal));
                    retVal = retVal.Replace("%_Discount_%", string.Format("{0:0.00}", objDbRegistrationPayment.Discount));
                }
            }
            else if (ContactType == "RenewalReminder")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/Renewal_Reminder.html")))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", lang == "en" ? model.FirstName : model.FirstName_ar);                   
                    if (model.MembershipType == "Corporate")
                    {
                        retVal = retVal.Replace("%_MembershipType_%", GetMultilanguageText(model.MembershipType, lang) + ", " + GetMultilanguageText(model.MembershipBadge, lang));
                    }
                    else
                    {
                        retVal = retVal.Replace("%_MembershipType_%", GetMultilanguageText(model.MembershipType, lang));
                    }

                    retVal = retVal.Replace("%_ExpireDate_%", model.MembershipEndDate.Value.ToString("dd MMM yyyy"));
                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));                    
                }
            }
            else if (ContactType == "MembershipUpgrade")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/Membership_Upgrade.html")))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", lang == "en" ? model.FirstName : model.FirstName_ar);
                    retVal = retVal.Replace("%_FirstName_%", lang == "en" ? model.FirstName : model.FirstName_ar);
                    retVal = retVal.Replace("%_MiddleName_%", lang == "en" ? model.MiddleName : model.MiddleName_ar);
                    retVal = retVal.Replace("%_LastName_%", lang == "en" ? model.LastName : model.LastName_ar);
                    retVal = retVal.Replace("%_Email_%", model.EmailAddress);
                    retVal = retVal.Replace("%_MobileNo_%", model.MobileNumber);
                    retVal = retVal.Replace("%_ExpireDate_%", model.MembershipEndDate.Value.ToString("dd MMM yyyy"));
                    if (model.MembershipType == "Corporate")
                    {
                        retVal = retVal.Replace("%_MembershipType_%", GetMultilanguageText(model.MembershipType, lang) + ", " + GetMultilanguageText(model.MembershipBadge, lang));
                    }
                    else
                    {
                        retVal = retVal.Replace("%_MembershipType_%", GetMultilanguageText(model.MembershipType, lang));
                    }

                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));

                    long PaymentId = long.Parse(Utility.GetApplicationCookies("payId").Value);
                    ctRegistrationPayment objDbRegistrationPayment = db.ctRegistrationPayments.Where(x => x.PaymentId == PaymentId).FirstOrDefault();

                    retVal = retVal.Replace("%_ShippingReference_%", objDbRegistrationPayment.ShippingReference);
                    retVal = retVal.Replace("%_InvoiceNumber_%", objDbRegistrationPayment.InvoiceNumber);
                    retVal = retVal.Replace("%_SubTotal_%", string.Format("{0:0.00}", objDbRegistrationPayment.SubTotal));
                    retVal = retVal.Replace("%_Shipping_%", string.Format("{0:0.00}", objDbRegistrationPayment.Shipping));
                    retVal = retVal.Replace("%_VAT_%", string.Format("{0:0.00}", objDbRegistrationPayment.VAT));
                    retVal = retVal.Replace("%_GrandTotal_%", string.Format("{0:0.00}", objDbRegistrationPayment.GrandTotal));
                    retVal = retVal.Replace("%_Discount_%", string.Format("{0:0.00}", objDbRegistrationPayment.Discount));
                }
            }
            return retVal;
        }

        public static string GetInvoiceMessage(InvoiceViewModel model, string CartItem, string lang)
        {
            string retVal = "";
            var ShippingInfo = db.ctShippingBillings.Where(x => x.ProfileId == model.InvoiceInfo.ProfileId).FirstOrDefault();
            var ProfileInfo = db.ctRegistrations.Where(x => x.ProfileId == model.InvoiceInfo.ProfileId).FirstOrDefault();

            using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/Cart_Purchase.html")))
            {
                retVal = reader.ReadToEnd();
                retVal = retVal.Replace("%_UserName_%", lang == "en" ? ProfileInfo.FirstName : ProfileInfo.FirstName_ar);
                retVal = retVal.Replace("%_Name_%", ShippingInfo.SMiddleName != "" ? ShippingInfo.SFirstName + " " + ShippingInfo.SMiddleName + " " + ShippingInfo.SLastName : ShippingInfo.SFirstName + " " + ShippingInfo.SLastName);
                retVal = retVal.Replace("%_Email_%", ShippingInfo.SEmailAddress);
                retVal = retVal.Replace("%_MobileNo_%", ShippingInfo.SMobileNumber);
                retVal = retVal.Replace("%_Country_%", db.CountryLists.Where(x => x.CountryCode == ShippingInfo.SCountry).FirstOrDefault().CountryName);
                retVal = retVal.Replace("%_City_%", ShippingInfo.SCity);
                retVal = retVal.Replace("%_Address_%", ShippingInfo.SAddress1 + "," + ShippingInfo.SAddress2 + "," + ShippingInfo.SAddress3);
                retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                retVal = retVal.Replace("%_IIAGlobalNumber_%", ProfileInfo.IIAGlobalNumber);

                retVal = retVal.Replace("%_ShippingReference_%", model.InvoiceInfo.ShippingReference);
                retVal = retVal.Replace("%_InvoiceNumber_%", model.InvoiceInfo.InvoiceNumber);
                retVal = retVal.Replace("%_SubTotal_%", string.Format("{0:0.00}", model.InvoiceInfo.SubTotal));
                retVal = retVal.Replace("%_Shipping_%", string.Format("{0:0.00}", model.InvoiceInfo.Shipping));
                retVal = retVal.Replace("%_VAT_%", string.Format("{0:0.00}", model.InvoiceInfo.VAT));
                retVal = retVal.Replace("%_Discount_%", string.Format("{0:0.00}", model.InvoiceInfo.Discount));
                retVal = retVal.Replace("%_GrandTotal_%", string.Format("{0:0.00}", model.InvoiceInfo.GrandTotal));                              

                retVal = retVal.Replace("%_CartItem_%", CartItem);

            }
            return retVal;
        }

        public static string GetCareerApplicationMessage(CareerApplyModel model, string lang)
        {
            string retVal = "";
            if (model.ApplyFor == "IIAPost")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/CareerApply.html")))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", lang == "en" ? "Admin" : "المشرف");
                    retVal = retVal.Replace("%_FirstName_%", model.FirstName);
                    retVal = retVal.Replace("%_LastName_%", model.LastName);
                    retVal = retVal.Replace("%_Email_%", model.EmailAddress);
                    retVal = retVal.Replace("%_ContactNo_%", model.ContactNo);
                    retVal = retVal.Replace("%_CV_%", WebConfigurationManager.AppSettings["WebUrl"].ToString() + "Media/ApplicantFile/" + model.CV);
                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                    retVal = retVal.Replace("%_ApplyFor_%", model.PostType);
                    retVal = retVal.Replace("%_Title_%", model.PostTitle);
                }
            }
            else if (model.ApplyFor == "CorporatePost")
            {
                var ProfileId = db.ctJobInternships.Where(x => x.PostId == model.PostId).FirstOrDefault().ProfileId;
                var ctRegistration = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/CareerApply.html")))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", lang == "en" ? ctRegistration.FirstName : ctRegistration.FirstName_ar);
                    retVal = retVal.Replace("%_FirstName_%", model.FirstName);
                    retVal = retVal.Replace("%_LastName_%", model.LastName);
                    retVal = retVal.Replace("%_Email_%", model.EmailAddress);
                    retVal = retVal.Replace("%_ContactNo_%", model.ContactNo);
                    retVal = retVal.Replace("%_CV_%", WebConfigurationManager.AppSettings["WebUrl"].ToString() + "Media/ApplicantFile/" + model.CV);
                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                    retVal = retVal.Replace("%_ApplyFor_%", model.PostType);
                    retVal = retVal.Replace("%_Title_%", model.PostTitle);
                }
            }
            return retVal;
        }

        public static string GetEventRegistrationMessage(EventRegister model, string lang)
        {
            string retVal = "";
            using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/EventRegister.html")))
            {
                retVal = reader.ReadToEnd();                
                retVal = retVal.Replace("%_FirstName_%", model.FirstName);
                retVal = retVal.Replace("%_LastName_%", model.LastName);
                retVal = retVal.Replace("%_Email_%", model.EmailAddress);
                retVal = retVal.Replace("%_ContactNo_%", model.ContactNo);                
                retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                retVal = retVal.Replace("%_Country_%", model.Country);
                retVal = retVal.Replace("%_City_%", model.City);
                retVal = retVal.Replace("%_ZipCode_%", model.ZipCode);
                retVal = retVal.Replace("%_EventTitle_%", model.EventCmsName);
            }           
           
            return retVal;
        }

        public static string GetEventRegistrationMessageUser(string UserName, string EmailMessage, string Dates, string Time, string Place, string Location, string lang)
        {
            string retVal = "";
            using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/EventRegisterAcknowledge.html")))
            {
                retVal = reader.ReadToEnd();
                retVal = retVal.Replace("%_UserName_%", UserName);
                retVal = retVal.Replace("%_EmailMessage_%", EmailMessage);
                retVal = retVal.Replace("%_Date_%", Dates);
                retVal = retVal.Replace("%_Time_%", Time);
                retVal = retVal.Replace("%_Place_%", Place);
                retVal = retVal.Replace("%_Location_%", Location);
                retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));                
            }

            return retVal;
        }

        public static string GetStockNotificationMessage(string productName, int currentStock, int stockLimit, string lang)
        {
            string retVal = "";
            using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/StockNotification.html")))
            {
                retVal = reader.ReadToEnd();
                retVal = retVal.Replace("%_Name_%", productName);
                retVal = retVal.Replace("%_CurrentStock_%", currentStock.ToString());
                retVal = retVal.Replace("%_MinStockLimit_%", stockLimit.ToString());
                retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
            }

            return retVal;
        }

        public static string GetCorporatePostMessage(JobInternshipViewModel model, string lang)
        {
            string retVal = "";
            using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/" + lang + "/JobInternship.html")))
            {
                retVal = reader.ReadToEnd();
                retVal = retVal.Replace("%_PostType_%", model.PostType);
                retVal = retVal.Replace("%_Title_%", model.JobTitle);
                retVal = retVal.Replace("%_Company_%", model.Company);
                retVal = retVal.Replace("%_Location_%", model.Location);
                retVal = retVal.Replace("%_Tenure_%", model.Tenure);
                retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
            }

            return retVal;
        }

        public static string GetExternalCourseMessage(string ContactType, long RegId, string lang)
        {
            string retVal = "";
            var regInfo = db.ctExternalCourseRegistrations.Where(x=>x.RegId==RegId).AsNoTracking().FirstOrDefault();            
            var profileInfo = db.ctRegistrations.Where(x => x.ProfileId == regInfo.ProfileId).AsNoTracking().FirstOrDefault();
            var helper = new UmbracoHelper(UmbracoContext.Current);
            var CourseInfo = helper.TypedContent(regInfo.CourseCmsId);

            if (ContactType == "UploadReceipt")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/ExternalCourseEmailTemplate/" + lang + "/UploadReceipt.html")))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", lang == "en" ? profileInfo.FirstName : profileInfo.FirstName_ar);
                    retVal = retVal.Replace("%_CourseName_%", regInfo.CourseCmsTitle);
                    retVal = retVal.Replace("%_PaymentReceipt_%", !string.IsNullOrEmpty(regInfo.EnglishSpokenPaymentReceipt)? "<a href='"+ WebConfigurationManager.AppSettings["WebUrl"].ToString() + "Media/ApplicantFile/" + regInfo.EnglishSpokenPaymentReceipt + "'>View file</a>": "");
                    retVal = retVal.Replace("%_Result_%", !string.IsNullOrEmpty(regInfo.EnglishSpokenResult) ? "<a href='" + WebConfigurationManager.AppSettings["WebUrl"].ToString() + "Media/ApplicantFile/" + regInfo.EnglishSpokenResult + "'>View file</a>" : "");

                    retVal = retVal.Replace("%_FirstName_%", lang == "en" ? profileInfo.FirstName : profileInfo.FirstName_ar);
                    retVal = retVal.Replace("%_MiddleName_%", lang == "en" ? profileInfo.MiddleName : profileInfo.MiddleName_ar);
                    retVal = retVal.Replace("%_LastName_%", lang == "en" ? profileInfo.LastName : profileInfo.LastName_ar);
                    retVal = retVal.Replace("%_Email_%", profileInfo.EmailAddress);
                    retVal = retVal.Replace("%_MobileNo_%", profileInfo.MobileNumber);

                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                }
            }
            else if (ContactType == "InterviewDate")
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/ExternalCourseEmailTemplate/" + lang + "/InterviewDate.html")))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", lang == "en" ? profileInfo.FirstName : profileInfo.FirstName_ar);
                    retVal = retVal.Replace("%_CourseName_%", regInfo.CourseCmsTitle);
                    retVal = retVal.Replace("%_InterviewDate_%", regInfo.InterviewDate.Value.ToString("dd MMM yyyy"));
                    retVal = retVal.Replace("%_InterviewTime_%", regInfo.InterviewTime);

                    retVal = retVal.Replace("%_FirstName_%", lang == "en" ? profileInfo.FirstName : profileInfo.FirstName_ar);
                    retVal = retVal.Replace("%_MiddleName_%", lang == "en" ? profileInfo.MiddleName : profileInfo.MiddleName_ar);
                    retVal = retVal.Replace("%_LastName_%", lang == "en" ? profileInfo.LastName : profileInfo.LastName_ar);
                    retVal = retVal.Replace("%_Email_%", profileInfo.EmailAddress);
                    retVal = retVal.Replace("%_MobileNo_%", profileInfo.MobileNumber);

                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                }
            }
            else if (ContactType == "CoursePayment")
            {
                var emailTemplate = "";
                if (regInfo.IsInterviewPass == "Y")
                {
                    emailTemplate = "~/ExternalCourseEmailTemplate/" + lang + "/CoursePayment.html";
                }
                else
                {
                    emailTemplate = "~/ExternalCourseEmailTemplate/" + lang + "/ApplicationFeePayment.html";
                }
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath(emailTemplate)))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", lang == "en" ? profileInfo.FirstName : profileInfo.FirstName_ar);
                    retVal = retVal.Replace("%_CourseName_%", regInfo.CourseCmsTitle);

                    if (regInfo.IsSeatConfirmed == "Y")
                    {
                        retVal = retVal.Replace("%_SeatInfo_%", CourseInfo.GetPropertyValue("seatConfirmedMessage").ToString());
                    }
                    else
                    {
                        retVal = retVal.Replace("%_SeatInfo_%", CourseInfo.GetPropertyValue("seatFullOccupiedMessage").ToString());
                    }

                    retVal = retVal.Replace("%_FirstName_%", lang == "en" ? profileInfo.FirstName : profileInfo.FirstName_ar);
                    retVal = retVal.Replace("%_MiddleName_%", lang == "en" ? profileInfo.MiddleName : profileInfo.MiddleName_ar);
                    retVal = retVal.Replace("%_LastName_%", lang == "en" ? profileInfo.LastName : profileInfo.LastName_ar);
                    retVal = retVal.Replace("%_Email_%", profileInfo.EmailAddress);
                    retVal = retVal.Replace("%_MobileNo_%", profileInfo.MobileNumber);

                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));

                    long PaymentId = long.Parse(Utility.GetApplicationCookies("ecpayId").Value);
                    ctExternalCoursePayment objDbExternalCoursePayment = db.ctExternalCoursePayments.Where(x => x.ExternalPaymentId == PaymentId).FirstOrDefault();
                                       
                    retVal = retVal.Replace("%_InvoiceNumber_%", objDbExternalCoursePayment.InvoiceNumber);
                    retVal = retVal.Replace("%_SubTotal_%", string.Format("{0:0.00}", objDbExternalCoursePayment.SubTotal));
                    retVal = retVal.Replace("%_Shipping_%", string.Format("{0:0.00}", objDbExternalCoursePayment.Shipping));
                    retVal = retVal.Replace("%_VAT_%", string.Format("{0:0.00}", objDbExternalCoursePayment.VAT));
                    retVal = retVal.Replace("%_GrandTotal_%", string.Format("{0:0.00}", objDbExternalCoursePayment.GrandTotal));
                    retVal = retVal.Replace("%_Discount_%", string.Format("{0:0.00}", objDbExternalCoursePayment.Discount));


                }
            }
            else if (ContactType == "SpokenResultStatusUpdate")
            {
                var TemplateName = regInfo.IsEnglishSpokenPass == "Y" ? "EnglishSpokenPass.html" : "EnglishSpokenFail.html";

                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/ExternalCourseEmailTemplate/" + lang + "/" + TemplateName)))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", lang == "en" ? profileInfo.FirstName : profileInfo.FirstName_ar);
                    retVal = retVal.Replace("%_CourseName_%", regInfo.CourseCmsTitle);
                    retVal = retVal.Replace("%_ResultScore_%", regInfo.EnglishSpokenResultScore);
                    retVal = retVal.Replace("%_ResultStatus_%", regInfo.IsEnglishSpokenPass == "Y" ? GetMultilanguageText("Passed", lang) : GetMultilanguageText("Failed", lang));

                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                }
            }
            else if (ContactType == "InterviewStatusUpdate")
            {
                var TemplateName = regInfo.IsInterviewPass == "Y" ? "InterviewPass.html" : "InterviewFail.html";

                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/ExternalCourseEmailTemplate/" + lang + "/" + TemplateName)))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", lang == "en" ? profileInfo.FirstName : profileInfo.FirstName_ar);
                    retVal = retVal.Replace("%_CourseName_%", regInfo.CourseCmsTitle);                   
                    retVal = retVal.Replace("%_ResultStatus_%", regInfo.IsInterviewPass == "Y" ? GetMultilanguageText("Passed", lang) : GetMultilanguageText("Failed", lang));

                    retVal = retVal.Replace("%_Submission_Date_%", DateTime.Now.ToString("dd MMM yyyy"));
                }
            }
            return retVal;
        }

        public static string GetMultilanguageText(string text, string lang)
        {
            string retText = text;

            if (text == "Students" && lang == "ar")
            {
                retText = "الطلاب";
            }           
            else if (text == "Individuals" && lang == "ar")
            {
                retText = "الأفراد";
            }
            else if (text == "Corporate" && lang == "ar")
            {
                retText = "المؤسسة";
            }
            else if (text == "Gold" && lang == "ar")
            {
                retText = "الذهبية";
            }
            else if (text == "Silver" && lang == "ar")
            {
                retText = "الفضية";
            }
            else if (text == "Bronze" && lang == "ar")
            {
                retText = "البرونزية";
            }
            else if (text == "Bronze" && lang == "ar")
            {
                retText = "البرونزية";
            }
            else if (text == "Professional" && lang == "ar")
            {
                retText = "متخصص";
            }
            else if (text == "Passed" && lang == "ar")
            {
                retText = "تم الاجتياز بنجاح";
            }
            else if (text == "Failed" && lang == "ar")
            {
                retText = "باءت بالفشل";
            }
            return retText;
        }

        public static string GetVotingMailMeesageBasedOnType(string lang , VotingMailType type)
        {
            string retVal = string.Empty;
            string mailHtmlPath = string.Concat("~/EmailTemplate/", lang);
            string mailHtml = type == VotingMailType.NominationApplications ? string.Concat(mailHtmlPath, "/VotingMail.html") :
                              type == VotingMailType.ElectionVotingStarts ? string.Concat(mailHtmlPath, "/MemberVotingStartedMail.html") : "";

            if (!string.IsNullOrEmpty(mailHtml))
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath(mailHtml)))
                {
                    retVal = reader.ReadToEnd();
                }
            }
            return retVal;
        }

        public static string GetMemberElectionObjectionForCandidate(string lang, ctCandidateInfo candidateInfo , string firstname = "", string firstnameAr = "")
        {
            string retVal = string.Empty;
            string mailHtml = string.Concat("~/EmailTemplate/", lang , "/MemberObjectionCandidateMail.html");
            string regName = lang == "en" ? firstname : firstnameAr;
            if (!string.IsNullOrEmpty(mailHtml))
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath(mailHtml)))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", regName);
                    retVal = retVal.Replace("%_ShortBio_%", candidateInfo.Bio);
                    retVal = retVal.Replace("%_Description_%", candidateInfo.Description);
                    retVal = retVal.Replace("%_Resume_%", xhtml.TidyHtml(GetUrlStringForCandidateMail(candidateInfo.ResumeFile, lang)));
                    retVal = retVal.Replace("%_DevelopmentPlan_%", xhtml.TidyHtml(GetUrlStringForCandidateMail(candidateInfo.ElectionProgramFile, lang)));
                    retVal = retVal.Replace("%_LetterOfInterest_%", xhtml.TidyHtml(GetUrlStringForCandidateMail(candidateInfo.LetterOfInterestFiles, lang)));
                    retVal = retVal.Replace("%_EducationCertifications_%", xhtml.TidyHtml(GetUrlStringForOtherDocumentsCandidate(candidateInfo.OtherDocumentFile, lang)));
                }
            }
            return retVal;
        }

        public static string GetMemberElectionObjectionForCandidateToCommittee(string lang, ctCandidateInfo candidateInfo, string firstname = "", string firstnameAr = "")
        {
            string retVal = string.Empty;
            string mailHtml = string.Concat("~/EmailTemplate/", lang, "/MemberObjectionCandidateMail.html");
            string regName = lang == "en" ? firstname : firstnameAr;
            if (!string.IsNullOrEmpty(mailHtml))
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath(mailHtml)))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", regName);
                    retVal = retVal.Replace("%_ShortBio_%", candidateInfo.Bio);
                    retVal = retVal.Replace("%_Description_%", candidateInfo.Description);
                    retVal = retVal.Replace("%_Resume_%", xhtml.TidyHtml(GetUrlStringForCandidateMail(candidateInfo.ResumeFile, lang)));
                    retVal = retVal.Replace("%_DevelopmentPlan_%", xhtml.TidyHtml(GetUrlStringForCandidateMail(candidateInfo.ElectionProgramFile, lang)));
                    retVal = retVal.Replace("%_LetterOfInterest_%", xhtml.TidyHtml(GetUrlStringForCandidateMail(candidateInfo.LetterOfInterestFiles, lang)));
                    retVal = retVal.Replace("%_EducationCertifications_%", xhtml.TidyHtml(GetUrlStringForOtherDocumentsCandidate(candidateInfo.OtherDocumentFile, lang)));
                }
            }
            return retVal;
        }
        
        public static string GetMemberElectionForApplyCandidate(string lang, ctCandidateInfo candidateInfo , string firstname= "" , string firstnameAr = "")
        {
            string retVal = string.Empty;
            string mailHtml = string.Concat("~/EmailTemplate/" , lang , "/MemberObjectionCandidateToCommitteeMail.html");
            string regName = lang == "en" ? firstname : firstnameAr;
            if(!string.IsNullOrEmpty(mailHtml))
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath(mailHtml)))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", regName);
                    retVal = retVal.Replace("%_ShortBio_%", candidateInfo.Bio);
                    retVal = retVal.Replace("%_Description_%", candidateInfo.Description);
                    retVal = retVal.Replace("%_Resume_%", xhtml.TidyHtml(GetUrlStringForCandidateMail(candidateInfo.ResumeFile, lang)));
                    retVal = retVal.Replace("%_DevelopmentPlan_%", xhtml.TidyHtml(GetUrlStringForCandidateMail(candidateInfo.ElectionProgramFile , lang)));
                    retVal = retVal.Replace("%_LetterOfInterest_%", xhtml.TidyHtml(GetUrlStringForCandidateMail(candidateInfo.LetterOfInterestFiles , lang)));
                    retVal = retVal.Replace("%_EducationCertifications_%", xhtml.TidyHtml(GetUrlStringForOtherDocumentsCandidate(candidateInfo.OtherDocumentFile , lang)));
                }
            }
            return retVal;
        }
        public static string GetMemberElectionForApplyCandidateStatusApproved(string lang, string firstname = "", string firstnameAr = "")
        {
            string retVal = string.Empty;
            string mailHtml = string.Concat("~/EmailTemplate/", lang, "/MemberElectionCandidateStatusApprovedMail.html");
            string regName = lang == "en" ? firstname : firstnameAr;
            if (!string.IsNullOrEmpty(mailHtml))
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath(mailHtml)))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", regName);
                    retVal = retVal.Replace("%_Year_%", DateTime.Now.Year.ToString());
                }
            }
            return retVal;
        }
        public static string GetMemberElectionForApplyCandidateStatusRejected(string lang, string firstname = "", string firstnameAr = "")
        {
            string retVal = string.Empty;
            string mailHtml = string.Concat("~/EmailTemplate/", lang, "/MemberElectionCandidateStatusRejectedMail.html");
            string regName = lang == "en" ? firstname : firstnameAr;
            if (!string.IsNullOrEmpty(mailHtml))
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath(mailHtml)))
                {
                    retVal = reader.ReadToEnd();
                    retVal = retVal.Replace("%_UserName_%", regName);
                    retVal = retVal.Replace("%_Year_%", DateTime.Now.Year.ToString());
                }
            }
            return retVal;
        }

        public static string GetUrlStringForCandidateMail(string url , string lang)
        {
            var urlHost = HttpContext.Current.Request.Url.Host;
            var innerText = lang == "en" ? "Show File" : "عرض الملف";
            var urlLink = urlHost + "/Media/ApplicantFile/" + url;
            var ancor = $@"<a href="+urlLink+ " style='margin-left:5px; margin-right:5px;'> "+innerText+" </a>";
            return ancor;
        }

        public static string GetUrlStringForOtherDocumentsCandidate(string otherDocumentFile , string lang)
        {
            var otherDocuments = JsonConvert.DeserializeObject<List<string>>(otherDocumentFile);
            var otherDocumentString = string.Empty;
            foreach (var doc in otherDocuments)
            {
                var str = GetUrlStringForCandidateMail(doc , lang);
                otherDocumentString = string.Concat(otherDocumentString, str);
            }
            return otherDocumentString;
        }

    }
}