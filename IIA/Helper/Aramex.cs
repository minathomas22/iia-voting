﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.IO;
using Umbraco.Core.Models;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Core.Services;
using System.Globalization;
using IIA.DBContext;
using System.Net;
using Newtonsoft.Json;
using System.Data.Entity;
using IIA.Models;
using Umbraco.Web.Mvc;
using IIA.ShippingRate;
using IIA.Shipping;
using IIA.Location;
using umbraco.cms.businesslogic;
using System.Threading.Tasks;

namespace IIA.Helpers
{    
    public static class Aramex
    {
        public static IIAContext db = new IIAContext();

        //public static decimal GetShippingRate(long profileId)
        //{           
        //    decimal rate = 37;                     
        //    return rate;
        //}

        public static decimal GetShippingRate(string CartKey)
        {
            decimal rate = 0;
            if (!string.IsNullOrEmpty(CartKey))
            {
                if (db.ctCarts.Where(x => x.CartKey == CartKey && x.ProductType == "P").Count() > 0)
                {
                    rate = 37;
                }
                else
                {
                    if (db.ctCarts.Where(x => x.CartKey == CartKey && x.ProductType == "C").Count() > 0)
                    {
                        try
                        {
                            var CartList = db.ctCarts.Where(x => x.CartKey == CartKey).ToList();
                            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
                            foreach (var item in CartList)
                            {
                                var CourseId = item.CourseCmsId;
                                var CourseContent = umbracoHelper.TypedContent(CourseId);
                                var IsShipppingAvailable = CourseContent.GetPropertyValue<Boolean>("isApplicableShipping");
                                if (IsShipppingAvailable == true)
                                {
                                    rate = 37;
                                    break;
                                }

                            }
                        }
                        catch
                        {

                        }
                    }
                }
            }
            return rate;
        }

        //public static decimal  GetShippingRate(long profileId)
        //{
        //    var BillingShipping = db.ctShippingBillings.Where(x => x.ProfileId == profileId).FirstOrDefault();
        //    var AramexInfo = db.ctAramexInfoes.FirstOrDefault();

        //    decimal rate = 0;

        //    RateCalculatorRequest _RateRequest = new RateCalculatorRequest();

        //    _RateRequest.ClientInfo = new ShippingRate.ClientInfo();
        //    _RateRequest.ClientInfo.AccountCountryCode = AramexInfo.AccountCountryCode;
        //    _RateRequest.ClientInfo.AccountEntity = AramexInfo.AccountEntity;
        //    _RateRequest.ClientInfo.AccountNumber = AramexInfo.AccountNumber;
        //    _RateRequest.ClientInfo.AccountPin = AramexInfo.AccountPin;
        //    _RateRequest.ClientInfo.UserName = AramexInfo.UserName;
        //    _RateRequest.ClientInfo.Password = AramexInfo.Password;
        //    _RateRequest.ClientInfo.Version = "v2.0";            

        //    _RateRequest.OriginAddress = new ShippingRate.Address();
        //    _RateRequest.OriginAddress.City = AramexInfo.OriginCity;
        //    _RateRequest.OriginAddress.CountryCode = AramexInfo.AccountCountryCode;

        //    _RateRequest.DestinationAddress = new ShippingRate.Address();
        //    _RateRequest.DestinationAddress.City = BillingShipping.SCity;
        //    _RateRequest.DestinationAddress.CountryCode = BillingShipping.SCountry;

        //    _RateRequest.ShipmentDetails = new ShippingRate.ShipmentDetails();

        //    if (BillingShipping.SCountry != AramexInfo.AccountCountryCode)
        //    {
        //        _RateRequest.ShipmentDetails.ProductGroup = "EXP";
        //        _RateRequest.ShipmentDetails.ProductType = "PDX";
        //    }
        //    else
        //    {
        //        _RateRequest.ShipmentDetails.ProductGroup = "DOM";
        //        _RateRequest.ShipmentDetails.ProductType = "OND";
        //    }

        //    _RateRequest.ShipmentDetails.PaymentType = "P";

        //    _RateRequest.ShipmentDetails.ActualWeight = new ShippingRate.Weight();
        //    _RateRequest.ShipmentDetails.ActualWeight.Value = Convert.ToDouble(1);
        //    _RateRequest.ShipmentDetails.ActualWeight.Unit = "KG";

        //    _RateRequest.ShipmentDetails.ChargeableWeight = new ShippingRate.Weight();
        //    _RateRequest.ShipmentDetails.ChargeableWeight.Value = Convert.ToDouble(1);
        //    _RateRequest.ShipmentDetails.ChargeableWeight.Unit = "KG";

        //    _RateRequest.ShipmentDetails.NumberOfPieces = 1;

        //    ShippingRate.Service_1_0Client _Client = new ShippingRate.Service_1_0Client();


        //    try
        //    {
        //        bool _HasErrors = false;
        //        ShippingRate.Money _TotalAmount;
        //        RateDetails rateDetails;

        //       ShippingRate.Notification[] _Notifications = _Client.CalculateRate(_RateRequest.ClientInfo, ref _RateRequest.Transaction, _RateRequest.OriginAddress, _RateRequest.DestinationAddress, _RateRequest.ShipmentDetails, AramexInfo.Currency, out _HasErrors, out _TotalAmount, out rateDetails);

        //        if (_HasErrors == true)
        //        {
        //            if (_Notifications != null && _Notifications.Length > 0)
        //            {
        //                for (int _Index = 0; _Index <= _Notifications.Length - 1; _Index++)
        //                {
        //                    string str = "Notification Code:" + _Notifications[_Index].Code + ", Notification Message:" + _Notifications[_Index].Message;
        //                    FeedbackResponse feedbackResponse = new FeedbackResponse();
        //                    feedbackResponse.Feedback = str;
        //                    feedbackResponse.LastUpdate = DateTime.Now;
        //                    db.FeedbackResponses.Add(feedbackResponse);
        //                    db.SaveChanges();
        //                }
        //            }
        //        }
        //        else
        //        {
        //            if (_TotalAmount != null)
        //            {
        //                rate = decimal.Parse(_TotalAmount.Value.ToString());                       
        //            }
        //        }   
        //    }
        //    catch (Exception ex)
        //    {
        //        FeedbackResponse feedbackResponse = new FeedbackResponse();
        //        feedbackResponse.Feedback = ex.Message.ToString();
        //        feedbackResponse.LastUpdate = DateTime.Now;
        //        db.FeedbackResponses.Add(feedbackResponse);
        //        db.SaveChanges();
        //    }
        //    finally
        //    {
        //        _Client.Close();
        //        _Client = null;
        //    }


        //    return rate;
        //}


        //public static async Task<string> CreateShipping(long profileId)
        //{
        //    var BillingShipping = db.ctShippingBillings.Where(x => x.ProfileId == profileId).FirstOrDefault();

        //    string shippingReference = "";
        //    List<Shipment> _Shipments = new List<Shipment>();
        //    Shipment _Shipment = new Shipment();
        //    List<Attachment> _Attachments = new List<Attachment>();
        //    List<Shipping.ShipmentItem> _Items = new List<Shipping.ShipmentItem>();

        //    Shipping.ShipmentItem _Item = new Shipping.ShipmentItem();
        //    _Item.PackageType = "Percel";
        //    _Item.Quantity = 1;
        //    _Item.Weight = new Shipping.Weight();
        //    _Item.Weight.Value = Convert.ToDouble(1);
        //    _Item.Weight.Unit = "KG";
        //    _Item.Comments = "";
        //    _Item.Reference = "";

        //    _Items.Add(_Item);



        //    //Shipper
        //    _Shipment.Shipper = new Party();

        //    _Shipment.Shipper.AccountNumber = "60514404";

        //    _Shipment.Shipper.PartyAddress = new Shipping.Address();
        //    _Shipment.Shipper.PartyAddress.Line1 = "Saudi Standards, Metrology and Quality Org";
        //    _Shipment.Shipper.PartyAddress.Line2 = "Building Riyadh, Al Muhammadiyah - in front of";
        //    _Shipment.Shipper.PartyAddress.Line3 = "King Saud University";
        //    _Shipment.Shipper.PartyAddress.City = "Riyadh";
        //    _Shipment.Shipper.PartyAddress.CountryCode = "SA";

        //    _Shipment.Shipper.Contact = new Contact();
        //    _Shipment.Shipper.Contact.PersonName = "IIA KSA";
        //    _Shipment.Shipper.Contact.Title = "IIA KSA";
        //    _Shipment.Shipper.Contact.CompanyName = "IIA KSA";
        //    _Shipment.Shipper.Contact.PhoneNumber1 = "0112076499";
        //    _Shipment.Shipper.Contact.CellPhone = "0112076499";
        //    _Shipment.Shipper.Contact.EmailAddress = "info@iia.org.sa";

        //    //Recipient
        //    _Shipment.Consignee = new Party();

        //    _Shipment.Consignee.PartyAddress = new Shipping.Address();
        //    _Shipment.Consignee.PartyAddress.Line1 = "Address 1";
        //    _Shipment.Consignee.PartyAddress.Line2 = "Address 2";
        //    _Shipment.Consignee.PartyAddress.Line3 = "Address 3";
        //    _Shipment.Consignee.PartyAddress.City = "Riyadh";
        //    _Shipment.Consignee.PartyAddress.CountryCode = "SA";

        //    _Shipment.Consignee.Contact = new Contact();
        //    _Shipment.Consignee.Contact.PersonName = "Mr Johns";
        //    _Shipment.Consignee.Contact.Title = "Mr Johns";
        //    _Shipment.Consignee.Contact.CompanyName = "Test Company";
        //    _Shipment.Consignee.Contact.PhoneNumber1 = "0551111111";
        //    _Shipment.Consignee.Contact.CellPhone = "0551111111";
        //    _Shipment.Consignee.Contact.EmailAddress = "test@abc.com";


        //    //Main
        //    _Shipment.Reference1 = "Books";
        //    _Shipment.TransportType = 0;

        //    _Shipment.ShippingDateTime = DateTime.Now.Date;
        //    _Shipment.DueDate = DateTime.Now.AddDays(2).Date;

        //    //Details
        //    _Shipment.Details = new Shipping.ShipmentDetails();


        //    _Shipment.Details.ActualWeight = new Shipping.Weight();
        //    _Shipment.Details.ActualWeight.Value = Convert.ToDouble(1);
        //    _Shipment.Details.ActualWeight.Unit = "KG";

        //    if (BillingShipping.SCountry != "SA")
        //    {
        //        _Shipment.Details.ProductGroup = "EXP";
        //        _Shipment.Details.ProductType = "PDX";
        //    }
        //    else
        //    {
        //        _Shipment.Details.ProductGroup = "DOM";
        //        _Shipment.Details.ProductType = "OND";
        //    }

        //    _Shipment.Details.PaymentType = "P";

        //    _Shipment.Details.NumberOfPieces = Convert.ToInt32(1);
        //    _Shipment.Details.DescriptionOfGoods = "Books";
        //    _Shipment.Details.GoodsOriginCountry = "SA";


        //    if ((_Items != null))
        //        _Shipment.Details.Items = _Items.ToArray();

        //    _Shipments.Add(_Shipment);


        //    ShipmentCreationRequest _Request = new ShipmentCreationRequest();
        //    _Request.ClientInfo = new Shipping.ClientInfo();            
        //    _Request.ClientInfo.UserName = Username;
        //    _Request.ClientInfo.Password = Password;
        //    _Request.ClientInfo.Version = "v2.0";
        //    _Request.ClientInfo.AccountCountryCode = "SA";
        //    _Request.ClientInfo.AccountEntity = "RUH";
        //    _Request.ClientInfo.AccountNumber = "60514404";
        //    _Request.ClientInfo.AccountPin = "432432";

        //    _Request.Transaction = new Shipping.Transaction();
        //    _Request.Transaction.Reference1 = "Books";                                 


        //    _Request.Shipments = _Shipments.ToArray();

        //    _Request.LabelInfo = new LabelInfo();
        //    _Request.LabelInfo.ReportID = 9201;
        //    _Request.LabelInfo.ReportType = "URL";

        //    Shipping.Service_1_0Client _Client = new Shipping.Service_1_0Client();

        //    try
        //    {
        //        bool _HasErrors = false;
        //        _Client.Open();
        //        var _Response = await _Client.CreateShipmentsAsync(_Request);
        //        _Client.Close();

        //        //if (_Notifications != null && _Notifications.Length > 0)
        //        //{
        //        //    for (int _Index = 0; _Index <= _Notifications.Length - 1; _Index++)
        //        //    {
        //        //        string str = "Notification Code:" + _Notifications[_Index].Code + ", Notification Message:" + _Notifications[_Index].Message;
        //        //        FeedbackResponse feedbackResponse = new FeedbackResponse();
        //        //        feedbackResponse.Feedback = str;
        //        //        feedbackResponse.LastUpdate = DateTime.Now;
        //        //        db.FeedbackResponses.Add(feedbackResponse);
        //        //        db.SaveChanges();
        //        //    }
        //        //}

        //        //if (processedShipments != null && processedShipments.Length > 0)
        //        //{
        //        //    for (int _Index = 0; _Index <= processedShipments.Length - 1; _Index++)
        //        //    {
        //        //        string AWB = processedShipments[_Index].ID;
        //        //        ShipmentLabel labelInfo= processedShipments[_Index].ShipmentLabel;
        //        //        FeedbackResponse feedbackResponse = new FeedbackResponse();
        //        //        feedbackResponse.Feedback = AWB+ labelInfo.LabelURL;
        //        //        feedbackResponse.LastUpdate = DateTime.Now;
        //        //        db.FeedbackResponses.Add(feedbackResponse);
        //        //        db.SaveChanges();
        //        //    }                    
        //        //}

        //        ProcessedShipment[] processedShipments = _Response.Shipments;
        //        Shipping.Notification[] _Notifications = _Response.Notifications;
        //        if (_Notifications != null && _Notifications.Length > 0)
        //        {
        //            for (int _Index = 0; _Index <= _Notifications.Length - 1; _Index++)
        //            {
        //                string str = "Notification Code:" + _Notifications[_Index].Code + ", Notification Message:" + _Notifications[_Index].Message;
        //                FeedbackResponse feedbackResponse = new FeedbackResponse();
        //                feedbackResponse.Feedback = str;
        //                feedbackResponse.LastUpdate = DateTime.Now;
        //                db.FeedbackResponses.Add(feedbackResponse);
        //                db.SaveChanges();
        //            }
        //        }


        //        if (processedShipments != null && processedShipments.Length > 0)
        //        {
        //            for (int _Index = 0; _Index < processedShipments.Length; _Index++)
        //            {
        //                //string AWB = processedShipments[_Index].ForeignHAWB;
        //                //ShipmentLabel labelInfo= processedShipments[_Index].ShipmentLabel;
        //                FeedbackResponse feedbackResponse = new FeedbackResponse();
        //                feedbackResponse.Feedback = processedShipments[_Index].ID;
        //                feedbackResponse.LastUpdate = DateTime.Now;
        //                db.FeedbackResponses.Add(feedbackResponse);
        //                db.SaveChanges();

        //                Shipping.Notification[] _Notifications1 = processedShipments[_Index].Notifications;
        //                if (_Notifications1 != null && _Notifications1.Length > 0)
        //                {
        //                    for (int _Index1 = 0; _Index1 <= _Notifications1.Length - 1; _Index1++)
        //                    {
        //                        string str = "Notification Code:" + _Notifications1[_Index].Code + ", Notification Message:" + _Notifications1[_Index].Message;
        //                        FeedbackResponse feedbackResponse2 = new FeedbackResponse();
        //                        feedbackResponse2.Feedback = str;
        //                        feedbackResponse2.LastUpdate = DateTime.Now;
        //                        db.FeedbackResponses.Add(feedbackResponse2);
        //                        db.SaveChanges();
        //                    }
        //                }
        //            }                    
        //        }


        //        FeedbackResponse feedbackResponse1 = new FeedbackResponse();
        //        feedbackResponse1.Feedback = _Response.HasErrors.ToString();
        //        feedbackResponse1.LastUpdate = DateTime.Now;
        //        db.FeedbackResponses.Add(feedbackResponse1);
        //        db.SaveChanges();

        //    }
        //    catch (Exception ex)
        //    {
        //        FeedbackResponse feedbackResponse = new FeedbackResponse();
        //        feedbackResponse.Feedback = ex.Message.ToString();
        //        feedbackResponse.LastUpdate = DateTime.Now;
        //        db.FeedbackResponses.Add(feedbackResponse);
        //        db.SaveChanges();
        //    }


        //    return shippingReference;
        //}

        public static ShippingModel CreateShipping(long profileId)
        {
            string TitleOfGoods = "Books & Materials";
            var BillingShipping = db.ctShippingBillings.Where(x => x.ProfileId == profileId).FirstOrDefault();
            var Registration = db.ctRegistrations.Where(x => x.ProfileId == profileId).FirstOrDefault();
            var AramexInfo = db.ctAramexInfoes.FirstOrDefault();
            ShippingModel shippingModel = new ShippingModel();

            List<Shipment> _Shipments = new List<Shipment>();
            Shipment _Shipment = new Shipment();


            //Shipper
            _Shipment.Shipper = new Party();

            _Shipment.Shipper.AccountNumber = AramexInfo.AccountNumber;

            _Shipment.Shipper.PartyAddress = new Shipping.Address();
            _Shipment.Shipper.PartyAddress.Line1 = "Saudi Standards, Metrology and Quality Org";
            _Shipment.Shipper.PartyAddress.Line2 = "Building Riyadh, Al Muhammadiyah - in front of";
            _Shipment.Shipper.PartyAddress.Line3 = "King Saud University";
            _Shipment.Shipper.PartyAddress.City = AramexInfo.OriginCity;
            _Shipment.Shipper.PartyAddress.CountryCode = AramexInfo.AccountCountryCode;

            _Shipment.Shipper.Contact = new Contact();
            _Shipment.Shipper.Contact.PersonName = "IIA KSA";
            //_Shipment.Shipper.Contact.Title = "IIA KSA";
            _Shipment.Shipper.Contact.CompanyName = "The Saudi Institute of Internal Auditors";
            _Shipment.Shipper.Contact.PhoneNumber1 = "0112076499";
            _Shipment.Shipper.Contact.CellPhone = "0112076499";
            _Shipment.Shipper.Contact.EmailAddress = "info@iia.org.sa";

            //Recipient
            _Shipment.Consignee = new Party();

            _Shipment.Consignee.PartyAddress = new Shipping.Address();
            if (!string.IsNullOrEmpty(BillingShipping.SAddress1))
            {
                _Shipment.Consignee.PartyAddress.Line1 = BillingShipping.SAddress1;
            }
            if (!string.IsNullOrEmpty(BillingShipping.SAddress2))
            {
                _Shipment.Consignee.PartyAddress.Line2 = BillingShipping.SAddress2;
            }
            if (!string.IsNullOrEmpty(BillingShipping.SAddress3))
            {
                _Shipment.Consignee.PartyAddress.Line3 = BillingShipping.SAddress3;
            }

            _Shipment.Consignee.PartyAddress.City = BillingShipping.SCity;
            _Shipment.Consignee.PartyAddress.CountryCode = BillingShipping.SCountry;
           

            _Shipment.Consignee.Contact = new Contact();
            if (!string.IsNullOrEmpty(BillingShipping.SMiddleName))
            {
                _Shipment.Consignee.Contact.PersonName = BillingShipping.SFirstName + " " + BillingShipping.SMiddleName + " " + BillingShipping.SLastName;
            }
            else
            {
               _Shipment.Consignee.Contact.PersonName = BillingShipping.SFirstName + " " + BillingShipping.SLastName;
            }

            _Shipment.Consignee.Contact.CompanyName = !string.IsNullOrEmpty(Registration.Organization) ? Registration.Organization : _Shipment.Consignee.Contact.PersonName;
            _Shipment.Consignee.Contact.PhoneNumber1 = BillingShipping.SMobileNumber;
            if (!string.IsNullOrEmpty(BillingShipping.SPhoneNumber))
            {
                _Shipment.Consignee.Contact.CellPhone = BillingShipping.SPhoneNumber;
            }
            else
            {
                _Shipment.Consignee.Contact.CellPhone = BillingShipping.SMobileNumber;
            }
            _Shipment.Consignee.Contact.EmailAddress = BillingShipping.SEmailAddress;

            //_Shipment.Consignee.Reference1 = Registration.IIAKSANumber;

            //Main
            _Shipment.Reference1 = TitleOfGoods;
            _Shipment.TransportType = 0;

            _Shipment.ShippingDateTime = DateTime.Now.Date;
            _Shipment.DueDate = DateTime.Now.AddDays(2).Date;

            //Details
            _Shipment.Details = new Shipping.ShipmentDetails();


            _Shipment.Details.ActualWeight = new Shipping.Weight();
            _Shipment.Details.ActualWeight.Value = Convert.ToDouble(1);
            _Shipment.Details.ActualWeight.Unit = "KG";

            if (BillingShipping.SCountry != "SA")
            {
                _Shipment.Details.ProductGroup = "EXP";
                _Shipment.Details.ProductType = "PDX";
            }
            else
            {
                _Shipment.Details.ProductGroup = "DOM";
                _Shipment.Details.ProductType = "OND";
            }

            _Shipment.Details.PaymentType = "P";

            _Shipment.Details.NumberOfPieces = Convert.ToInt32(1);
            _Shipment.Details.DescriptionOfGoods = TitleOfGoods;
            _Shipment.Details.GoodsOriginCountry = AramexInfo.AccountCountryCode;


            _Shipments.Add(_Shipment);


            ShipmentCreationRequest _Request = new ShipmentCreationRequest();
            _Request.ClientInfo = new Shipping.ClientInfo();
            _Request.ClientInfo.AccountCountryCode = AramexInfo.AccountCountryCode;
            _Request.ClientInfo.AccountEntity = AramexInfo.AccountEntity;
            _Request.ClientInfo.AccountNumber = AramexInfo.AccountNumber;
            _Request.ClientInfo.AccountPin = AramexInfo.AccountPin;
            _Request.ClientInfo.UserName = AramexInfo.UserName;
            _Request.ClientInfo.Password = AramexInfo.Password;
            _Request.ClientInfo.Version = "v2.0";

            _Request.Transaction = new Shipping.Transaction();
            _Request.Transaction.Reference1 = TitleOfGoods;


            _Request.Shipments = _Shipments.ToArray();

            _Request.LabelInfo = new LabelInfo();
            _Request.LabelInfo.ReportID = 9201;
            _Request.LabelInfo.ReportType = "URL";

            Shipping.Service_1_0Client _Client = new Shipping.Service_1_0Client();
            Shipping.ProcessedShipment[] processedShipments = null;

            try
            {
                bool _HasErrors = false;
                _Client.Open();
                Shipping.Notification[] _Notification = _Client.CreateShipments(_Request.ClientInfo, ref _Request.Transaction, _Request.Shipments, _Request.LabelInfo, out _HasErrors, out processedShipments);
                _Client.Close();

                if (_Notification != null && _Notification.Length > 0)
                {
                    for (int _Index = 0; _Index <= _Notification.Length - 1; _Index++)
                    {
                        string str = "Notification Code:" + _Notification[_Index].Code + ", Notification Message:" + _Notification[_Index].Message;
                        FeedbackResponse feedbackResponse = new FeedbackResponse();
                        feedbackResponse.Feedback = str;
                        feedbackResponse.LastUpdate = DateTime.Now;
                        db.FeedbackResponses.Add(feedbackResponse);
                        db.SaveChanges();
                    }
                }

                if (processedShipments != null && processedShipments.Length > 0)
                {
                    for (int i = 0; i < processedShipments.Length; i++)
                    {
                        Shipping.ProcessedShipment processedShipment = processedShipments[i];
                        if (processedShipment.HasErrors == true)
                        {
                            Shipping.Notification[] _Notifications1 = processedShipment.Notifications;
                            if (_Notifications1 != null && _Notifications1.Length > 0)
                            {
                                for (int _Index1 = 0; _Index1 <= _Notifications1.Length - 1; _Index1++)
                                {
                                    string str = "Notification Code:" + _Notifications1[_Index1].Code + ", Notification Message:" + _Notifications1[_Index1].Message;
                                    FeedbackResponse feedbackResponse2 = new FeedbackResponse();
                                    feedbackResponse2.Feedback = str;
                                    feedbackResponse2.LastUpdate = DateTime.Now;
                                    db.FeedbackResponses.Add(feedbackResponse2);
                                    db.SaveChanges();
                                }
                            }
                        }
                        else
                        {
                            shippingModel.AWBNo = processedShipment.ID;
                            Shipping.ShipmentLabel shipmentLabel = processedShipment.ShipmentLabel;
                            if (shipmentLabel != null)
                            {
                                shippingModel.AWBPDF = shipmentLabel.LabelURL;
                            }

                            FeedbackResponse feedbackResponse2 = new FeedbackResponse();
                            feedbackResponse2.Feedback = shippingModel.AWBNo + shippingModel.AWBPDF;
                            feedbackResponse2.LastUpdate = DateTime.Now;
                            db.FeedbackResponses.Add(feedbackResponse2);
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                FeedbackResponse feedbackResponse = new FeedbackResponse();
                feedbackResponse.Feedback = ex.Message.ToString();
                feedbackResponse.LastUpdate = DateTime.Now;
                db.FeedbackResponses.Add(feedbackResponse);
                db.SaveChanges();
            }


            return shippingModel;
        }

        public static List<CityModel> GetCity(string CountryCode, string NameStartsWith)
        {
            var AramexInfo = db.ctAramexInfoes.FirstOrDefault();
            List<CityModel> cityModels = new List<CityModel>();

            Location.CitiesFetchingRequest _Request = new Location.CitiesFetchingRequest();
            _Request.ClientInfo = new Location.ClientInfo();
            _Request.ClientInfo.AccountCountryCode = AramexInfo.AccountCountryCode;
            _Request.ClientInfo.AccountEntity = AramexInfo.AccountEntity;
            _Request.ClientInfo.AccountNumber = AramexInfo.AccountNumber;
            _Request.ClientInfo.AccountPin = AramexInfo.AccountPin;
            _Request.ClientInfo.UserName = AramexInfo.UserName;
            _Request.ClientInfo.Password = AramexInfo.Password;

            //_Request.ClientInfo.AccountCountryCode = "JO";
            //_Request.ClientInfo.AccountEntity = "AMM";
            //_Request.ClientInfo.AccountNumber = "20016";
            //_Request.ClientInfo.AccountPin = "331421";
            //_Request.ClientInfo.UserName = "testingapi@aramex.com";
            //_Request.ClientInfo.Password = "R123456789$r";
            _Request.ClientInfo.Version = "v2.0";
            _Request.ClientInfo.Source = 24;

            _Request.Transaction = new Location.Transaction();
            _Request.Transaction.Reference1 = "";

            _Request.CountryCode = CountryCode;

            _Request.NameStartsWith = NameStartsWith;

            try
            {
                bool _HasError = false;
                string[] cities = null;
                Location.Service_1_0Client _Client = new Location.Service_1_0Client();
               Location.Notification[] _Notification = _Client.FetchCities(_Request.ClientInfo, ref _Request.Transaction, _Request.CountryCode, "", _Request.NameStartsWith, out _HasError, out cities);

                if (_Notification != null && _Notification.Length > 0)
                {
                    for (int _Index = 0; _Index <= _Notification.Length - 1; _Index++)
                    {
                        string str = "Notification Code:" + _Notification[_Index].Code + ", Notification Message:" + _Notification[_Index].Message;
                        FeedbackResponse feedbackResponse = new FeedbackResponse();
                        feedbackResponse.Feedback = str;
                        feedbackResponse.LastUpdate = DateTime.Now;
                        db.FeedbackResponses.Add(feedbackResponse);
                        db.SaveChanges();
                    }
                }

                foreach (string _C in cities)
                {
                    CityModel model = new CityModel();
                    model.CityName = _C;
                    cityModels.Add(model);                  
                }                

            }
            catch (Exception ex)
            {
                string str = ex.Message.ToString();
                FeedbackResponse feedbackResponse = new FeedbackResponse();
                feedbackResponse.Feedback = str;
                feedbackResponse.LastUpdate = DateTime.Now;
                db.FeedbackResponses.Add(feedbackResponse);
                db.SaveChanges();
            }

            return cityModels;
        }

    }
}