﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
namespace CMS.Helpers
{
    public class VideoModule
    {
        public string VideoIFrame;
        public string VideoCoreURL;
        public string SmallImage;
        public string BigImage;


        public int ProcessVideoURL(string txtLink, string width, string height)
        {
            string vCode = "";
            int retValue = -1;

            try
            {
                if (txtLink.Contains("www.youtube.com"))
                {
                    txtLink = txtLink.StartsWith("http") ? txtLink : "https://" + txtLink;
                    if (txtLink.Contains("v="))
                    {
                        vCode = GetVcode(txtLink);
                        VideoIFrame = GenerateIframeCode(vCode, width, height);
                        SmallImage = GenerateSmallImageURL(vCode);
                        BigImage = GenetareBigImageURL(vCode);
                        VideoCoreURL = "https://www.youtube.com/embed/" + vCode;
                        retValue = 1;
                    }
                    else
                    {
                        retValue = -2;
                        return retValue;
                    }
                }
                else if (txtLink.Contains("//youtu.be/"))
                {
                    txtLink = txtLink.StartsWith("http") ? txtLink : "https://" + txtLink;
                    var segemnts = txtLink.Split('/');
                    vCode = segemnts[segemnts.Length - 1];
                    if (vCode != "")
                    {
                        VideoIFrame = GenerateIframeCode(vCode, width, height);
                        SmallImage = GenerateSmallImageURL(vCode);
                        BigImage = GenetareBigImageURL(vCode);
                        VideoCoreURL = "https://www.youtube.com/embed/" + vCode;
                        retValue = 1;
                    }
                    else
                    {
                        retValue = -2;
                        return retValue;
                    }
                }
                else if (txtLink.Contains("vimeo.com/"))
                {
                    txtLink = txtLink.StartsWith("http") ? txtLink : "https://" + txtLink;
                    try
                    {
                        var segemnts = txtLink.Split('/');
                        vCode = segemnts[segemnts.Length - 1];
                        VideoCoreURL = "https://player.vimeo.com/video/" + vCode;
                    }
                    catch (Exception ex)
                    {
                        retValue = -3;
                        return retValue;
                    }
                    try
                    {
                        GetVimeoVideoNImage(vCode, width, height);
                        retValue = 1;
                    }
                    catch (Exception ex)
                    {
                        retValue = -4;
                        return retValue;
                    }
                }
                else
                {
                    retValue = -2;
                    return retValue;
                }
            }


            catch
            {
                retValue = -1;
            }

            return retValue;
        }

        /// <summary>
        ///     ''' Youtube
        ///     ''' </summary>
        ///     ''' <param name="url"></param>
        ///     ''' <returns></returns>
        ///     ''' <remarks></remarks>
        private string GetVcode(string url)
        {
            // http://www.youtube.com/watch?v=YVOquldmUXE&feature=player_embedded
            string vCode = "";
            if (url.Contains("v="))
            {
                int vIndex = url.IndexOf("v=");
                int ampIndex = url.IndexOf("&", vIndex);
                if ((ampIndex > 0))
                    vCode = url.Substring(vIndex + 2, ampIndex - (vIndex + 2));
                else
                    vCode = url.Substring(vIndex + 2);
            }

            return vCode;
        }
        /// <summary>
        ///     ''' Youtube Iframe
        ///     ''' </summary>
        ///     ''' <param name="vCode"></param>
        ///     ''' <param name="width"></param>
        ///     ''' <param name="hight"></param>
        ///     ''' <returns></returns>
        ///     ''' <remarks></remarks>
        private string GenerateIframeCode(string vCode, string width, string hight)
        {
            // <iframe title="YouTube video player" src="http://www.youtube.com/embed/-9hsl6iq-v8" allowfullscreen="" width="550" frameborder="0" height="450"></iframe>
            string iFrame = "";
            if (vCode != "")
                // iFrame = "<object width=""" & width & """ height=""" & hight & """><param name=""movie"" value=""http://www.youtube.com/v/" & vCode & """></param><param name=""wmode"" value=""transparent""></param><embed src=""http://www.youtube.com/v/" & vCode & """ type=""application/x-shockwave-flash"" wmode=""transparent"" width=""" & width & """ height=""" & hight & """></embed></object>"
                iFrame = "<iframe title=\"YouTube video player\" src=\"http://www.youtube.com/embed/" + vCode + "?wmode=opaque\" allowfullscreen=\"\" width=\"790\" frameborder=\"0\" height=\"390\"></iframe>";
            return iFrame;
        }

        private string GenerateSmallImageURL(string vCode)
        {
            return "http://img.youtube.com/vi/" + vCode + "/1.jpg";
        }

        private string GenetareBigImageURL(string vCode)
        {
            return "http://img.youtube.com/vi/" + vCode + "/0.jpg";
        }

        /// <summary>
        ///     ''' Vimeo
        ///     ''' </summary>
        ///     ''' <param name="vCode"></param>
        ///     ''' <param name="width"></param>
        ///     ''' <param name="height"></param>
        ///     ''' <remarks></remarks>
        private void GetVimeoVideoNImage(string vCode, string width, string height)
        {
            XmlDocument m_xmld;
            XmlNodeList m_nodelist;
            XmlNode m_node;
            // Create the XML Document

            m_xmld = new XmlDocument();
            // Load the Xml file

            m_xmld.Load("http://vimeo.com/api/oembed.xml?url=http://vimeo.com/" + vCode);
            // Get the list of name nodes 

            m_nodelist = m_xmld.SelectNodes("/oembed");
            // Loop through the nodes

            foreach (XmlNode m_node1 in m_nodelist)
            {
                BigImage = m_node1["thumbnail_url"].InnerText;
                SmallImage = m_node1["thumbnail_url"].InnerText;
                // hdnVideoURL.Value = "<object width=""" & width & """ height=""" & height & """><param name=""allowfullscreen"" value=""true"" /><param name=""allowscriptaccess"" value=""always"" /><param name=""movie"" value=""http://vimeo.com/moogaloop.swf?clip_id=" & vCode & "&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=1&amp;color=00adef&amp;fullscreen=1&amp;autoplay=0&amp;loop=0"" /><embed src=""http://vimeo.com/moogaloop.swf?clip_id=" & vCode & "&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=1&amp;color=00adef&amp;fullscreen=1&amp;autoplay=0&amp;loop=0"" type=""application/x-shockwave-flash"" allowfullscreen=""true"" allowscriptaccess=""always"" width=""" & width & """ height=""" & height & """></embed></object><p><a href=""http://vimeo.com/" & vCode & """>Ten for Grandpa</a> from <a href=""http://vimeo.com/pieface"">Pie Face Pictures</a> on <a href=""http://vimeo.com"">Vimeo</a>.</p>"
                // hdnVideoURL.Value = "<iframe src=""http://player.vimeo.com/video/" & vCode & "?title=0&amp;byline=0&amp;portrait=0"" width=""" & width & """ height=""" & height & """ frameborder=""0""></iframe>"
                // VideoIFrame = "<object width=""" & width & """ height=""" & height & """><param name=""movie"" value=""http://vimeo.com/moogaloop.swf?clip_id=" & vCode & "&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=0&amp;color=00adef&amp;fullscreen=1&amp;autoplay=0&amp;loop=0""></param><param name=""wmode"" value=""transparent""></param><embed src=""http://vimeo.com/moogaloop.swf?clip_id=" & vCode & "&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=0&amp;color=00adef&amp;fullscreen=1&amp;autoplay=0&amp;loop=0"" type=""application/x-shockwave-flash"" wmode=""transparent"" width=""" & width & """ height=""" & height & """></embed></object>"
                // VideoIFrame = "<object width=""" & width & """ height=""" & height & """><param name=""movie"" value=""https://vimeo.com/moogaloop.swf?clip_id=" & vCode & "&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=0&amp;show_portrait=0&amp;color=#000&amp;fullscreen=1&amp;autoplay=0&amp;loop=0""></param><param name=""wmode"" value=""transparent""></param><embed src=""https://vimeo.com/moogaloop.swf?clip_id=" & vCode & "&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=0&amp;color=00adef&amp;fullscreen=1&amp;autoplay=0&amp;loop=0"" type=""application/x-shockwave-flash"" wmode=""transparent"" width=""" & width & """ height=""" & height & """></embed></object>"
                VideoIFrame = "<iframe src='https://player.vimeo.com/video/" + vCode + "' width='" + width + "' height='" + height + "' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";
            }
        }
    }
}