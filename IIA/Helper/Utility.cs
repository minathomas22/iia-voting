﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.IO;
using Umbraco.Core.Models;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Core.Services;
using System.Globalization;
using IIA.DBContext;
using System.Net;
using Newtonsoft.Json;
using System.Data.Entity;
using IIA.Models;
using Umbraco.Web.Mvc;
using IIA.Helpers;
using System.Xml;
using IIA.Helper;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Umbraco.Core.Security;

namespace CMS.Helpers
{
    public static class Utility
    {
        public static IIAContext db = new IIAContext();
        //public static string PaymentIntegrationMode = "Live";
        //public static string PaymentIntegrationUrl = "https://oppwa.com/";        
        public static string PaymentIntegrationMode = WebConfigurationManager.AppSettings["PaymentIntegrationMode"].ToString();
        public static string PaymentIntegrationUrl = WebConfigurationManager.AppSettings["PaymentIntegrationUrl"].ToString();
        public static Regex pattern1 = new Regex(@"^(000\.000\.|000\.100\.1|000\.[36])");
        public static Regex pattern2 = new Regex(@"^(000\.400\.0[^3]|000\.400\.100)");

        public static IHtmlString reCaptcha(this HtmlHelper helper, double size = 1)
        {
            StringBuilder sb = new StringBuilder();
            string publickey = WebConfigurationManager.AppSettings["RecaptchaPublicKey"];
            sb.AppendLine("<script type=\"text/javascript\" src='https://www.google.com/recaptcha/api.js'></script>");
            sb.AppendLine("");
            sb.AppendLine("<div class=\"g-recaptcha\" data-sitekey=\"" + publickey + "\" style=\"transform:scale(" + size + ");-webkit-transform:scale(" + size + ");transform-origin:0 0;-webkit-transform-origin:0 0;\"></div>");
            return MvcHtmlString.Create(sb.ToString());

        }

        public static MvcHtmlString ShareButtons(this HtmlHelper helper, string Url, string ImageFullUrl, string Title, string SmallDescription)
        {
            if (!Url.ToLower().StartsWith("http"))
            {
                Url = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + (Url.StartsWith("/") ? Url : "/" + Url);
            }
            StringBuilder sb = new StringBuilder();
            sb.Append(@"<div class='social-icons'>
                            <a href = 'javasctipt:;' rel='nofollow'  onclick=""MM_openBrWindow('https://www.facebook.com/sharer.php?s=100&p[url]=" + Url + @"&p[images][0]=" + ImageFullUrl + @"&p[title]=" + Title + @"&p[summary]=" + SmallDescription + @"', 'TEXTEDITOR', 'scrollbars=yes,resizable=yes,width=800,height=600')""><i class='fa fa-facebook' aria-hidden='true'></i></a>
                            <a href = 'javasctipt:;'  rel='nofollow'  onclick=""MM_openBrWindow('https://twitter.com/share?url=" + Url + @"&text=" + Title + @"', 'TEXTEDITOR', 'scrollbars=yes,resizable=yes,width=700,height=300')""><i class='fa fa-twitter' aria-hidden='true'></i></a>
                            <a href = 'javasctipt:;'  rel='nofollow'  onclick=""MM_openBrWindow('https://plus.google.com/share?url=" + Url + @"&text=" + Title + @"', 'TEXTEDITOR', 'scrollbars=yes,resizable=yes,width=700,height=300')""><i class='fa fa-google-plus' aria-hidden='true'></i></a>
                            <!--<a href = 'javasctipt:;' ><i class='fa fa-youtube-play' aria-hidden='true'></i></a>-->
                            <a href = 'javasctipt:;' rel='nofollow'  onclick=""MM_openBrWindow('http://www.linkedin.com/shareArticle?mini=true&url=" + Url + @"&title=" + Title + @"&summary=" + SmallDescription + @"', 'TEXTEDITOR', 'scrollbars=yes,resizable=yes,width=500,height=400')""><i class='fa fa-linkedin' aria-hidden='true'></i></a>
                        </div>");
            return MvcHtmlString.Create(sb.ToString());

        }

        public static MvcHtmlString ShareButtonsOnDetailsPage(this HtmlHelper helper, string Url, string ImageFullUrl, string Title, string SmallDescription)
        {
            if (!Url.ToLower().StartsWith("http"))
            {
                Url = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + (Url.StartsWith("/") ? Url : "/" + Url);
            }
            StringBuilder sb = new StringBuilder();

            sb.Append(@"<ul class='unstyled'>
                        <!--<li>
                            <a href='javascript:;' ><img src='/ui/media/elements/icon-like.jpg'></a>
                        </li>-->
                        <li>
                            <a href='javascript:;' rel='nofollow'  onclick=""MM_openBrWindow('https://www.facebook.com/sharer.php?s=100&p[url]=" + Url + @"&p[images][0]=" + ImageFullUrl + @"&p[title]=" + Title + @"&p[summary]=" + SmallDescription + @"', 'TEXTEDITOR', 'scrollbars=yes,resizable=yes,width=800,height=600')""><img src='/ui/media/elements/icon-share.jpg'></a>
                        </li>
                        <li>
                            <a href='javascript:;' rel='nofollow'  onclick=""MM_openBrWindow('https://twitter.com/share?url=" + Url + @"&text=" + Title + @"', 'TEXTEDITOR', 'scrollbars=yes,resizable=yes,width=700,height=300')""><img src='/ui/media/elements/icon-tweet.jpg'></a>
                        </li>
                        <li>
                            <a href='javascript:;' rel='nofollow'  onclick=""MM_openBrWindow('https://plus.google.com/share?url=" + Url + @"&text=" + Title + @"', 'TEXTEDITOR', 'scrollbars=yes,resizable=yes,width=700,height=300')""><img src='/ui/media/elements/icon-g-share.jpg'></a>
                        </li>
                    </ul>");

            return MvcHtmlString.Create(sb.ToString());

        }

        //public static void SendMail(string FromName, string FromEmail, string ReceiverEmail, string CC, string BCC, string subj, string Mssg)
        //{
        //    // Assemble parameters
        //    var IP = "";
        //    string ipList = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        //    if (!string.IsNullOrEmpty(ipList))
        //        IP = ipList.Split(',')[0];
        //    else
        //        IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];


        //    Dictionary<string, string> data = new Dictionary<string, string>();
        //    data.Add("FromName", FromName);
        //    data.Add("FromEmail", FromEmail);
        //    data.Add("ReceiverEmail", ReceiverEmail);
        //    data.Add("CC", CC);
        //    data.Add("BCC", BCC);
        //    data.Add("ReplyTo", "");
        //    data.Add("subj", subj);
        //    data.Add("Mssg", Mssg);
        //    data.Add("attahments", "");
        //    data.Add("IP", IP);

        //    var URL = "http://webapi.nexacmstest.com/api/sendmail";

        //    var jsonData = (new System.Web.Script.Serialization.JavaScriptSerializer()).Serialize(data);
        //    System.Net.WebRequest myReq = System.Net.WebRequest.Create(URL);
        //    myReq.Method = "POST";
        //    myReq.ContentLength = jsonData.Length;
        //    myReq.ContentType = "application/json; charset=UTF-8";
        //    myReq.Headers.Add("Authorization", AuthorizationInBase64());
        //    UTF8Encoding enc = new UTF8Encoding();
        //    using (Stream ds = myReq.GetRequestStream())
        //    {
        //        ds.Write(enc.GetBytes(jsonData), 0, jsonData.Length);
        //    }

        //    System.Net.WebResponse wr = myReq.GetResponse();
        //    System.IO.Stream receiveStream = wr.GetResponseStream();
        //    System.IO.StreamReader reader = new System.IO.StreamReader(receiveStream, Encoding.UTF8);
        //    string content = reader.ReadToEnd();

        //    //MailMessage objmsg = new MailMessage();
        //    //objmsg.From = new MailAddress(FromEmail, FromName);
        //    //objmsg.To.Add(ReceiverEmail);
        //    //objmsg.CC.Add(CC);
        //    //objmsg.Bcc.Add(BCC);
        //    //objmsg.Subject = subj;
        //    //objmsg.IsBodyHtml = true;
        //    //objmsg.SubjectEncoding = Encoding.UTF8;
        //    //objmsg.BodyEncoding = Encoding.UTF8;
        //    //objmsg.Body = Mssg;

        //    //SmtpClient smtp = new SmtpClient();
        //    //smtp.Host = "smtp.sendgrid.net";
        //    //smtp.EnableSsl = true;
        //    //NetworkCredential NetworkCred = new NetworkCredential("apikey", "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");
        //    //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
        //    //smtp.Credentials = NetworkCred;
        //    //smtp.Port = 587;
        //    //smtp.Send(objmsg);

        //}

        public static void SendMail(string FromName, string FromEmail, string ReceiverEmail, string CC, string BCC, string subj, string Mssg)
        {

            try
            {

                MailMessage objmsg = new MailMessage();
                objmsg.From = new MailAddress(FromEmail, FromName);

                if (!string.IsNullOrEmpty(ReceiverEmail))
                {
                    var splitReceiverEmail = ReceiverEmail.Split(new char[] { ';' });
                    if (splitReceiverEmail.Length > 0)
                    {
                        foreach (string _email in splitReceiverEmail)
                        {
                            if (!string.IsNullOrEmpty(_email))
                            {
                                objmsg.To.Add(_email.Trim());
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(CC))
                {
                    var splitCC = CC.Split(new char[] { ';' });

                    if (splitCC.Length > 0)
                    {
                        foreach (string _email in splitCC)
                        {
                            if (!string.IsNullOrEmpty(_email))
                            {
                                objmsg.CC.Add(_email.Trim());
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(BCC))
                {
                    var splitBCC = BCC.Split(new char[] { ';' });
                    if (splitBCC.Length > 0)
                    {
                        foreach (string _email in splitBCC)
                        {
                            if (!string.IsNullOrEmpty(_email))
                            {
                                objmsg.Bcc.Add(_email.Trim());
                            }
                        }
                    }
                }

                objmsg.Subject = subj;
                objmsg.IsBodyHtml = true;
                objmsg.SubjectEncoding = Encoding.UTF8;
                objmsg.BodyEncoding = Encoding.UTF8;
                objmsg.Body = Mssg;

                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.sendgrid.net";
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential("apikey", "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");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(objmsg);
            }
            catch(Exception ex)
            {
                FeedbackResponse feedbackResponse = new FeedbackResponse();
                feedbackResponse.Feedback = "SendGrid:" + ex.Message.ToString()+ex.InnerException.Message;
                feedbackResponse.LastUpdate = DateTime.Now;
                db.FeedbackResponses.Add(feedbackResponse);
                db.SaveChanges();
            }

        }
        
        private static string AuthorizationInBase64()
        {
            var usernamePassword = "SMTPUser:!5MT4Us9r123*#";
            return "Basic " + Convert.ToBase64String((new UTF8Encoding()).GetBytes(usernamePassword));
        }

        public static string GetLanguage(int pageId)
        {
            try
            {
                var service = ApplicationContext.Current.Services;
                var languageCode = System.Threading.Thread.CurrentThread.CurrentCulture.DisplayName;
                ILocalizationService ls = service.LocalizationService;
                ILanguage language2 = ls.GetLanguageByCultureCode(languageCode);
                //// set default
                //var languageCode = "en-US";

                //// try getting domain mappings for current page
                //IEnumerable<IDomain> currentPageDomains = ApplicationContext.Current.Services.DomainService.GetAssignedDomains(pageId, false);

                //if (currentPageDomains.Count() > 0)
                //{
                //    // get code from first entry as multiple entries will always have the same language code
                //    languageCode = currentPageDomains.FirstOrDefault().LanguageIsoCode;
                //}
                //else
                //{
                //    // if no domain mapping for currentPage, navigate to root node and get language code
                //    var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
                //    var currentPage = umbracoHelper.TypedContent(pageId);
                //    var rootPageId = currentPage.AncestorOrSelf(1).Id;

                //    IEnumerable<IDomain> rootPageDomains = ApplicationContext.Current.Services.DomainService.GetAssignedDomains(rootPageId, false);

                //    languageCode = rootPageDomains.FirstOrDefault().LanguageIsoCode;
                //}


                if (languageCode == "English (United States)")
                {
                    return "en";
                }
                else
                {
                    return "ar";
                }

            }

            catch (Exception)
            {
                return "en";
            }
        }

        public static EmailSettingModel GetEmailSetting(string ContactType, string PageId, string lang)
        {
            EmailSettingModel model = new EmailSettingModel();
            model.FromName = "IIA KSA";
            model.FromEmail = "info@iia.org.sa";
            //string ToAdmin = "info@iia.org.sa";
            //string BCCAdmin = "info@iia.org.sa";            
                                  

            if (ContactType == "ContactAdmin")
            {
                model.EmailTo = "info@iia.org.sa";
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang=="en"? "Contact the IIA KSA" : "التواصل مع الجمعية السعودية للمراجعين الداخليين";
            }
            else if (ContactType == "ContactMember")
            {
                long ProfileId = long.Parse(PageId);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                model.EmailTo = "members@iia.org.sa";
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Member Contact with the IIA KSA" : "التواصل كعضو مع الجمعية السعودية للمراجعين الداخليين";
            }
            else if (ContactType == "MemberEnrolled")
            {
                long ProfileId = long.Parse(PageId);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "members@iia.org.sa";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Member Enrolled with the IIA KSA" : "الالتحاق كعضو في الجمعية السعودية للمراجعين الداخليين";
            }
            else if (ContactType == "MemberRegistration")
            {
                long ProfileId = long.Parse(PageId);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "members@iia.org.sa";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Member Registration with the IIA KSA" : "تسجيل العضوية في الجمعية السعودية للمراجعين الداخليين";
            }
            else if (ContactType == "EnrollMemberRegistration")
            {
                long ProfileId = long.Parse(PageId);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "members@iia.org.sa";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Corporate Enrolment Member Registration with IIA KSA" : "تسجيل عضوية شركة في الجمعية السعودية للمراجعين الداخليين";
            }
            else if (ContactType == "NonMemberRegistration")
            {
                long ProfileId = long.Parse(PageId);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "members@iia.org.sa";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Non-Member Registration with IIA KSA" : "تسجيل دون عضوية في الجمعية السعودية للمراجعين الداخليين";
            }
            else if (ContactType == "CartPayment")
            {
                long ProfileId = long.Parse(PageId);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "finance@iia.org.sa;training@iia.org.sa;o.alsanad@iia.org.sa";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Thank you for your purchase from the IIA KSA" : "نشكرك على اهتمامك بالشراء من الجمعية السعودية للمراجعين الداخليين";
            }
            else if (ContactType == "ApplyMentoring")
            {
                model.EmailTo = "members@iia.org.sa";
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Apply for Mentoring with IIA KSA" : "التسجيل للتوجيه مع الجمعية السعودية للمراجعين الداخليين";
            }
            else if (ContactType == "MemberActivation")
            {
                long ProfileId = long.Parse(PageId);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Member Activation for the IIA KSA" : "تنشيط حساب الأعضاء في الجمعية السعودية للمراجعين الداخليين";
            }
            else if (ContactType == "NonMemberActivation")
            {
                long ProfileId = long.Parse(PageId);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Non-Member Activation for IIA KSA" : "تنشيط حساب غير الأعضاء في الجمعية السعودية للمراجعين الداخليين";
            }

            else if (ContactType == "ApproveMentoring")
            {
                long ProfileId = long.Parse(PageId);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Mentoring request approval from the IIA KSA" : "موافقة الجمعية السعودية للمراجعين الداخليين على طلب التوجيه";
            }
            else if (ContactType == "IIAPost")
            {               

                model.EmailTo = "requirement@iia.org.sa";
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Career Application with the IIA KSA" : "التقديم لوظيفة لدى الجمعية السعودية للمراجعين الداخليين";
            }
            else if (ContactType == "CorporatePost")
            {
                var PostID = long.Parse(PageId);
                var ProfileId = db.ctJobInternships.Where(x => x.PostId == PostID).FirstOrDefault().ProfileId;
                var ctRegistration = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                model.EmailTo = ctRegistration.EmailAddress;
                model.EmailCC = "requirement@iia.org.sa";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Career Application with the IIA KSA" : "التقديم لوظيفة لدى الجمعية السعودية للمراجعين الداخليين";
            }
            else if (ContactType == "ContactMentor")
            {
                long ProfileId = long.Parse(PageId);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                model.EmailTo = "members@iia.org.sa";
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Mentor Contact with the IIA KSA" : "التواصل كعضو مع الجمعية السعودية للمراجعين الداخليين";
            }
            else if (ContactType == "EventRegister")
            {

                model.EmailTo = "info@iia.org.sa";
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Event Registration with the IIA KSA" : "التسجيل لحدث مع الجمعية السعودية للمراجعين الداخليين";
            }
            else if (ContactType == "EventRegisterUser")
            {                
                model.EmailTo = PageId;
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "The 8th Internal Audit Conference Registration" : "اكتمال التسجيل في مؤتمر مستقبل المراجعة الداخلية";
            }
            else if (ContactType == "ContactUs")
            {
                model.EmailTo = "info@iia.org.sa";
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Contact the IIA KSA" : "التواصل مع الجمعية السعودية للمراجعين الداخليين";
            }
            else if (ContactType == "ForgotPassword")
            {
                long ProfileId = long.Parse(PageId);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Reset your password for the IIA KSA" : "تغيير كلمة المرور على موقع الجمعية السعودية للمراجعين الداخليين";
            }
            else if (ContactType == "RequestCallBack")
            {

                model.EmailTo = "info@iia.org.sa";
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Request a Call Back" : "أطلب معاودة الاتصال بك";
            }
            else if (ContactType == "DownloadBrochure")
            {
                model.EmailTo = PageId;
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Download Brochure" : "حمل الكتيب";
            }
            else if (ContactType == "DownloadExamContent")
            {
                model.EmailTo = PageId;
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Download Brochure" : "حمل الكتيب";
            }
            else if (ContactType == "AboutInternalAudition")
            {
                model.EmailTo = "info@iia.org.sa";
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Contact the IIA KSA" : "التواصل مع الجمعية السعودية للمراجعين الداخليين";               
            }
            else if (ContactType == "StockNotification")
            {
                model.EmailTo = "finance@iia.org.sa";
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Product Stock Notification for the IIA KSA" : "Product Stock Notification for the IIA KSA";
            }
            else if (ContactType == "CorporatePostSubmission")
            {
                model.EmailTo = "members@iia.org.sa";
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Corporate career post submission for the IIA KSA" : "Corporate career post submission for the IIA KSA";
            }
            else if (ContactType == "MembershipRenewal")
            {
                long ProfileId = long.Parse(PageId);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "members@iia.org.sa";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Membership Renewal with the IIA KSA" : "تجديد عضويتك مع الجمعية السعودية للمراجعين الداخليين";
            }
            else if (ContactType == "RenewalReminder")
            {
                long ProfileId = long.Parse(PageId);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Membership Renewal reminder with the IIA KSA" : "تذكير بتجديد العضوية مع الجمعية السعودية للمراجعين الداخليين";
            }
            else if (ContactType == "MembershipUpgrade")
            {
                long ProfileId = long.Parse(PageId);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "members@iia.org.sa";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Membership Upgrade with the IIA KSA" : "ترقية للعضوية مع الجمعية السعودية للمراجعين الداخليين في المملكة العربية السعودية";
            }

            // Nomination Applications Mail
            // Mail Number 1
            else if (ContactType == "NominationApplications")
            {
                long ProfileId = long.Parse(PageId);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "members@iia.org.sa";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "IIA KSA Nomination Applications" : "الجمعية السعودية للمراجعين الداخليين: استقبال طلبات الترشح لمجلس الجمعية.";
            }

            // Nomination Application Submission Mail
            // Mail Number 2
            else if (ContactType == "ApplyCandidate")
            {
                long ProfileId = long.Parse(PageId);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "members@iia.org.sa";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "IIA Saudi Arabia: Nomination Application Submission" : "الجمعية السعودية للمراجعين الداخليين: طلب الترشح لانتخابات المجلس";
            }

            // Objection Request Submission Mail
            // Mail Number 3
            else if (ContactType == "ObjectionForCandidate")
            {
                long ProfileId = long.Parse(PageId);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "members@iia.org.sa";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "IIA-Saudi Arabia: Objection Request Submission" : "الجمعية السعودية للمراجعين الداخليين: طلب اعتراض";
            }
            else if (ContactType == "ObjectionForCandidateToCommittee")
            {
                long ProfileId = long.Parse(PageId);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                //model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "members@iia.org.sa";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "IIA-Saudi Arabia: Objection Request Submission" : "الجمعية السعودية للمراجعين الداخليين: طلب اعتراض";
            }            
            // Election Voting Starts Mail
            // Mail Number 4
            else if (ContactType == "ElectionVotingStarts")
            {
                long ProfileId = long.Parse(PageId);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "members@iia.org.sa";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "IIA Saudi Arabia: Board Election Voting Starts" : "الجمعية السعودية للمراجعين الداخليين: بدء التصويت للمجلس";
            }
            else if (ContactType == "ApplyCandidateStatusChange")
            {
                long ProfileId = long.Parse(PageId);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "members@iia.org.sa";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Application Status" : "حالة طلب الترشح";
            }

            return model;
        }

        public static EmailSettingModel GetEmailSettingEvent(string ContactType, string Email, string Subject, string lang)
        {
            EmailSettingModel model = new EmailSettingModel();
            model.FromName = "IIA KSA";
            model.FromEmail = "info@iia.org.sa";  
            if (ContactType == "EventRegister")
            {

                model.EmailTo = "info@iia.org.sa";
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Event Registration with the IIA KSA" : "التسجيل لحدث مع الجمعية السعودية للمراجعين الداخليين";
            }
            else if (ContactType == "EventRegisterUser")
            {
                model.EmailTo = Email;
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = Subject;
            }            
            return model;

        }

        public static EmailSettingModel GetExternalCourseEmailSetting(string ContactType, long ProfileId, string lang)
        {
            EmailSettingModel model = new EmailSettingModel();
            model.FromName = "IIA KSA";
            model.FromEmail = "info@iia.org.sa"; 
            if (ContactType == "UploadReceipt")
            {
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "leadership@iia.org.sa";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "IIA Leadership Program: English Test" : "برنامج قيادة المراجعة الداخلية : اختبار كفاءة اللغة الإنجليزية";
            }
            else if (ContactType == "InterviewDate")
            {
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "leadership@iia.org.sa";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "IIA Leadership Program: Interview" : "برنامج قيادة المراجعة الداخلية : المقابلة الشخصية";
            }
            else if (ContactType == "CoursePayment")
            {
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "leadership@iia.org.sa";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "IIA Leadership Program: Course Registration Successful" : "برنامج قيادة المراجعة الداخلية : تأكيد حجز مقعدك بنجاح";
            }
            else if (ContactType == "CourseApplicationFeePayment")
            {
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "Course application fee confirmation: IIA KSA" : "Course application fee confirmation: IIA KSA";
            }
            else if (ContactType == "SpokenResultStatusUpdate")
            {
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "IIA Leadership Program: English Test" : "برنامج قيادة المراجعة الداخلية : اختبار كفاءة اللغة الإنجليزية";
            }
            else if (ContactType == "InterviewStatusUpdate")
            {
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                model.EmailTo = objDbModel.EmailAddress;
                model.EmailCC = "";
                model.EmailBCC = "";
                model.Subject = lang == "en" ? "IIA Leadership Program: Interview" : "برنامج قيادة المراجعة الداخلية : المقابلة الشخصية";
            }
            return model;
        }

        public static void SetApplicationCookies(string key, string value)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[key];
            if (cookie == null)
            {
                // no cookie found, create it
                cookie = new HttpCookie(key);
                cookie.Value = value;
            }
            else
            {
                // update the cookie values                
                cookie.Value = value;
            }

            // overwrite the cookie
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static void SetCartCookies(string key, string value)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[key];
            if (cookie == null)
            {
                // no cookie found, create it
                cookie = new HttpCookie(key);
                cookie.Value = value;
                cookie.Expires = DateTime.Now.AddDays(7);
            }
            else
            {
                // update the cookie values                
                cookie.Value = value;
                cookie.Expires = DateTime.Now.AddDays(7);
            }

            // overwrite the cookie
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static HttpCookie GetApplicationCookies(string key)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[key];
            return cookie;
        }

        public static void ClearRegistrationCookies()
        {
            if (HttpContext.Current.Request.Cookies["proId"] != null)
            {
                HttpCookie proId = HttpContext.Current.Request.Cookies["proId"];
                HttpContext.Current.Response.Cookies.Remove("proId");
                proId.Expires = DateTime.Now.AddDays(-10);
                proId.Value = null;
                HttpContext.Current.Response.SetCookie(proId);
            }
            if (HttpContext.Current.Request.Cookies["payId"] != null)
            {
                HttpCookie payId = HttpContext.Current.Request.Cookies["payId"];
                HttpContext.Current.Response.Cookies.Remove("payId");
                payId.Expires = DateTime.Now.AddDays(-10);
                payId.Value = null;
                HttpContext.Current.Response.SetCookie(payId);
            }
            if (HttpContext.Current.Request.Cookies["IsMR"] != null)
            {
                HttpCookie IsMR = HttpContext.Current.Request.Cookies["IsMR"];
                HttpContext.Current.Response.Cookies.Remove("IsMR");
                IsMR.Expires = DateTime.Now.AddDays(-10);
                IsMR.Value = null;
                HttpContext.Current.Response.SetCookie(IsMR);
            }

            if (HttpContext.Current.Request.Cookies["IsNMTM"] != null)
            {
                HttpCookie IsNMTM = HttpContext.Current.Request.Cookies["IsNMTM"];
                HttpContext.Current.Response.Cookies.Remove("IsNMTM");
                IsNMTM.Expires = DateTime.Now.AddDays(-10);
                IsNMTM.Value = null;
                HttpContext.Current.Response.SetCookie(IsNMTM);
            }
        }

        public static void ClearRenewalMembershipCookies()
        {
            
            if (HttpContext.Current.Request.Cookies["payId"] != null)
            {
                HttpCookie payId = HttpContext.Current.Request.Cookies["payId"];
                HttpContext.Current.Response.Cookies.Remove("payId");
                payId.Expires = DateTime.Now.AddDays(-10);
                payId.Value = null;
                HttpContext.Current.Response.SetCookie(payId);
            }           

        }
        public static void ClearPurchaseCookies()
        {            
            if (HttpContext.Current.Request.Cookies["invId"] != null)
            {
                HttpCookie invId = HttpContext.Current.Request.Cookies["invId"];
                HttpContext.Current.Response.Cookies.Remove("invId");
                invId.Expires = DateTime.Now.AddDays(-10);
                invId.Value = null;
                HttpContext.Current.Response.SetCookie(invId);
            }           
        }
        public static void ClearCartCookies()
        {
            if (HttpContext.Current.Request.Cookies["CartKey"] != null)
            {
                HttpCookie CartKey = HttpContext.Current.Request.Cookies["CartKey"];
                HttpContext.Current.Response.Cookies.Remove("CartKey");
                CartKey.Expires = DateTime.Now.AddDays(-10);
                CartKey.Value = null;
                HttpContext.Current.Response.SetCookie(CartKey);
            }
        }

        public static void ClearLoginCookies()
        {
            if (HttpContext.Current.Request.Cookies["IsLogin"] != null)
            {
                HttpCookie IsLogin = HttpContext.Current.Request.Cookies["IsLogin"];
                HttpContext.Current.Response.Cookies.Remove("IsLogin");
                IsLogin.Expires = DateTime.Now.AddDays(-10);
                IsLogin.Value = null;
                HttpContext.Current.Response.SetCookie(IsLogin);
            }

            if (HttpContext.Current.Request.Cookies["proId"] != null)
            {
                HttpCookie proId = HttpContext.Current.Request.Cookies["proId"];
                HttpContext.Current.Response.Cookies.Remove("proId");
                proId.Expires = DateTime.Now.AddDays(-10);
                proId.Value = null;
                HttpContext.Current.Response.SetCookie(proId);
            }
        }

        public static void ClearExternalCourseCookies()
        {

            if (HttpContext.Current.Request.Cookies["ecpayId"] != null)
            {
                HttpCookie ecpayId = HttpContext.Current.Request.Cookies["ecpayId"];
                HttpContext.Current.Response.Cookies.Remove("ecpayId");
                ecpayId.Expires = DateTime.Now.AddDays(-10);
                ecpayId.Value = null;
                HttpContext.Current.Response.SetCookie(ecpayId);
            }

        }


        public static string UploadFileInFolder(HttpPostedFileBase file)
        {
            string prefix = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + "_";
            string ImageFile = System.IO.Path.GetFileName(file.FileName).Replace(" ", "-").Replace("&", "");
            string ImagePath = "/Media/ApplicantFile/" + prefix + ImageFile;
            string ImageFilePath = HttpContext.Current.Server.MapPath(ImagePath);
            file.SaveAs(ImageFilePath);

            return prefix + ImageFile;
        }

        public static string UploadExcelFileInFolder(HttpPostedFileBase file)
        {
            string prefix = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + "_";
            string ImageFile = System.IO.Path.GetFileName(file.FileName).Replace(" ", "-").Replace("&", "");
            string ImagePath = "/Media/ExcelFile/" + prefix + ImageFile;
            string ImageFilePath = HttpContext.Current.Server.MapPath(ImagePath);
            file.SaveAs(ImageFilePath);

            return prefix + ImageFile;
        }

        public static string GenerateIIAKSANumber(string profileType)
        {
            string Number = "";            
            var max = db.ctRegistrations.Where(x => x.ProfileType == profileType).OrderByDescending(p => p.ProfileId).FirstOrDefault().IIAKSANumber;

            Number = (int.Parse(max) + 1).ToString();
            return Number;
        }

        public static string GenerateMemberInvoiceNumber()
        {
            string Number = "";
            string prefix = "IIAM";

            Number = prefix + ExpandString((db.ctRegistrationPayments.Count()) + 1);
            return Number;
        }

        public static string GenerateShopInvoiceNumber()
        {
            string Number = "";
            string prefix = "IIAS";

            Number = prefix + ExpandString((db.ctInvoices.Count()) + 1);
            return Number;
        }

        public static string GenerateExternalCourseInvoiceNumber()
        {
            string Number = "";
            string prefix = "IIAEC";

            Number = prefix + ExpandString((db.ctExternalCoursePayments.Count()) + 1);
            return Number;
        }

        public  static string ExpandString(int count)
        {
            string text = count.ToString("000000");
            return text;
        }


        public static void CheckRegistrationPaymentStatus(string id, string resourcePath, string lang)
        {
            try
            {
                long PaymentId = long.Parse(Utility.GetApplicationCookies("payId").Value);
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                ctRegistrationPayment objDbModel = db.ctRegistrationPayments.Where(x => x.PaymentId == PaymentId).FirstOrDefault();

                if (id != "" && resourcePath != "")
                {
                    string PaymentTransactionId = "";                    

                    var PaymentInfo = db.ctRegistrationPayments.Where(x => x.PaymentId == PaymentId).FirstOrDefault();
                    var PaymentGatewayInfo = db.ctPaymentGatewayInfoes.Where(x => x.IntegrationMode == PaymentIntegrationMode).FirstOrDefault();
                    var entityId = "";
                    if (PaymentInfo.PaymentMethod == "VISA")
                    {
                        entityId = PaymentGatewayInfo.VIsaEntityId;
                    }
                    else if (PaymentInfo.PaymentMethod == "MASTER")
                    {
                        entityId = PaymentGatewayInfo.MasterEntityId;
                    }
                    else if (PaymentInfo.PaymentMethod == "MADA")
                    {
                        entityId = PaymentGatewayInfo.MadaEntityId;
                    }

                    var TransactionResult = PaymentStatusCheck(entityId, PaymentGatewayInfo.BearerToken, id, PaymentInfo.InvoiceNumber, PaymentInfo.TransactionAmount.Value);
                    if (!string.IsNullOrEmpty(TransactionResult.TransactionReference))
                    {
                        if (pattern1.IsMatch(TransactionResult.TransactioCode) || pattern2.IsMatch(TransactionResult.TransactioCode))
                        {
                            objDbModel.TransactionReference = TransactionResult.TransactionReference;
                            objDbModel.TransactionStatus = "Success";
                            db.Entry(objDbModel).State = EntityState.Modified;
                            db.SaveChanges();

                            QuickBook.QuickbookCreateForRegistration(long.Parse(Utility.GetApplicationCookies("payId").Value));

                            if (HttpContext.Current.Request.Cookies["IsLogin"] != null)
                            {
                                HttpCookie IsLogin = HttpContext.Current.Request.Cookies["IsLogin"];
                                HttpContext.Current.Response.Cookies.Remove("IsLogin");
                                IsLogin.Expires = DateTime.Now.AddDays(-10);
                                IsLogin.Value = null;
                                HttpContext.Current.Response.SetCookie(IsLogin);
                            }


                            HttpContext.Current.Response.Redirect("/" + lang + "/registration/registration-payment-success/");
                        }
                        else
                        {
                            HttpContext.Current.Response.Redirect("/" + lang + "/registration/registration-payment-failed/");
                        }
                    }
                    else
                    {                       

                        HttpContext.Current.Response.Redirect("/" + lang + "/registration/registration-payment-failed/");
                    }
                }
                else
                {                   
                    objDbModel.TransactionReference = "";
                    objDbModel.TransactionStatus = "Failed";
                    db.Entry(objDbModel).State = EntityState.Modified;
                    db.SaveChanges();

                    HttpContext.Current.Response.Redirect("/" + lang + "/registration/registration-payment-failed/");
                }

            }
            catch
            {
                
            }
            
        }

        public static void CheckCartPaymentStatus(string id, string resourcePath, string lang)
        {
            try
            {
                long InvoiceId = long.Parse(Utility.GetApplicationCookies("invId").Value);
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);

                ctInvoice objDbModel = db.ctInvoices.Where(x => x.InvoiceId == InvoiceId).FirstOrDefault();

                if (id != "" && resourcePath != "")
                {
                    string PaymentTransactionId = "";

                    var PaymentInfo = db.ctInvoices.Where(x => x.InvoiceId == InvoiceId).FirstOrDefault();
                    var PaymentGatewayInfo = db.ctPaymentGatewayInfoes.Where(x => x.IntegrationMode == PaymentIntegrationMode).FirstOrDefault();
                    var entityId = "";
                    if (PaymentInfo.PaymentMethod == "VISA")
                    {
                        entityId = PaymentGatewayInfo.VIsaEntityId;
                    }
                    else if (PaymentInfo.PaymentMethod == "MASTER")
                    {
                        entityId = PaymentGatewayInfo.MasterEntityId;
                    }
                    else if (PaymentInfo.PaymentMethod == "MADA")
                    {
                        entityId = PaymentGatewayInfo.MadaEntityId;
                    }

                    var TransactionResult = PaymentStatusCheck(entityId, PaymentGatewayInfo.BearerToken, id, PaymentInfo.InvoiceNumber, PaymentInfo.TransactionAmount.Value);
                    if (!string.IsNullOrEmpty(TransactionResult.TransactionReference))
                    {
                        if (pattern1.IsMatch(TransactionResult.TransactioCode) || pattern2.IsMatch(TransactionResult.TransactioCode))
                        {
                            if (objDbModel.Shipping > 0)
                            {
                                ShippingModel shippingModel = Aramex.CreateShipping(ProfileId);
                                if (shippingModel != null)
                                {
                                    objDbModel.ShippingReference = shippingModel.AWBNo;
                                    objDbModel.ShippingReferencePDF = shippingModel.AWBPDF;
                                }
                            }
                            objDbModel.TransactionReference = TransactionResult.TransactionReference;
                            objDbModel.TransactionStatus = "Success";
                            db.Entry(objDbModel).State = EntityState.Modified;
                            db.SaveChanges();

                            QuickBook.QuickbookCreateForCart(long.Parse(Utility.GetApplicationCookies("invId").Value));

                            HttpContext.Current.Response.Redirect("/" + lang + "/shop/cart-payment-success/");
                        }
                        else
                        {                            

                            HttpContext.Current.Response.Redirect("/" + lang + "/shop/cart-payment-failed/");
                        }
                    }
                    else
                    {                      

                        HttpContext.Current.Response.Redirect("/" + lang + "/shop/cart-payment-failed/");
                    }
                }
                else
                {
                    objDbModel.TransactionReference = "";
                    objDbModel.TransactionStatus = "Failed";
                    db.Entry(objDbModel).State = EntityState.Modified;
                    db.SaveChanges();

                    HttpContext.Current.Response.Redirect("/" + lang + "/shop/cart-payment-failed/");
                }

            }
            catch(Exception ex)
            {
                SaveResponse(ex.Message.ToString(), "CartPaymentError");
            }

        }

        public static void CheckMembershipRenewalPaymentStatus(string id, string resourcePath, string lang)
        {
            try
            {
                long PaymentId = long.Parse(Utility.GetApplicationCookies("payId").Value);
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                ctRegistrationPayment objDbModel = db.ctRegistrationPayments.Where(x => x.PaymentId == PaymentId).FirstOrDefault();

                if (id != "" && resourcePath != "")
                {
                    string PaymentTransactionId = "";

                    var PaymentInfo = db.ctRegistrationPayments.Where(x => x.PaymentId == PaymentId).FirstOrDefault();
                    var PaymentGatewayInfo = db.ctPaymentGatewayInfoes.Where(x => x.IntegrationMode == PaymentIntegrationMode).FirstOrDefault();
                    var entityId = "";
                    if (PaymentInfo.PaymentMethod == "VISA")
                    {
                        entityId = PaymentGatewayInfo.VIsaEntityId;
                    }
                    else if (PaymentInfo.PaymentMethod == "MASTER")
                    {
                        entityId = PaymentGatewayInfo.MasterEntityId;
                    }
                    else if (PaymentInfo.PaymentMethod == "MADA")
                    {
                        entityId = PaymentGatewayInfo.MadaEntityId;
                    }

                    var TransactionResult = PaymentStatusCheck(entityId, PaymentGatewayInfo.BearerToken, id, PaymentInfo.InvoiceNumber, PaymentInfo.TransactionAmount.Value);
                    if (!string.IsNullOrEmpty(TransactionResult.TransactionReference))
                    {
                        if (pattern1.IsMatch(TransactionResult.TransactioCode) || pattern2.IsMatch(TransactionResult.TransactioCode))
                        {
                            objDbModel.TransactionReference = TransactionResult.TransactionReference;
                            objDbModel.TransactionStatus = "Success";
                            db.Entry(objDbModel).State = EntityState.Modified;
                            db.SaveChanges();

                            QuickBook.QuickbookCreateForRegistration(long.Parse(Utility.GetApplicationCookies("payId").Value));

                            HttpContext.Current.Response.Redirect("/" + lang + "/registration/membership-renewal-payment-success/");
                        }
                        else
                        {
                            HttpContext.Current.Response.Redirect("/" + lang + "/registration/membership-renewal-payment-failed/");
                        }
                    }
                    else
                    {
                        HttpContext.Current.Response.Redirect("/" + lang + "/registration/membership-renewal-payment-failed/");
                    }
                }
                else
                {
                    objDbModel.TransactionReference = "";
                    objDbModel.TransactionStatus = "Failed";
                    db.Entry(objDbModel).State = EntityState.Modified;
                    db.SaveChanges();

                    HttpContext.Current.Response.Redirect("/" + lang + "/registration/membership-renewal-payment-failed/");
                }

            }
            catch
            {

            }

        }

        public static void CheckMembershipUpgradePaymentStatus(string id, string resourcePath, string lang)
        {
            try
            {
                long PaymentId = long.Parse(Utility.GetApplicationCookies("payId").Value);
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                ctRegistrationPayment objDbModel = db.ctRegistrationPayments.Where(x => x.PaymentId == PaymentId).FirstOrDefault();

                if (id != "" && resourcePath != "")
                {
                    string PaymentTransactionId = "";

                    var PaymentInfo = db.ctRegistrationPayments.Where(x => x.PaymentId == PaymentId).FirstOrDefault();
                    var PaymentGatewayInfo = db.ctPaymentGatewayInfoes.Where(x => x.IntegrationMode == PaymentIntegrationMode).FirstOrDefault();
                    var entityId = "";
                    if (PaymentInfo.PaymentMethod == "VISA")
                    {
                        entityId = PaymentGatewayInfo.VIsaEntityId;
                    }
                    else if (PaymentInfo.PaymentMethod == "MASTER")
                    {
                        entityId = PaymentGatewayInfo.MasterEntityId;
                    }
                    else if (PaymentInfo.PaymentMethod == "MADA")
                    {
                        entityId = PaymentGatewayInfo.MadaEntityId;
                    }

                    var TransactionResult = PaymentStatusCheck(entityId, PaymentGatewayInfo.BearerToken, id, PaymentInfo.InvoiceNumber, PaymentInfo.TransactionAmount.Value);
                    if (!string.IsNullOrEmpty(TransactionResult.TransactionReference))
                    {
                        if (pattern1.IsMatch(TransactionResult.TransactioCode) || pattern2.IsMatch(TransactionResult.TransactioCode))
                        {
                            objDbModel.TransactionReference = TransactionResult.TransactionReference;
                            objDbModel.TransactionStatus = "Success";
                            db.Entry(objDbModel).State = EntityState.Modified;
                            db.SaveChanges();

                            QuickBook.QuickbookCreateForRegistration(long.Parse(Utility.GetApplicationCookies("payId").Value));

                            HttpContext.Current.Response.Redirect("/" + lang + "/registration/membership-upgrade-payment-success/");
                        }
                        else
                        {
                            HttpContext.Current.Response.Redirect("/" + lang + "/registration/membership-upgrade-payment-failed/");
                        }
                    }
                    else
                    {
                        HttpContext.Current.Response.Redirect("/" + lang + "/registration/membership-upgrade-payment-failed/");
                    }
                }
                else
                {
                    objDbModel.TransactionReference = "";
                    objDbModel.TransactionStatus = "Failed";
                    db.Entry(objDbModel).State = EntityState.Modified;
                    db.SaveChanges();

                    HttpContext.Current.Response.Redirect("/" + lang + "/registration/membership-upgrade-payment-failed/");
                }

            }
            catch
            {

            }

        }

        public static void CheckCoursentStatus(string id, string resourcePath, string lang)
        {
            try
            {
                long PaymentId = long.Parse(Utility.GetApplicationCookies("ecpayId").Value);
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                ctExternalCoursePayment objDbModel = db.ctExternalCoursePayments.Where(x => x.ExternalPaymentId == PaymentId).FirstOrDefault();

                if (id != "" && resourcePath != "")
                {                  
                    var PaymentInfo = db.ctExternalCoursePayments.Where(x => x.ExternalPaymentId == PaymentId).FirstOrDefault();
                    var PaymentGatewayInfo = db.ctPaymentGatewayInfoes.Where(x => x.IntegrationMode == PaymentIntegrationMode).FirstOrDefault();
                    var entityId = "";
                    if (PaymentInfo.PaymentMethod == "VISA")
                    {
                        entityId = PaymentGatewayInfo.VIsaEntityId;
                    }
                    else if (PaymentInfo.PaymentMethod == "MASTER")
                    {
                        entityId = PaymentGatewayInfo.MasterEntityId;
                    }
                    else if (PaymentInfo.PaymentMethod == "MADA")
                    {
                        entityId = PaymentGatewayInfo.MadaEntityId;
                    }

                    var TransactionResult = PaymentStatusCheck(entityId, PaymentGatewayInfo.BearerToken, id, PaymentInfo.InvoiceNumber, PaymentInfo.TransactionAmount.Value);
                    if (!string.IsNullOrEmpty(TransactionResult.TransactionReference))
                    {
                        if (pattern1.IsMatch(TransactionResult.TransactioCode) || pattern2.IsMatch(TransactionResult.TransactioCode))
                        {
                            objDbModel.TransactionReference = TransactionResult.TransactionReference;
                            objDbModel.TransactionStatus = "Success";
                            db.Entry(objDbModel).State = EntityState.Modified;
                            db.SaveChanges();

                            QuickBook.QuickbookCreateForExternalCourse(long.Parse(Utility.GetApplicationCookies("ecpayId").Value));

                            HttpContext.Current.Response.Redirect("/" + lang + "/my-account/course-payment-success/");
                        }
                        else
                        {
                            HttpContext.Current.Response.Redirect("/" + lang + "/my-account/course-payment-failed/");
                        }
                    }
                    else
                    {
                        HttpContext.Current.Response.Redirect("/" + lang + "/my-account/course-payment-failed/");
                    }
                }
                else
                {
                    objDbModel.TransactionReference = "";
                    objDbModel.TransactionStatus = "Failed";
                    db.Entry(objDbModel).State = EntityState.Modified;
                    db.SaveChanges();

                    HttpContext.Current.Response.Redirect("/" + lang + "/my-account/course-payment-failed/");
                }

            }
            catch
            {

            }

        }

        public static CartPaymentResponseModel PaymentStatusCheck(string entityId, string BearerToken, string id, string merchantTransactionId, decimal amount)
        {
            CartPaymentResponseModel model = new CartPaymentResponseModel();
            string PaymentTransactionReference = "";
            string data = "";
            string retMerchantTransactionId = "";
            string retAmount = "";
            string orgAmount = string.Format("{0:0.00}", amount);
            try
            {
                data += "entityId=" + entityId;
                string url = PaymentIntegrationUrl + "v1/checkouts/" + id + "/payment?" + data;
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
                request.Method = "GET";
                request.Headers["Authorization"] = "Bearer " + BearerToken;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);
                    var result = reader.ReadToEnd();
                    object responseData = JsonConvert.DeserializeObject(result);
                    if (responseData != null)
                    {
                        retMerchantTransactionId = ((dynamic)responseData).merchantTransactionId;
                        retAmount = ((dynamic)responseData).amount;
                        if (retMerchantTransactionId == merchantTransactionId)
                        {
                            model.TransactionReference= ((dynamic)responseData).id;
                            model.TransactioCode= ((dynamic)responseData).result.code;

                            SaveResponse(model.TransactionReference + "_" + model.TransactioCode, "CartPaymentCode");
                            //PaymentTransactionReference = ((dynamic)responseData).id;
                        }
                    }
                    reader.Close();
                    dataStream.Close();

                    SaveResponse(result.ToString(), "CartPaymentResponse");
                }

            }
            catch (Exception ex)
            {
                SaveResponse(ex.Message.ToString(), "CartPaymentFailed");
            }
            return model;
        }

        public static string PaymentGatewayGetCheckoutId(string entityId, decimal amount, string currency, string paymentType, string testMode, string merchantTransactionId, string customerEmail, string customerAddress, string customerCity, string customerState, string customerCountry, string customerPostCode, string customerFirstName, string customerLastName, string BearerToken, string paymentMethod)
        {
            string PaymentGatewayGetCheckoutId = "";
            string data = "";
            try
            {
                data += "entityId=" + entityId;
                data += "&amount=" + string.Format("{0:0.00}", amount);
                data += "&currency=" + currency;
                data += "&paymentType=" + paymentType;
                if (PaymentIntegrationMode == "Test")
                {
                    if (paymentMethod != "MADA")
                    {
                        data += "&testMode=" + testMode;
                    }
                }
                data += "&merchantTransactionId=" + merchantTransactionId;
                data += "&customer.email=" + customerEmail;
                data += "&customer.givenName=" + customerFirstName;
                data += "&customer.surname=" + customerLastName;
                data += "&billing.street1=" + (customerAddress.Length>50?customerAddress.Substring(0,50):customerAddress);
                data += "&billing.city=" + customerCity;
                data += "&billing.country=" + customerCountry;
                if (customerState != "")
                {
                    data += "&billing.state=" + (customerState.Length > 50 ? customerState.Substring(0, 50) : customerState);
                }
                //if (customerPostCode != "")
                //{
                //    data += "&billing.postcode=" + customerPostCode;
                //}


                string url = PaymentIntegrationUrl + "v1/checkouts";
                byte[] buffer = new UTF8Encoding().GetBytes(data);
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
                request.Method = "POST";
                request.Headers["Authorization"] = "Bearer " + BearerToken;
                //request.Headers.Add("Content-Type", "text/html; charset=UTF-8");
                request.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";  
                Stream PostData = request.GetRequestStream();
                PostData.Write(buffer, 0, buffer.Length);
                PostData.Close();
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);
                    var result = reader.ReadToEnd();
                    object responseData = JsonConvert.DeserializeObject(result);
                    if (responseData != null)
                    {
                        PaymentGatewayGetCheckoutId = ((dynamic)responseData).id;
                    }

                    reader.Close();
                    dataStream.Close();
                }

            }
            catch (Exception ex)
            {
                FeedbackResponse feedbackResponse = new FeedbackResponse();
                feedbackResponse.Feedback = ex.Message.ToString();
                feedbackResponse.FeedbackType = "PaymentCheckoutIdError";
                feedbackResponse.LastUpdate = DateTime.Now;
                db.FeedbackResponses.Add(feedbackResponse);
                db.SaveChanges();
            }
            return PaymentGatewayGetCheckoutId;
        }

        public static decimal GetMemberDiscount(decimal amount, int quantity)
        {
            decimal discount = 0;
            if (GetApplicationCookies("proId") != null)
            {
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                var MemberInfo = db.ctRegistrations.Where(x => x.ProfileType == "Member" && x.ProfileId == ProfileId && x.MembershipEndDate>=DateTime.Now && x.IsActive==true).FirstOrDefault();
                if (MemberInfo != null)
                {
                    int discountPercentage = MemberInfo.MembershipDiscount != null ? MemberInfo.MembershipDiscount.Value : 0;
                    if (discountPercentage > 0)
                    {
                        discount = quantity * ((amount * discountPercentage) / 100);
                    }
                }
            }
            return discount;
        }

        public static void SaveResponse(string respone, string type)
        {
            FeedbackResponse feedbackResponse = new FeedbackResponse();
            feedbackResponse.Feedback = respone;
            feedbackResponse.FeedbackType = type;
            feedbackResponse.LastUpdate = DateTime.Now;
            db.FeedbackResponses.Add(feedbackResponse);
            db.SaveChanges();
        }

        public static string GetLanguageFromUrl(string url)
        {
            string lang = "";
            try
            {
                if (url.Contains("/en/"))
                {
                    lang = "en";
                }
                else
                {
                    lang = "ar";
                }
            }
            catch
            {
                lang = "ar";
            }
            
            return lang;
        }


        public static RegistrationViewModel UserProfileById(string proId, string lang)
        {
            RegistrationViewModel model = new RegistrationViewModel();
            try
            {
                long ProfileId = long.Parse(proId);              
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                string middleName = lang == "en" ? objDbModel.MiddleName : objDbModel.MiddleName_ar;
                model.FirstName = lang == "en" ? objDbModel.FirstName : objDbModel.FirstName_ar;
                model.MiddleName = !string.IsNullOrEmpty(middleName) ? middleName : "";
                model.LastName = lang == "en" ? objDbModel.LastName : objDbModel.LastName_ar;
                if (!string.IsNullOrEmpty(objDbModel.ProfilePhoto))
                {
                    model.ProfilePhoto = "/Media/ApplicantFile/" + objDbModel.ProfilePhoto;
                }
                else
                {
                    model.ProfilePhoto = "/ui/media/images/avater.jpg";
                }
               
                model.JobTitle = lang == "en" ? objDbModel.JobTitle : objDbModel.JobTitle_ar;
                model.Organization = lang == "en" ? objDbModel.Organization : objDbModel.Organization_ar;
                model.EmailAddress = objDbModel.EmailAddress;
                model.Qualification = lang == "en" ? objDbModel.Qualification : objDbModel.Qualification_ar;
                model.Bio = lang == "en" ? objDbModel.Bio : objDbModel.Bio_ar;
                model.Experience = lang == "en" ? objDbModel.Experience : objDbModel.Experience_ar;
                model.MobileNumber = objDbModel.MobileNumber;
                model.PhoneNumber = objDbModel.PhoneNumber;
                model.ProfileType = objDbModel.ProfileType;                
                model.Country = db.CountryLists.Where(x => x.CountryCode == objDbModel.Country).FirstOrDefault().CountryName;
                model.City = lang == "en" ? objDbModel.City : objDbModel.City_ar;
                model.ProfileId = objDbModel.ProfileId;
                model.ParentProfileId = objDbModel.ParentProfileId;

                return model;
            }
            catch (Exception ex)
            {
                return model;

            }
        }

        public static List<JobInternshipViewModel> GetCorportePost(string postType)
        {
            List<JobInternshipViewModel> model = new List<JobInternshipViewModel>();
            try
            {
                var jobInternship = db.ctJobInternships.Where(x => x.PostType == postType && x.IsApprove == true);
                foreach(var item in jobInternship)
                {
                    JobInternshipViewModel objItem = new JobInternshipViewModel();
                    objItem.JobTitle = item.JobTitle;
                    objItem.Location = item.Location;
                    objItem.Company = item.Company;
                    objItem.JobDescription = item.JobDescription;
                    objItem.JobResponsibilities = item.JobResponsibilities;
                    objItem.Tenure = item.Tenure;
                    objItem.BannerImage = item.BannerImage;
                    objItem.BackgroundImage = item.BackgroundImage;
                    objItem.DatePosted = item.DatePosted.Value;
                    objItem.PostId = item.PostId;
                    model.Add(objItem);
                }

                return model;
            }
            catch (Exception ex)
            {
                return model;

            }
        }

        public static JobInternshipViewModel GetCorportePostById(long postId)
        {
            JobInternshipViewModel model = new JobInternshipViewModel();
            try
            {
                var jobInternship = db.ctJobInternships.Where(x => x.PostId == postId).FirstOrDefault();
                model.JobTitle = jobInternship.JobTitle;
                model.Location = jobInternship.Location;
                model.Company = jobInternship.Company;
                model.JobDescription = jobInternship.JobDescription;
                model.JobResponsibilities = jobInternship.JobResponsibilities;
                model.Tenure = jobInternship.Tenure;
                model.BannerImage = jobInternship.BannerImage;
                model.BackgroundImage = jobInternship.BackgroundImage;
                model.DatePosted = jobInternship.DatePosted.Value;
                model.PostId = jobInternship.PostId;
                model.PostType = jobInternship.PostType;
                return model;
            }
            catch (Exception ex)
            {
                return model;

            }
        }
    }
}