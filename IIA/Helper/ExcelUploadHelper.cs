﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;

namespace IIA.Helper
{
    public class ExcelUploadHelper
    {
        /// <summary>
        /// Provides method to read excel file.
        /// </summary>
        /// <param name="filePath">The file path to be read.</param>
        /// <param name="sheetName">Excel file sheet name for gets data.</param>
        /// <param name="selectFields">Select fields like * or fields with comma separated.</param>
        /// <param name="tableName">The name of return DataTable.</param>
        /// <param name="fileIncludesHeaders">Indicates whether file includes headers.</param>
        /// <returns>DataTable</returns>
        public static DataTable ReadExcelFile(String filePath,
                                          String sheetName,
                                          String selectFields,
                                          String tableName,
                                          Boolean fileIncludesHeaders)
        {
            DataSet dataSet = null;
            DataTable dtReturn = null;
            string connectionString = string.Empty;
            string commandText = string.Empty;

            // Indicates the Excel file with header or not.
            string headerYesNo = string.Empty;
            string fileExtension = string.Empty;
            try
            {
                if (fileIncludesHeaders == true)
                {
                    // Set YES if excel WithHeader is TRUE.
                    headerYesNo = "YES";
                }
                else
                {
                    // Set NO if excel WithHeader is FALSE.
                    headerYesNo = "NO";
                }

                // Gets file extension for checking.
                fileExtension = Path.GetExtension(filePath);

                switch (fileExtension.ToUpper())
                {
                    case ".XLS":

                        //Take Connection For Microsoft Excel File.
                        connectionString =
                          string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;IMEX=2.0;HDR={1}""",
                                        filePath,
                                        headerYesNo);

                        break;

                    case ".XLSX":

                        //Take Connection For Microsoft Excel File.
                        connectionString =
                          string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=""Excel 12.0;IMEX=2.0;HDR={1}""",
                                        filePath,
                                        headerYesNo);

                        break;

                    default:

                        throw new Exception("File is invalid.");
                }

                commandText = string.Format("SELECT {0} FROM [{1}$]",
                                            selectFields,
                                            sheetName);

                dataSet = new DataSet();
                using (OleDbConnection dbConnection = new OleDbConnection(connectionString))
                {
                    OleDbCommand dbCommand = new OleDbCommand(commandText, dbConnection);
                    dbCommand.CommandType = CommandType.Text;
                    OleDbDataAdapter dbDataAdapter = new OleDbDataAdapter(dbCommand);
                    dbDataAdapter.Fill(dataSet);
                }

                if (dataSet != null &&
                    dataSet.Tables.Count > 0)
                {
                    dataSet.Tables[0].TableName = tableName;

                    // Sets reference of data table.
                    dtReturn = dataSet.Tables[tableName];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dtReturn;
        }

    }
}