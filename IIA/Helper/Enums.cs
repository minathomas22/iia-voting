﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Helper
{
    public enum ElectionStatus
    {
        Working = 1 ,
        Paused = 2,
        Completed = 3
    }

    public enum CandidateRequestStatus
    {

        UnderRequest = 1 ,
        PendingRequest = 2,
        Pending = 3 ,
        Approve = 4 ,
        Reject = 5
    }


    public enum MemberCanVoteToElectionStatus
    {
        Opened = 1 , 
        Confirmed = 2 ,
        VottingNotStarted = 3,
        Closed = 4
    }

    public enum CandidateObjectionStatus
    {
        Pending = 1,
        Approved = 2,
        Rejected = 3
    }

    public enum CandidateCanObjectStatus
    {
        CanObject = 1 ,
        AlreadyObjected = 2 ,
        SameLoggedInUser = 3 ,
        CloseObject = 4 , 

    }

    public enum MemberElectionMailType
    {
        ApplyCandidate = 1 ,
        ObjectionForCandidate = 2
    }

    public enum VotingMailType
    {
        NominationApplications = 1 ,
        ElectionVotingStarts = 2
    }


}