﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.IO;
using Umbraco.Core.Models;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Core.Services;
using System.Globalization;
using IIA.DBContext;
using System.Net;
using Newtonsoft.Json;
using System.Data.Entity;
using IIA.Models;
using Umbraco.Web.Mvc;
using IIA.Helpers;
using System.Xml;
using IIA.Helper;
using System.Net.Mail;
using System.Text.RegularExpressions;
using SelectPdf;

namespace IIA.Helper
{
    public static class PDFGenerate
    {
        public static IIAContext db = new IIAContext();

        public static string GetPdfFromUrl(string html)
        {

            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            // create a new pdf document converting an url
            PdfDocument doc = converter.ConvertUrl("http://selectpdf.com");

            // save pdf document
            doc.Save(HttpContext.Current.Response, false, "Sample.pdf");

            // close pdf document
            doc.Close();

            return "sfdf";
        }

        public static byte[] GetPDFFromHtmlToByteArray(string html)
        {
            string htmlString = html;

            string pdf_page_size = "A4";
            PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
                pdf_page_size, true);

            string pdf_orientation = "Portrait";
            PdfPageOrientation pdfOrientation =
                (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                pdf_orientation, true);

            int webPageWidth = 1024;
            try
            {
                webPageWidth = Convert.ToInt32(1024);
            }
            catch { }

            int webPageHeight = 0;
            try
            {
                webPageHeight = Convert.ToInt32(0);
            }
            catch { }

            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            // set converter options
            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            converter.Options.WebPageHeight = webPageHeight;

            // create a new pdf document converting an url
            PdfDocument doc = converter.ConvertHtmlString(htmlString);
            byte[] pdf = doc.Save();
            doc.Close();
            return pdf;
        }

        public static void GetPDFFromHtmlToFile(string html)
        {
            string htmlString = html;

            string pdf_page_size = "A4";
            PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
                pdf_page_size, true);

            string pdf_orientation = "Portrait";
            PdfPageOrientation pdfOrientation =
                (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                pdf_orientation, true);

            int webPageWidth = 1024;
            try
            {
                webPageWidth = Convert.ToInt32(1024);
            }
            catch { }

            int webPageHeight = 0;
            try
            {
                webPageHeight = Convert.ToInt32(0);
            }
            catch { }

            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            // set converter options
            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            converter.Options.WebPageHeight = webPageHeight;

            // create a new pdf document converting an url
            // save pdf document
            PdfDocument doc = converter.ConvertHtmlString(htmlString);
            doc.Save(HttpContext.Current.Response, false, "Sample.pdf");
            // close pdf document
            doc.Close();
        }

    }
}