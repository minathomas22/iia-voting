﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace CMS.Helpers
{
    public static class Pagination
    {
        public static IHtmlString Pager(int currentPage, int totalRows, int pageSize, int totalPageNumToShow, string urlToNavigateWithQStr, string anchorWord = "", string previousText = "Previous", string nextText = "Next", string ulClass = "pagination")
        {
            var totalNumOfPage = (int)Math.Ceiling((decimal)totalRows / pageSize);
            return PaginationMain(currentPage, totalNumOfPage, totalPageNumToShow, urlToNavigateWithQStr, anchorWord, previousText, nextText, ulClass);
        }
        private static MvcHtmlString PaginationMain(int currentPage, int totalNumOfPage, int totalPageNumToShow, string urlToNavigateWithQStr, string anchorWord = "", string previousText = "Previous", string nextText = "Next", string ulClass = "pagination")
        {
            var previousLinkTemplate = "<li class='page-item'><a class='page-link' href='{0}'>" + previousText + "</a></li>";
            var nextLinkTemplate = "<li class='page-item'><a class='page-link' href='{0}'>" + nextText + "</a></li>";
            var linkTemplate = "<li class='page-item'><a class='page-link' href='{0}'>{1}</a></li>";
            var emptyLinkTemplate = "<li class='page-item'><a class='page-link' href='{0}'>{1}</a></li>"; // for the ... 
            var activeLinkTemplate = "<li class='page-item active'><a class='page-link' href='javascript:;'>{0}</a></li>";


            int i;
            int loopStartNum, loopEndNum, presentNum, maxShownNum;
            StringBuilder pagerString = new StringBuilder();
            presentNum = currentPage;
            maxShownNum = totalPageNumToShow;
            int middleFactor = maxShownNum / 2;
            pagerString.AppendFormat("<ul class=\"{0}\">", ulClass);
            if (totalNumOfPage <= totalPageNumToShow)
            {
                loopStartNum = 1;
                loopEndNum = totalNumOfPage;
                var link = urlToNavigateWithQStr + (presentNum <= 1 ? totalNumOfPage : (presentNum - 1)) + (anchorWord != "" ? "#" + anchorWord : "");
                if (currentPage != 1)
                {
                    pagerString.AppendFormat(previousLinkTemplate, link);
                }

                for (i = loopStartNum; i <= loopEndNum; i++)
                    if ((i == presentNum))
                        pagerString.AppendFormat(activeLinkTemplate, i);
                    else
                    {
                        link = urlToNavigateWithQStr + i + (anchorWord != "" ? "#" + anchorWord : "");
                        pagerString.AppendFormat(linkTemplate, link, i);
                    }

                link = urlToNavigateWithQStr + (presentNum == totalNumOfPage ? 1 : (presentNum + 1)) + (anchorWord != "" ? "#" + anchorWord : "");
                if (currentPage != totalNumOfPage)
                {
                    pagerString.AppendFormat(nextLinkTemplate, link);
                }
            }
            else
            {
                loopStartNum = presentNum <= (middleFactor + 1) ? 1 : presentNum + middleFactor >= totalNumOfPage ? totalNumOfPage - (maxShownNum - 1) : presentNum - middleFactor;
                loopEndNum = presentNum <= (middleFactor + 1) ? maxShownNum : presentNum + middleFactor >= totalNumOfPage ? totalNumOfPage : presentNum + middleFactor;
                loopEndNum = loopEndNum > totalNumOfPage ? totalNumOfPage : loopEndNum;
                var link = urlToNavigateWithQStr + (presentNum == 1 ? totalNumOfPage : (presentNum - 1)) + (anchorWord != "" ? "#" + anchorWord : "");
                if (currentPage != 1)
                {
                    pagerString.AppendFormat(previousLinkTemplate, link);
                }
                if (loopStartNum > 1)
                {
                    link = urlToNavigateWithQStr + (loopStartNum - 1) + (anchorWord != "" ? "#" + anchorWord : "");
                   // pagerString.AppendFormat(emptyLinkTemplate, link, "...");
                }

                for (i = loopStartNum; i <= loopEndNum; i++)
                    if ((i == presentNum))
                        pagerString.AppendFormat(activeLinkTemplate, i);
                    else
                    {
                        link = urlToNavigateWithQStr + i + (anchorWord != "" ? "#" + anchorWord : "");
                        pagerString.AppendFormat(linkTemplate, link, i);
                    }

                if (totalNumOfPage > loopEndNum)
                {
                    link = urlToNavigateWithQStr + i + (anchorWord != "" ? "#" + anchorWord : "");
                   // pagerString.AppendFormat(emptyLinkTemplate, link, "...");
                }
                link = urlToNavigateWithQStr + (presentNum == totalNumOfPage ? 1 : (presentNum + 1)) + (anchorWord != "" ? "#" + anchorWord : "");
                if (currentPage != totalNumOfPage)
                {
                    pagerString.AppendFormat(nextLinkTemplate, link);
                }
            }

            pagerString.Append("</ul>");

            return new MvcHtmlString(pagerString.ToString());
        }
    }
}