﻿using IIA.DBContext;
using IIA.Models;
using Lucene.Net.Search;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using umbraco.editorControls.SettingControls.Pickers;
using umbraco.editorControls.tinyMCE3.webcontrol;
using Umbraco.Web;

namespace IIA.Helper
{
    public class QuickBook
    {
        public static IIAContext db = new IIAContext();
        public static string IntegrationMode = WebConfigurationManager.AppSettings["QuickbookIntegrationMode"].ToString();
        public static string getAccessToken()
        {
            var QuickbookInfo = db.ctQuickBookIntegrationKeys.Where(x => x.Mode == IntegrationMode).FirstOrDefault();

            string existingRefreshToken = QuickbookInfo.RefreshToken;
            string ExistingAccessToken = QuickbookInfo.AccessToken;
            DateTime ExistingTokenTime = QuickbookInfo.TokenGenerateTime.Value;
            TimeSpan result = DateTime.Now - ExistingTokenTime;

            double minute = DateTime.Now.Subtract(ExistingTokenTime).TotalMinutes;

            if (minute >= 50)
            {
                ExistingAccessToken = GenerateRefreshToken();
            }

            return ExistingAccessToken;
        }

        private static string GenerateRefreshToken()
        {
            ctQuickBookIntegrationKey QuickbookInfo = db.ctQuickBookIntegrationKeys.Where(x => x.Mode == IntegrationMode).FirstOrDefault();

            string strResponse = "";
            string AccessToken = "";
            string RefreshToken = "";

            try
            {


                //string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(QuickbookInfo.ClientId + ":" + QuickbookInfo.ClientSecret));

                var params1 = new Dictionary<string, string>();
                var url = QuickbookInfo.RefreshTokenUrl;
                params1.Add("grant_type", "refresh_token");
                params1.Add("refresh_token", QuickbookInfo.RefreshToken);


                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/x-www-form-urlencoded");

                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(QuickbookInfo.ClientId + ":" + QuickbookInfo.ClientSecret)));

                    //client.DefaultRequestHeaders.Add("Authorization", "Basic " + svcCredentials);

                    HttpResponseMessage response1 = client.PostAsync(url, new FormUrlEncodedContent(params1)).Result;
                    strResponse = response1.Content.ReadAsStringAsync().Result;

                    FeedbackResponse feedbackResponse = new FeedbackResponse();
                    feedbackResponse.Feedback = strResponse;
                    feedbackResponse.FeedbackType = "Quickbook Response";
                    db.FeedbackResponses.Add(feedbackResponse);
                    db.SaveChanges();

                }

                if (!string.IsNullOrEmpty(strResponse))
                {
                    object responseData = JsonConvert.DeserializeObject(strResponse);

                    if (responseData != null)
                    {
                        AccessToken = ((dynamic)responseData).access_token;
                        RefreshToken = ((dynamic)responseData).refresh_token;

                        QuickbookInfo.AccessToken = AccessToken;
                        QuickbookInfo.RefreshToken = RefreshToken;
                        QuickbookInfo.TokenGenerateTime = DateTime.Now;
                        db.Entry(QuickbookInfo).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                return AccessToken;
            }
            catch (Exception ex)
            {
                FeedbackResponse feedbackResponse = new FeedbackResponse();
                feedbackResponse.Feedback = ex.Message.ToString();
                feedbackResponse.FeedbackType = "Quickbook Error";
                db.FeedbackResponses.Add(feedbackResponse);
                db.SaveChanges();
                return AccessToken;
            }
        }

        public static string QuickbookCreateForCart(long InvoiceId)
        {
            string retVal = "";
            ctQuickBookIntegrationKey QuickbookInfo = db.ctQuickBookIntegrationKeys.Where(x => x.Mode == IntegrationMode).FirstOrDefault();
            string token = getAccessToken();
            int Invoice = CreateCartInvoice(QuickbookInfo, token, InvoiceId);
            return retVal;
        }

        public static string QuickbookCreateForRegistration(long PaymentId)
        {
            string retVal = "";
            ctQuickBookIntegrationKey QuickbookInfo = db.ctQuickBookIntegrationKeys.Where(x => x.Mode == IntegrationMode).FirstOrDefault();
            string token = getAccessToken();
            int Invoice = CreateRegistrationInvoice(QuickbookInfo, token, PaymentId);
            return retVal;
        }

        public static string QuickbookCreateForExternalCourse(long PaymentId)
        {
            string retVal = "";
            ctQuickBookIntegrationKey QuickbookInfo = db.ctQuickBookIntegrationKeys.Where(x => x.Mode == IntegrationMode).FirstOrDefault();
            string token = getAccessToken();
            int Invoice = CreateExternalCourseInvoice(QuickbookInfo, token, PaymentId);
            return retVal;
        }

        public static int CreateCustomer(ctQuickBookIntegrationKey QuickbookInfo, string email, string phone, string firstName, string middleName, string lastName, string companyName, string token, long profileId)
        {
            int customerid=0;
            string json = "";
            try
            {
                var shippingBilling = db.ctShippingBillings.Where(x => x.ProfileId == profileId).FirstOrDefault();
                if (shippingBilling != null)
                {
                    firstName = !string.IsNullOrEmpty(firstName) ? firstName : shippingBilling.BFirstName;
                    lastName = !string.IsNullOrEmpty(lastName) ? lastName : shippingBilling.BLastName;
                    phone = !string.IsNullOrEmpty(phone) ? phone : shippingBilling.BMobileNumber;
                }
                QuickBookCustomer model = new QuickBookCustomer();

                PrimaryEmailAddr primaryEmailAddr = new PrimaryEmailAddr();
                primaryEmailAddr.Address = email;
                PrimaryPhone primaryPhone = new PrimaryPhone();
                primaryPhone.FreeFormNumber = phone;

                model.FullyQualifiedName = !string.IsNullOrEmpty(middleName) ? firstName + " " + middleName + " " + lastName : firstName + " " + lastName;
                model.DisplayName = !string.IsNullOrEmpty(middleName) ? firstName + " " + middleName + " " + lastName : firstName +" " + lastName;
                model.FamilyName = firstName;
                model.MiddleName = !string.IsNullOrEmpty(middleName) ? middleName : firstName;
                model.GivenName = lastName;
                model.CompanyName = !string.IsNullOrEmpty(companyName) ? companyName : firstName;
                model.PrimaryEmailAddr = primaryEmailAddr;
                model.PrimaryPhone = primaryPhone;


                json = new JavaScriptSerializer().Serialize(model);

                FeedbackResponse feedbackResponse1 = new FeedbackResponse();
                feedbackResponse1.Feedback = json;
                feedbackResponse1.FeedbackType = "Customer JSON";
                db.FeedbackResponses.Add(feedbackResponse1);
                db.SaveChanges();

                //string json = "{\"FullyQualifiedName\":\"Saiful1 Alam rana\"," +
                //"\"PrimaryEmailAddr\":{\"Address\":\"saiful12@digitalnexa.com\"}," +
                //"\"DisplayName\":\"Saiful1 Alam rana\"," +
                //"\"MiddleName\":\"\"," +
                //"\"FamilyName\":\"Saiful1 Alam rana\"," +
                //"\"PrimaryPhone\":{\"FreeFormNumber\":\"(555) 555-5555\"}," +
                //"\"CompanyName\":\"Nexa\"," +
                //"\"GivenName\":\"Saiful1 Alam rana\"}";

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(QuickbookInfo.QBOBaseUrl+ "/v3/company/"+QuickbookInfo.CompantID+"/customer?minorversion=57");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Accept = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Headers.Add("Authorization", "Bearer " + token);

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();

                    if (!string.IsNullOrEmpty(result))
                    {
                        object responseData = JsonConvert.DeserializeObject(result);

                        if (responseData != null)
                        {
                            customerid = ((dynamic)responseData).Customer.Id;
                            ctQBCustomer ctQBCustomer = new ctQBCustomer();
                            ctQBCustomer.CustomerId = customerid;
                            ctQBCustomer.CustomerEmail = email;
                            ctQBCustomer.CreatingDate = DateTime.Now;
                            ctQBCustomer.IntegrationMode = IntegrationMode;
                            db.ctQBCustomers.Add(ctQBCustomer);
                        }
                    }

                    FeedbackResponse feedbackResponse = new FeedbackResponse();
                    feedbackResponse.Feedback = result;
                    feedbackResponse.FeedbackType = "Customer Response";
                    db.FeedbackResponses.Add(feedbackResponse);
                    db.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                FeedbackResponse feedbackResponse = new FeedbackResponse();
                feedbackResponse.Feedback = ex.Message.ToString();
                feedbackResponse.FeedbackType = "Customer Error";
                db.FeedbackResponses.Add(feedbackResponse);
                db.SaveChanges();
            }
            

            return customerid;
        }

        public static int CreateItem(ctQuickBookIntegrationKey QuickbookInfo, string ItemName, string token)
        {
            int ItemId = 0;
            string json = "";
            try
            {
                QuickBookItem model = new QuickBookItem();
                IncomeAccountRef IncomeAccountRef = new IncomeAccountRef();
                IncomeAccountRef.name = "Sales of Product Income";
                IncomeAccountRef.value = "79";
                model.Name = ItemName;
                model.Type = "Service";
                model.IncomeAccountRef = IncomeAccountRef;

                json = new JavaScriptSerializer().Serialize(model);

                FeedbackResponse feedbackResponse1 = new FeedbackResponse();
                feedbackResponse1.Feedback = json;
                feedbackResponse1.FeedbackType = "Item JSON";
                db.FeedbackResponses.Add(feedbackResponse1);
                db.SaveChanges();

                //string json = "{\"Name\":\"CIA Level2, Online Training, 12/3/2012-12/3/2021\"," +
                //    "\"IncomeAccountRef\":{\"name\":\"Sales of Product Income\",\"value\":\"79\"}," +
                //    "\"Type\":\"Service\"}";

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(QuickbookInfo.QBOBaseUrl + "/v3/company/" + QuickbookInfo.CompantID + "/item?minorversion=57");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Accept = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Headers.Add("Authorization", "Bearer " + QuickbookInfo.AccessToken);

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();

                    if (!string.IsNullOrEmpty(result))
                    {
                        object responseData = JsonConvert.DeserializeObject(result);

                        if (responseData != null)
                        {
                            ItemId = ((dynamic)responseData).Item.Id;
                            ctQBItem ctQBItem = new ctQBItem();
                            ctQBItem.ItemId = ItemId;
                            ctQBItem.ItemName = ItemName;
                            ctQBItem.CreatingDate = DateTime.Now;
                            ctQBItem.IntegrationMode = IntegrationMode;
                            db.ctQBItems.Add(ctQBItem);
                        }
                    }

                    FeedbackResponse feedbackResponse = new FeedbackResponse();
                    feedbackResponse.Feedback = result;
                    feedbackResponse.FeedbackType = "Item Response";
                    db.FeedbackResponses.Add(feedbackResponse);
                    db.SaveChanges();

                }

            }
            catch (Exception ex)
            {
                FeedbackResponse feedbackResponse = new FeedbackResponse();
                feedbackResponse.Feedback = ex.Message.ToString();
                feedbackResponse.FeedbackType = "Item Error";
                db.FeedbackResponses.Add(feedbackResponse);
                db.SaveChanges();
            }


            return ItemId;

        }

        public static int CreateCartInvoice(ctQuickBookIntegrationKey QuickbookInfo, string token, long InvoiceId)
        {
            int QBInvoiceId = 0;
            string json = "";
            int customerId = 0;

            try
            {
                ctInvoice ctInvoice = db.ctInvoices.Where(x => x.InvoiceId == InvoiceId).FirstOrDefault();
                ctRegistration ctRegistration = db.ctRegistrations.Where(x => x.ProfileId == ctInvoice.ProfileId).FirstOrDefault();
                List<ctInvoiceDetail> ctInvoiceDetail = db.ctInvoiceDetails.Where(x => x.InvoiceId == ctInvoice.InvoiceId).ToList();
                int totalCount = ctInvoiceDetail.Count();

                if (db.ctQBCustomers.Where(x=>x.CustomerEmail==ctRegistration.EmailAddress && x.IntegrationMode==IntegrationMode).Count()>0)
                {
                    customerId = db.ctQBCustomers.Where(x => x.CustomerEmail == ctRegistration.EmailAddress && x.IntegrationMode == IntegrationMode).FirstOrDefault().CustomerId;
                }
                else
                {
                    customerId = CreateCustomer(QuickbookInfo, ctRegistration.EmailAddress, ctRegistration.MobileNumber, ctRegistration.FirstName, ctRegistration.MiddleName, ctRegistration.LastName, ctRegistration.Organization, token, ctRegistration.ProfileId);
                }

                CustomerRef customerRef = new CustomerRef();
                customerRef.value = customerId.ToString();

                Line[] Line = new Line[totalCount+3];
                int i = 0;

                foreach (var item in ctInvoiceDetail)
                {                    
                    Line lineItem = new Line();
                    var ItemName = "";
                    var ItemId = 0;

                    if (item.ProductType == "P")
                    {
                        ItemName = item.ProductCmsName; 
                    }
                    else
                    {
                        ItemName = item.CourseCmsName + ", " + item.LevelCmsName + ", " + item.StudyMethodCmsName + ", " + item.StartDate + "-" + item.EndDate;
                    }

                    if (db.ctQBItems.Where(x => x.ItemName == ItemName && x.IntegrationMode == IntegrationMode).Count()>0)
                    {
                        ItemId = db.ctQBItems.Where(x => x.ItemName == ItemName && x.IntegrationMode == IntegrationMode).FirstOrDefault().ItemId;
                    }
                    else
                    {
                        ItemId = CreateItem(QuickbookInfo, ItemName, token);
                    }

                    lineItem.DetailType = "SalesItemLineDetail";
                    lineItem.Amount = decimal.Parse(string.Format("{0:0.00}", item.Quantity * item.Price.Value));
                    SalesItemLineDetail salesItemLineDetail = new SalesItemLineDetail();
                    ItemRef itemRef = new ItemRef();
                    itemRef.name = ItemName;
                    itemRef.value = ItemId.ToString();
                    salesItemLineDetail.ItemRef = itemRef;
                    salesItemLineDetail.Qty = decimal.Parse(string.Format("{0:0}", item.Quantity));
                    salesItemLineDetail.UnitPrice = decimal.Parse(string.Format("{0:0.00}", item.Price.Value));

                    TaxCodeRef taxCodeRef = new TaxCodeRef();
                    taxCodeRef.name = IntegrationMode == "Live" ? "No Tax" : "TAX";
                    taxCodeRef.value = IntegrationMode == "Live" ? "4" : "TAX";
                    salesItemLineDetail.TaxCodeRef = taxCodeRef;

                    lineItem.SalesItemLineDetail = salesItemLineDetail;

                    Line[i] = lineItem;
                    i = i + 1;
                }

                // Discount Item                

                Line[i] = GetDiscountLineItem(QuickbookInfo, token, ctInvoice.Discount.Value);


                // Shipping Item                              

                Line[i+1] = GetShippingLineItem(QuickbookInfo, token, ctInvoice.Shipping.Value);

                // VAT Item                              

                Line[i + 2] = GetVATLineItem(QuickbookInfo, token, ctInvoice.VAT.Value);


                QuickbookInvoiceModel model = new QuickbookInvoiceModel();
                model.Line = Line;
                model.CustomerRef = customerRef;

                ShipAddr shipAddr = new ShipAddr();
                shipAddr = GetShippingAddress(ctRegistration.ProfileId);
                model.ShipAddr = shipAddr;

                BillAddr billAddr = new BillAddr();
                billAddr = GetBillingAddress(ctRegistration.ProfileId);
                model.BillAddr = billAddr;

                json = new JavaScriptSerializer().Serialize(model);


                FeedbackResponse feedbackResponse1 = new FeedbackResponse();
                feedbackResponse1.Feedback = json;
                feedbackResponse1.FeedbackType = "Invoice JSON";
                db.FeedbackResponses.Add(feedbackResponse1);
                db.SaveChanges();

                //string json = "{\"Line\":[{\"DetailType\":\"SalesItemLineDetail\",\"Amount\":100,\"SalesItemLineDetail\":{\"ItemRef\":{\"name\":\"CIA Level2, Online Training, 12/3/2012-12/3/2021\",\"value\":\"23\"}}}],\"CustomerRef\":{\"value\":\"61\"}}";

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(QuickbookInfo.QBOBaseUrl + "/v3/company/" + QuickbookInfo.CompantID + "/invoice?minorversion=57");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Accept = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Headers.Add("Authorization", "Bearer " + token);

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();

                    if (!string.IsNullOrEmpty(result))
                    {
                        object responseData = JsonConvert.DeserializeObject(result);

                        if (responseData != null)
                        {
                            QBInvoiceId = ((dynamic)responseData).Invoice.Id;
                            ctQBInvoice ctQBInvoice = new ctQBInvoice();
                            ctQBInvoice.InvoiceId = QBInvoiceId;                           
                            ctQBInvoice.CreatingDate = DateTime.Now;
                            ctQBInvoice.IntegrationMode = IntegrationMode;
                            db.ctQBInvoices.Add(ctQBInvoice);
                        }
                    }

                    FeedbackResponse feedbackResponse = new FeedbackResponse();
                    feedbackResponse.Feedback = result;
                    feedbackResponse.FeedbackType = "Invoice Response";
                    db.FeedbackResponses.Add(feedbackResponse);
                    db.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                FeedbackResponse feedbackResponse = new FeedbackResponse();
                feedbackResponse.Feedback = ex.Message.ToString();
                feedbackResponse.FeedbackType = "Invoice Error";
                db.FeedbackResponses.Add(feedbackResponse);
                db.SaveChanges();
            }


            return QBInvoiceId;

        }

        public static int CreateRegistrationInvoice(ctQuickBookIntegrationKey QuickbookInfo, string token, long PaymentId)
        {
            int QBInvoiceId = 0;
            string json = "";
            int customerId = 0;

            try
            {
                ctRegistrationPayment ctRegistrationPayment = db.ctRegistrationPayments.Where(x => x.PaymentId == PaymentId).FirstOrDefault();
                ctRegistration ctRegistration = db.ctRegistrations.Where(x => x.ProfileId == ctRegistrationPayment.ProfileId).FirstOrDefault();

                int totalCount = 4;

                if (db.ctQBCustomers.Where(x => x.CustomerEmail == ctRegistration.EmailAddress && x.IntegrationMode == IntegrationMode).Count() > 0)
                {
                    customerId = db.ctQBCustomers.Where(x => x.CustomerEmail == ctRegistration.EmailAddress && x.IntegrationMode == IntegrationMode).FirstOrDefault().CustomerId;
                }
                else
                {
                    customerId = CreateCustomer(QuickbookInfo, ctRegistration.EmailAddress, ctRegistration.MobileNumber, ctRegistration.FirstName, ctRegistration.MiddleName, ctRegistration.LastName, ctRegistration.Organization, token, ctRegistration.ProfileId);
                }

                CustomerRef customerRef = new CustomerRef();
                customerRef.value = customerId.ToString();

                Line[] Line = new Line[totalCount];

                Line lineItem = new Line();
                var ItemName = "";
                var ItemId = 0;

                if (!string.IsNullOrEmpty(ctRegistration.MembershipBadge))
                {
                    ItemName = ctRegistration.MembershipType + "," + ctRegistration.MembershipBadge;
                }
                else
                {
                    ItemName = ctRegistration.MembershipType;
                }

                if (db.ctQBItems.Where(x => x.ItemName == ItemName && x.IntegrationMode == IntegrationMode).Count() > 0)
                {
                    ItemId = db.ctQBItems.Where(x => x.ItemName == ItemName && x.IntegrationMode == IntegrationMode).FirstOrDefault().ItemId;
                }
                else
                {
                    ItemId = CreateItem(QuickbookInfo, ItemName, token);
                }

                lineItem.DetailType = "SalesItemLineDetail";
                lineItem.Amount = decimal.Parse(string.Format("{0:0.00}", ctRegistrationPayment.SubTotal.Value));
                SalesItemLineDetail salesItemLineDetail = new SalesItemLineDetail();
                ItemRef itemRef = new ItemRef();
                itemRef.name = ItemName;
                itemRef.value = ItemId.ToString();
                salesItemLineDetail.ItemRef = itemRef;
                salesItemLineDetail.Qty = decimal.Parse(string.Format("{0:0}", 1));
                salesItemLineDetail.UnitPrice = decimal.Parse(string.Format("{0:0.00}", ctRegistrationPayment.SubTotal.Value));

                TaxCodeRef taxCodeRef = new TaxCodeRef();
                taxCodeRef.name = IntegrationMode == "Live" ? "No Tax" : "TAX";
                taxCodeRef.value = IntegrationMode == "Live" ? "4" : "TAX";
                salesItemLineDetail.TaxCodeRef = taxCodeRef;

                lineItem.SalesItemLineDetail = salesItemLineDetail;

                Line[0] = lineItem;
              

                // Discount Item                

                Line[1] = GetDiscountLineItem(QuickbookInfo, token, ctRegistrationPayment.Discount.Value);


                // Shipping Item                              

               Line[2] = GetShippingLineItem(QuickbookInfo, token, ctRegistrationPayment.Shipping.Value);

                // VAT Item                              

                Line[3] = GetVATLineItem(QuickbookInfo, token, ctRegistrationPayment.VAT.Value);


                QuickbookInvoiceModel model = new QuickbookInvoiceModel();
                model.Line = Line;
                model.CustomerRef = customerRef;

                if (db.ctShippingBillings.Where(x => x.ProfileId == ctRegistration.ProfileId).Count() > 0)
                {
                    ShipAddr shipAddr = new ShipAddr();
                    shipAddr = GetShippingAddress(ctRegistration.ProfileId);
                    model.ShipAddr = shipAddr;

                    BillAddr billAddr = new BillAddr();
                    billAddr = GetBillingAddress(ctRegistration.ProfileId);
                    model.BillAddr = billAddr;
                }

                json = new JavaScriptSerializer().Serialize(model);


                FeedbackResponse feedbackResponse1 = new FeedbackResponse();
                feedbackResponse1.Feedback = json;
                feedbackResponse1.FeedbackType = "Invoice JSON";
                db.FeedbackResponses.Add(feedbackResponse1);
                db.SaveChanges();

                //string json = "{\"Line\":[{\"DetailType\":\"SalesItemLineDetail\",\"Amount\":100,\"SalesItemLineDetail\":{\"ItemRef\":{\"name\":\"CIA Level2, Online Training, 12/3/2012-12/3/2021\",\"value\":\"23\"}}}],\"CustomerRef\":{\"value\":\"61\"}}";

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(QuickbookInfo.QBOBaseUrl + "/v3/company/" + QuickbookInfo.CompantID + "/invoice?minorversion=57");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Accept = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Headers.Add("Authorization", "Bearer " + token);

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();

                    if (!string.IsNullOrEmpty(result))
                    {
                        object responseData = JsonConvert.DeserializeObject(result);

                        if (responseData != null)
                        {
                            QBInvoiceId = ((dynamic)responseData).Invoice.Id;
                            ctQBInvoice ctQBInvoice = new ctQBInvoice();
                            ctQBInvoice.InvoiceId = QBInvoiceId;
                            ctQBInvoice.CreatingDate = DateTime.Now;
                            ctQBInvoice.IntegrationMode = IntegrationMode;
                            db.ctQBInvoices.Add(ctQBInvoice);
                        }
                    }

                    FeedbackResponse feedbackResponse = new FeedbackResponse();
                    feedbackResponse.Feedback = result;
                    feedbackResponse.FeedbackType = "Invoice Response";
                    db.FeedbackResponses.Add(feedbackResponse);
                    db.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                FeedbackResponse feedbackResponse = new FeedbackResponse();
                feedbackResponse.Feedback = ex.Message.ToString();
                feedbackResponse.FeedbackType = "Invoice Error";
                db.FeedbackResponses.Add(feedbackResponse);
                db.SaveChanges();
            }


            return QBInvoiceId;

        }

        public static int CreateExternalCourseInvoice(ctQuickBookIntegrationKey QuickbookInfo, string token, long PaymentId)
        {
            int QBInvoiceId = 0;
            string json = "";
            int customerId = 0;

            try
            {
                ctExternalCoursePayment ctExternalCoursePayment = db.ctExternalCoursePayments.Where(x => x.ExternalPaymentId == PaymentId).FirstOrDefault();
                ctRegistration ctRegistration = db.ctRegistrations.Where(x => x.ProfileId == ctExternalCoursePayment.ProfileId).FirstOrDefault();
                ctExternalCourseRegistration ctExternalCourseRegistration = db.ctExternalCourseRegistrations.Where(x => x.RegId == ctExternalCoursePayment.RegId).FirstOrDefault();
                               


                int totalCount = 4;

                if (db.ctQBCustomers.Where(x => x.CustomerEmail == ctRegistration.EmailAddress && x.IntegrationMode == IntegrationMode).Count() > 0)
                {
                    customerId = db.ctQBCustomers.Where(x => x.CustomerEmail == ctRegistration.EmailAddress && x.IntegrationMode == IntegrationMode).FirstOrDefault().CustomerId;
                }
                else
                {
                    customerId = CreateCustomer(QuickbookInfo, ctRegistration.EmailAddress, ctRegistration.MobileNumber, ctRegistration.FirstName, ctRegistration.MiddleName, ctRegistration.LastName, ctRegistration.Organization, token, ctRegistration.ProfileId);
                }

                CustomerRef customerRef = new CustomerRef();
                customerRef.value = customerId.ToString();

                Line[] Line = new Line[totalCount];

                Line lineItem = new Line();
                var ItemName = "";
                var ItemId = 0;

                var helper = new UmbracoHelper(UmbracoContext.Current);
                var CourseInfo = helper.TypedContent(ctExternalCourseRegistration.CourseCmsId);

                if (ctExternalCourseRegistration.IsInterviewPass == "Y")
                {
                    ItemName = ctExternalCourseRegistration.CourseCmsTitle;
                }
                else
                {
                    ItemName = CourseInfo.GetPropertyValue("applicationFeeTitle").ToString();
                }
                

                if (db.ctQBItems.Where(x => x.ItemName == ItemName && x.IntegrationMode == IntegrationMode).Count() > 0)
                {
                    ItemId = db.ctQBItems.Where(x => x.ItemName == ItemName && x.IntegrationMode == IntegrationMode).FirstOrDefault().ItemId;
                }
                else
                {
                    ItemId = CreateItem(QuickbookInfo, ItemName, token);
                }

                lineItem.DetailType = "SalesItemLineDetail";
                lineItem.Amount = decimal.Parse(string.Format("{0:0.00}", ctExternalCoursePayment.SubTotal.Value));
                SalesItemLineDetail salesItemLineDetail = new SalesItemLineDetail();
                ItemRef itemRef = new ItemRef();
                itemRef.name = ItemName;
                itemRef.value = ItemId.ToString();
                salesItemLineDetail.ItemRef = itemRef;
                salesItemLineDetail.Qty = decimal.Parse(string.Format("{0:0}", 1));
                salesItemLineDetail.UnitPrice = decimal.Parse(string.Format("{0:0.00}", ctExternalCoursePayment.SubTotal.Value));

                TaxCodeRef taxCodeRef = new TaxCodeRef();
                taxCodeRef.name = IntegrationMode == "Live" ? "No Tax" : "TAX";
                taxCodeRef.value = IntegrationMode == "Live" ? "4" : "TAX";
                salesItemLineDetail.TaxCodeRef = taxCodeRef;

                lineItem.SalesItemLineDetail = salesItemLineDetail;

                Line[0] = lineItem;


                // Discount Item                

                Line[1] = GetDiscountLineItem(QuickbookInfo, token, ctExternalCoursePayment.Discount.Value);


                // Shipping Item                              

                Line[2] = GetShippingLineItem(QuickbookInfo, token, ctExternalCoursePayment.Shipping.Value);

                // VAT Item                              

                Line[3] = GetVATLineItem(QuickbookInfo, token, ctExternalCoursePayment.VAT.Value);


                QuickbookInvoiceModel model = new QuickbookInvoiceModel();
                model.Line = Line;
                model.CustomerRef = customerRef;

                if (db.ctShippingBillings.Where(x => x.ProfileId == ctRegistration.ProfileId).Count() > 0)
                {
                    ShipAddr shipAddr = new ShipAddr();
                    shipAddr = GetShippingAddress(ctRegistration.ProfileId);
                    model.ShipAddr = shipAddr;

                    BillAddr billAddr = new BillAddr();
                    billAddr = GetBillingAddress(ctRegistration.ProfileId);
                    model.BillAddr = billAddr;
                }

                json = new JavaScriptSerializer().Serialize(model);


                FeedbackResponse feedbackResponse1 = new FeedbackResponse();
                feedbackResponse1.Feedback = json;
                feedbackResponse1.FeedbackType = "Invoice JSON";
                db.FeedbackResponses.Add(feedbackResponse1);
                db.SaveChanges();

                //string json = "{\"Line\":[{\"DetailType\":\"SalesItemLineDetail\",\"Amount\":100,\"SalesItemLineDetail\":{\"ItemRef\":{\"name\":\"CIA Level2, Online Training, 12/3/2012-12/3/2021\",\"value\":\"23\"}}}],\"CustomerRef\":{\"value\":\"61\"}}";

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(QuickbookInfo.QBOBaseUrl + "/v3/company/" + QuickbookInfo.CompantID + "/invoice?minorversion=57");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Accept = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Headers.Add("Authorization", "Bearer " + token);

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();

                    if (!string.IsNullOrEmpty(result))
                    {
                        object responseData = JsonConvert.DeserializeObject(result);

                        if (responseData != null)
                        {
                            QBInvoiceId = ((dynamic)responseData).Invoice.Id;
                            ctQBInvoice ctQBInvoice = new ctQBInvoice();
                            ctQBInvoice.InvoiceId = QBInvoiceId;
                            ctQBInvoice.CreatingDate = DateTime.Now;
                            ctQBInvoice.IntegrationMode = IntegrationMode;
                            db.ctQBInvoices.Add(ctQBInvoice);
                        }
                    }

                    FeedbackResponse feedbackResponse = new FeedbackResponse();
                    feedbackResponse.Feedback = result;
                    feedbackResponse.FeedbackType = "Invoice Response";
                    db.FeedbackResponses.Add(feedbackResponse);
                    db.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                FeedbackResponse feedbackResponse = new FeedbackResponse();
                feedbackResponse.Feedback = ex.Message.ToString();
                feedbackResponse.FeedbackType = "Invoice Error";
                db.FeedbackResponses.Add(feedbackResponse);
                db.SaveChanges();
            }


            return QBInvoiceId;

        }

        public static Line GetDiscountLineItem(ctQuickBookIntegrationKey QuickbookInfo, string token, decimal amount)
        {
            int ItemId = 0;
            if (db.ctQBItems.Where(x => x.ItemName == "Discount" && x.IntegrationMode == IntegrationMode).Count() > 0)
            {
                ItemId = db.ctQBItems.Where(x => x.ItemName == "Discount" && x.IntegrationMode == IntegrationMode).FirstOrDefault().ItemId;
            }
            else
            {
                ItemId = CreateItem(QuickbookInfo, "Discount", token);
            }

            Line lineItem = new Line();

            lineItem.DetailType = "SalesItemLineDetail";
            lineItem.Amount = -decimal.Parse(string.Format("{0:0.00}", amount));
            SalesItemLineDetail ItemLineDetail = new SalesItemLineDetail();
            ItemRef itemRef = new ItemRef();
            itemRef.name = "Discount";
            itemRef.value = ItemId.ToString();
            ItemLineDetail.ItemRef = itemRef;
            ItemLineDetail.Qty = 1;
            ItemLineDetail.UnitPrice = -decimal.Parse(string.Format("{0:0.00}", amount));

            TaxCodeRef taxCodeRef = new TaxCodeRef();
            taxCodeRef.name = IntegrationMode=="Live"?"No Tax": "TAX";
            taxCodeRef.value = IntegrationMode == "Live" ? "4" : "TAX";
            ItemLineDetail.TaxCodeRef = taxCodeRef;

            lineItem.SalesItemLineDetail = ItemLineDetail;
            return lineItem;
        }

        public static Line GetShippingLineItem(ctQuickBookIntegrationKey QuickbookInfo, string token, decimal amount)
        {
            int ItemId = 0;
            if (db.ctQBItems.Where(x => x.ItemName == "Shipping" && x.IntegrationMode == IntegrationMode).Count() > 0)
            {
                ItemId = db.ctQBItems.Where(x => x.ItemName == "Shipping" && x.IntegrationMode == IntegrationMode).FirstOrDefault().ItemId;
            }
            else
            {
                ItemId = CreateItem(QuickbookInfo, "Shipping", token);
            }

            Line lineItem = new Line();

            lineItem.DetailType = "SalesItemLineDetail";
            lineItem.Amount = decimal.Parse(string.Format("{0:0.00}", amount));
            SalesItemLineDetail ItemLineDetail = new SalesItemLineDetail();
            ItemRef itemRef = new ItemRef();
            itemRef.name = "Shipping";
            itemRef.value = ItemId.ToString();
            ItemLineDetail.ItemRef = itemRef;
            ItemLineDetail.Qty = 1;
            ItemLineDetail.UnitPrice = decimal.Parse(string.Format("{0:0.00}", amount));

            TaxCodeRef taxCodeRef = new TaxCodeRef();
            taxCodeRef.name = IntegrationMode == "Live" ? "No Tax" : "TAX";
            taxCodeRef.value = IntegrationMode == "Live" ? "4" : "TAX";
            ItemLineDetail.TaxCodeRef = taxCodeRef;

            lineItem.SalesItemLineDetail = ItemLineDetail;
            return lineItem;
        }

        public static Line GetVATLineItem(ctQuickBookIntegrationKey QuickbookInfo, string token, decimal amount)
        {
            int ItemId = 0;
            if (db.ctQBItems.Where(x => x.ItemName == "VAT" && x.IntegrationMode == IntegrationMode).Count() > 0)
            {
                ItemId = db.ctQBItems.Where(x => x.ItemName == "VAT" && x.IntegrationMode == IntegrationMode).FirstOrDefault().ItemId;
            }
            else
            {
                ItemId = CreateItem(QuickbookInfo, "VAT", token);
            }

            Line lineItem = new Line();

            lineItem.DetailType = "SalesItemLineDetail";
            lineItem.Amount = decimal.Parse(string.Format("{0:0.00}", amount));
            SalesItemLineDetail ItemLineDetail = new SalesItemLineDetail();
            ItemRef itemRef = new ItemRef();
            itemRef.name = "VAT";
            itemRef.value = ItemId.ToString();
            ItemLineDetail.ItemRef = itemRef;
            ItemLineDetail.Qty = 1;
            ItemLineDetail.UnitPrice = decimal.Parse(string.Format("{0:0.00}", amount));

            TaxCodeRef taxCodeRef = new TaxCodeRef();
            taxCodeRef.name = IntegrationMode == "Live" ? "No Tax" : "TAX";
            taxCodeRef.value = IntegrationMode == "Live" ? "4" : "TAX";
            ItemLineDetail.TaxCodeRef = taxCodeRef;

            lineItem.SalesItemLineDetail = ItemLineDetail;
            return lineItem;
        }

        public static ShipAddr GetShippingAddress(long ProfileId)
        {
            ctShippingBilling ctShippingBilling = db.ctShippingBillings.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
            ShipAddr physicalAddress = new ShipAddr();
            physicalAddress.Line1 = ctShippingBilling.SFirstName+" "+ctShippingBilling.SLastName;
            physicalAddress.Line2 = (ctShippingBilling.SArea.Length > 50 ? ctShippingBilling.SArea.Substring(0, 50) : ctShippingBilling.SArea);
            physicalAddress.Line3 = (ctShippingBilling.SAddress1.Length > 50 ? ctShippingBilling.SAddress1.Substring(0, 50) : ctShippingBilling.SAddress1);
            physicalAddress.Line4 = !string.IsNullOrEmpty(ctShippingBilling.SAddress2)? ctShippingBilling.SAddress2:"";
            physicalAddress.Line5 = !string.IsNullOrEmpty(ctShippingBilling.SAddress3) ? ctShippingBilling.SAddress3 : "";
            physicalAddress.Country = db.CountryLists.Where(x => x.CountryCode == ctShippingBilling.SCountry).FirstOrDefault().CountryName;
            physicalAddress.City = ctShippingBilling.SCity;
            return physicalAddress;

        }

        public static BillAddr GetBillingAddress(long ProfileId)
        {
            ctShippingBilling ctShippingBilling = db.ctShippingBillings.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
            BillAddr physicalAddress = new BillAddr();
            physicalAddress.Line1 = ctShippingBilling.BFirstName + " " + ctShippingBilling.BLastName;
            physicalAddress.Line2 = (ctShippingBilling.BArea.Length > 50 ? ctShippingBilling.BArea.Substring(0, 50) : ctShippingBilling.BArea);
            physicalAddress.Line3 = (ctShippingBilling.BAddress1.Length > 50 ? ctShippingBilling.BAddress1.Substring(0, 50) : ctShippingBilling.BAddress1);
            physicalAddress.Line4 = !string.IsNullOrEmpty(ctShippingBilling.BAddress2) ? ctShippingBilling.BAddress2 : "";
            physicalAddress.Line5 = !string.IsNullOrEmpty(ctShippingBilling.BAddress3) ? ctShippingBilling.BAddress3 : "";
            physicalAddress.Country = db.CountryLists.Where(x => x.CountryCode == ctShippingBilling.BCountry).FirstOrDefault().CountryName;
            physicalAddress.City = ctShippingBilling.BCity;
            return physicalAddress;

        }
    }
}