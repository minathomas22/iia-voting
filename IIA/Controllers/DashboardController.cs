﻿using System;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using IIA.DBContext;
using System.Linq.Dynamic;
using umbraco.cms.helpers;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using IIA.Models;
using CMS.Helpers;
using System.Data.Entity;
using Umbraco.Web;
using System.Web.Configuration;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using umbraco.editorControls.SettingControls.Pickers;
using Newtonsoft.Json;
using Umbraco.Core.Models;
using IIA.Helper;
using Umbraco.Web.Media.EmbedProviders;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using umbraco.MacroEngines;
using umbraco.presentation;

namespace IIA.Controllers
{
    public class DashboardController : SurfaceController
    {
        IIAContext db = new IIAContext();
        [HttpGet]
        public ActionResult TopMenu()
        {
            try
            {
                var lang = Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString());

                RegistrationViewModel model = new RegistrationViewModel();
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                string middleName = lang == "en" ? objDbModel.MiddleName : objDbModel.MiddleName_ar;
                model.MembershipType = objDbModel.MembershipType;
                model.MembershipBadge = objDbModel.MembershipBadge;
                model.FirstName = lang == "en" ? objDbModel.FirstName : objDbModel.FirstName_ar;
                model.MiddleName = !string.IsNullOrEmpty(middleName) ? middleName : "";
                model.LastName = lang == "en" ? objDbModel.LastName : objDbModel.LastName_ar;
                if (!string.IsNullOrEmpty(objDbModel.ProfilePhoto))
                {
                    model.ProfilePhoto = "/Media/ApplicantFile/" + objDbModel.ProfilePhoto;
                }
                else
                {
                    model.ProfilePhoto = "/ui/media/images/avater.jpg";
                }


                model.ProfileType = objDbModel.ProfileType;
                model.ParentProfileId = objDbModel.ParentProfileId;
                return PartialView("Dashboard/TopMenu", model);
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult LeftMenu()
        {
            try
            {
                LeftMenuModel leftMenuModel = new LeftMenuModel();
                RegistrationViewModel model = new RegistrationViewModel();
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                model.MembershipType = objDbModel.MembershipType;
                model.ParentProfileId = objDbModel.ParentProfileId;
                model.ProfileType = objDbModel.ProfileType;
                model.MembershipEndDate = objDbModel.MembershipEndDate;
                leftMenuModel.RegistrationViewModel = model;
                if (canMemberApplyForCandidate(objDbModel))
                {
                    leftMenuModel.Elections = getOpenElectionsForApply();
                }
                return PartialView("Dashboard/LeftMenu", leftMenuModel);
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult UserProfile()
        {
            try
            {
                var lang = Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString());

                UserProfileDashboardModel model = new UserProfileDashboardModel();

                RegistrationViewModel RegisterInfo = new RegistrationViewModel();

                List<InvoiceDetailsModel> PurchaseList = new List<InvoiceDetailsModel>();

                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                string middleName = lang == "en" ? objDbModel.MiddleName : objDbModel.MiddleName_ar;

                RegisterInfo.FirstName = lang == "en" ? objDbModel.FirstName : objDbModel.FirstName_ar;
                RegisterInfo.MiddleName = !string.IsNullOrEmpty(middleName) ? middleName : "";
                RegisterInfo.LastName = lang == "en" ? objDbModel.LastName : objDbModel.LastName_ar;

                if (!string.IsNullOrEmpty(objDbModel.ProfilePhoto))
                {
                    RegisterInfo.ProfilePhoto = "/Media/ApplicantFile/" + objDbModel.ProfilePhoto;
                }
                else
                {
                    RegisterInfo.ProfilePhoto = "/ui/media/images/avater.jpg";
                }


                RegisterInfo.JobTitle = lang == "en" ? objDbModel.JobTitle : objDbModel.JobTitle_ar;
                RegisterInfo.Organization = lang == "en" ? objDbModel.Organization : objDbModel.Organization_ar;
                RegisterInfo.EmailAddress = objDbModel.EmailAddress;
                RegisterInfo.Qualification = lang == "en" ? objDbModel.Qualification : objDbModel.Qualification_ar;
                RegisterInfo.Bio = lang == "en" ? objDbModel.Bio : objDbModel.Bio_ar;
                RegisterInfo.Experience = lang == "en" ? objDbModel.Experience : objDbModel.Experience_ar;
                RegisterInfo.MobileNumber = objDbModel.MobileNumber;
                RegisterInfo.PhoneNumber = objDbModel.PhoneNumber;
                RegisterInfo.ProfileType = objDbModel.ProfileType;
                RegisterInfo.MemberActivationDate = objDbModel.MemberActivationDate;
                RegisterInfo.Country = db.CountryLists.Where(x => x.CountryCode == objDbModel.Country).FirstOrDefault().CountryName;
                RegisterInfo.City = lang == "en" ? objDbModel.City : objDbModel.City_ar;
                RegisterInfo.ProfileId = objDbModel.ProfileId;
                RegisterInfo.ParentProfileId = objDbModel.ParentProfileId;
                RegisterInfo.MembershipType = objDbModel.MembershipType;
                RegisterInfo.IIAGlobalNumber = objDbModel.IIAGlobalNumber;
                RegisterInfo.MembershipEndDate = objDbModel.MembershipEndDate;

                try
                {
                    var Invoice = db.ctInvoices.Where(x => x.ProfileId == ProfileId).ToList();

                    foreach (var invoiceMain in Invoice)
                    {
                        var InvoiceItem = db.ctInvoiceDetails.Where(x => x.InvoiceId == invoiceMain.InvoiceId).ToList();
                        foreach (var item in InvoiceItem)
                        {
                            InvoiceDetailsModel invoiceModel = new InvoiceDetailsModel();
                            if (item.ProductType == "C")
                            {
                                invoiceModel.CourseCmsName = Umbraco.TypedContent(item.CourseCmsId).GetPropertyValue("courseTitle").ToString();
                                invoiceModel.LevelCmsName = Umbraco.TypedContent(item.LevelCmsId).GetPropertyValue("courseLevel").ToString();
                                invoiceModel.StudyMethodCmsName = Umbraco.TypedContent(item.StudyMethodCmsId).GetPropertyValue("title").ToString();
                                invoiceModel.StartDate = item.StartDate;
                                invoiceModel.EndDate = item.EndDate;
                            }
                            else
                            {
                                invoiceModel.ProductCmsName = Umbraco.TypedContent(item.ProductCmsId).GetPropertyValue("title").ToString();
                                invoiceModel.ProductPicture = Umbraco.TypedContent(item.ProductCmsId).GetPropertyValue<IPublishedContent>("image").Url.ToString();

                            }

                            invoiceModel.Price = item.Price.Value;
                            invoiceModel.Discount = item.Discount.Value;
                            invoiceModel.Quantity = item.Quantity.Value;
                            invoiceModel.ProductType = item.ProductType;

                            PurchaseList.Add(invoiceModel);
                        }
                    }

                }
                catch
                {

                }

                model.RegisterInfo = RegisterInfo;
                model.PurchaseList = PurchaseList;


                return PartialView("Dashboard/UserProfile", model);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);


            }

        }

        [HttpGet]
        public ActionResult Financial()
        {
            try
            {
                RegistrationViewModel model = new RegistrationViewModel();
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                model.MembershipType = objDbModel.MembershipType;
                model.MembershipBadge = objDbModel.MembershipBadge;
                model.MembershipDiscount = objDbModel.MembershipDiscount != null ? objDbModel.MembershipDiscount.Value : 0;
                model.ProfileType = objDbModel.ProfileType;
                model.MemberActivationDate = objDbModel.MemberActivationDate != null ? objDbModel.MemberActivationDate : null;
                model.MembershipStartDate = objDbModel.MembershipStartDate != null ? objDbModel.MembershipStartDate : null;
                model.MembershipEndDate = objDbModel.MembershipEndDate != null ? objDbModel.MembershipEndDate : null;
                model.ParentProfileId = objDbModel.ParentProfileId;
                model.GraduationDate = objDbModel.GraduationDate != null ? objDbModel.GraduationDate : null;
                return PartialView("Dashboard/Financial", model);
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult ContactAdmin()
        {
            try
            {

                return PartialView("Dashboard/ContactAdmin");
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult MemberDirectory()
        {
            try
            {
                return PartialView("Dashboard/MemberDirectory");
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }


        [HttpPost]
        public ActionResult GetMemberDirectory()
        {
            try
            {
                return PartialView("Dashboard/MemberDirectory");
            }

            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }


        [HttpPost]
        public ActionResult ContactAdmin(ContactFormModel model)
        {
            try
            {
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                ctContact dbModel = new ctContact()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    EmailAddress = model.EmailAddress,
                    ContactNo = model.ContactNo,
                    Message = model.Message,
                    ContactType = model.ContactType,
                    PageId = ProfileId.ToString(),
                    ContactDate = DateTime.Now
                };

                db.ctContacts.Add(dbModel);
                db.SaveChanges();

                EmailSettingModel eModel = Utility.GetEmailSetting(model.ContactType, model.PageId, model.lang);

                string Message = EmailMessage.GetContactAdminMessage(model, model.lang);

                Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);

                return Json(new
                {
                    status = true,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpPost]
        public ActionResult ContactMember(ContactFormModel model)
        {
            try
            {
                ctContact dbModel = new ctContact()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    EmailAddress = model.EmailAddress,
                    ContactNo = model.ContactNo,
                    Message = model.Message,
                    ContactType = model.ContactType,
                    PageId = model.PageId,
                    ContactDate = DateTime.Now
                };

                db.ctContacts.Add(dbModel);
                db.SaveChanges();

                EmailSettingModel eModel = Utility.GetEmailSetting(model.ContactType, model.PageId, model.lang);

                string Message = EmailMessage.GetContactAdminMessage(model, model.lang);

                Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);

                return Json(new
                {
                    status = true,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpPost]
        public ActionResult GetMemberList(string keyword, string company, string page, string lang)
        {

            try
            {
                int pageIndex = !string.IsNullOrEmpty(page) ? int.Parse(page) : 1;
                int pageSize = 24;
                int totalPage = 0;
                var pageUrl = "";

                List<RegistrationViewModel> model = new List<RegistrationViewModel>();
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);

                var count = db.ctRegistrations.Where(x => x.ProfileType == "Member" & x.IsActive == true && x.ProfileId != ProfileId && x.MembershipType != "Corporate" && !string.IsNullOrEmpty(x.FirstName)).Count();

                var dbModel = db.ctRegistrations.Where(x => x.ProfileType == "Member" & x.IsActive == true && x.ProfileId != ProfileId && x.MembershipType != "Corporate" && !string.IsNullOrEmpty(x.FirstName)).ToList();

                if (keyword != "")
                {
                    keyword = keyword.ToLower();
                    if (lang == "en")
                    {
                        dbModel = dbModel.Where(x =>
                        (
                        x.FirstName?.ToLower().Contains(keyword) ?? false)
                        || (x.MiddleName?.ToLower().Contains(keyword) ?? false)
                        || (x.LastName?.ToLower().Contains(keyword) ?? false)
                        || (x.JobTitle?.ToLower().Contains(keyword) ?? false)
                        || (x.Organization?.ToLower().Contains(keyword) ?? false)
                        || (x.EmailAddress?.ToLower().Contains(keyword) ?? false)
                        || (x.NationalIDNumber?.ToLower().Contains(keyword) ?? false)
                        || (x.MobileNumber?.ToLower().Contains(keyword) ?? false)
                        || (x.City?.ToLower().Contains(keyword) ?? false)
                        ).ToList();
                    }
                    else
                    {
                        dbModel = dbModel.Where(x =>
                         (
                         x.FirstName_ar?.ToLower().Contains(keyword) ?? false)
                         || (x.MiddleName_ar?.ToLower().Contains(keyword) ?? false)
                         || (x.LastName_ar?.ToLower().Contains(keyword) ?? false)
                         || (x.JobTitle_ar?.ToLower().Contains(keyword) ?? false)
                         || (x.Organization_ar?.ToLower().Contains(keyword) ?? false)
                         || (x.EmailAddress?.ToLower().Contains(keyword) ?? false)
                         || (x.NationalIDNumber?.ToLower().Contains(keyword) ?? false)
                         || (x.MobileNumber?.ToLower().Contains(keyword) ?? false)
                         || (x.City_ar?.ToLower().Contains(keyword) ?? false)
                         ).ToList();
                    }
                    if (dbModel != null)
                    {
                        count = dbModel.Count();
                    }

                    pageUrl = "/" + lang + "/my-account/member-directory/?keyword=" + keyword + "&page=";
                }
                else
                {
                    pageUrl = "/" + lang + "/my-account/member-directory/?page=";
                }

                if (count > 0)
                {
                    dbModel = dbModel.Where(x => x.ProfileType == "Member" & x.IsActive == true && x.ProfileId != ProfileId && x.MembershipType != "Corporate").OrderBy(x => x.MemberActivationDate).Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToList();

                    foreach (var item in dbModel)
                    {
                        RegistrationViewModel regItem = new RegistrationViewModel();
                        string middleName = lang == "en" ? item.MiddleName : item.MiddleName_ar;
                        regItem.ProfileId = item.ProfileId;
                        regItem.FirstName = lang == "en" ? item.FirstName : item.FirstName_ar;
                        regItem.MiddleName = !string.IsNullOrEmpty(middleName) ? middleName : "";
                        regItem.LastName = lang == "en" ? item.LastName : item.LastName_ar;
                        if (!string.IsNullOrEmpty(item.ProfilePhoto))
                        {
                            regItem.ProfilePhoto = "/Media/ApplicantFile/" + item.ProfilePhoto;
                        }
                        else
                        {
                            regItem.ProfilePhoto = "/ui/media/images/avater.jpg";
                        }

                        regItem.ProfileType = item.ProfileType;
                        regItem.JobTitle = lang == "en" ? item.JobTitle : item.JobTitle_ar;
                        regItem.Organization = lang == "en" ? item.Organization : item.Organization_ar;
                        model.Add(regItem);
                    }

                    totalPage = (int)Math.Ceiling((double)count / pageSize);
                    var pagination = Pagination.Pager(pageIndex, count, pageSize, 5, pageUrl, "", "<<", ">>", "pagination");


                    return Json(new
                    {
                        status = true,
                        MemberInfo = model,
                        totalPage = totalPage,
                        pageIndex = pageIndex,
                        pageSize = pageSize,
                        pagination = pagination.ToString()
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = false
                    }, JsonRequestBehavior.AllowGet);

                }
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpGet]
        public ActionResult UserProfileById(string proId)
        {
            try
            {
                var lang = Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString());

                long ProfileId = long.Parse(proId);
                RegistrationViewModel model = new RegistrationViewModel();
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                string middleName = lang == "en" ? objDbModel.MiddleName : objDbModel.MiddleName_ar;
                model.FirstName = lang == "en" ? objDbModel.FirstName : objDbModel.FirstName_ar;
                model.MiddleName = !string.IsNullOrEmpty(middleName) ? middleName : "";
                model.LastName = lang == "en" ? objDbModel.LastName : objDbModel.LastName_ar;
                if (!string.IsNullOrEmpty(objDbModel.ProfilePhoto))
                {
                    model.ProfilePhoto = "/Media/ApplicantFile/" + objDbModel.ProfilePhoto;
                }
                else
                {
                    model.ProfilePhoto = "/ui/media/images/avater.jpg";
                }

                model.JobTitle = lang == "en" ? objDbModel.JobTitle : objDbModel.JobTitle_ar;
                model.Organization = lang == "en" ? objDbModel.Organization : objDbModel.Organization_ar;
                model.EmailAddress = objDbModel.EmailAddress;
                model.Qualification = lang == "en" ? objDbModel.Qualification : objDbModel.Qualification_ar;
                model.Bio = lang == "en" ? objDbModel.Bio : objDbModel.Bio_ar;
                model.Experience = lang == "en" ? objDbModel.Experience : objDbModel.Experience_ar;
                model.MobileNumber = objDbModel.MobileNumber;
                model.PhoneNumber = objDbModel.PhoneNumber;
                model.ProfileType = objDbModel.ProfileType;
                model.MemberActivationDate = objDbModel.MemberActivationDate.Value;
                model.Country = db.CountryLists.Where(x => x.CountryCode == objDbModel.Country).FirstOrDefault().CountryName;
                model.City = lang == "en" ? objDbModel.City : objDbModel.City_ar;
                model.ProfileId = objDbModel.ProfileId;
                model.ParentProfileId = objDbModel.ParentProfileId;

                return PartialView("Dashboard/UserProfileDetails", model);
            }
            catch (Exception ex)
            {

            }
            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult UpdateUserProfile()
        {
            try
            {
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                RegistrationViewModel model = new RegistrationViewModel();
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                model.FirstName = objDbModel.FirstName;
                model.MiddleName = objDbModel.MiddleName;
                model.LastName = objDbModel.LastName;
                if (!string.IsNullOrEmpty(objDbModel.ProfilePhoto))
                {
                    model.ProfilePhoto = "/Media/ApplicantFile/" + objDbModel.ProfilePhoto;
                }
                else
                {
                    model.ProfilePhoto = "/ui/media/images/avater.jpg";
                }

                model.JobTitle = objDbModel.JobTitle;
                model.Organization = objDbModel.Organization;
                model.EmailAddress = objDbModel.EmailAddress;
                model.Qualification = objDbModel.Qualification;
                model.Bio = objDbModel.Bio;
                model.Experience = objDbModel.Experience;
                model.MobileNumber = objDbModel.MobileNumber;
                model.PhoneNumber = objDbModel.PhoneNumber;
                model.ProfileType = objDbModel.ProfileType;
                model.Country = objDbModel.Country;
                model.City = objDbModel.City;
                model.Gender = objDbModel.Gender;
                model.DateOfBirth = objDbModel.DateOfBirth;
                model.NationalIDNumber = objDbModel.NationalIDNumber;
                model.FaxNumber = objDbModel.FaxNumber;
                model.Zip_Postal_Code = objDbModel.Zip_Postal_Code;

                model.ProfileId = objDbModel.ProfileId;
                model.ParentProfileId = objDbModel.ParentProfileId;

                model.PreferredLanguage = objDbModel.lang;
                model.FirstName_ar = objDbModel.FirstName_ar;
                model.MiddleName_ar = objDbModel.MiddleName_ar;
                model.LastName_ar = objDbModel.LastName_ar;
                model.JobTitle_ar = objDbModel.JobTitle_ar;
                model.Organization_ar = objDbModel.Organization_ar;
                model.City_ar = objDbModel.City_ar;
                model.Experience_ar = objDbModel.Experience_ar;
                model.Qualification_ar = objDbModel.Qualification_ar;
                model.Bio_ar = objDbModel.Bio_ar;


                return PartialView("Dashboard/UpdateUserProfile", model);
            }
            catch (Exception ex)
            {

            }
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult UpdateUserProfile(RegistrationViewModel model, HttpPostedFileBase imageUpload)
        {
            try
            {
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == model.ProfileId).FirstOrDefault();
                objDbModel.FirstName = model.FirstName;
                objDbModel.MiddleName = model.MiddleName;
                objDbModel.LastName = model.LastName;
                objDbModel.Gender = model.Gender;
                objDbModel.DateOfBirth = model.DateOfBirth;
                objDbModel.JobTitle = model.JobTitle;
                objDbModel.Organization = model.Organization;
                objDbModel.Country = model.Country;
                objDbModel.City = model.City;
                objDbModel.Zip_Postal_Code = model.Zip_Postal_Code;
                objDbModel.MobileNumber = model.MobileNumber;
                objDbModel.PhoneNumber = model.PhoneNumber;
                objDbModel.FaxNumber = model.FaxNumber;
                objDbModel.NationalIDNumber = model.NationalIDNumber;
                objDbModel.Experience = model.Experience;
                objDbModel.Qualification = model.Qualification;
                objDbModel.Bio = model.Bio;
                objDbModel.lang = model.PreferredLanguage;
                objDbModel.FirstName_ar = model.FirstName_ar;
                objDbModel.MiddleName_ar = model.MiddleName_ar;
                objDbModel.LastName_ar = model.LastName_ar;
                objDbModel.JobTitle_ar = model.JobTitle_ar;
                objDbModel.Organization_ar = model.Organization_ar;
                objDbModel.City_ar = model.City_ar;
                objDbModel.Experience_ar = model.Experience_ar;
                objDbModel.Qualification_ar = model.Qualification_ar;
                objDbModel.Bio_ar = model.Bio_ar;

                objDbModel.LastUpdated = DateTime.Now;

                if (imageUpload != null)
                {
                    string FileName = Utility.UploadFileInFolder(imageUpload);
                    objDbModel.ProfilePhoto = FileName;
                }

                db.Entry(objDbModel).State = EntityState.Modified;
                db.SaveChanges();

                if (model.lang == "en")
                {
                    return RedirectToUmbracoPage(1912);
                }
                else
                {
                    return RedirectToUmbracoPage(2720);
                }
            }
            catch (Exception ex)
            {

                if (model.lang == "en")
                {
                    return RedirectToUmbracoPage(1866);
                }
                else
                {
                    return RedirectToUmbracoPage(2734);
                }
            }

        }

        #region Elections
        [HttpGet]
        public ActionResult ApplyCandidate()
        {
                try
                {
                    var lang = Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString());
                    RegistrationWithCandidateViewModel RegisterInfo = new RegistrationWithCandidateViewModel();
                    long memberId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                    long electionId = long.Parse(Utility.GetApplicationCookies("Election").Value);
                    ctRegistration objDbModel = db.ctRegistrations.FirstOrDefault(x => x.ProfileId == memberId);
                    ctCandidate ctCandidateDb = db.ctCandidates.FirstOrDefault(x => x.MemberId == memberId && x.ElectionId == electionId);
                    ctElection ctElectionDb = db.ctElections.FirstOrDefault(x => x.ElectionId == electionId);
                if (objDbModel != null)
                {
                    string middleName = lang == "en" ? objDbModel.MiddleName : objDbModel.MiddleName_ar;
                    RegisterInfo.FirstName = lang == "en" ? objDbModel.FirstName : objDbModel.FirstName_ar;
                    RegisterInfo.MiddleName = !string.IsNullOrEmpty(middleName) ? middleName : "";
                    RegisterInfo.LastName = lang == "en" ? objDbModel.LastName : objDbModel.LastName_ar;
                    RegisterInfo.ProfilePhoto = !string.IsNullOrEmpty(objDbModel.ProfilePhoto) ? "/Media/ApplicantFile/" + objDbModel.ProfilePhoto : "/ui/media/images/avater.jpg";
                    RegisterInfo.JobTitle = lang == "en" ? objDbModel.JobTitle : objDbModel.JobTitle_ar;
                    RegisterInfo.Organization = lang == "en" ? objDbModel.Organization : objDbModel.Organization_ar;
                    RegisterInfo.EmailAddress = objDbModel.EmailAddress;
                    RegisterInfo.Qualification = lang == "en" ? objDbModel.Qualification : objDbModel.Qualification_ar;
                    RegisterInfo.Bio = lang == "en" ? objDbModel.Bio : objDbModel.Bio_ar;
                    RegisterInfo.ProfileType = objDbModel.ProfileType;
                    RegisterInfo.ProfileId = objDbModel.ProfileId;
                    RegisterInfo.ParentProfileId = objDbModel.ParentProfileId;
                    RegisterInfo.IIAGlobalNumber = objDbModel.IIAGlobalNumber;
                    RegisterInfo.ElectionId = electionId;
                    RegisterInfo.Comment = ctCandidateDb == null ? "" : ctCandidateDb.Comment;
                    RegisterInfo.ElectionTitle = ctElectionDb == null ? "" : ctElectionDb.Title;
                    RegisterInfo.CandidateStatus = ctCandidateDb == null ? CandidateRequestStatus.UnderRequest : (CandidateRequestStatus)ctCandidateDb.Status;
                    if (ctCandidateDb != null)
                    {
                        var candidateInfo = db.ctCandidateInfoes.FirstOrDefault(a => a.CandidateId == ctCandidateDb.CandidateId);
                        if (candidateInfo != null)
                        {
                            RegisterInfo.IsAgree = false;
                            RegisterInfo.IsAcknowledge = false;
                            RegisterInfo.OtherDocumentFile = !string.IsNullOrEmpty(candidateInfo.OtherDocumentFile) ? "/Media/ApplicantFile/" + candidateInfo.OtherDocumentFile : ""; ;
                            RegisterInfo.Description = candidateInfo.Description;
                            RegisterInfo.Bio = candidateInfo.Bio;
                            RegisterInfo.ResumeFile = !string.IsNullOrEmpty(candidateInfo.ResumeFile) ? "/Media/ApplicantFile/" + candidateInfo.ResumeFile : ""; ;
                            RegisterInfo.ElectionProgramFile = !string.IsNullOrEmpty(candidateInfo.ElectionProgramFile) ? "/Media/ApplicantFile/" + candidateInfo.ElectionProgramFile : ""; ;
                            RegisterInfo.LetterOfInterestFile = !string.IsNullOrEmpty(candidateInfo.LetterOfInterestFiles) ? "/Media/ApplicantFile/" + candidateInfo.LetterOfInterestFiles : ""; ;
                            RegisterInfo.OtherDocumentFile = !string.IsNullOrEmpty(candidateInfo.OtherDocumentFile) ? "/Media/ApplicantFile/" + candidateInfo.OtherDocumentFile : ""; ;
                            var files = !string.IsNullOrEmpty(candidateInfo.OtherDocumentFile) ? JsonConvert.DeserializeObject<List<string>>(candidateInfo.OtherDocumentFile) : new List<string>();
                            var linksList = new List<string>();
                            foreach (var file in files)
                            {
                                linksList.Add("/Media/ApplicantFile/" + file);
                            }
                            RegisterInfo.OtherDocumentFiles = linksList;
                            //RegisterInfo.OtherDocumentFiles = files.Select(e => string { "/Media/ApplicantFile/" + e }).
                        }
                    }
                }
                    return PartialView("Dashboard/ApplyCandidate", RegisterInfo);
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        status = false,
                        message = ex.Message.ToString()
                    }, JsonRequestBehavior.AllowGet);
                }

            }
        #endregion

        [HttpPost]
        public ActionResult GetBasicUserInfo()
        {
            try
            {
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                return Json(new
                {
                    status = true,
                    FirstName = !string.IsNullOrEmpty(objDbModel.FirstName) ? objDbModel.FirstName : "",
                    LastName = !string.IsNullOrEmpty(objDbModel.LastName) ? objDbModel.LastName : "",
                    EmailAddress = objDbModel.EmailAddress,
                    MobileNumber = objDbModel.MobileNumber
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ChangePassword(string oldPassword, string newPassword)
        {
            try
            {
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                if (objDbModel.Password == oldPassword)
                {
                    objDbModel.Password = newPassword;
                    db.Entry(objDbModel).State = EntityState.Modified;
                    db.SaveChanges();

                    return Json(new
                    {
                        status = true
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult TrainingCalendar()
        {
            try
            {

                return PartialView("Dashboard/TrainingCalendar");
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult PostJob()
        {
            try
            {

                return PartialView("Dashboard/PostaJob");
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }


        [HttpGet]
        public ActionResult PostInternship()
        {
            try
            {

                return PartialView("Dashboard/PostaInternship");
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult PostJobInternship(JobInternshipViewModel model)
        {
            try
            {
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                ctJobInternship objDbModel = new ctJobInternship();

                var BannerImage = Request.Files["BannerImage"];
                if (BannerImage != null)
                {
                    objDbModel.BannerImage = Utility.UploadFileInFolder(BannerImage);
                }

                var BackgroundImage = Request.Files["BackgroundImage"];
                if (BackgroundImage != null)
                {
                    objDbModel.BackgroundImage = Utility.UploadFileInFolder(BackgroundImage);
                }

                objDbModel.PostType = model.PostType;
                objDbModel.ProfileId = ProfileId;
                objDbModel.JobTitle = model.JobTitle;
                objDbModel.Company = model.Company;
                objDbModel.Location = model.Location;
                objDbModel.Tenure = model.Tenure;
                objDbModel.JobDescription = model.JobDescription;
                objDbModel.JobResponsibilities = model.JobResponsibilities;
                objDbModel.DatePosted = DateTime.Now;
                objDbModel.LastUpdated = DateTime.Now;
                objDbModel.IsApprove = false;
                objDbModel.IsActive = true;
                db.ctJobInternships.Add(objDbModel);
                db.SaveChanges();

                EmailSettingModel eModel = Utility.GetEmailSetting("CorporatePostSubmission", "", model.lang);

                string Message = EmailMessage.GetCorporatePostMessage(model, model.lang);

                Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);


                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GetJobInternship(string PostType)
        {
            try
            {
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                List<JobInternshipViewModel> model = new List<JobInternshipViewModel>();
                var dbModel = db.ctJobInternships.Where(x => x.ProfileId == ProfileId && x.PostType == PostType).OrderByDescending(x => x.DatePosted);
                foreach (var item in dbModel)
                {
                    JobInternshipViewModel itemModel = new JobInternshipViewModel();
                    itemModel.PostId = item.PostId;
                    itemModel.JobTitle = item.JobTitle;
                    itemModel.Company = item.Company;
                    itemModel.Location = item.Location;
                    itemModel.Tenure = item.Tenure;
                    itemModel.JobDescription = item.JobDescription;
                    itemModel.JobResponsibilities = item.JobResponsibilities;
                    itemModel.PostType = item.PostType;
                    itemModel.DatePosted = item.DatePosted.Value;
                    itemModel.strDatePosted = item.DatePosted.Value.ToString("dd MMM yyyy");
                    itemModel.IsApprove = item.IsApprove.Value;
                    itemModel.IsActive = item.IsActive.Value;
                    model.Add(itemModel);
                }

                return Json(new
                {
                    status = true,
                    availablePost = model
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public ActionResult GetJobInternshipById(string postId)
        {
            try
            {
                long PostID = long.Parse(postId);
                JobInternshipViewModel model = new JobInternshipViewModel();
                var dbModel = db.ctJobInternships.Where(x => x.PostId == PostID).FirstOrDefault();
                model.PostId = dbModel.PostId;
                model.JobTitle = dbModel.JobTitle;
                model.Company = dbModel.Company;
                model.Location = dbModel.Location;
                model.Tenure = dbModel.Tenure;
                model.JobDescription = dbModel.JobDescription;
                model.JobResponsibilities = dbModel.JobResponsibilities;
                model.PostType = dbModel.PostType;
                model.DatePosted = dbModel.DatePosted.Value;
                model.strDatePosted = dbModel.DatePosted.Value.ToString("dd MMM yyyy");
                model.IsApprove = dbModel.IsApprove.Value;
                model.IsActive = dbModel.IsActive.Value;
                model.BackgroundImage = dbModel.BackgroundImage;
                model.BannerImage = dbModel.BannerImage;

                return PartialView("Dashboard/UpdateJobInternship", model);
            }
            catch (Exception ex)
            {

            }
            return HttpNotFound();

        }

        [HttpPost]
        public ActionResult UpdateJobInternship(JobInternshipViewModel model)
        {
            try
            {
                ctJobInternship objDbModel = db.ctJobInternships.Where(x => x.PostId == model.PostId).FirstOrDefault();

                var BannerImage = Request.Files["BannerImage"];
                if (BannerImage != null)
                {
                    objDbModel.BannerImage = Utility.UploadFileInFolder(BannerImage);
                }

                var BackgroundImage = Request.Files["BackgroundImage"];
                if (BackgroundImage != null)
                {
                    objDbModel.BackgroundImage = Utility.UploadFileInFolder(BackgroundImage);
                }

                objDbModel.JobTitle = model.JobTitle;
                objDbModel.Company = model.Company;
                objDbModel.Location = model.Location;
                objDbModel.Tenure = model.Tenure;
                objDbModel.JobDescription = model.JobDescription;
                objDbModel.JobResponsibilities = model.JobResponsibilities;
                objDbModel.LastUpdated = DateTime.Now;

                db.Entry(objDbModel).State = EntityState.Modified;
                db.SaveChanges();

                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public ActionResult EmployeeEnrolled()
        {
            try
            {

                return PartialView("Dashboard/EmployeeEnrolled");
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult GetEmployeeEnrolled(string name, string emailAddress, string lang)
        {

            try
            {

                List<RegistrationViewModel> model = new List<RegistrationViewModel>();
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                var dbModel = db.ctRegistrations.Where(x => x.ProfileType == "Member" && x.ParentProfileId == ProfileId).ToList();
                if (name != "")
                {
                    dbModel = dbModel.Where(x => x.FirstName.StartsWith(name)).ToList();
                }
                if (emailAddress != "")
                {
                    dbModel = dbModel.Where(x => x.EmailAddress.StartsWith(emailAddress)).ToList();
                }
                foreach (var item in dbModel)
                {
                    RegistrationViewModel regItem = new RegistrationViewModel();
                    string middleName = lang == "en" ? item.MiddleName : item.MiddleName_ar;
                    regItem.ProfileId = item.ProfileId;
                    regItem.FirstName = lang == "en" ? item.FirstName : item.FirstName_ar;
                    regItem.MiddleName = !string.IsNullOrEmpty(middleName) ? middleName : "";
                    regItem.LastName = lang == "en" ? item.LastName : item.LastName_ar;
                    if (!string.IsNullOrEmpty(item.ProfilePhoto))
                    {
                        regItem.ProfilePhoto = "/Media/ApplicantFile/" + item.ProfilePhoto;
                    }
                    else
                    {
                        regItem.ProfilePhoto = "/ui/media/images/profile/sample.png";
                    }
                    regItem.MembershipType = item.MembershipType;
                    regItem.MembershipBadge = item.MembershipBadge;
                    regItem.IsActive = item.IsActive.Value;
                    regItem.EmailAddress = item.EmailAddress;

                    model.Add(regItem);
                }

                return Json(new
                {
                    status = true,
                    MemberInfo = model
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpPost]
        public ActionResult AddEnrolledMember(RegistrationViewModel model)
        {
            try
            {
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                var parentProfile = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                ctRegistration objDbModel = new ctRegistration();
                objDbModel.ProfileType = "Member";
                objDbModel.ParentProfileId = ProfileId;
                objDbModel.MembershipType = "Professional";
                objDbModel.MembershipBadge = parentProfile.MembershipBadge;
                objDbModel.MembershipDiscount = parentProfile.MembershipDiscount;
                objDbModel.IIAKSANumber = Utility.GenerateIIAKSANumber("Member");
                objDbModel.FirstName = model.FirstName;
                objDbModel.FirstName_ar = model.FirstName_ar;
                objDbModel.MiddleName = model.MiddleName;
                objDbModel.MiddleName_ar = model.MiddleName_ar;
                objDbModel.LastName = model.LastName;
                objDbModel.LastName_ar = model.LastName_ar;
                objDbModel.EmailAddress = model.EmailAddress;
                objDbModel.IsActive = false;
                objDbModel.IsEmailVerify = true;
                objDbModel.IsApplyForMentor = false;
                objDbModel.IsApproveMentor = false;
                objDbModel.LastUpdated = DateTime.Now;

                db.ctRegistrations.Add(objDbModel);
                db.SaveChanges();

                long newProfileId = objDbModel.ProfileId;

                ctTempKey dbTempKey = new ctTempKey();
                dbTempKey.ModuleType = "MemberEnrolled";
                dbTempKey.TempKey = Guid.NewGuid().ToString();
                dbTempKey.ModuleId = newProfileId.ToString();
                dbTempKey.GeneratedTime = DateTime.Now;
                db.ctTempKeys.Add(dbTempKey);
                db.SaveChanges();

                string Url = WebConfigurationManager.AppSettings["WebUrl"].ToString() + model.lang + "/registration/member-enrolled-registration/?key=" + dbTempKey.TempKey;

                EmailSettingModel eModel = Utility.GetEmailSetting("MemberEnrolled", newProfileId.ToString(), model.lang);

                string Message = EmailMessage.GetRegistrationMessage(parentProfile, "MemberEnrolled", Url, model.FirstName, model.lang);

                Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);


                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult RegistrationEnrolledMember(string key)
        {
            try
            {
                RegistrationViewModel model = new RegistrationViewModel();
                if (!string.IsNullOrEmpty(key))
                {
                    var ctTempKey = db.ctTempKeys.Where(x => x.TempKey == key && x.ModuleType == "MemberEnrolled").FirstOrDefault();
                    long ProfileId = long.Parse(ctTempKey.ModuleId);
                    var ctRegistration = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                    model.FirstName = ctRegistration.FirstName;
                    model.MiddleName = ctRegistration.MiddleName;
                    model.LastName = ctRegistration.LastName;
                    model.FirstName_ar = ctRegistration.FirstName_ar;
                    model.MiddleName_ar = ctRegistration.MiddleName_ar;
                    model.LastName_ar = ctRegistration.LastName_ar;
                    model.EmailAddress = ctRegistration.EmailAddress;
                    model.IsEnrolledValid = ctRegistration.IsActive.Value ? "No" : "Yes";
                    model.ProfileId = ctRegistration.ProfileId;
                }
                else
                {
                    model.IsEnrolledValid = "No";
                }

                return PartialView("Dashboard/RegistrationEnrolledMember", model);
            }
            catch (Exception ex)
            {

            }
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult EnrolledMemberRegistration(RegistrationViewModel model, IEnumerable<HttpPostedFileBase> additionalDocument, HttpPostedFileBase imageUpload)
        {

            try
            {
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == model.ProfileId).FirstOrDefault();

                objDbModel.FirstName = model.FirstName;
                objDbModel.MiddleName = model.MiddleName;
                objDbModel.LastName = model.LastName;
                objDbModel.Gender = model.Gender;
                objDbModel.DateOfBirth = model.DateOfBirth;
                objDbModel.JobTitle = model.JobTitle;
                objDbModel.Organization = model.Organization;
                objDbModel.Country = model.Country;
                objDbModel.City = model.City;
                objDbModel.Zip_Postal_Code = model.Zip_Postal_Code;
                objDbModel.MobileNumber = model.MobileNumber;
                objDbModel.PhoneNumber = model.PhoneNumber;
                objDbModel.FaxNumber = model.FaxNumber;
                objDbModel.NationalIDNumber = model.NationalIDNumber;
                objDbModel.EmailAddress = model.EmailAddress;
                objDbModel.Password = model.Password;
                objDbModel.LastUpdated = DateTime.Now;
                objDbModel.lang = string.IsNullOrEmpty(model.PreferredLanguage) ? "ar" : model.PreferredLanguage;
                objDbModel.FirstName_ar = model.FirstName_ar;
                objDbModel.MiddleName_ar = model.MiddleName_ar;
                objDbModel.LastName_ar = model.LastName_ar;
                objDbModel.JobTitle_ar = model.JobTitle_ar;
                objDbModel.Organization_ar = model.Organization_ar;
                objDbModel.City_ar = model.City_ar;

                if (imageUpload != null)
                {
                    string FileName = Utility.UploadFileInFolder(imageUpload);
                    objDbModel.ProfilePhoto = FileName;
                }

                db.Entry(objDbModel).State = EntityState.Modified;
                db.SaveChanges();

                if (additionalDocument != null)
                {
                    foreach (HttpPostedFileBase file in additionalDocument)
                    {
                        if (file != null)
                        {
                            string FileName = Utility.UploadFileInFolder(file);
                            ctRegistrationDocument objDocument = new ctRegistrationDocument();
                            objDocument.DocumentUrl = FileName;
                            objDocument.ProfileId = model.ProfileId;
                            objDocument.LastUpdated = DateTime.Now;

                            db.ctRegistrationDocuments.Add(objDocument);
                            db.SaveChanges();
                        }
                    }
                }

                EmailSettingModel eModel = Utility.GetEmailSetting("EnrollMemberRegistration", objDbModel.ProfileId.ToString(), model.lang);

                string Message = EmailMessage.GetRegistrationMessage(objDbModel, "EnrollMemberRegistration", "", objDbModel.FirstName, model.lang);

                Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);

                if (model.PreferredLanguage == "en")
                {
                    return RedirectToUmbracoPage(2050);
                }
                else
                {
                    return RedirectToUmbracoPage(2710);
                }
            }
            catch (Exception ex)
            {
                if (model.PreferredLanguage == "en")
                {
                    return RedirectToUmbracoPage(1866);
                }
                else
                {
                    return RedirectToUmbracoPage(2734);
                }
            }

        }

        [HttpGet]
        public ActionResult EmployeeEnrolledSearch()
        {

            try
            {

                return PartialView("Dashboard/MemberEnrollSearch");
            }

            catch
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        public ActionResult Unroll(string profileId)
        {
            try
            {
                long MemberProfileId = long.Parse(profileId);

                ctRegistration ctRegistration = db.ctRegistrations.Where(x => x.ProfileId == MemberProfileId).FirstOrDefault();
                ctRegistration.IsActive = false;
                ctRegistration.LastUpdated = DateTime.Now;
                db.Entry(ctRegistration).State = EntityState.Modified;
                db.SaveChanges();

                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult RemoveMember(string profileId)
        {
            try
            {
                long MemberProfileId = long.Parse(profileId);

                ctRegistration ctRegistration = db.ctRegistrations.Where(x => x.ProfileId == MemberProfileId).FirstOrDefault();
                db.ctRegistrations.Remove(ctRegistration);
                db.SaveChanges();

                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SendInvite(string profileId, string lang)
        {
            try
            {
                long MemberProfileId = long.Parse(profileId);

                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                var parentProfile = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                ctRegistration ctRegistration = db.ctRegistrations.Where(x => x.ProfileId == MemberProfileId).FirstOrDefault();

                ctTempKey ctTempKey = db.ctTempKeys.Where(x => x.ModuleId == MemberProfileId.ToString() && x.ModuleType == "MemberEnrolled").FirstOrDefault();
                var Key = Guid.NewGuid().ToString();

                if (ctTempKey != null)
                {
                    ctTempKey.TempKey = Key;
                    ctTempKey.GeneratedTime = DateTime.Now;
                    db.Entry(ctTempKey).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    ctTempKey dbTempKey = new ctTempKey();
                    dbTempKey.ModuleType = "MemberEnrolled";
                    dbTempKey.TempKey = Key;
                    dbTempKey.ModuleId = MemberProfileId.ToString();
                    dbTempKey.GeneratedTime = DateTime.Now;
                    db.ctTempKeys.Add(dbTempKey);
                    db.SaveChanges();
                }

                string Url = WebConfigurationManager.AppSettings["WebUrl"].ToString() + lang + "/registration/member-enrolled-registration/?key=" + Key;

                EmailSettingModel eModel = Utility.GetEmailSetting("MemberEnrolled", MemberProfileId.ToString(), lang);

                string Message = EmailMessage.GetRegistrationMessage(parentProfile, "MemberEnrolled", Url, ctRegistration.FirstName, lang);

                Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);

                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ApplyMentoring(string lang)
        {
            try
            {


                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                if (db.ctRegistrations.Where(x => x.ProfileId == ProfileId && x.IsApplyForMentor == true).Count() <= 0)
                {

                    ctRegistration ctRegistration = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                    ctRegistration.IsApplyForMentor = true;
                    ctRegistration.LastUpdated = DateTime.Now;
                    db.Entry(ctRegistration).State = EntityState.Modified;
                    db.SaveChanges();

                    EmailSettingModel eModel = Utility.GetEmailSetting("ApplyMentoring", ProfileId.ToString(), lang);

                    string Message = EmailMessage.GetRegistrationMessage(ctRegistration, "ApplyMentoring", "", ctRegistration.FirstName, lang);

                    Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);

                    return Json(new
                    {
                        status = true
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetInvoice()
        {

            try
            {

                return PartialView("Dashboard/Invoice");
            }

            catch
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        public ActionResult GetInvoiceList()
        {

            try
            {
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                List<OrderHistoryModel> model = new List<OrderHistoryModel>();
                var ctInvoice = db.ctInvoices.Where(x => x.ProfileId == ProfileId && x.TransactionStatus == "Success").OrderByDescending(x => x.TransactionDate).ToList();
                foreach (var item in ctInvoice)
                {
                    OrderHistoryModel ModelItem = new OrderHistoryModel();
                    ModelItem.InvoiceId = item.InvoiceId;
                    ModelItem.InvoiceDate = item.TransactionDate.Value;
                    ModelItem.GrandTotal = string.Format("{0:0.00}", item.GrandTotal);
                    ModelItem.strInvoiceDate = item.TransactionDate.Value.ToString("dd MMM yyyy");
                    ModelItem.InvoiceType = "SI";
                    model.Add(ModelItem);
                }

                return Json(new
                {
                    status = true,
                    InvoiceList = model
                }, JsonRequestBehavior.AllowGet);
            }

            catch
            {
                return HttpNotFound();
            }
        }

        [HttpGet]
        public ActionResult InvoiceById()
        {
            try
            {
                if (Request.QueryString["type"] == "SI")
                {
                    InvoiceViewModel model = new InvoiceViewModel();
                    InvoiceModel InvoiceInfo = new InvoiceModel();
                    List<InvoiceDetailsModel> ListInvoiceItem = new List<InvoiceDetailsModel>();

                    try
                    {
                        long InvoiceId = long.Parse(Request.QueryString["invId"]);
                        ctInvoice dbInvoice = db.ctInvoices.Where(x => x.InvoiceId == InvoiceId).FirstOrDefault();

                        var dbInvoiceItem = db.ctInvoiceDetails.Where(x => x.InvoiceId == InvoiceId).ToList();

                        foreach (var item in dbInvoiceItem)
                        {
                            InvoiceDetailsModel invoiceModel = new InvoiceDetailsModel();
                            if (item.ProductType == "C")
                            {
                                invoiceModel.CourseCmsName = Umbraco.TypedContent(item.CourseCmsId).GetPropertyValue("courseTitle").ToString();
                                invoiceModel.LevelCmsName = Umbraco.TypedContent(item.LevelCmsId).GetPropertyValue("courseLevel").ToString();
                                invoiceModel.StudyMethodCmsName = Umbraco.TypedContent(item.StudyMethodCmsId).GetPropertyValue("title").ToString();
                                invoiceModel.StartDate = item.StartDate;
                                invoiceModel.EndDate = item.EndDate;
                                invoiceModel.ShowHideCounter = "none;";
                                invoiceModel.CourseCmsId = item.CourseCmsId.Value;
                                invoiceModel.LevelCmsId = item.LevelCmsId.Value;
                                invoiceModel.StudyMethodCmsId = item.StudyMethodCmsId.Value;
                            }
                            else
                            {
                                invoiceModel.ProductCmsName = Umbraco.TypedContent(item.ProductCmsId).GetPropertyValue("title").ToString();
                                invoiceModel.ProductPicture = Umbraco.TypedContent(item.ProductCmsId).GetPropertyValue<IPublishedContent>("image").Url.ToString();
                                invoiceModel.ShowHideCounter = "none;";
                                invoiceModel.ProductCmsId = item.ProductCmsId.Value;
                            }

                            invoiceModel.Price = item.Price.Value;
                            invoiceModel.Discount = item.Discount.Value;
                            invoiceModel.Quantity = item.Quantity.Value;
                            invoiceModel.ProductType = item.ProductType;

                            ListInvoiceItem.Add(invoiceModel);
                        }

                        InvoiceInfo.SubTotal = dbInvoice.SubTotal.Value;
                        InvoiceInfo.Shipping = dbInvoice.Shipping.Value;
                        InvoiceInfo.Discount = dbInvoice.Discount.Value;
                        InvoiceInfo.VAT = dbInvoice.VAT.Value;
                        InvoiceInfo.GrandTotal = dbInvoice.GrandTotal.Value;
                        InvoiceInfo.TransactionAmount = dbInvoice.TransactionAmount.Value;
                        InvoiceInfo.TransactionCurrency = dbInvoice.TransactionCurrency;
                        InvoiceInfo.InvoiceNumber = dbInvoice.InvoiceNumber;
                        InvoiceInfo.ShippingReference = dbInvoice.ShippingReference;
                        InvoiceInfo.ShippingReferencePDF = dbInvoice.ShippingReferencePDF;
                        InvoiceInfo.ProfileId = dbInvoice.ProfileId.Value;

                        model.InvoiceInfo = InvoiceInfo;
                        model.ListInvoiceItem = ListInvoiceItem;

                        return PartialView("Dashboard/InvoiceDetails", model);
                    }
                    catch
                    {
                        return HttpNotFound();
                    }
                }
                else
                {
                    return PartialView("Dashboard/RegistrationPaymentDetails");
                }

            }

            catch
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        public ActionResult RemovePost(string postId)
        {
            try
            {
                long PostId = long.Parse(postId);

                ctJobInternship ctJobInternship = db.ctJobInternships.Where(x => x.PostId == PostId).FirstOrDefault();
                db.ctJobInternships.Remove(ctJobInternship);
                db.SaveChanges();

                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult EnrollUserProfile(string proId)
        {
            try
            {
                string lang = Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString());

                UserProfileDashboardModel model = new UserProfileDashboardModel();

                RegistrationViewModel RegisterInfo = new RegistrationViewModel();

                List<InvoiceDetailsModel> PurchaseList = new List<InvoiceDetailsModel>();

                long ProfileId = long.Parse(proId);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                string middleName = lang == "en" ? objDbModel.MiddleName : objDbModel.MiddleName_ar;
                RegisterInfo.FirstName = lang == "en" ? objDbModel.FirstName : objDbModel.FirstName_ar;
                RegisterInfo.MiddleName = !string.IsNullOrEmpty(middleName) ? middleName : "";
                RegisterInfo.LastName = lang == "en" ? objDbModel.LastName : objDbModel.LastName_ar;

                if (!string.IsNullOrEmpty(objDbModel.ProfilePhoto))
                {
                    RegisterInfo.ProfilePhoto = "/Media/ApplicantFile/" + objDbModel.ProfilePhoto;
                }
                else
                {
                    RegisterInfo.ProfilePhoto = "/ui/media/images/avater.jpg";
                }


                RegisterInfo.JobTitle = lang == "en" ? objDbModel.JobTitle : objDbModel.JobTitle_ar;
                RegisterInfo.Organization = lang == "en" ? objDbModel.Organization : objDbModel.Organization_ar;
                RegisterInfo.EmailAddress = objDbModel.EmailAddress;
                RegisterInfo.Qualification = lang == "en" ? objDbModel.Qualification : objDbModel.Qualification_ar;
                RegisterInfo.Bio = lang == "en" ? objDbModel.Bio : objDbModel.Bio_ar;
                RegisterInfo.Experience = lang == "en" ? objDbModel.Experience : objDbModel.Experience_ar;
                RegisterInfo.MobileNumber = objDbModel.MobileNumber;
                RegisterInfo.PhoneNumber = objDbModel.PhoneNumber;
                RegisterInfo.ProfileType = objDbModel.ProfileType;
                RegisterInfo.MemberActivationDate = objDbModel.MemberActivationDate.Value;
                RegisterInfo.Country = db.CountryLists.Where(x => x.CountryCode == objDbModel.Country).FirstOrDefault().CountryName;
                RegisterInfo.City = lang == "en" ? objDbModel.City : objDbModel.City_ar;
                RegisterInfo.ProfileId = objDbModel.ProfileId;
                RegisterInfo.ParentProfileId = objDbModel.ParentProfileId;
                RegisterInfo.MembershipType = objDbModel.MembershipType;
                RegisterInfo.IIAGlobalNumber = objDbModel.IIAGlobalNumber;
                RegisterInfo.MembershipStartDate = objDbModel.MembershipStartDate;
                RegisterInfo.MembershipEndDate = objDbModel.MembershipEndDate;
                RegisterInfo.MembershipType = objDbModel.MembershipType;
                RegisterInfo.MembershipBadge = objDbModel.MembershipBadge;

                var Invoice = db.ctInvoices.Where(x => x.ProfileId == ProfileId).ToList();
                foreach (var invoiceMain in Invoice)
                {
                    var InvoiceItem = db.ctInvoiceDetails.Where(x => x.InvoiceId == invoiceMain.InvoiceId).ToList();
                    foreach (var item in InvoiceItem)
                    {
                        InvoiceDetailsModel invoiceModel = new InvoiceDetailsModel();
                        if (item.ProductType == "C")
                        {
                            invoiceModel.CourseCmsName = Umbraco.TypedContent(item.CourseCmsId).GetPropertyValue("courseTitle").ToString();
                            invoiceModel.LevelCmsName = Umbraco.TypedContent(item.LevelCmsId).GetPropertyValue("courseLevel").ToString();
                            invoiceModel.StudyMethodCmsName = Umbraco.TypedContent(item.StudyMethodCmsId).GetPropertyValue("title").ToString();
                            invoiceModel.StartDate = item.StartDate;
                            invoiceModel.EndDate = item.EndDate;
                        }
                        else
                        {
                            invoiceModel.ProductCmsName = Umbraco.TypedContent(item.ProductCmsId).GetPropertyValue("title").ToString();
                            invoiceModel.ProductPicture = Umbraco.TypedContent(item.ProductCmsId).GetPropertyValue<IPublishedContent>("image").Url.ToString();

                        }

                        invoiceModel.Price = item.Price.Value;
                        invoiceModel.Discount = item.Discount.Value;
                        invoiceModel.Quantity = item.Quantity.Value;
                        invoiceModel.ProductType = item.ProductType;

                        PurchaseList.Add(invoiceModel);
                    }
                }

                model.RegisterInfo = RegisterInfo;
                model.PurchaseList = PurchaseList;

                return PartialView("Dashboard/EnrollUserProfile", model);
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }
        private bool canMemberApplyForCandidate(ctRegistration objDbModel)
        {
            DateTime monthAgoDate = DateTime.Today.AddMonths(-4);
            if (objDbModel.MembershipStartDate == null)
                return false;
            if (objDbModel.BanDate != null && DateTime.Today <= objDbModel.BanDate)
                return false;
            return objDbModel.MembershipStartDate <= monthAgoDate && objDbModel.MembershipType != "Students";
        }
        private List<ctElection> getOpenElectionsForApply()
        {
            return db.ctElections.Where(x => (DateTime.Today >= x.ApplyStartDate &&
                                              DateTime.Today <= x.ApplyEndDate &&
                                              x.Status == (int)ElectionStatus.Working)).ToList();

        }

        private bool canMemberApplyForVote(ctRegistration objDbModel)
        {
            return objDbModel.MembershipType != "Students";
        }

        [HttpPost]
        public ActionResult getOpenElectionsForVoting()
        {
            long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
            ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
            if (canMemberApplyForVote(objDbModel))
            {
                var data =  db.ctElections.Where(x => (DateTime.Today >= x.StartDate && DateTime.Today <= x.EndDate && 
                                                objDbModel.MembershipStartDate <= x.ApplyStartDate && x.Status == (int)ElectionStatus.Working))
                    .Select(x=> new {
                        x.Title,
                        x.TitleAr,
                        x.ElectionId
                    }).ToList();
                return Json(new
                {
                    elections = data
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    elections = new { },
                }, JsonRequestBehavior.AllowGet) ;
            }
            
            

        }

    }
}