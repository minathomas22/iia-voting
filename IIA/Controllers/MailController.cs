﻿using CMS.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace IIA.Controllers
{
    public class MailController : SurfaceController
    {
        // GET: Mail
        public ActionResult Index()
        {
            try
            {

                //EmailSettingModel eModel = Utility.GetEmailSetting("VottingMail", ctRegistration.ProfileId.ToString(), ctRegistration.lang);
                //string Message = EmailMessage.GetVoittingMailMeesage(ctRegistration.lang);
                //Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);
                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    error = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}