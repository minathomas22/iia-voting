﻿using System;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using IIA.DBContext;
using System.Linq.Dynamic;
using umbraco.cms.helpers;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using IIA.Models;
using CMS.Helpers;
using System.Data.Entity;
using Umbraco.Web;
using System.Web.Configuration;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using umbraco.editorControls.SettingControls.Pickers;
using Newtonsoft.Json;
using Umbraco.Core.Models;
using IIA.Helper;
using System.Data;
using System.Data.SqlClient;
using Umbraco.Web.Media.EmbedProviders;
using umbraco;
using System.Web.Mvc.Html;

namespace IIA.Controllers
{
    public class ElectionCommitteeController : SurfaceController
    {
        IIAContext db = new IIAContext();

        [HttpPost]
        public ActionResult GetAllActiveMembers()
        {
            try
            {
                var members = db.umbracoUsers.ToList();
                var model = members.Select(e => new
                {
                    MemberId = e.id,
                    MemberName = string.Concat(e.userName, " - ", e.userEmail)
                }).ToList();

                return Json(new
                {
                    status = true,
                    MembersInfo = model
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetAssignedMembersToElection(long electionId)
        {
            try
            {
                List<AdminUserViewModel> model = new List<AdminUserViewModel>();
                var memberIds = db.ctElectionCommittees.Where(e => e.ElectionId == electionId).Select(a => a.UserAdminId).ToList();
                if (memberIds.Any())
                {
                    var dbModel = db.umbracoUsers
                                     .Where(e => memberIds.Contains(e.id))
                                     .OrderBy(x => x.id).ToList();

                    foreach (var item in dbModel)
                    {
                        AdminUserViewModel regItem = new AdminUserViewModel();
                        regItem.AdminId = item.id;
                        regItem.UserName = item.userName;
                        regItem.Email = item.userEmail;
                        model.Add(regItem);
                    }
                }

                return Json(new
                {
                    status = true,
                    MemberInfo = model
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AssignElectionToMembers(ElectionCommitteeViewModel model)
        {
            try
            {
                var ctElectionCommitteeObj = new ctElectionCommittee();
                var dbAssignedMembersToElection = db.ctElectionCommittees.Where(e => e.ElectionId == model.ElectionId).ToList();
                var dbAssignedMembers = dbAssignedMembersToElection.Select(e => e.UserAdminId).ToList();
                var savedMembers = ConvertStringToListOfInt(model.MemberIds);
                var newMembers = savedMembers.Where(e => !dbAssignedMembers.Contains(e)).ToList();
                var deletedMembers = dbAssignedMembersToElection.Where(e => !savedMembers.Contains(e.UserAdminId)).ToList();
                var userGroupId = GetElectionCommitteUserGroup();
                // Add New Assigned Members To Election
                db.ctElectionCommittees.AddRange(newMembers.Select(e => new ctElectionCommittee()
                {
                    ElectionId = model.ElectionId,
                    UserAdminId = e,
                    CreatedOn = DateTime.Now,
                }));
                db.SaveChanges();

                // Update Deleted Assigned Members To Election
                foreach (var member in deletedMembers)
                {
                    member.UpdateOn = DateTime.Now;
                    db.Entry(member).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return Json(new
                {
                    data = model,
                    error = "",
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    data = model,
                    error = ex.Message,
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult RemoveMemberAssignedToElection(long memberId, long electionId)
        {
            try
            {
                var electionCommittee = db.ctElectionCommittees.FirstOrDefault(e => e.UserAdminId == memberId && e.ElectionId == electionId);
                if (electionCommittee != null)
                {
                    db.ctElectionCommittees.Remove(electionCommittee);
                    db.SaveChanges();
                }
                return Json(new
                {
                    model = electionCommittee,
                    data = new { electionId, memberId },
                    error = "",
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    data = new { electionId, memberId },
                    error = ex.Message,
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }


        public int GetElectionCommitteUserGroup()
        {
            var userGroup = db.umbracoUserGroups.FirstOrDefault(e => e.userGroupAlias == "electionCommittee");
            return userGroup.id;
        }

        public List<int> ConvertStringToListOfInt(string array)
        {
            var lst = array.Split(',').Select(e => int.Parse(e)).ToList();
            return lst;
        }

    }
}