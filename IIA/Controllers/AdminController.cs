﻿using System;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using IIA.DBContext;
using System.Linq.Dynamic;
using umbraco.cms.helpers;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using IIA.Models;
using CMS.Helpers;
using System.Data.Entity;
using Umbraco.Web;
using System.Web.Configuration;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using umbraco.editorControls.SettingControls.Pickers;
using Newtonsoft.Json;
using Umbraco.Core.Models;
using IIA.Helper;
using System.Data;
using System.Data.SqlClient;
using Umbraco.Web.Media.EmbedProviders;
using umbraco;

namespace IIA.Controllers
{
    public class AdminController : SurfaceController
    {
        IIAContext db = new IIAContext();
        [HttpPost]
        public ActionResult GetMemberList(bool status)

        {

            try
            {
                List<RegistrationViewModel> model = new List<RegistrationViewModel>();                
                var dbModel = db.ctRegistrations.Where(x => x.ProfileType == "Member" & x.IsActive == status).OrderByDescending(x=>x.ProfileId).ToList();                
                foreach (var item in dbModel)
                {
                    RegistrationViewModel regItem = new RegistrationViewModel();
                    regItem.ProfileId = item.ProfileId;
                    regItem.FirstName = item.FirstName;
                    regItem.MiddleName = !string.IsNullOrEmpty(item.MiddleName)?item.MiddleName:"";
                    regItem.LastName = item.LastName;
                    regItem.EmailAddress = item.EmailAddress;                                      
                    model.Add(regItem);
                }

                var jsonResult = Json(new
                {
                    status = true,
                    MemberInfo = model
                }, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);

                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;

                //return Json(new
                //{
                //    status = true,
                //    MemberInfo = model
                //}, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpPost]
        public ActionResult GetMemberByID(long profileId)
        {
            try
            {
                RegistrationViewModel model = new RegistrationViewModel();
                var objDbModel = db.ctRegistrations.Where(x => x.ProfileId == profileId).FirstOrDefault(); 
                model.IIAKSANumber = objDbModel.IIAKSANumber;
                model.IIAGlobalNumber = objDbModel.IIAGlobalNumber;
                model.MobileNumber = objDbModel.MobileNumber;
                model.Country = db.CountryLists.Where(x => x.CountryCode == objDbModel.Country).FirstOrDefault().CountryName;
                model.City = !string.IsNullOrEmpty(objDbModel.City) ? objDbModel.City : "";
                model.MembershipBadge = objDbModel.MembershipBadge;
                model.MembershipType = objDbModel.MembershipType;
                model.IsActive = objDbModel.IsActive.Value;
                model.Experience = objDbModel.Experience;
                model.Qualification = objDbModel.Qualification;
                model.Bio = objDbModel.Bio;
                model.FirstName = objDbModel.FirstName;
                model.MiddleName = !string.IsNullOrEmpty(objDbModel.MiddleName) ? objDbModel.MiddleName : "";
                model.LastName = objDbModel.LastName;
                model.EmailAddress = objDbModel.EmailAddress;
                

                return Json(new
                {
                    status = true,
                    MemberInfo = model
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ActiveInactive(long profileId, string status, string IIAGlobalNumber)
        {
            try
            {
                DateTime MembershipStartDate;
                DateTime MembershipEndDate;
                ctRegistration ctRegistration = db.ctRegistrations.Where(x => x.ProfileId == profileId).FirstOrDefault();
                if (bool.Parse(status) == true)
                {
                    if (ctRegistration.ParentProfileId == 0)
                    {
                        if (ctRegistration.MembershipStartDate.HasValue == false && ctRegistration.MembershipEndDate.HasValue == false)
                        {
                            MembershipStartDate = DateTime.Now;
                            MembershipEndDate = DateTime.Now.AddYears(1);
                        }
                        else
                        {
                            MembershipStartDate = ctRegistration.MembershipStartDate.Value;
                            MembershipEndDate = ctRegistration.MembershipEndDate.Value;
                        }                       
                    }
                    else
                    {
                        ctRegistration ctParentRegistration = db.ctRegistrations.Where(x => x.ProfileId == ctRegistration.ParentProfileId).FirstOrDefault();
                        MembershipStartDate = ctParentRegistration.MembershipStartDate.Value;
                        MembershipEndDate = ctParentRegistration.MembershipEndDate.Value;
                    }
                    ctRegistration.IsActive = true;
                    ctRegistration.MemberActivationDate = DateTime.Now;
                    ctRegistration.MembershipStartDate = MembershipStartDate;
                    ctRegistration.MembershipEndDate = MembershipEndDate;
                    ctRegistration.IIAGlobalNumber = IIAGlobalNumber;
                    ctRegistration.LastUpdated = DateTime.Now;
                    db.Entry(ctRegistration).State = EntityState.Modified;
                    db.SaveChanges();

                    EmailSettingModel eModel = Utility.GetEmailSetting("MemberActivation", ctRegistration.ProfileId.ToString(), ctRegistration.lang);

                    string Message = EmailMessage.GetRegistrationMessage(ctRegistration, "MemberActivation", "", ctRegistration.FirstName, ctRegistration.lang);

                    Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);
                }
                else
                {
                    ctRegistration.IsActive = false;
                    ctRegistration.LastUpdated = DateTime.Now;
                    db.Entry(ctRegistration).State = EntityState.Modified;
                    db.SaveChanges();
                }
               
                return Json(new
                {
                    status = true                    
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetNonMemberList(bool status)
        {

            try
            {
                List<RegistrationViewModel> model = new List<RegistrationViewModel>();
                var dbModel = db.ctRegistrations.Where(x => x.ProfileType == "NonMember" & x.IsActive == status).OrderByDescending(x => x.ProfileId).ToList();
                foreach (var item in dbModel)
                {
                    RegistrationViewModel regItem = new RegistrationViewModel();
                    regItem.ProfileId = item.ProfileId;
                    regItem.FirstName = item.FirstName;
                    regItem.MiddleName = !string.IsNullOrEmpty(item.MiddleName) ? item.MiddleName : "";
                    regItem.LastName = item.LastName;
                    regItem.EmailAddress = item.EmailAddress;
                    model.Add(regItem);
                }

                return Json(new
                {
                    status = true,
                    MemberInfo = model
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpPost]
        public ActionResult ActiveInactiveNonMember(long profileId, string status)
        {
            try
            {
                ctRegistration ctRegistration = db.ctRegistrations.Where(x => x.ProfileId == profileId).FirstOrDefault();
                if (bool.Parse(status) == true)
                {
                    ctRegistration.IsActive = true;
                    ctRegistration.MemberActivationDate = DateTime.Now;
                    ctRegistration.LastUpdated = DateTime.Now;
                    db.Entry(ctRegistration).State = EntityState.Modified;
                    db.SaveChanges();

                    EmailSettingModel eModel = Utility.GetEmailSetting("NonMemberActivation", ctRegistration.ProfileId.ToString(), ctRegistration.lang);

                    string Message = EmailMessage.GetRegistrationMessage(ctRegistration, "NonMemberActivation", "", ctRegistration.FirstName, ctRegistration.lang);

                    Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);
                }
                else
                {
                    ctRegistration.IsActive = false;
                    ctRegistration.LastUpdated = DateTime.Now;
                    db.Entry(ctRegistration).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetMentoringMemberList(bool status)
        {

            try
            {
                List<RegistrationViewModel> model = new List<RegistrationViewModel>();
                var dbModel = db.ctRegistrations.Where(x => x.ProfileType == "Member" & x.IsApproveMentor == status && x.IsApplyForMentor==true).OrderByDescending(x => x.ProfileId).ToList();
                foreach (var item in dbModel)
                {
                    RegistrationViewModel regItem = new RegistrationViewModel();
                    regItem.ProfileId = item.ProfileId;
                    regItem.FirstName = item.FirstName;
                    regItem.MiddleName = !string.IsNullOrEmpty(item.MiddleName) ? item.MiddleName : "";
                    regItem.LastName = item.LastName;
                    regItem.EmailAddress = item.EmailAddress;
                    model.Add(regItem);
                }

                return Json(new
                {
                    status = true,
                    MemberInfo = model
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpPost]
        public ActionResult ApproveUnApporveMentoring(long profileId, string status)
        {
            try
            {
                ctRegistration ctRegistration = db.ctRegistrations.Where(x => x.ProfileId == profileId).FirstOrDefault();
                if (bool.Parse(status) == true)
                {
                    ctRegistration.IsApproveMentor = true;                    
                    ctRegistration.LastUpdated = DateTime.Now;
                    db.Entry(ctRegistration).State = EntityState.Modified;
                    db.SaveChanges();

                    EmailSettingModel eModel = Utility.GetEmailSetting("ApproveMentoring", ctRegistration.ProfileId.ToString(), ctRegistration.lang);

                    string Message = EmailMessage.GetRegistrationMessage(ctRegistration, "ApproveMentoring", "", ctRegistration.FirstName, ctRegistration.lang);

                    Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);
                }
                else
                {
                    ctRegistration.IsApproveMentor = false;
                    ctRegistration.LastUpdated = DateTime.Now;
                    db.Entry(ctRegistration).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetCorporateList(string postType)
        {
            try
            {
                List<RegistrationViewModel> model = new List<RegistrationViewModel>();

                string conn = WebConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;  
                SqlConnection sqlConn = new SqlConnection(conn);
                sqlConn.Open();
                string sqlString = "spGetCorporateListForPost";
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = sqlConn;
                cmd.CommandText = sqlString;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("PostType", postType);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    RegistrationViewModel regItem = new RegistrationViewModel();
                    regItem.ProfileId = long.Parse(reader["ProfileId"].ToString());
                    regItem.FirstName = reader["FirstName"].ToString();
                    regItem.MiddleName = !string.IsNullOrEmpty(reader["MiddleName"].ToString()) ? reader["MiddleName"].ToString() : "";
                    regItem.LastName = reader["LastName"].ToString();
                    regItem.EmailAddress = reader["EmailAddress"].ToString();
                    model.Add(regItem);
                }
                reader.Close();
                sqlConn.Close();
                return Json(new
                {
                    status = true,
                    MemberInfo = model
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetPostList(bool status, string profileId, string postType)
        {
            try
            {
                List<JobInternshipViewModel> model = new List<JobInternshipViewModel>();
                var jobIntership = db.ctJobInternships.Where(x => x.IsApprove == status && x.PostType == postType).ToList();
                if (profileId != "")
                {
                    long ProfileID = long.Parse(profileId);
                    jobIntership = jobIntership.Where(x => x.ProfileId == ProfileID).ToList();
                }
                foreach(var item in jobIntership)
                {
                    JobInternshipViewModel itemModel = new JobInternshipViewModel();
                    itemModel.JobTitle = item.JobTitle;
                    itemModel.PostId = item.PostId;
                    model.Add(itemModel);
                }

                return Json(new
                {
                    status = true,
                    PostInfo = model
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GetPostInfo(long postId)
        {
            try
            {
                JobInternshipViewModel model = new JobInternshipViewModel();
                var objDbModel = db.ctJobInternships.Where(x => x.PostId == postId).FirstOrDefault();
                model.JobTitle = objDbModel.JobTitle;
                model.Location = objDbModel.Location;
                model.Company = objDbModel.Company;
                model.Tenure = objDbModel.Tenure;
                model.BackgroundImage = !string.IsNullOrEmpty(objDbModel.BackgroundImage)? "/Media/ApplicantFile/" + objDbModel.BackgroundImage:"";
                model.BannerImage = !string.IsNullOrEmpty(objDbModel.BannerImage) ? "/Media/ApplicantFile/"+objDbModel.BannerImage : "";
                model.JobDescription = objDbModel.JobDescription;
                model.JobResponsibilities = objDbModel.JobResponsibilities;

                return Json(new
                {
                    status = true,
                    PostInfo = model
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult ApproveUnapprovePost(long postId, string status)
        {
            try
            {
                ctJobInternship ctJobInternship = db.ctJobInternships.Where(x => x.PostId == postId).FirstOrDefault();
                if (bool.Parse(status) == true)
                {
                    ctJobInternship.IsApprove = true;
                    ctJobInternship.LastUpdated = DateTime.Now;
                    db.Entry(ctJobInternship).State = EntityState.Modified;
                    db.SaveChanges();                   
                }
                else
                {
                    ctJobInternship.IsApprove = false;
                    ctJobInternship.LastUpdated = DateTime.Now;
                    db.Entry(ctJobInternship).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SearchMember(string sFirstName_en, string sFirstName_ar, string sMiddleName_en, string sMiddleName_ar, string sLastName_en, string sLastName_ar, string sEmail, string sGender, string sCountry, string sCity_en, string sCity_ar, string sMobile, string sPhone, string sNationalId, string sMembershipType, string sMembershipBadge, string sIIAGlobalNumber, string sJobTitle_en, string sJobTitle_ar, string sOrganization_en, string sOrganization_ar, string sDOB, string sMembershipStartDate, string sMembershipEndDate)
        {
            try
            {
                List<AdminMemberSearchModel> model = new List<AdminMemberSearchModel>();
                var dbModel = db.ctRegistrations.Where(x => x.ProfileType == "Member").OrderBy(x=>x.FirstName).ToList();
                if (!string.IsNullOrEmpty(sFirstName_en))
                {
                    dbModel = dbModel.Where(x => x.FirstName?.ToLower().Contains(sFirstName_en.ToLower()) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sFirstName_ar))
                {
                    dbModel = dbModel.Where(x => x.FirstName_ar?.ToLower().Contains(sFirstName_ar.ToLower()) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sMiddleName_en))
                {
                    dbModel = dbModel.Where(x => x.MiddleName?.ToLower().Contains(sMiddleName_en.ToLower()) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sMiddleName_ar))
                {
                    dbModel = dbModel.Where(x => x.MiddleName_ar?.ToLower().Contains(sMiddleName_ar.ToLower()) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sLastName_en))
                {
                    dbModel = dbModel.Where(x => x.LastName?.ToLower().Contains(sLastName_en.ToLower()) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sLastName_ar))
                {
                    dbModel = dbModel.Where(x => x.LastName_ar?.ToLower().Contains(sLastName_ar.ToLower()) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sEmail))
                {
                    dbModel = dbModel.Where(x => x.EmailAddress?.ToLower().Contains(sEmail.ToLower()) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sGender))
                {
                    dbModel = dbModel.Where(x => x.Gender?.ToLower().Contains(sGender.ToLower()) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sCountry))
                {
                    dbModel = dbModel.Where(x => x.Country?.ToLower().Contains(sCountry.ToLower()) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sCity_en))
                {
                    dbModel = dbModel.Where(x => x.City?.ToLower().Contains(sCity_en.ToLower()) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sCity_ar))
                {
                    dbModel = dbModel.Where(x => x.City_ar?.ToLower().Contains(sCity_ar.ToLower()) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sMobile))
                {
                    dbModel = dbModel.Where(x => x.MobileNumber?.ToLower().Contains(sMobile.ToLower()) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sPhone))
                {
                    dbModel = dbModel.Where(x => x.PhoneNumber?.ToLower().Contains(sPhone.ToLower()) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sNationalId))
                {
                    dbModel = dbModel.Where(x => x.NationalIDNumber?.ToLower().Contains(sNationalId.ToLower()) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sMembershipType))
                {
                    dbModel = dbModel.Where(x => x.MembershipType?.ToLower().Contains(sMembershipType.ToLower()) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sMembershipBadge))
                {
                    dbModel = dbModel.Where(x => x.MembershipBadge?.ToLower().Contains(sMembershipBadge.ToLower()) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sIIAGlobalNumber))
                {
                    dbModel = dbModel.Where(x => x.IIAGlobalNumber?.ToLower().Contains(sIIAGlobalNumber.ToLower()) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sJobTitle_en))
                {
                    dbModel = dbModel.Where(x => x.JobTitle?.ToLower().Contains(sJobTitle_en.ToLower()) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sJobTitle_ar))
                {
                    dbModel = dbModel.Where(x => x.JobTitle_ar?.ToLower().Contains(sJobTitle_ar.ToLower()) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sOrganization_en))
                {
                    dbModel = dbModel.Where(x => x.Organization?.ToLower().Contains(sOrganization_en.ToLower()) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sOrganization_ar))
                {
                    dbModel = dbModel.Where(x => x.Organization_ar?.ToLower().Contains(sOrganization_ar.ToLower()) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sDOB))
                {
                    dbModel = dbModel.Where(x => x.DateOfBirth?.Equals(sDOB) ?? false).ToList();
                }
                if (!string.IsNullOrEmpty(sMembershipStartDate))
                {
                    DateTime startDate = DateTime.Parse(sMembershipStartDate);
                    dbModel = dbModel.Where(x => x.MembershipStartDate >= startDate).ToList();
                }
                if (!string.IsNullOrEmpty(sMembershipEndDate))
                {
                    DateTime endtDate = DateTime.Parse(sMembershipEndDate);
                    dbModel = dbModel.Where(x => x.MembershipEndDate <= endtDate).ToList();
                }
                foreach (var item in dbModel)
                {
                    AdminMemberSearchModel regItem = new AdminMemberSearchModel();
                    regItem.ProfileId = item.ProfileId;
                    regItem.FirstName = item.FirstName;
                    regItem.MiddleName = !string.IsNullOrEmpty(item.MiddleName) ? item.MiddleName : "";
                    regItem.LastName = item.LastName;
                    regItem.EmailAddress = item.EmailAddress;
                    regItem.MembershipType = !string.IsNullOrEmpty(item.MembershipBadge)? item.MembershipType+", "+item.MembershipBadge:item.MembershipType; 
                    regItem.MobileNumber = !string.IsNullOrEmpty(item.MobileNumber) ? item.MobileNumber : "";
                    regItem.Country = !string.IsNullOrEmpty(item.Country)? db.CountryLists.Where(x => x.CountryCode == item.Country).FirstOrDefault().CountryName:"";
                    regItem.City = !string.IsNullOrEmpty(item.City)?item.City:"";
                    regItem.MembershipStartDate = item.MembershipStartDate.HasValue?item.MembershipStartDate.Value.ToString("dd MMM yyyy"):"";
                    regItem.MembershipEndDate = item.MembershipEndDate.HasValue ? item.MembershipEndDate.Value.ToString("dd MMM yyyy") : "";
                    regItem.IsActive = item.IsActive.Value==true? "Yes" : "No";
                    model.Add(regItem);
                }

                return Json(new
                {
                    status = true,
                    MemberInfo = model
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }


        }
        [HttpPost]
        public ActionResult GetSearchMemberByID(long profileId)
        {
            try
            {
                RegistrationViewModel model = new RegistrationViewModel();
                var objDbModel = db.ctRegistrations.Where(x => x.ProfileId == profileId).FirstOrDefault();
                model.ProfileId = objDbModel.ProfileId;
                model.FirstName = objDbModel.FirstName;
                model.MiddleName = objDbModel.MiddleName;
                model.LastName = objDbModel.LastName;
                model.FirstName_ar = objDbModel.FirstName_ar;
                model.MiddleName_ar = objDbModel.MiddleName_ar;
                model.LastName_ar = objDbModel.LastName_ar;
                model.Gender = objDbModel.Gender;
                model.DateOfBirth = objDbModel.DateOfBirth;
                model.JobTitle = objDbModel.JobTitle;
                model.JobTitle_ar = objDbModel.JobTitle_ar;
                model.Organization = objDbModel.Organization;
                model.Organization_ar = objDbModel.Organization_ar;
                model.Country = objDbModel.Country;
                model.City = objDbModel.City;
                model.City_ar = objDbModel.City_ar;
                model.Zip_Postal_Code = objDbModel.Zip_Postal_Code;
                model.MembershipBadge = objDbModel.MembershipBadge;
                model.MembershipType = objDbModel.MembershipType;
                model.EmailAddress = objDbModel.EmailAddress;
                model.IIAKSANumber = objDbModel.IIAKSANumber;
                model.IIAGlobalNumber = objDbModel.IIAGlobalNumber;
                model.NationalIDNumber = objDbModel.NationalIDNumber;
                model.MobileNumber = objDbModel.MobileNumber;
                model.PhoneNumber = objDbModel.PhoneNumber;
                model.FaxNumber = objDbModel.FaxNumber;                             
                model.IsActive = objDbModel.IsActive.Value;
                model.Experience = objDbModel.Experience;
                model.Qualification = objDbModel.Qualification;
                model.Bio = objDbModel.Bio;
                model.Experience_ar = objDbModel.Experience_ar;
                model.Qualification_ar = objDbModel.Qualification_ar;
                model.Bio_ar = objDbModel.Bio_ar;
                model.strBanDate = objDbModel.BanDate.HasValue ? objDbModel.BanDate.Value.ToShortDateString() : "";
                model.strMembershipStartDate = objDbModel.MembershipStartDate.HasValue ? objDbModel.MembershipStartDate.Value.ToShortDateString() : "";
                model.strMembershipEndDate = objDbModel.MembershipEndDate.HasValue ? objDbModel.MembershipEndDate.Value.ToShortDateString() : "";
                return Json(new
                {
                    status = true,
                    MemberInfo = model
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateMemberByID(RegistrationViewModel model)
        {
            try
            {
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == model.ProfileId).FirstOrDefault();

                objDbModel.FirstName = model.FirstName;
                objDbModel.FirstName_ar = model.FirstName_ar;
                objDbModel.MiddleName = model.MiddleName;
                objDbModel.MiddleName_ar = model.MiddleName_ar;
                objDbModel.LastName = model.LastName;
                objDbModel.LastName_ar = model.LastName_ar;
                objDbModel.NationalIDNumber = model.NationalIDNumber;
                objDbModel.Gender = model.Gender;
                objDbModel.DateOfBirth = model.DateOfBirth;
                objDbModel.JobTitle = model.JobTitle;
                objDbModel.JobTitle_ar = model.JobTitle_ar;
                objDbModel.Organization = model.Organization;
                objDbModel.Organization_ar = model.Organization_ar;
                objDbModel.Country = model.Country;
                objDbModel.City = model.City;
                objDbModel.City_ar = model.City_ar;
                objDbModel.Zip_Postal_Code = model.Zip_Postal_Code;
                objDbModel.MobileNumber = model.MobileNumber;
                objDbModel.PhoneNumber = model.PhoneNumber;
                objDbModel.FaxNumber = model.FaxNumber;              
                if (objDbModel.EmailAddress != model.EmailAddress)
                {
                    objDbModel.EmailAddress = model.EmailAddress;
                }
                if (objDbModel.IIAGlobalNumber != model.IIAGlobalNumber)
                {
                    objDbModel.IIAGlobalNumber = model.IIAGlobalNumber;
                }
                if (objDbModel.MembershipStartDate != model.MembershipStartDate)
                {
                    objDbModel.MembershipStartDate = model.MembershipStartDate;
                }
                if (objDbModel.MembershipEndDate != model.MembershipEndDate)
                {
                    objDbModel.MembershipEndDate = model.MembershipEndDate;
                }
                if (objDbModel.BanDate != model.BanDate)
                {
                    objDbModel.BanDate = model.BanDate;
                }
                if (objDbModel.MembershipType != model.MembershipType || objDbModel.MembershipBadge != model.MembershipBadge)
                {
                    var TypeOfMembership = model.MembershipType == "Professional" ? "Corporate" : model.MembershipType;

                    objDbModel.MembershipType = model.MembershipType;
                    objDbModel.MembershipBadge = TypeOfMembership == "Corporate" ? model.MembershipBadge : "";                                      

                    var BecomeQualifiedPage = Umbraco.TypedContent(1810);
                    var MembershipType = BecomeQualifiedPage.Children.Where(x => x.GetPropertyValue("typeOfMembership").ToString() == TypeOfMembership).FirstOrDefault();

                    if (model.MembershipType == "Corporate" || model.MembershipType == "Professional")
                    {
                        if (model.MembershipBadge == "Bronze")
                        {
                            objDbModel.MembershipDiscount = int.Parse(MembershipType.GetPropertyValue("bronzeDiscount").ToString());
                        }
                        else if (model.MembershipBadge == "Silver")
                        {
                            objDbModel.MembershipDiscount = int.Parse(MembershipType.GetPropertyValue("silverDiscount").ToString());
                        }
                        else if (model.MembershipBadge == "Gold")
                        {
                            objDbModel.MembershipDiscount = int.Parse(MembershipType.GetPropertyValue("goldDiscount").ToString());
                        }
                    }
                    else
                    {
                        objDbModel.MembershipDiscount = int.Parse(MembershipType.GetPropertyValue("generalDiscount").ToString());
                    }
                }
                objDbModel.Qualification = model.Qualification;
                objDbModel.Qualification_ar = model.Qualification_ar;
                objDbModel.Bio = model.Bio;
                objDbModel.Bio_ar = model.Bio_ar;
                objDbModel.Experience = model.Experience;
                objDbModel.Experience_ar = model.Experience_ar;               
                objDbModel.LastUpdated = DateTime.Now;

                db.Entry(objDbModel).State = EntityState.Modified;
                db.SaveChanges();

                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message=ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateJobInternship(JobInternshipViewModel model)
        {
            try
            {
                ctJobInternship objDbModel = db.ctJobInternships.Where(x => x.PostId == model.PostId).FirstOrDefault();

                var BannerImage = Request.Files["BannerImage"];
                if (BannerImage != null)
                {
                    objDbModel.BannerImage = Utility.UploadFileInFolder(BannerImage);
                }

                var BackgroundImage = Request.Files["BackgroundImage"];
                if (BackgroundImage != null)
                {
                    objDbModel.BackgroundImage = Utility.UploadFileInFolder(BackgroundImage);
                }

                objDbModel.JobTitle = model.JobTitle;
                objDbModel.Company = model.Company;
                objDbModel.Location = model.Location;
                objDbModel.Tenure = model.Tenure;
                objDbModel.JobDescription = model.JobDescription;
                objDbModel.JobResponsibilities = model.JobResponsibilities;
                objDbModel.LastUpdated = DateTime.Now;

                db.Entry(objDbModel).State = EntityState.Modified;
                db.SaveChanges();

                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult DeleteMemberById(long profileId)
        {
            try
            {               
                var objDbModel = db.ctRegistrations.Where(x => x.ProfileId == profileId).FirstOrDefault();
                if (objDbModel != null)
                {
                    if ((db.ctRegistrationPayments.Where(x => x.ProfileId == objDbModel.ProfileId && x.TransactionStatus == "Success").Count() > 0) || (db.ctInvoices.Where(x => x.ProfileId == objDbModel.ProfileId && x.TransactionStatus == "Success").Count() > 0))
                    {
                        return Json(new
                        {
                            status = false,
                            message = "Sorry, this account can't be deleted as there is some order or membership payment history available in this account. Please contact with administrator."
                        }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        db.ctRegistrations.Remove(objDbModel);
                        db.SaveChanges();
                        return Json(new
                        {
                            status = true
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new
                    {
                        status = false,
                        message = "There is an error for delete this member."
                    }, JsonRequestBehavior.AllowGet);
                }
                
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message=ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult DeleteNonMemberById(long profileId)
        {
            try
            {
                var objDbModel = db.ctRegistrations.Where(x => x.ProfileId == profileId).FirstOrDefault();
                if (objDbModel != null)
                {
                    if (db.ctInvoices.Where(x => x.ProfileId == objDbModel.ProfileId && x.TransactionStatus == "Success").Count() > 0)
                    {
                        return Json(new
                        {
                            status = false,
                            message = "Sorry, this account can't be deleted as there is some order history available in this account. Please contact with administrator."
                        }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        db.ctRegistrations.Remove(objDbModel);
                        db.SaveChanges();
                        return Json(new
                        {
                            status = true
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new
                    {
                        status = false,
                        message = "There is an error for delete this member."
                    }, JsonRequestBehavior.AllowGet);
                }

            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult MemberBulkUpload(FormCollection form)
        {
            try
            {
                var EmailAddress = "";
                var ExcelFile = Request.Files["ExcelFile"];
                if (ExcelFile != null)
                {
                    string FileName = Utility.UploadExcelFileInFolder(ExcelFile);
                    string ImageFilePath = Server.MapPath("~/Media/ExcelFile/") + FileName;
                    DataTable dtTable = ExcelUploadHelper.ReadExcelFile(ImageFilePath, "Sheet1", "*", "MemberInfo", true);
                    int newRecord = 0;
                    int updateRecord = 0;
                    for (int i = 0; i < dtTable.Rows.Count; i++)
                    {                       
                        EmailAddress = dtTable.Rows[i][6].ToString().Trim();
                        if (!string.IsNullOrEmpty(EmailAddress))
                        {
                            if (db.ctRegistrations.Where(x => x.EmailAddress == EmailAddress).Count() <= 0)
                            {
                                ctRegistration model = new ctRegistration();
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][0].ToString()))
                                {
                                    model.FirstName = dtTable.Rows[i][0].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][1].ToString()))
                                {
                                    model.FirstName_ar = dtTable.Rows[i][1].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][2].ToString()))
                                {
                                    model.MiddleName = dtTable.Rows[i][2].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][3].ToString()))
                                {
                                    model.MiddleName_ar = dtTable.Rows[i][3].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][4].ToString()))
                                {
                                    model.LastName = dtTable.Rows[i][4].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][5].ToString()))
                                {
                                    model.LastName_ar = dtTable.Rows[i][5].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][6].ToString()))
                                {
                                    model.EmailAddress = dtTable.Rows[i][6].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][7].ToString()))
                                {
                                    model.MobileNumber = dtTable.Rows[i][7].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][8].ToString()))
                                {
                                    model.NationalIDNumber = dtTable.Rows[i][8].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][9].ToString()))
                                {
                                    model.IIAGlobalNumber = dtTable.Rows[i][9].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][10].ToString()))
                                {
                                    if (dtTable.Rows[i][10].ToString().Trim() == "Individual" || dtTable.Rows[i][10].ToString().Trim() == "individual" || dtTable.Rows[i][10].ToString().Trim() == "individuals")
                                    {
                                        model.MembershipType = "Individuals";
                                    }
                                    else if (dtTable.Rows[i][10].ToString().Trim() == "Student" || dtTable.Rows[i][10].ToString().Trim() == "student" || dtTable.Rows[i][10].ToString().Trim() == "students")
                                    {
                                        model.MembershipType = "Students";
                                    }
                                    else
                                    {
                                        model.MembershipType = dtTable.Rows[i][10].ToString().Trim();
                                    }
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][11].ToString()))
                                {
                                    model.MembershipStartDate = DateTime.Parse(dtTable.Rows[i][11].ToString());
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][12].ToString()))
                                {
                                    model.MembershipEndDate = DateTime.Parse(dtTable.Rows[i][12].ToString());
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][13].ToString()))
                                {
                                    model.JobTitle = dtTable.Rows[i][13].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][14].ToString()))
                                {
                                    model.JobTitle_ar = dtTable.Rows[i][14].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][15].ToString()))
                                {
                                    model.Organization = dtTable.Rows[i][15].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][16].ToString()))
                                {
                                    model.Organization_ar = dtTable.Rows[i][16].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][17].ToString()))
                                {
                                    model.City = dtTable.Rows[i][17].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][18].ToString()))
                                {
                                    model.City_ar = dtTable.Rows[i][18].ToString();
                                }
                                model.MemberActivationDate = model.MembershipStartDate;
                                model.ProfileType = "Member";
                                model.ParentProfileId = 0;
                                model.MembershipBadge = "";
                                model.IIAKSANumber = Utility.GenerateIIAKSANumber("Member");
                                model.IsActive = true;
                                model.IsEmailVerify = true;
                                model.IsApplyForMentor = false;
                                model.IsApproveMentor = false;
                                model.LastUpdated = DateTime.Now;
                                model.lang = "ar";
                                model.MembershipDiscount = 10;
                                model.Country = "SA";

                                db.ctRegistrations.Add(model);
                                db.SaveChanges();

                                newRecord = newRecord + 1;
                            }
                            else
                            {
                                ctRegistration model = db.ctRegistrations.Where(x => x.EmailAddress == EmailAddress).FirstOrDefault();
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][0].ToString()))
                                {
                                    model.FirstName = dtTable.Rows[i][0].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][1].ToString()))
                                {
                                    model.FirstName_ar = dtTable.Rows[i][1].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][2].ToString()))
                                {
                                    model.MiddleName = dtTable.Rows[i][2].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][3].ToString()))
                                {
                                    model.MiddleName_ar = dtTable.Rows[i][3].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][4].ToString()))
                                {
                                    model.LastName = dtTable.Rows[i][4].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][5].ToString()))
                                {
                                    model.LastName_ar = dtTable.Rows[i][5].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][6].ToString()))
                                {
                                    model.EmailAddress = dtTable.Rows[i][6].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][7].ToString()))
                                {
                                    model.MobileNumber = dtTable.Rows[i][7].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][8].ToString()))
                                {
                                    model.NationalIDNumber = dtTable.Rows[i][8].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][9].ToString()))
                                {
                                    model.IIAGlobalNumber = dtTable.Rows[i][9].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][10].ToString()))
                                {
                                    if (dtTable.Rows[i][10].ToString().Trim() == "Individual" || dtTable.Rows[i][10].ToString().Trim() == "individual" || dtTable.Rows[i][10].ToString().Trim() == "individuals")
                                    {
                                        model.MembershipType = "Individuals";
                                    }
                                    else if(dtTable.Rows[i][10].ToString().Trim() == "Student" || dtTable.Rows[i][10].ToString().Trim() == "student" || dtTable.Rows[i][10].ToString().Trim() == "students")
                                    {
                                        model.MembershipType = "Students";
                                    }
                                    else
                                    {
                                        model.MembershipType = dtTable.Rows[i][10].ToString().Trim();
                                    }
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][11].ToString()))
                                {
                                    model.MembershipStartDate = DateTime.Parse(dtTable.Rows[i][11].ToString());
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][12].ToString()))
                                {
                                    model.MembershipEndDate = DateTime.Parse(dtTable.Rows[i][12].ToString());
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][13].ToString()))
                                {
                                    model.JobTitle = dtTable.Rows[i][13].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][14].ToString()))
                                {
                                    model.JobTitle_ar = dtTable.Rows[i][14].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][15].ToString()))
                                {
                                    model.Organization = dtTable.Rows[i][15].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][16].ToString()))
                                {
                                    model.Organization_ar = dtTable.Rows[i][16].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][17].ToString()))
                                {
                                    model.City = dtTable.Rows[i][17].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][18].ToString()))
                                {
                                    model.City_ar = dtTable.Rows[i][18].ToString();
                                }
                                model.LastUpdated = DateTime.Now;

                                db.Entry(model).State = EntityState.Modified;
                                db.SaveChanges();

                                updateRecord = updateRecord + 1;
                            }
                        }                       
                    }
                    return Json(new
                    {
                        status = true,
                        message = "Total Record Inserted: " + newRecord.ToString() + ", Record Updated: " + updateRecord.ToString()
                    }, JsonRequestBehavior.AllowGet); ;
                }
                else
                {
                    return Json(new
                    {
                        status = false,
                        message = "Member Upload failed."
                    }, JsonRequestBehavior.AllowGet);
                }

               
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetCorporateAccount()
        {
            try
            {
                List<RegistrationViewModel> model = new List<RegistrationViewModel>();
                var member = db.ctRegistrations.Where(x => x.ProfileType == "Member" && x.MembershipType == "Corporate" && x.IsActive == true).ToList();
                foreach(var item in member)
                {
                    RegistrationViewModel modelItem = new RegistrationViewModel();
                    modelItem.ProfileId = item.ProfileId;
                    modelItem.FirstName = !string.IsNullOrEmpty(item.FirstName) ? item.FirstName : "";
                    modelItem.MiddleName = !string.IsNullOrEmpty(item.MiddleName)?item.MiddleName:"";
                    modelItem.LastName = !string.IsNullOrEmpty(item.LastName) ? item.LastName : "";
                    modelItem.EmailAddress = item.EmailAddress;
                    model.Add(modelItem);
                }
                return Json(new
                {
                    status = true,
                    MemberInfo = model
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddNewMember(RegistrationViewModel model)
        {
            try
            {
                if (db.ctRegistrations.Where(x => x.EmailAddress == model.EmailAddress).Count() <= 0)
                {
                    ctRegistration objDbModel = new ctRegistration();
                    objDbModel.FirstName = model.FirstName;
                    objDbModel.FirstName_ar = model.FirstName_ar;
                    objDbModel.MiddleName = model.MiddleName;
                    objDbModel.MiddleName_ar = model.MiddleName_ar;
                    objDbModel.LastName = model.LastName;
                    objDbModel.LastName_ar = model.LastName_ar;
                    objDbModel.NationalIDNumber = model.NationalIDNumber;
                    objDbModel.Gender = model.Gender;
                    objDbModel.DateOfBirth = model.DateOfBirth;
                    objDbModel.JobTitle = model.JobTitle;
                    objDbModel.JobTitle_ar = model.JobTitle_ar;
                    objDbModel.Organization = model.Organization;
                    objDbModel.Organization_ar = model.Organization_ar;
                    objDbModel.Country = model.Country;
                    objDbModel.City = model.City;
                    objDbModel.City_ar = model.City_ar;
                    objDbModel.Zip_Postal_Code = model.Zip_Postal_Code;
                    objDbModel.MobileNumber = model.MobileNumber;
                    objDbModel.PhoneNumber = model.PhoneNumber;
                    objDbModel.FaxNumber = model.FaxNumber;
                    objDbModel.EmailAddress = model.EmailAddress;
                    objDbModel.IIAGlobalNumber = model.IIAGlobalNumber;
                    objDbModel.MembershipStartDate = model.MembershipType == "Professional" ? db.ctRegistrations.Where(x => x.ProfileId == model.ParentProfileId).FirstOrDefault().MembershipStartDate : model.MembershipStartDate;
                    objDbModel.MembershipEndDate = model.MembershipType == "Professional" ? db.ctRegistrations.Where(x => x.ProfileId == model.ParentProfileId).FirstOrDefault().MembershipEndDate : model.MembershipEndDate;
                    objDbModel.MembershipType = model.MembershipType;
                    objDbModel.MembershipBadge = model.MembershipType == "Professional" ? db.ctRegistrations.Where(x => x.ProfileId == model.ParentProfileId).FirstOrDefault().MembershipBadge : model.MembershipBadge;
                    objDbModel.ParentProfileId = model.ParentProfileId;
                    objDbModel.Qualification = model.Qualification;
                    objDbModel.Qualification_ar = model.Qualification_ar;
                    objDbModel.Bio = model.Bio;
                    objDbModel.Bio_ar = model.Bio_ar;
                    objDbModel.Experience = model.Experience;
                    objDbModel.Experience_ar = model.Experience_ar;

                    var TypeOfMembership = model.MembershipType == "Professional" ? "Corporate" : model.MembershipType;
                    var BecomeQualifiedPage = Umbraco.TypedContent(1810);
                    var MembershipType = BecomeQualifiedPage.Children.Where(x => x.GetPropertyValue("typeOfMembership").ToString() == TypeOfMembership).FirstOrDefault();

                    if (model.MembershipType == "Corporate")
                    {
                        if (model.MembershipBadge == "Bronze")
                        {
                            objDbModel.MembershipDiscount = int.Parse(MembershipType.GetPropertyValue("bronzeDiscount").ToString());
                        }
                        else if (model.MembershipBadge == "Silver")
                        {
                            objDbModel.MembershipDiscount = int.Parse(MembershipType.GetPropertyValue("silverDiscount").ToString());
                        }
                        else if (model.MembershipBadge == "Gold")
                        {
                            objDbModel.MembershipDiscount = int.Parse(MembershipType.GetPropertyValue("goldDiscount").ToString());
                        }
                    }
                    else if(model.MembershipType == "Professional")
                    {
                        objDbModel.MembershipDiscount = db.ctRegistrations.Where(x => x.ProfileId == model.ParentProfileId).FirstOrDefault().MembershipDiscount;
                    }
                    else
                    {
                        objDbModel.MembershipDiscount = int.Parse(MembershipType.GetPropertyValue("generalDiscount").ToString());
                    }

                    objDbModel.MemberActivationDate = DateTime.Now;
                    objDbModel.ProfileType = "Member";
                    objDbModel.IIAKSANumber = Utility.GenerateIIAKSANumber("Member");
                    objDbModel.IsActive = true;
                    objDbModel.IsEmailVerify = true;
                    objDbModel.IsApplyForMentor = false;
                    objDbModel.IsApproveMentor = false;
                    objDbModel.lang = "ar";
                    objDbModel.LastUpdated = DateTime.Now;
                    db.ctRegistrations.Add(objDbModel);
                    db.SaveChanges();

                    return Json(new
                    {
                       status = true
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = false,
                        message = "Sorry, this email address already exists!"
                    }, JsonRequestBehavior.AllowGet);
                }
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        private string GetCorporateBadge(long parentProfileId)
        {
            try
            {
                var badge = "";
                badge = db.ctRegistrations.Where(x => x.ProfileId == parentProfileId).FirstOrDefault().MembershipBadge;
                return badge;
            }
            catch
            {
                return "";
            }
        }


        [HttpPost]
        public ActionResult CorporateMemberBulkUpload(FormCollection form)
        {
            try
            {
                var EmailAddress = "";
                var ExcelFile = Request.Files["ExcelFile"];
                var ParentProfileId = long.Parse(form["ParentProfileId"].ToString());
                if (ExcelFile != null)
                {
                    string FileName = Utility.UploadExcelFileInFolder(ExcelFile);
                    string ImageFilePath = Server.MapPath("~/Media/ExcelFile/") + FileName;
                    DataTable dtTable = ExcelUploadHelper.ReadExcelFile(ImageFilePath, "Sheet1", "*", "MemberInfo", true);
                    int newRecord = 0;
                    int updateRecord = 0;
                    for (int i = 0; i < dtTable.Rows.Count; i++)
                    {
                        EmailAddress = dtTable.Rows[i][6].ToString().Trim();
                        if (!string.IsNullOrEmpty(EmailAddress))
                        {
                            if (db.ctRegistrations.Where(x => x.EmailAddress == EmailAddress).Count() <= 0)
                            {
                                ctRegistration model = new ctRegistration();
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][0].ToString()))
                                {
                                    model.FirstName = dtTable.Rows[i][0].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][1].ToString()))
                                {
                                    model.FirstName_ar = dtTable.Rows[i][1].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][2].ToString()))
                                {
                                    model.MiddleName = dtTable.Rows[i][2].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][3].ToString()))
                                {
                                    model.MiddleName_ar = dtTable.Rows[i][3].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][4].ToString()))
                                {
                                    model.LastName = dtTable.Rows[i][4].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][5].ToString()))
                                {
                                    model.LastName_ar = dtTable.Rows[i][5].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][6].ToString()))
                                {
                                    model.EmailAddress = dtTable.Rows[i][6].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][7].ToString()))
                                {
                                    model.MobileNumber = dtTable.Rows[i][7].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][8].ToString()))
                                {
                                    model.NationalIDNumber = dtTable.Rows[i][8].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][9].ToString()))
                                {
                                    model.IIAGlobalNumber = dtTable.Rows[i][9].ToString();
                                }
                                model.MembershipType = "Professional";
                                model.MembershipStartDate = db.ctRegistrations.Where(x => x.ProfileId == ParentProfileId).FirstOrDefault().MembershipStartDate;
                                model.MembershipEndDate = db.ctRegistrations.Where(x => x.ProfileId == ParentProfileId).FirstOrDefault().MembershipEndDate;
                                model.MembershipBadge = db.ctRegistrations.Where(x => x.ProfileId == ParentProfileId).FirstOrDefault().MembershipBadge;
                                model.MembershipDiscount = db.ctRegistrations.Where(x => x.ProfileId == ParentProfileId).FirstOrDefault().MembershipDiscount;

                                if (!string.IsNullOrEmpty(dtTable.Rows[i][13].ToString()))
                                {
                                    model.JobTitle = dtTable.Rows[i][13].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][14].ToString()))
                                {
                                    model.JobTitle_ar = dtTable.Rows[i][14].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][15].ToString()))
                                {
                                    model.Organization = dtTable.Rows[i][15].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][16].ToString()))
                                {
                                    model.Organization_ar = dtTable.Rows[i][16].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][17].ToString()))
                                {
                                    model.City = dtTable.Rows[i][17].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][18].ToString()))
                                {
                                    model.City_ar = dtTable.Rows[i][18].ToString();
                                }
                                model.MemberActivationDate = DateTime.Now;
                                model.ProfileType = "Member";
                                model.ParentProfileId = ParentProfileId;                                
                                model.IIAKSANumber = Utility.GenerateIIAKSANumber("Member");
                                model.IsActive = true;
                                model.IsEmailVerify = true;
                                model.IsApplyForMentor = false;
                                model.IsApproveMentor = false;
                                model.LastUpdated = DateTime.Now;
                                model.lang = "ar";                                
                                model.Country = "SA";
                                db.ctRegistrations.Add(model);
                                db.SaveChanges();
                                newRecord = newRecord + 1;
                            }
                            else
                            {
                                ctRegistration model = db.ctRegistrations.Where(x => x.EmailAddress == EmailAddress).FirstOrDefault();
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][0].ToString()))
                                {
                                    model.FirstName = dtTable.Rows[i][0].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][1].ToString()))
                                {
                                    model.FirstName_ar = dtTable.Rows[i][1].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][2].ToString()))
                                {
                                    model.MiddleName = dtTable.Rows[i][2].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][3].ToString()))
                                {
                                    model.MiddleName_ar = dtTable.Rows[i][3].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][4].ToString()))
                                {
                                    model.LastName = dtTable.Rows[i][4].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][5].ToString()))
                                {
                                    model.LastName_ar = dtTable.Rows[i][5].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][6].ToString()))
                                {
                                    model.EmailAddress = dtTable.Rows[i][6].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][7].ToString()))
                                {
                                    model.MobileNumber = dtTable.Rows[i][7].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][8].ToString()))
                                {
                                    model.NationalIDNumber = dtTable.Rows[i][8].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][9].ToString()))
                                {
                                    model.IIAGlobalNumber = dtTable.Rows[i][9].ToString();
                                }

                                model.MembershipType = "Professional";
                                model.MembershipStartDate = db.ctRegistrations.Where(x => x.ProfileId == ParentProfileId).FirstOrDefault().MembershipStartDate;
                                model.MembershipEndDate = db.ctRegistrations.Where(x => x.ProfileId == ParentProfileId).FirstOrDefault().MembershipEndDate;
                                model.MembershipBadge = db.ctRegistrations.Where(x => x.ProfileId == ParentProfileId).FirstOrDefault().MembershipBadge;
                                model.MembershipDiscount = db.ctRegistrations.Where(x => x.ProfileId == ParentProfileId).FirstOrDefault().MembershipDiscount;

                                if (!string.IsNullOrEmpty(dtTable.Rows[i][13].ToString()))
                                {
                                    model.JobTitle = dtTable.Rows[i][13].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][14].ToString()))
                                {
                                    model.JobTitle_ar = dtTable.Rows[i][14].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][15].ToString()))
                                {
                                    model.Organization = dtTable.Rows[i][15].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][16].ToString()))
                                {
                                    model.Organization_ar = dtTable.Rows[i][16].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][17].ToString()))
                                {
                                    model.City = dtTable.Rows[i][17].ToString();
                                }
                                if (!string.IsNullOrEmpty(dtTable.Rows[i][18].ToString()))
                                {
                                    model.City_ar = dtTable.Rows[i][18].ToString();
                                }

                                model.ParentProfileId = ParentProfileId;
                                model.LastUpdated = DateTime.Now;

                                db.Entry(model).State = EntityState.Modified;
                                db.SaveChanges();

                                updateRecord = updateRecord + 1;
                            }
                        }
                    }
                    return Json(new
                    {
                        status = true,
                        message = "Total Record Inserted: " + newRecord.ToString() + ", Record Updated: " + updateRecord.ToString()
                    }, JsonRequestBehavior.AllowGet); ;
                }
                else
                {
                    return Json(new
                    {
                        status = false,
                        message = "Member Upload failed."
                    }, JsonRequestBehavior.AllowGet);
                }


            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}