﻿using System;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using IIA.DBContext;
using System.Linq.Dynamic;
using umbraco.cms.helpers;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using IIA.Models;
using CMS.Helpers;
using System.Data.Entity;
using Umbraco.Web;
using System.Web.Configuration;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using umbraco.editorControls.SettingControls.Pickers;
using Newtonsoft.Json;
using Umbraco.Core.Models;
using IIA.Helper;
using Umbraco.Web.Media.EmbedProviders;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using umbraco.MacroEngines;
using umbraco.presentation;
using System.Reflection;

namespace IIA.Controllers
{
    public class ExternalCourseController : SurfaceController
    {
        IIAContext db = new IIAContext();
        public string PaymentIntegrationMode = WebConfigurationManager.AppSettings["PaymentIntegrationMode"].ToString();
        public string PaymentIntegrationUrl = WebConfigurationManager.AppSettings["PaymentIntegrationUrl"].ToString();
        public string Currency = "SAR";
        public double CurrencyRate = 1;
        public int VAT = 15;

        // GET: ExternalCourse
        [HttpGet]
        public ActionResult ExternalCourse()
        {
            try
            {                
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();               
                if(objDbModel.ProfileType!="NonMember" && objDbModel.MembershipEndDate>=DateTime.Now)
                {
                    return PartialView("ExternalCourse/ExternalCourse");
                }
                else
                {
                    return PartialView("ExternalCourse/NotEligibleProgram");
                }
                
            }
            catch (Exception ex)
            {
                return PartialView("ExternalCourse/NotEligibleProgram");
            }

            return HttpNotFound();



        }

        [HttpPost]
        public ActionResult GetRegistrationInfo(int CourseCmsId, string lang)
        {
            try
            {
                ExternalCourseRegistrationModel model = new ExternalCourseRegistrationModel();
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                var CmsCourse = Umbraco.TypedContent(CourseCmsId);
                var CourseCode= CmsCourse.GetPropertyValue("currentRegistrationCode").ToString();
                ctExternalCourseRegistration objDbModel = db.ctExternalCourseRegistrations.Where(x => x.ProfileId == ProfileId && x.CourseCode == CourseCode).FirstOrDefault();
                if (objDbModel != null)
                {
                    model.RegId = objDbModel.RegId;
                    model.ProfileId = objDbModel.ProfileId;
                    model.CourseCode = objDbModel.CourseCode;
                    model.EnglishSpokenPaymentReceipt = !string.IsNullOrEmpty(objDbModel.EnglishSpokenPaymentReceipt) ? objDbModel.EnglishSpokenPaymentReceipt : "";
                    model.EnglishSpokenResult = !string.IsNullOrEmpty(objDbModel.EnglishSpokenResult) ? objDbModel.EnglishSpokenResult : "";
                    model.EnglishSpokenResultScore = !string.IsNullOrEmpty(objDbModel.EnglishSpokenResultScore) ? objDbModel.EnglishSpokenResultScore : "";
                    model.IsEnglishSpokenPass = !string.IsNullOrEmpty(objDbModel.IsEnglishSpokenPass) ? objDbModel.IsEnglishSpokenPass : "";
                    model.strInterviewDate = objDbModel.InterviewDate.HasValue?objDbModel.InterviewDate.Value.ToString("dd MMM yyyy"):"";
                    model.IsInterviewPass = !string.IsNullOrEmpty(objDbModel.IsInterviewPass) ? objDbModel.IsInterviewPass : "";
                    model.IsSeatConfirmed = !string.IsNullOrEmpty(objDbModel.IsSeatConfirmed) ? objDbModel.IsSeatConfirmed : "";
                    model.IsPaymentSuccess = !string.IsNullOrEmpty(objDbModel.IsPaymentSuccess) ? objDbModel.IsPaymentSuccess : "";
                    model.IsApplicationFeePaid = !string.IsNullOrEmpty(objDbModel.IsApplicationFeePaid) ? objDbModel.IsApplicationFeePaid : "";
                    return Json(new
                    {
                        status = true,
                        regInfo = model
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
                
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GetMemberAdditionInfo(string lang)
        {
            try
            {
                ExternalCourseMemberInfoModel model = new ExternalCourseMemberInfoModel();
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);                
                ctExternalCourseMemberInfo objDbModel = db.ctExternalCourseMemberInfoes.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                if (objDbModel != null)
                {                    
                    model.ProfileId = objDbModel.ProfileId;
                    model.Education = objDbModel.Education;
                    model.EducationCertificate = objDbModel.EducationCertificate;
                    model.CIAHolder = objDbModel.CIAHolder;
                    model.CIAHolderCertificate = objDbModel.CIAHolderCertificate;
                    model.EnglishProficiency = objDbModel.EnglishProficiency;
                    model.HoldEnglishProficiency = objDbModel.HoldEnglishProficiency;
                    model.EnglishProficiencyCertificate = objDbModel.EnglishProficiencyCertificate;
                    model.OtherProfessionalCertificates = objDbModel.OtherProfessionalCertificates;
                                       
                    return Json(new
                    {
                        status = true,
                        memberInfo = model
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }

            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GetMemberExistingInfo(string lang)
        {
            try
            {
                RegistrationViewModel model = new RegistrationViewModel();
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                if (objDbModel != null)
                {
                    model.FirstName = objDbModel.FirstName;
                    model.FirstName_ar = objDbModel.FirstName_ar;
                    model.MiddleName = objDbModel.MiddleName;
                    model.MiddleName_ar = objDbModel.MiddleName_ar;
                    model.LastName = objDbModel.LastName;
                    model.LastName_ar = objDbModel.LastName_ar;
                    model.EmailAddress = objDbModel.EmailAddress;
                    model.IIAGlobalNumber = objDbModel.IIAGlobalNumber;
                    model.NationalIDNumber = objDbModel.NationalIDNumber;
                    model.MobileNumber = objDbModel.MobileNumber;
                    model.JobTitle = objDbModel.JobTitle;
                    model.JobTitle_ar = objDbModel.JobTitle_ar;
                    model.Organization = objDbModel.Organization;
                    model.Organization_ar = objDbModel.Organization_ar;
                    model.Experience = objDbModel.Experience;
                    model.Experience_ar = objDbModel.Experience_ar;

                    if (!string.IsNullOrEmpty(objDbModel.ProfilePhoto))
                    {
                        model.ProfilePhoto = "/Media/ApplicantFile/" + objDbModel.ProfilePhoto;
                    }
                    else
                    {
                        model.ProfilePhoto = "/ui/media/images/avater.jpg";
                    }

                    return Json(new
                    {
                        status = true,
                        memberInfo = model
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }

            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GetRegistrationCountByCourse(int CourseCmsId, string lang)
        {
            try
            {               
                
                var CmsCourse = Umbraco.TypedContent(CourseCmsId);
                var CourseCode = CmsCourse.GetPropertyValue("currentRegistrationCode").ToString();
                int NoOfSeat = CmsCourse.GetPropertyValue<int>("noOfSeat");
                if (NoOfSeat > db.ctExternalCourseRegistrations.Where(x => x.CourseCode == CourseCode && x.IsPaymentSuccess == "Y").Count())
                {
                    return Json(new
                    {
                        status = true,
                        count = (NoOfSeat - db.ctExternalCourseRegistrations.Where(x => x.CourseCode == CourseCode && x.IsPaymentSuccess == "Y").Count())
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GetRegistrationCountByRegId(string lang)
        {
            try
            {
                long RegId = long.Parse(Utility.GetApplicationCookies("regId").Value);              
               
                ctExternalCourseRegistration ctExternalCourseRegistration = db.ctExternalCourseRegistrations.Where(x => x.RegId == RegId).FirstOrDefault();

                var CourseInfo = Umbraco.TypedContent(ctExternalCourseRegistration.CourseCmsId);
                int NoOfSeat = CourseInfo.GetPropertyValue<int>("noOfSeat");
                
                if (NoOfSeat > db.ctExternalCourseRegistrations.Where(x => x.CourseCode == ctExternalCourseRegistration.CourseCode && x.IsPaymentSuccess == "Y").Count())
                {
                    return Json(new
                    {
                        status = true,
                        count = db.ctExternalCourseRegistrations.Where(x => x.CourseCode == ctExternalCourseRegistration.CourseCode && x.IsPaymentSuccess == "Y").Count()
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult ProcessedApplicationFee(int CourseCmsId, string lang)
        {
            try
            {
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                var CmsCourse = Umbraco.TypedContent(CourseCmsId);
                var CourseCmsTitle = CmsCourse.GetPropertyValue("title").ToString();
                var CourseCode = CmsCourse.GetPropertyValue("currentRegistrationCode").ToString();
                var RegInfo = db.ctExternalCourseRegistrations.Where(x => x.ProfileId == ProfileId && x.CourseCode == CourseCode).FirstOrDefault();
                if (RegInfo == null)
                {
                    ctExternalCourseRegistration obj = new ctExternalCourseRegistration();
                    obj.ProfileId = ProfileId;
                    obj.CourseCmsId = CourseCmsId;
                    obj.CourseCmsTitle = CourseCmsTitle;
                    obj.CourseCode = CourseCode;
                    obj.LastUpdated = DateTime.Now;
                    db.ctExternalCourseRegistrations.Add(obj);
                    db.SaveChanges();
                    Utility.SetApplicationCookies("regId", obj.RegId.ToString());
                }              
                             
                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult UploadPaymentReceipt(int CourseCmsId, string lang)
        {
            try
            {
               // long RegId = long.Parse(Utility.GetApplicationCookies("regId").Value);
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);

                ctExternalCourseRegistration model = new ctExternalCourseRegistration();

                var PaymentReceipt = Request.Files["PaymentReceipt"];
                if (PaymentReceipt != null)
                {
                    model.EnglishSpokenPaymentReceipt = Utility.UploadFileInFolder(PaymentReceipt);
                }

                var Result = Request.Files["Result"];
                if (Result != null)
                {
                    model.EnglishSpokenResult = Utility.UploadFileInFolder(Result);
                }

                model.CourseCmsId = CourseCmsId;

                var CmsCourse = Umbraco.TypedContent(CourseCmsId);

                model.CourseCmsTitle = CmsCourse.GetPropertyValue("title").ToString();
                model.CourseCode = CmsCourse.GetPropertyValue("currentRegistrationCode").ToString();
                model.ProfileId = ProfileId;
                model.LastUpdated = DateTime.Now;

                db.ctExternalCourseRegistrations.Add(model);
                db.SaveChanges();

                EmailSettingModel eModel = Utility.GetExternalCourseEmailSetting("UploadReceipt", ProfileId, lang);

                string Message = EmailMessage.GetExternalCourseMessage("UploadReceipt", model.RegId, lang);

                Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);

                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult UpdateInterviewDate(long RegId, string InterviewDate, string InterviewTime, string lang)
        {
            try
            {
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                ctExternalCourseRegistration model = db.ctExternalCourseRegistrations.Where(x => x.RegId == RegId).FirstOrDefault();
                model.InterviewDate = DateTime.Parse(InterviewDate);
                model.InterviewTime = InterviewTime;
                model.LastUpdated = DateTime.Now;

                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                EmailSettingModel eModel = Utility.GetExternalCourseEmailSetting("InterviewDate", ProfileId, lang);

                string Message = EmailMessage.GetExternalCourseMessage("InterviewDate", model.RegId, lang);

                Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);

                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public ActionResult ExternalCourseCheckout()
        {
            try
            {
                long profileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                long regId = long.Parse(Utility.GetApplicationCookies("regId").Value);

                ExternalCoursePaymentModel model = new ExternalCoursePaymentModel();
                var regData = db.ctExternalCourseRegistrations.Where(x => x.ProfileId == profileId && x.RegId==regId).FirstOrDefault();
                var CourseData = Umbraco.TypedContent(regData.CourseCmsId);

                if (regData.IsInterviewPass == "Y")
                {
                    model.SubTotal = decimal.Parse(CourseData.GetPropertyValue("courseFee").ToString());
                    model.CourseName = CourseData.GetPropertyValue("title").ToString();
                }
                else
                {
                    model.SubTotal = decimal.Parse(CourseData.GetPropertyValue("applicationFee").ToString());
                    model.CourseName = CourseData.GetPropertyValue("applicationFeeTitle").ToString();
                }
                model.Shipping = 0;
                model.Discount = 0;
                model.VAT = ((model.SubTotal + model.Shipping - model.Discount) * VAT) / 100;
                model.GrandTotal = (model.SubTotal + model.VAT + model.Shipping - model.Discount);

                return PartialView("ExternalCourse/CourseCheckout", model);
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult ExternalCourseCheckout(ExternalCoursePaymentModel model)
        {
            try
            {
                long profileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                long regId = long.Parse(Utility.GetApplicationCookies("regId").Value);
                ctExternalCoursePayment dbModel = new ctExternalCoursePayment();

                var regData = db.ctExternalCourseRegistrations.Where(x => x.ProfileId == profileId && x.RegId == regId).FirstOrDefault();
                var CourseData = Umbraco.TypedContent(regData.CourseCmsId);

                if (regData.IsInterviewPass == "Y")
                {
                    dbModel.SubTotal = decimal.Parse(CourseData.GetPropertyValue("courseFee").ToString());
                    dbModel.PaymentType = "CourseFee";
                }
                else
                {
                    dbModel.SubTotal = decimal.Parse(CourseData.GetPropertyValue("applicationFee").ToString());
                    dbModel.PaymentType = "ApplicationFee";
                }
                
                dbModel.Shipping = 0;
                dbModel.Discount = 0;
                dbModel.VAT = ((dbModel.SubTotal + dbModel.Shipping - dbModel.Discount) * VAT) / 100;
                dbModel.GrandTotal = (dbModel.SubTotal + dbModel.VAT + dbModel.Shipping - dbModel.Discount);
                dbModel.InvoiceNumber = Utility.GenerateExternalCourseInvoiceNumber();
                dbModel.ProfileId = profileId;
                dbModel.RegId = regId;
                dbModel.TransactionAmount = decimal.Parse((double.Parse(dbModel.GrandTotal.ToString()) * CurrencyRate).ToString());
                dbModel.TransactionCurrency = Currency;
                dbModel.PaymentMethod = model.PaymentMethod;
                dbModel.TransactionDate = DateTime.Now;
                dbModel.PromoCode = "";
                db.ctExternalCoursePayments.Add(dbModel);
                db.SaveChanges();
                Utility.SetApplicationCookies("ecpayId", dbModel.ExternalPaymentId.ToString());

                if (model.lang == "en")
                {
                    return RedirectToUmbracoPage(PaymentIntegrationMode == "Test" ? 3500 : 3678);
                }
                else
                {
                    return RedirectToUmbracoPage(PaymentIntegrationMode == "Test" ? 3508 : 3690);
                }                
            }
            catch (Exception ex)
            {
                if (model.lang == "en")
                {                   
                   return RedirectToUmbracoPage(1866);
                }
                else
                {
                    return RedirectToUmbracoPage(2734);
                }
            }
            
        }

        [HttpGet]
        public ActionResult ExternalCoursePayment()
        {
            try
            {
                string CheckoutId = "";
                string lang = Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString());
                PaymentGateayStep1Model model = new PaymentGateayStep1Model();

                long PaymentId = long.Parse(Utility.GetApplicationCookies("ecpayId").Value);
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                long regId = long.Parse(Utility.GetApplicationCookies("regId").Value);

                var MemberInfo = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                var ShippingBilling = db.ctShippingBillings.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                var PaymentInfo = db.ctExternalCoursePayments.Where(x => x.ExternalPaymentId == PaymentId).FirstOrDefault();
                var PaymentGatewayInfo = db.ctPaymentGatewayInfoes.Where(x => x.IntegrationMode == PaymentIntegrationMode).FirstOrDefault();
                var entityId = "";
                if (PaymentInfo.PaymentMethod == "VISA")
                {
                    entityId = PaymentGatewayInfo.VIsaEntityId;
                }
                else if (PaymentInfo.PaymentMethod == "MASTER")
                {
                    entityId = PaymentGatewayInfo.MasterEntityId;
                }
                else if (PaymentInfo.PaymentMethod == "MADA")
                {
                    entityId = PaymentGatewayInfo.MadaEntityId;
                }

                if (ShippingBilling != null)
                {
                    CheckoutId = Utility.PaymentGatewayGetCheckoutId(entityId, PaymentInfo.TransactionAmount.Value, PaymentInfo.TransactionCurrency, "DB", "EXTERNAL", PaymentInfo.InvoiceNumber, ShippingBilling.BEmailAddress, ShippingBilling.BAddress1, ShippingBilling.BCity, ShippingBilling.BArea, ShippingBilling.BCountry, MemberInfo.Zip_Postal_Code, ShippingBilling.BFirstName, ShippingBilling.BLastName, PaymentGatewayInfo.BearerToken, PaymentInfo.PaymentMethod);
                }
                else
                {
                    CheckoutId = Utility.PaymentGatewayGetCheckoutId(entityId, PaymentInfo.TransactionAmount.Value, PaymentInfo.TransactionCurrency, "DB", "EXTERNAL", PaymentInfo.InvoiceNumber, MemberInfo.EmailAddress, MemberInfo.City, MemberInfo.City, MemberInfo.City, MemberInfo.Country, MemberInfo.Zip_Postal_Code, MemberInfo.FirstName, MemberInfo.LastName, PaymentGatewayInfo.BearerToken, PaymentInfo.PaymentMethod);
                }
                model.CheckoutId = CheckoutId;
                model.TransactionAmount = PaymentInfo.TransactionCurrency + " " + string.Format("{0:n2}", PaymentInfo.TransactionAmount.Value);
                model.PaymentMethod = PaymentInfo.PaymentMethod;
                model.PaymentUrl = PaymentIntegrationUrl + "v1/paymentWidgets.js?checkoutId=" + model.CheckoutId;
                model.RedirectUrl = WebConfigurationManager.AppSettings["WebUrl"].ToString() + lang + "/my-account/course-payment-status/";

                return PartialView("ExternalCourse/CoursePayment", model);
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult ExternalCourseSuccess()
        {
            try
            {

                ExternalCoursePaymentModel model = new ExternalCoursePaymentModel();
                long PaymentId = long.Parse(Utility.GetApplicationCookies("ecpayId").Value);
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                long RegId = long.Parse(Utility.GetApplicationCookies("regId").Value);

                ctExternalCoursePayment ctExternalCoursePayment = db.ctExternalCoursePayments.Where(x => x.ExternalPaymentId == PaymentId).FirstOrDefault();
                ctRegistration objDbRegistration = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                ctExternalCourseRegistration ctExternalCourseRegistration = db.ctExternalCourseRegistrations.Where(x => x.RegId == RegId).FirstOrDefault();

                if (ctExternalCoursePayment.TransactionStatus == "Success")
                {
                    var CourseInfo = Umbraco.TypedContent(ctExternalCourseRegistration.CourseCmsId);
                    int NoOfSeat = CourseInfo.GetPropertyValue<int>("noOfSeat");

                    if (ctExternalCourseRegistration.IsInterviewPass == "Y")
                    {
                        model.CourseName = ctExternalCourseRegistration.CourseCmsTitle;
                    }
                    else
                    {                       
                        model.CourseName = CourseInfo.GetPropertyValue("applicationFeeTitle").ToString();
                    }

                   
                    model.InvoiceNumber = ctExternalCoursePayment.InvoiceNumber;
                    model.SubTotal = ctExternalCoursePayment.SubTotal.Value;
                    model.Shipping = ctExternalCoursePayment.Shipping.Value;
                    model.VAT = ctExternalCoursePayment.VAT.Value;
                    model.Discount = ctExternalCoursePayment.Discount.Value;
                    model.GrandTotal = ctExternalCoursePayment.GrandTotal.Value;
                    model.TransactionAmount = ctExternalCoursePayment.TransactionAmount.Value;
                    model.TransactionCurrency = ctExternalCoursePayment.TransactionCurrency;

                    if (ctExternalCourseRegistration.IsInterviewPass == "Y")
                    {
                        ctExternalCourseRegistration.IsSeatConfirmed = NoOfSeat > db.ctExternalCourseRegistrations.Where(x => x.CourseCode == ctExternalCourseRegistration.CourseCode && x.IsPaymentSuccess == "Y").Count() ? "Y" : "N";
                        ctExternalCourseRegistration.IsPaymentSuccess = "Y";                        
                        db.Entry(ctExternalCourseRegistration).State = EntityState.Modified;
                        db.SaveChanges();

                        if (ctExternalCourseRegistration.IsSeatConfirmed == "Y")
                        {
                            model.SeatConfirmMessage = CourseInfo.GetPropertyValue("seatConfirmedMessage").ToString();
                        }
                        else
                        {
                            model.SeatConfirmMessage = CourseInfo.GetPropertyValue("seatFullOccupiedMessage").ToString();
                        }
                    } 
                    
                    else
                    {                        
                        ctExternalCourseRegistration.IsApplicationFeePaid = "Y";
                        db.Entry(ctExternalCourseRegistration).State = EntityState.Modified;
                        db.SaveChanges();                        
                    }

                    string EmailSubjectType = "";
                    if (ctExternalCourseRegistration.IsInterviewPass == "Y")
                    {
                        EmailSubjectType = "CoursePayment";
                    }
                    else
                    {
                        EmailSubjectType = "CourseApplicationFeePayment";
                    }

                    EmailSettingModel eModel = Utility.GetExternalCourseEmailSetting(EmailSubjectType, ProfileId, Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString()));

                    string Message = EmailMessage.GetExternalCourseMessage("CoursePayment", RegId, Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString()));

                    Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);

                    Utility.ClearExternalCourseCookies();

                    return PartialView("ExternalCourse/CoursePaymentSuccess", model);
                }
                else
                {
                    return HttpNotFound();
                }
            }
            catch
            {
                return HttpNotFound();
            }
        }

        [HttpGet]
        public ActionResult ExternalCourseFailed()
        {
            return PartialView("ExternalCourse/CoursePaymentFailed");
        }

        [HttpPost]
        public ActionResult UpdateMemberInfo(MemberCourseRegistrationModel model, IEnumerable<HttpPostedFileBase> OtherProfessionalCertificates)
        {
            try
            {
                string strOtherProfessionalCertificates = "";

                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                ctRegistration ctRegistration = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                ctRegistration.FirstName = model.FirstName;
                ctRegistration.FirstName_ar = model.FirstName_ar;
                ctRegistration.MiddleName = model.MiddleName;
                ctRegistration.MiddleName_ar = model.MiddleName_ar;
                ctRegistration.LastName = model.LastName;
                ctRegistration.LastName_ar = model.LastName_ar;
                ctRegistration.JobTitle = model.JobTitle;
                ctRegistration.JobTitle_ar = model.JobTitle_ar;
                ctRegistration.Organization = model.Organization;
                ctRegistration.Organization_ar = model.Organization_ar;
                ctRegistration.Experience = model.Experience;
                ctRegistration.Experience_ar = model.Experience_ar;
                ctRegistration.NationalIDNumber = model.NationalIDNumber;
                ctRegistration.MobileNumber = model.MobileNumber;
                ctRegistration.LastUpdated = DateTime.Now;

                if (Request.Files["ProfilePhoto"] != null)
                {
                    ctRegistration.ProfilePhoto = Utility.UploadFileInFolder(Request.Files["ProfilePhoto"]);
                }

                db.Entry(ctRegistration).State = EntityState.Modified;
                db.SaveChanges();

                ctExternalCourseMemberInfo ctExternalCourseMemberInfo = db.ctExternalCourseMemberInfoes.Where(x => x.ProfileId == ProfileId).Count() > 0 ? db.ctExternalCourseMemberInfoes.Where(x => x.ProfileId == ProfileId).FirstOrDefault() : new ctExternalCourseMemberInfo();

                ctExternalCourseMemberInfo.ProfileId = ProfileId;
                ctExternalCourseMemberInfo.Education = model.Education;
                ctExternalCourseMemberInfo.CIAHolder = model.CIAHolder;
                ctExternalCourseMemberInfo.EnglishProficiency = model.EnglishProficiency;
                ctExternalCourseMemberInfo.HoldEnglishProficiency = model.HoldEnglishProficiency;
                ctExternalCourseMemberInfo.LastUpdated = DateTime.Now;
                if (Request.Files["EducationCertificate"] != null)
                {
                    ctExternalCourseMemberInfo.EducationCertificate = Utility.UploadFileInFolder(Request.Files["EducationCertificate"]);
                }
                if (Request.Files["CIAHolderCertificate"] != null)
                {
                    ctExternalCourseMemberInfo.CIAHolderCertificate = Utility.UploadFileInFolder(Request.Files["CIAHolderCertificate"]);
                }
                if (Request.Files["EnglishProficiencyCertificate"] != null)
                {
                    ctExternalCourseMemberInfo.EnglishProficiencyCertificate = Utility.UploadFileInFolder(Request.Files["EnglishProficiencyCertificate"]);
                }
                               

                //for (int i = 0; i < Request.Files.Count; i++)
                //{
                //    HttpPostedFileBase postedFile = Request.Files[i];
                //    if (postedFile != null)
                //    {
                //        string FileName = Utility.UploadFileInFolder(postedFile);
                //        strOtherProfessionalCertificates += FileName + ",";
                //    }
                //}



                if (OtherProfessionalCertificates != null)
                {
                    //IEnumerable<HttpPostedFileBase> OtherProfessionalCertificates = (IEnumerable<HttpPostedFileBase>)Request.Files["OtherProfessionalCertificates[]"];
                    foreach (HttpPostedFileBase file in OtherProfessionalCertificates)
                    {
                        if (file != null)
                        {
                            string FileName = Utility.UploadFileInFolder(file);
                            strOtherProfessionalCertificates += FileName + ",";
                        }
                    }
                }

                ctExternalCourseMemberInfo.OtherProfessionalCertificates = !string.IsNullOrEmpty(strOtherProfessionalCertificates)?strOtherProfessionalCertificates.Remove(strOtherProfessionalCertificates.Length-1, 1): strOtherProfessionalCertificates;


                if (db.ctExternalCourseMemberInfoes.Where(x => x.ProfileId == ProfileId).Count() > 0)
                {
                    db.Entry(ctExternalCourseMemberInfo).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    db.ctExternalCourseMemberInfoes.Add(ctExternalCourseMemberInfo);
                    db.SaveChanges();
                }


                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GetAvailableDate(int CourseCmsId, string lang)
        {
            try
            {
                List<InterviewDateSchedule> model = new List<InterviewDateSchedule>();
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                var CmsCourse = Umbraco.TypedContent(CourseCmsId);
                var CourseCode = CmsCourse.GetPropertyValue("currentRegistrationCode").ToString();
                var settingId = PaymentIntegrationMode == "Test" ? 3537 : 3654;
                var SettingContent = Umbraco.TypedContent(settingId);
                var InterviewSchedule = SettingContent.Children.Where(x => x.IsVisible() && x.DocumentTypeAlias == "interviewSchedule").FirstOrDefault();
                var ListInterviewDate = InterviewSchedule.GetPropertyValue<IEnumerable<IPublishedContent>>("interviewDate");
                var ListInterviewTime = InterviewSchedule.GetPropertyValue<IEnumerable<IPublishedContent>>("interviewTime");
                var TotalTimeSlot = ListInterviewTime.Count();
                int i = 1;
                foreach (var item in ListInterviewDate)
                {
                    InterviewDateSchedule modelItem = new InterviewDateSchedule();
                    DateTime dt = DateTime.Parse(item.GetPropertyValue("interviewDateSlot").ToString());
                    modelItem.InterviewDate = dt.Date.ToShortDateString();
                    modelItem.InterviewDateMonth = dt.ToString("MMM");
                    modelItem.InterviewDateDay = dt.Day.ToString();
                    modelItem.InterviewDateYear = dt.Year.ToString();
                    modelItem.TotalAvailableSlot = TotalTimeSlot - db.ctExternalCourseRegistrations.Where(x => x.CourseCode == CourseCode && x.InterviewDate == dt).Count();
                    modelItem.SerialNo = "Date" + i.ToString();
                    modelItem.IsDefaultChecked = i == 1 ? "checked='checked'" : "";
                    model.Add(modelItem);
                    i += 1;
                }
                return Json(new
                {
                    AvailableDates = model,
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult GetAvailableTime(int CourseCmsId, string lang, string date)
        {
            try
            {
                DateTime selectedDate = new DateTime();
                List<InterviewTimeSchedule> model = new List<InterviewTimeSchedule>();
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                var CmsCourse = Umbraco.TypedContent(CourseCmsId);
                var CourseCode = CmsCourse.GetPropertyValue("currentRegistrationCode").ToString();
                var settingId = PaymentIntegrationMode == "Test" ? 3537 : 3654;
                var SettingContent = Umbraco.TypedContent(settingId);
                var InterviewSchedule = SettingContent.Children.Where(x => x.IsVisible() && x.DocumentTypeAlias == "interviewSchedule").FirstOrDefault();
                var ListInterviewDate = InterviewSchedule.GetPropertyValue<IEnumerable<IPublishedContent>>("interviewDate");
                var ListInterviewTime = InterviewSchedule.GetPropertyValue<IEnumerable<IPublishedContent>>("interviewTime");
                int i = 1;
                if (!string.IsNullOrEmpty(date))
                {
                    selectedDate = DateTime.Parse(date);
                }
                else
                {
                    int j = 0;
                    foreach (var Itemdate in ListInterviewDate)
                    {
                        if (j == 0)
                        {
                            selectedDate = DateTime.Parse(Itemdate.GetPropertyValue("interviewDateSlot").ToString());
                        }
                        j += 1;
                    }
                }
                foreach (var item in ListInterviewTime)
                {
                    InterviewTimeSchedule modelItem = new InterviewTimeSchedule();
                    modelItem.InterviewDisplayTime = item.GetPropertyValue("interviewTimeSlot").ToString();
                    modelItem.InterviewTime = item.GetPropertyValue("interviewTimeSlot").ToString();
                    modelItem.SerialNo = "Time" + i.ToString();

                    if (db.ctExternalCourseRegistrations.Where(x => x.CourseCode == CourseCode && x.InterviewDate == selectedDate && x.InterviewTime == modelItem.InterviewTime).Count() > 0)
                    {
                        modelItem.IsDisable = "disabled";
                    }
                    else
                    {
                        modelItem.IsDisable = "";
                    }

                    model.Add(modelItem);
                    i += 1;
                }
                return Json(new
                {
                    AvailableTime = model,
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}