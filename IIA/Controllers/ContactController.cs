﻿using System;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using IIA.DBContext;
using System.Linq.Dynamic;
using umbraco.cms.helpers;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using IIA.Models;
using CMS.Helpers;
using System.Data.Entity;
using Umbraco.Web;
using System.Web.Configuration;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using umbraco.editorControls.SettingControls.Pickers;
using Newtonsoft.Json;
using Umbraco.Core.Models;
using IIA.Helper;
using System.Data;
using System.Data.SqlClient;
using Umbraco.Web.Media.EmbedProviders;
using System.Configuration;

namespace IIA.Controllers
{
    public class ContactController : SurfaceController
    {
        IIAContext db = new IIAContext();
        [HttpPost]
        public ActionResult ContactUs(ContactFormModel model)
        {
            try
            {                
                ctContact dbModel = new ctContact()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    EmailAddress = model.EmailAddress,
                    ContactNo = model.ContactNo,
                    Message = model.Message,
                    ContactType = model.ContactType,
                    PageId = model.PageId,
                    ContactDate = DateTime.Now
                };

                db.ctContacts.Add(dbModel);
                db.SaveChanges();

                EmailSettingModel eModel = Utility.GetEmailSetting(model.ContactType,"", model.lang);

                string Message = EmailMessage.GetContactAdminMessage(model, model.lang);

                Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);

                return Json(new
                {
                    status = true,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpPost]
        public ActionResult RequestCallBack(ContactFormModel model)
        {
            try
            {
                ctContact dbModel = new ctContact()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    EmailAddress = model.EmailAddress,
                    ContactNo = model.ContactNo,
                    Message = model.Message,
                    ContactType = model.ContactType,
                    PageId = model.PageId,
                    ContactDate = DateTime.Now
                };

                db.ctContacts.Add(dbModel);
                db.SaveChanges();

                EmailSettingModel eModel = Utility.GetEmailSetting(model.ContactType, "", model.lang);

                string Message = EmailMessage.GetContactAdminMessage(model, model.lang);

                Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);

                return Json(new
                {
                    status = true,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpPost]
        public ActionResult DownloadBrochure(ContactFormModel model)
        {
            try
            {
                ctContact dbModel = new ctContact()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    EmailAddress = model.EmailAddress,
                    ContactNo = model.ContactNo,
                    Message = model.Message,
                    ContactType = model.ContactType,
                    PageId = model.PageId,
                    ContactDate = DateTime.Now
                };

                db.ctContacts.Add(dbModel);
                db.SaveChanges();

                EmailSettingModel eModel = Utility.GetEmailSetting(model.ContactType, model.EmailAddress, model.lang);

                string Message = EmailMessage.GetContactAdminMessage(model, model.lang);

                Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);

                return Json(new
                {
                    status = true,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }


        }


        [HttpPost]
        public ActionResult AboutInternalAudition(ContactFormModel model)
        {
            try
            {
                ctContact dbModel = new ctContact()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    EmailAddress = model.EmailAddress,
                    ContactNo = model.ContactNo,
                    Company=model.Company,
                    Designation=model.Designation,
                    Message = model.Message,
                    ContactType = model.ContactType,
                    PageId = model.PageId,
                    ContactDate = DateTime.Now
                };

                db.ctContacts.Add(dbModel);
                db.SaveChanges();

                EmailSettingModel eModel = Utility.GetEmailSetting(model.ContactType, "", model.lang);

                string Message = EmailMessage.GetContactAdminMessage(model, model.lang);

                Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);

                return Json(new
                {
                    status = true,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }


        }

        //[HttpGet]
        //public ActionResult InsertExistingMember()
        //{
        //    int i = 0;
        //    string cs = ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;
        //    SqlConnection connection = new SqlConnection(cs);
        //    string query = "select * from [dbo].[ctExistingMemberEnglishName]";
        //    connection.Open();
        //    SqlCommand command = new SqlCommand(query, connection);
        //    SqlDataReader reader = command.ExecuteReader();
        //    while (reader.Read())
        //    {
        //        string name = reader["English_Name"].ToString();
        //        string email= reader["Email_ID"].ToString();
        //        var splitname = name.Split(' ');
        //        string firstName = "";
        //        string middleName = "";
        //        string lastName = "";
        //        if(splitname.Length==1)
        //        {
        //            firstName = splitname[0];
        //        }
        //        else if (splitname.Length == 2)
        //        {
        //            firstName = splitname[0];
        //            lastName= splitname[1];
        //        }
        //        else if (splitname.Length == 3)
        //        {
        //            firstName = splitname[0];
        //            middleName = splitname[1];
        //            lastName = splitname[2];
        //        }
        //        else if (splitname.Length == 4)
        //        {
        //            firstName = splitname[0] + " "+ splitname[1];
        //            middleName = splitname[2];
        //            lastName = splitname[3];
        //        }
        //        else if (splitname.Length == 5)
        //        {
        //            firstName = splitname[0] + " " + splitname[1];
        //            middleName = splitname[2];
        //            lastName = splitname[3] + " " + splitname[4];
        //        }
        //        string newQuery = "update ctRegistration set FirstName=@FirstName, MiddleName=@MiddleName, LastName=@LastName where EmailAddress=@EmailAddress";
        //        SqlCommand command1 = new SqlCommand(newQuery, connection);
        //        command1.Parameters.AddWithValue("FirstName", firstName);
        //        command1.Parameters.AddWithValue("MiddleName", middleName);
        //        command1.Parameters.AddWithValue("LastName", lastName);
        //        command1.Parameters.AddWithValue("EmailAddress", email);
        //        command1.ExecuteNonQuery();

        //        i = i + 1;
        //    }
        //    reader.Close();
        //    connection.Close();

        //    return Content("Updated:" + i.ToString());
        //}


        [HttpGet]
        public ActionResult InsertExistingMember()
        {
            int i = 0;
            string cs = ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;
            SqlConnection connection = new SqlConnection(cs);
            string query = "select * from [dbo].[ctExistingCorporateMember]";
            connection.Open();
            SqlCommand command = new SqlCommand(query, connection);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                string EmailAddress = reader["EmailAddress"].ToString();
                string English_Name = reader["English_Name"].ToString();                
                var splitEnglish_Name = English_Name.Split(' ');

                string Arabic_Name = reader["Arabic_Name"].ToString();
                var splitArabic_Name = Arabic_Name.Split(' ');

                string firstName = "";
                string middleName = "";
                string lastName = "";
                string firstName_ar = "";
                string middleName_ar = "";
                string lastName_ar = "";
                if (splitEnglish_Name.Length == 1)
                {
                    firstName = splitEnglish_Name[0];
                }
                else if (splitEnglish_Name.Length == 2)
                {
                    firstName = splitEnglish_Name[0];
                    lastName = splitEnglish_Name[1];
                }
                else if (splitEnglish_Name.Length == 3)
                {
                    firstName = splitEnglish_Name[0];
                    middleName = splitEnglish_Name[1];
                    lastName = splitEnglish_Name[2];
                }
                else if (splitEnglish_Name.Length == 4)
                {
                    firstName = splitEnglish_Name[0] + " " + splitEnglish_Name[1];
                    middleName = splitEnglish_Name[2];
                    lastName = splitEnglish_Name[3];
                }
                else if (splitEnglish_Name.Length == 5)
                {
                    firstName = splitEnglish_Name[0] + " " + splitEnglish_Name[1];
                    middleName = splitEnglish_Name[2];
                    lastName = splitEnglish_Name[3] + " " + splitEnglish_Name[4];
                }
                else
                {
                    firstName = English_Name;
                }

                if (splitArabic_Name.Length == 1)
                {
                    firstName_ar = splitArabic_Name[0];
                }
                else if (splitArabic_Name.Length == 2)
                {
                    firstName_ar = splitArabic_Name[0];
                    lastName_ar = splitArabic_Name[1];
                }
                else if (splitArabic_Name.Length == 3)
                {
                    firstName_ar = splitArabic_Name[0];
                    middleName_ar = splitArabic_Name[1];
                    lastName_ar = splitArabic_Name[2];
                }
                else if (splitArabic_Name.Length == 4)
                {
                    firstName_ar = splitArabic_Name[0] + " " + splitArabic_Name[1];
                    middleName_ar = splitArabic_Name[2];
                    lastName_ar = splitArabic_Name[3];
                }
                else if (splitArabic_Name.Length == 5)
                {
                    firstName_ar = splitArabic_Name[0] + " " + splitArabic_Name[1];
                    middleName_ar = splitArabic_Name[2];
                    lastName_ar = splitArabic_Name[3] + " " + splitArabic_Name[4];
                }
                else
                {
                    firstName_ar = Arabic_Name;
                }

                if (!string.IsNullOrEmpty(EmailAddress))
                {
                    if (db.ctRegistrations.Where(x => x.EmailAddress == EmailAddress && x.ProfileType == "Member").Count() <= 0)
                    {
                        ctRegistration objDbModel = new ctRegistration();
                        objDbModel.ProfileType = "Member";
                        objDbModel.ParentProfileId = 3158;
                        objDbModel.MembershipType = "Professional";
                        objDbModel.MembershipBadge = "Gold";
                        objDbModel.FirstName = firstName;
                        objDbModel.MiddleName = middleName;
                        objDbModel.LastName = lastName;
                        objDbModel.FirstName_ar = firstName_ar;
                        objDbModel.MiddleName_ar = middleName_ar;
                        objDbModel.LastName_ar = lastName_ar;
                        objDbModel.IIAKSANumber = Utility.GenerateIIAKSANumber("Member");
                        objDbModel.Country = "SA";
                        objDbModel.MobileNumber = reader["MobileNumber"].ToString();
                        objDbModel.NationalIDNumber = reader["NationalIDNumber"].ToString();
                        objDbModel.EmailAddress = reader["EmailAddress"].ToString();
                        objDbModel.IIAGlobalNumber = "0" + (i + 1).ToString();
                        objDbModel.MembershipStartDate = DateTime.Now;
                        objDbModel.MembershipEndDate = DateTime.Now.AddYears(1);
                        objDbModel.MemberActivationDate = DateTime.Now;
                        objDbModel.IsActive = true;
                        objDbModel.IsEmailVerify = true;
                        objDbModel.IsApplyForMentor = false;
                        objDbModel.IsApproveMentor = false;
                        objDbModel.LastUpdated = DateTime.Now;
                        objDbModel.lang = "ar";
                        objDbModel.MembershipDiscount = 30;
                        db.ctRegistrations.Add(objDbModel);
                        db.SaveChanges();
                    }
                    else
                    {
                        ctRegistration objDbModel = db.ctRegistrations.Where(x => x.EmailAddress == EmailAddress && x.ProfileType == "Member").FirstOrDefault();
                        objDbModel.ParentProfileId = 3158;
                        objDbModel.MembershipType = "Professional";
                        objDbModel.MembershipBadge = "Gold";
                        objDbModel.FirstName = firstName;
                        objDbModel.MiddleName = middleName;
                        objDbModel.LastName = lastName;
                        objDbModel.FirstName_ar = firstName_ar;
                        objDbModel.MiddleName_ar = middleName_ar;
                        objDbModel.LastName_ar = lastName_ar;
                        objDbModel.Country = "SA";
                        objDbModel.MobileNumber = reader["MobileNumber"].ToString();
                        objDbModel.NationalIDNumber = reader["NationalIDNumber"].ToString();
                        objDbModel.MembershipStartDate = DateTime.Now;
                        objDbModel.MembershipEndDate = DateTime.Now.AddYears(1);
                        objDbModel.MemberActivationDate = DateTime.Now;
                        objDbModel.IsActive = true;
                        objDbModel.IsEmailVerify = true;
                        objDbModel.IsApplyForMentor = false;
                        objDbModel.IsApproveMentor = false;
                        objDbModel.LastUpdated = DateTime.Now;
                        objDbModel.lang = "ar";
                        objDbModel.MembershipDiscount = 30;

                        db.Entry(objDbModel).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    i = i + 1;
                }
            }
            reader.Close();
            connection.Close();

            return Content("Total Record:" + i.ToString());
        }

    }
}