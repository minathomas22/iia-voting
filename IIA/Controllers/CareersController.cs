﻿using System;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using IIA.DBContext;
using System.Linq.Dynamic;
using umbraco.cms.helpers;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using IIA.Models;
using CMS.Helpers;
using System.Data.Entity;
using Umbraco.Web;
using System.Web.Configuration;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using umbraco.editorControls.SettingControls.Pickers;
using Newtonsoft.Json;
using IIA.Helper;
using IIA.Helpers;
using umbraco;

namespace IIA.Controllers
{
    public class CareersController : SurfaceController
    {
        IIAContext db = new IIAContext();
        [HttpPost]
        public ActionResult ApplyPost(CareerApplyModel model)
        {
            try
            {
                string CV = "";
                var FileCV = Request.Files["CV"];
                if (FileCV != null)
                {
                    CV = Utility.UploadFileInFolder(FileCV);
                    model.CV = CV;
                }
                ctCareerApply dbModel = new ctCareerApply()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    EmailAddress = model.EmailAddress,
                    ContactNo = model.ContactNo,
                    Message = model.Message,
                    CV = CV,
                    ApplyFor = model.ApplyFor,
                    PostId = model.PostId,
                    PostTitle = model.PostTitle,
                    ApplyDate = DateTime.Now,
                    PostType = model.PostType
                };

                db.ctCareerApplies.Add(dbModel);
                db.SaveChanges();

                EmailSettingModel eModel = Utility.GetEmailSetting(model.ApplyFor, model.PostId.ToString(), model.lang);

                string Message = EmailMessage.GetCareerApplicationMessage(model, model.lang);

                Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);

                return Json(new
                {
                    status = true,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpPost]
        public ActionResult GetMentoringList(string keyword, string company, string lang)
        {
            try
            {                              

                List <RegistrationViewModel> model = new List<RegistrationViewModel>();               
                var dbModel = db.ctRegistrations.Where(x => x.ProfileType == "Member" & x.IsActive == true && x.IsApproveMentor==true).ToList();
                
                foreach (var item in dbModel)
                {
                    RegistrationViewModel regItem = new RegistrationViewModel();
                    string middleName = lang == "en" ? item.MiddleName : item.MiddleName_ar;
                     regItem.ProfileId = item.ProfileId;
                    regItem.FirstName = lang=="en"? item.FirstName:item.FirstName_ar;
                    regItem.MiddleName = !string.IsNullOrEmpty(middleName) ? middleName : "";
                    regItem.LastName = lang == "en" ? item.LastName : item.LastName_ar;
                    if (!string.IsNullOrEmpty(item.ProfilePhoto))
                    {
                        regItem.ProfilePhoto = "/Media/ApplicantFile/" + item.ProfilePhoto;
                    }
                    else
                    {
                        regItem.ProfilePhoto = "/ui/media/images/avater.jpg";
                    }
                   
                    regItem.ProfileType = item.ProfileType;
                    regItem.JobTitle = lang == "en" ? item.JobTitle : item.JobTitle_ar;
                    regItem.Organization = lang == "en" ? item.Organization : item.Organization_ar;
                    model.Add(regItem);
                }

                return Json(new
                {
                    status = true,
                    MemberInfo = model
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetMemberList(string keyword, string company, string page, string lang)
        {
            try
            {

                int pageIndex = !string.IsNullOrEmpty(page) ? int.Parse(page) : 1;
                int pageSize = 24;
                int totalPage = 0;
                var pageUrl = "";

                List<RegistrationViewModel> model = new List<RegistrationViewModel>();
                var count = db.ctRegistrations.Where(x => x.ProfileType == "Member" & x.IsActive == true && x.MembershipType != "Corporate" && !string.IsNullOrEmpty(x.FirstName)).Count();

                var dbModel = db.ctRegistrations.Where(x => x.ProfileType == "Member" & x.IsActive == true && x.MembershipType != "Corporate" && !string.IsNullOrEmpty(x.FirstName)).ToList();

                if (keyword != "")
                {
                    keyword = keyword.ToLower();
                    if (lang == "en")
                    {
                        dbModel = dbModel.Where(x =>
                        (
                        x.FirstName?.ToLower().Contains(keyword) ?? false)
                        || (x.MiddleName?.ToLower().Contains(keyword) ?? false)
                        || (x.LastName?.ToLower().Contains(keyword) ?? false)
                        || (x.JobTitle?.ToLower().Contains(keyword) ?? false)
                        || (x.Organization?.ToLower().Contains(keyword) ?? false)
                        || (x.EmailAddress?.ToLower().Contains(keyword) ?? false)
                        || (x.NationalIDNumber?.ToLower().Contains(keyword) ?? false)
                        || (x.MobileNumber?.ToLower().Contains(keyword) ?? false)
                        || (x.City?.ToLower().Contains(keyword) ?? false)
                        ).ToList();
                    }
                    else
                    {
                        dbModel = dbModel.Where(x =>
                         (
                         x.FirstName_ar?.ToLower().Contains(keyword) ?? false)
                         || (x.MiddleName_ar?.ToLower().Contains(keyword) ?? false)
                         || (x.LastName_ar?.ToLower().Contains(keyword) ?? false)
                         || (x.JobTitle_ar?.ToLower().Contains(keyword) ?? false)
                         || (x.Organization_ar?.ToLower().Contains(keyword) ?? false)
                         || (x.EmailAddress?.ToLower().Contains(keyword) ?? false)
                         || (x.NationalIDNumber?.ToLower().Contains(keyword) ?? false)
                         || (x.MobileNumber?.ToLower().Contains(keyword) ?? false)
                         || (x.City_ar?.ToLower().Contains(keyword) ?? false)
                         ).ToList();
                    }
                    if (dbModel != null)
                    {
                        count = dbModel.Count();
                    }

                    pageUrl = "/" + lang + "/media-center/careers/iia-graduates/?keyword=" + keyword + "&page=";
                }
                else
                {
                    pageUrl = "/" + lang + "/media-center/careers/iia-graduates/?page=";
                }

                if (count > 0)
                {
                    dbModel = dbModel.Where(x => x.ProfileType == "Member" & x.IsActive == true && x.MembershipType != "Corporate").OrderBy(x => x.MemberActivationDate).Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToList();

                    foreach (var item in dbModel)
                    {
                        RegistrationViewModel regItem = new RegistrationViewModel();
                        string middleName = lang == "en" ? item.MiddleName : item.MiddleName_ar;
                        regItem.ProfileId = item.ProfileId;
                        regItem.FirstName = lang == "en" ? item.FirstName : item.FirstName_ar;
                        regItem.MiddleName = !string.IsNullOrEmpty(middleName) ? middleName : "";
                        regItem.LastName = lang == "en" ? item.LastName : item.LastName_ar;
                        if (!string.IsNullOrEmpty(item.ProfilePhoto))
                        {
                            regItem.ProfilePhoto = "/Media/ApplicantFile/" + item.ProfilePhoto;
                        }
                        else
                        {
                            regItem.ProfilePhoto = "/ui/media/images/avater.jpg";
                        }

                        regItem.ProfileType = item.ProfileType;
                        regItem.JobTitle = lang == "en" ? item.JobTitle : item.JobTitle_ar;
                        regItem.Organization = lang == "en" ? item.Organization : item.Organization_ar;
                        model.Add(regItem);
                    }

                    totalPage = (int)Math.Ceiling((double)count / pageSize);
                    var pagination = Pagination.Pager(pageIndex, count, pageSize, 5, pageUrl, "", "<<", ">>", "pagination");


                    return Json(new
                    {
                        status = true,
                        MemberInfo = model,
                        totalPage = totalPage,
                        pageIndex = pageIndex,
                        pageSize = pageSize,
                        pagination = pagination.ToString()
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = false
                    }, JsonRequestBehavior.AllowGet);

                }
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet); ;
            }
        }


        [HttpPost]
        public ActionResult ContactMentor(ContactFormModel model)
        {
            try
            {
                ctContact dbModel = new ctContact()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    EmailAddress = model.EmailAddress,
                    ContactNo = model.ContactNo,
                    Message = model.Message,
                    ContactType = model.ContactType,
                    PageId = model.PageId,
                    ContactDate = DateTime.Now
                };

                db.ctContacts.Add(dbModel);
                db.SaveChanges();

                EmailSettingModel eModel = Utility.GetEmailSetting(model.ContactType, model.PageId, model.lang);

                string Message = EmailMessage.GetContactAdminMessage(model, model.lang);

                Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);

                return Json(new
                {
                    status = true,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }


        }

    }
}