﻿using System;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using IIA.DBContext;
using System.Linq.Dynamic;
using umbraco.cms.helpers;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using IIA.Models;
using CMS.Helpers;
using System.Data.Entity;
using Umbraco.Web;
using System.Web.Configuration;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using umbraco.editorControls.SettingControls.Pickers;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using Umbraco.Core.Models;


namespace IIA.Controllers
{
    public class BookShopController : SurfaceController
    {
        IIAContext db = new IIAContext();
        public int VAT = 15;
        // GET: BookShop
        [HttpPost]
        public JsonResult GetCourseCategory(string lang)
        {
            List<SelectListItem> listItem = new List<SelectListItem>();

            int BookshopsandMaterialsId = lang == "en" ? 2060 : 2614;
            var BookshopsandMaterials = Umbraco.TypedContent(BookshopsandMaterialsId);
            var CourseCategory = BookshopsandMaterials.Descendants().Where(x => x.DocumentTypeAlias == "booksAndMaterialsCategory" && x.IsVisible()).ToList();
            foreach (var item in CourseCategory)
            {
                SelectListItem lItem = new SelectListItem();
                lItem.Text = item.GetPropertyValue("title").ToString();
                lItem.Value = item.Id.ToString();
                listItem.Add(lItem);
            }
            return Json(new { CourseCategory = listItem }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetLevel(string lang, string courseCategoryId)
        {
            List<SelectListItem> listItem = new List<SelectListItem>();
            var CourseCategory = Umbraco.TypedContent(courseCategoryId);
            var Level = CourseCategory.Descendants().Where(x => x.DocumentTypeAlias == "booksAndMaterialsLevel" && x.IsVisible()).ToList();
            foreach (var item in Level)
            {
                SelectListItem lItem = new SelectListItem();
                lItem.Text = item.GetPropertyValue("title").ToString();
                lItem.Value = item.Id.ToString();
                listItem.Add(lItem);
            }
            return Json(new { Level = listItem }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetProducts(string lang, string courseCategoryId, string levelId, string page)
        {
            try
            {
                int pageIndex = !string.IsNullOrEmpty(page) ? int.Parse(page) : 1;
                int pageSize = 8;
                int totalPage = 0;

                List<BookShopViewModelcs> listItem = new List<BookShopViewModelcs>();

                int BookshopsandMaterialsId = lang == "en" ? 2060 : 2614;
                var BookshopsandMaterials = Umbraco.TypedContent(BookshopsandMaterialsId);

                var ProductList = BookshopsandMaterials.Descendants().Where(x => x.DocumentTypeAlias == "booksAndMaterials" && x.IsVisible()).ToList();

                // ProductList = ProductList.Where(x => x.GetPropertyValue<int>("currentStockQty") > x.GetPropertyValue<int>("stockLimitForDisplay")).ToList();

                ProductList = ProductList.Where(x => x.GetPropertyValue<int>("currentStockQty") > 0).ToList();

                if (!string.IsNullOrEmpty(courseCategoryId))
                {
                    ProductList = ProductList.Where(x => x.Parent.Parent.Id == int.Parse(courseCategoryId)).ToList();
                }

                if (!string.IsNullOrEmpty(levelId))
                {
                    ProductList = ProductList.Where(x => x.Parent.Id == int.Parse(levelId)).ToList();
                }

                foreach (var item in ProductList)
                {
                    BookShopViewModelcs lItem = new BookShopViewModelcs();
                    lItem.ProductId = item.Id;
                    lItem.ProductCmsName = item.GetPropertyValue("title").ToString();
                    lItem.Title = item.GetPropertyValue("title").ToString();
                    lItem.Image = item.GetPropertyValue<IPublishedContent>("image").Url.ToString();
                    lItem.Price = decimal.Parse(item.GetPropertyValue("price").ToString());
                    lItem.DisplayPrice = lItem.Price + (decimal.Parse(item.GetPropertyValue("price").ToString()) * VAT) / 100;
                    lItem.Url = item.Url;
                    listItem.Add(lItem);
                }

                totalPage = (int)Math.Ceiling((double)listItem.Count() / pageSize);

                return Json(new { status = true, ProductList = listItem, totalPage = totalPage }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(new { status = false, message=ex.Message.ToString()}, JsonRequestBehavior.AllowGet);
            }
            
        }

        [HttpPost]
        public JsonResult GetOtherProducts(string lang, string productCmsId)
        {
            try
            {               
                List<BookShopViewModelcs> listItem = new List<BookShopViewModelcs>();

                int BookshopsandMaterialsId = lang == "en" ? 2060 : 2614;
                var BookshopsandMaterials = Umbraco.TypedContent(BookshopsandMaterialsId);

                var currentProduct = Umbraco.TypedContent(productCmsId);

                var ProductList = BookshopsandMaterials.Descendants().Where(x => x.DocumentTypeAlias == "booksAndMaterials" && x.IsVisible() && x.Id != int.Parse(productCmsId) && x.Parent== currentProduct.Parent).ToList();
                foreach (var item in ProductList)
                {
                    BookShopViewModelcs lItem = new BookShopViewModelcs();
                    lItem.ProductId = item.Id;
                    lItem.ProductCmsName = item.GetPropertyValue("title").ToString();
                    lItem.Title = item.GetPropertyValue("title").ToString();
                    lItem.Image = item.GetPropertyValue<IPublishedContent>("image").Url.ToString();
                    lItem.Price = decimal.Parse(item.GetPropertyValue("price").ToString());
                    lItem.DisplayPrice = lItem.Price + (decimal.Parse(item.GetPropertyValue("price").ToString()) * VAT) / 100;
                    lItem.Url = item.Url;
                    listItem.Add(lItem);
                }              

                return Json(new { status = true, ProductList = listItem}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }

        }

    }
}