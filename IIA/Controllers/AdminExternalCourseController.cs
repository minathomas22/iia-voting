﻿using System;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using IIA.DBContext;
using System.Linq.Dynamic;
using umbraco.cms.helpers;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using IIA.Models;
using CMS.Helpers;
using System.Data.Entity;
using Umbraco.Web;
using System.Web.Configuration;
using System.Text;
using System.Net;

using System.IO;
using System.Web.Script.Serialization;
using umbraco.editorControls.SettingControls.Pickers;
using Newtonsoft.Json;
using Umbraco.Core.Models;
using IIA.Helper;
using System.Data;
using System.Data.SqlClient;
using Umbraco.Web.Media.EmbedProviders;
using umbraco;

namespace IIA.Controllers
{
    public class AdminExternalCourseController : SurfaceController
    {
        IIAContext db = new IIAContext();
       
        [HttpPost]
        public ActionResult GetCourseCode(string prefix)
        {
            try
            {
                List<SelectListItem> listCourseCode = new List<SelectListItem>();
                var CourseRegInfo = db.ctExternalCourseRegistrations.Where(x => x.CourseCode.ToLower().Contains(prefix)).Select(x => x.CourseCode).Distinct();
                foreach (var item in CourseRegInfo)
                {
                    SelectListItem selectListItem = new SelectListItem();
                    selectListItem.Text = item;
                    selectListItem.Value = item;
                    listCourseCode.Add(selectListItem);
                }
                return Json(new
                {
                    status = true,
                    CourseInfo = listCourseCode
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetMemberList(string CourseCode, string MemberFor)
        {
            try
            {
                if (!string.IsNullOrEmpty(CourseCode))
                {
                    List<SelectListItem> listMember = new List<SelectListItem>();
                    var MemberInfo = db.ctExternalCourseRegistrations.Where(x => x.CourseCode == CourseCode).ToList();
                    if (MemberFor == "ResultUpdate")
                    {
                        MemberInfo = MemberInfo.Where(x => string.IsNullOrEmpty(x.IsEnglishSpokenPass)).ToList();
                    }
                    else if (MemberFor == "InterviewUpdate")
                    {
                        MemberInfo = MemberInfo.Where(x => x.IsEnglishSpokenPass=="Y" && string.IsNullOrEmpty(x.IsInterviewPass)).ToList();
                    }
                    
                    foreach (var item in MemberInfo)
                    {
                        var PersonnalInfo = db.ctRegistrations.Where(x => x.ProfileId == item.ProfileId).FirstOrDefault();

                        SelectListItem selectListItem = new SelectListItem();
                        selectListItem.Text = PersonnalInfo.FirstName + " " + PersonnalInfo.MiddleName + " " + PersonnalInfo.LastName + " (" + PersonnalInfo.EmailAddress + ")";
                        selectListItem.Value = item.ProfileId.ToString();
                        listMember.Add(selectListItem);
                    }
                    return Json(new
                    {
                        status = true,
                        MemberInfo = listMember
                    }, "application/json",Encoding.UTF8,JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetRegistrationInfo(string CourseCode, long ProfileId)
        {
            try
            {
                MemberCourseRegistrationModel model = new MemberCourseRegistrationModel();
                if (!string.IsNullOrEmpty(CourseCode) && !string.IsNullOrEmpty(ProfileId.ToString()))
                {
                    var ObjRegInfo = db.ctExternalCourseRegistrations.Where(x => x.CourseCode == CourseCode && x.ProfileId == ProfileId).FirstOrDefault();
                    if (ObjRegInfo != null)
                    {
                        model.EnglishSpokenPaymentReceipt = !string.IsNullOrEmpty(ObjRegInfo.EnglishSpokenPaymentReceipt) ? ObjRegInfo.EnglishSpokenPaymentReceipt : "";
                        model.EnglishSpokenResult = !string.IsNullOrEmpty(ObjRegInfo.EnglishSpokenResult) ? ObjRegInfo.EnglishSpokenResult : "";
                        model.InterviewDate = ObjRegInfo.InterviewDate.HasValue ? ObjRegInfo.InterviewDate.Value.ToString("dd MMM yyyy") : "";
                        model.InterviewTime = !string.IsNullOrEmpty(ObjRegInfo.InterviewTime) ? ObjRegInfo.InterviewTime : "";
                        model.EnglishSpokenResultScore = ObjRegInfo.EnglishSpokenResultScore;
                        model.IsEnglishSpokenPass = ObjRegInfo.IsEnglishSpokenPass;
                        model.IsInterviewPass = ObjRegInfo.IsInterviewPass;

                        var objMember = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                        model.FirstName = objMember.FirstName;
                        model.MiddleName = !string.IsNullOrEmpty(objMember.MiddleName) ? objMember.MiddleName:"";
                        model.LastName = objMember.LastName;
                        model.EmailAddress = objMember.EmailAddress;
                        model.MobileNumber = objMember.MobileNumber;
                        model.IIAGlobalNumber = objMember.IIAGlobalNumber;
                        model.NationalIDNumber = objMember.NationalIDNumber;
                        model.Experience = !string.IsNullOrEmpty(objMember.Experience) ? objMember.Experience : "";
                        model.JobTitle = !string.IsNullOrEmpty(objMember.JobTitle) ? objMember.JobTitle : "";
                        model.Organization = !string.IsNullOrEmpty(objMember.Organization) ? objMember.Organization : "";

                        var objMemberAddInfo = db.ctExternalCourseMemberInfoes.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                        model.Education = objMemberAddInfo.Education;
                        model.EducationCertificate = objMemberAddInfo.EducationCertificate;
                        model.CIAHolder = objMemberAddInfo.CIAHolder;
                        model.CIAHolderCertificate = !string.IsNullOrEmpty(objMemberAddInfo.CIAHolderCertificate) ? objMemberAddInfo.CIAHolderCertificate : "";
                        model.EnglishProficiency = objMemberAddInfo.EnglishProficiency;
                        model.HoldEnglishProficiency = objMemberAddInfo.HoldEnglishProficiency;
                        model.EnglishProficiencyCertificate = !string.IsNullOrEmpty(objMemberAddInfo.EnglishProficiencyCertificate) ? objMemberAddInfo.EnglishProficiencyCertificate : "";
                        model.OtherProfessionalCertificates = !string.IsNullOrEmpty(objMemberAddInfo.OtherProfessionalCertificates) ? objMemberAddInfo.OtherProfessionalCertificates : "";

                        return Json(new
                        {
                            status = true,
                            regInfo = model
                        }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new
                        {
                            status = false
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new
                    {
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveSpokenResultInfo(ExternalCourseRegistrationModel model)
        {
            try
            {
                ctExternalCourseRegistration objDb = db.ctExternalCourseRegistrations.Where(x => x.CourseCode == model.CourseCode && x.ProfileId == model.ProfileId).FirstOrDefault();
                objDb.EnglishSpokenResultScore = model.EnglishSpokenResultScore;
                objDb.IsEnglishSpokenPass = model.IsEnglishSpokenPass;
                objDb.LastUpdated = DateTime.Now;
                db.Entry(objDb).State = EntityState.Modified;
                db.SaveChanges();                

                ctRegistration ctRegistration = db.ctRegistrations.Where(x => x.ProfileId == objDb.ProfileId).FirstOrDefault();

                EmailSettingModel eModel = Utility.GetExternalCourseEmailSetting("SpokenResultStatusUpdate", objDb.ProfileId.Value, ctRegistration.lang);

                string Message = EmailMessage.GetExternalCourseMessage("SpokenResultStatusUpdate", objDb.RegId, ctRegistration.lang);

                Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);


                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult SaveInterviewInfo(ExternalCourseRegistrationModel model)
        {
            try
            {
                ctExternalCourseRegistration objDb = db.ctExternalCourseRegistrations.Where(x => x.CourseCode == model.CourseCode && x.ProfileId == model.ProfileId).FirstOrDefault();
                objDb.IsInterviewPass = model.IsInterviewPass;
                objDb.LastUpdated = DateTime.Now;
                db.Entry(objDb).State = EntityState.Modified;
                db.SaveChanges();               

                ctRegistration ctRegistration = db.ctRegistrations.Where(x => x.ProfileId == objDb.ProfileId).FirstOrDefault();

                EmailSettingModel eModel = Utility.GetExternalCourseEmailSetting("InterviewStatusUpdate", objDb.ProfileId.Value, ctRegistration.lang);

                string Message = EmailMessage.GetExternalCourseMessage("InterviewStatusUpdate", objDb.RegId, ctRegistration.lang);

                Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);

                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public FileResult GeneratePDF(string courseCode, string proId)
        {
            string lang = "en";
            int profileId = int.Parse(proId);
            var memberInfo = db.ctRegistrations.Where(x => x.ProfileId == profileId).FirstOrDefault();
            var regInfo = db.ctExternalCourseRegistrations.Where(x => x.CourseCode == courseCode && x.ProfileId == profileId).FirstOrDefault();
            var additionInfo = db.ctExternalCourseMemberInfoes.Where(x => x.ProfileId == profileId).FirstOrDefault();
            string retVal = "";
            using (System.IO.StreamReader reader = new System.IO.StreamReader(Server.MapPath("~/ExternalCourseEmailTemplate/HarvardPDF.html")))
            {
                retVal = reader.ReadToEnd();
                retVal = retVal.Replace("%_Name_%", memberInfo.FirstName + " " + memberInfo.MiddleName + " " + memberInfo.LastName);
                retVal = retVal.Replace("%_Email_%", memberInfo.EmailAddress);
                retVal = retVal.Replace("%_Phone_%", memberInfo.MobileNumber);
                retVal = retVal.Replace("%_IIAGlobalNo_%", memberInfo.IIAGlobalNumber);
                retVal = retVal.Replace("%_NationalID_%", memberInfo.NationalIDNumber);
                retVal = retVal.Replace("%_Experience_%", memberInfo.Experience);
                retVal = retVal.Replace("%_JobTitle_%", memberInfo.JobTitle);
                retVal = retVal.Replace("%_Organization_%", memberInfo.Organization);
                retVal = retVal.Replace("%_Education_%", additionInfo.Education);
                retVal = retVal.Replace("%_CIAHolder_%", additionInfo.CIAHolder);
                retVal = retVal.Replace("%_EnglishProficiency_%", additionInfo.EnglishProficiency);
                retVal = retVal.Replace("%_EnglishProficiencyCertificate_%", !string.IsNullOrEmpty(additionInfo.EnglishProficiencyCertificate) ? "<a href='" + WebConfigurationManager.AppSettings["WebUrl"].ToString() + "Media/ApplicantFile/" + additionInfo.EnglishProficiencyCertificate + "'>View file</a>" : WordGet("N", lang));
                retVal = retVal.Replace("%_Previously_owned_English_Proficiency_Results_%", !string.IsNullOrEmpty(regInfo.EnglishSpokenPaymentReceipt) ? "<a href='" + WebConfigurationManager.AppSettings["WebUrl"].ToString() + "Media/ApplicantFile/" + regInfo.EnglishSpokenPaymentReceipt + "'>View file</a>" : WordGet("N", lang));
                retVal = retVal.Replace("%_New_English_proficiency_Results_%", !string.IsNullOrEmpty(regInfo.EnglishSpokenResult) ? "<a href='" + WebConfigurationManager.AppSettings["WebUrl"].ToString() + "Media/ApplicantFile/" + regInfo.EnglishSpokenResult + "'>View file</a>" : WordGet("N", lang));
                retVal = retVal.Replace("%_SpokenEnglishTestResult_%", WordGet(regInfo.IsEnglishSpokenPass, lang));
                retVal = retVal.Replace("%_SpokenEnglishTestMarks_%", regInfo.EnglishSpokenResultScore);
                retVal = retVal.Replace("%_InterviewSchedule_%", !string.IsNullOrEmpty(regInfo.InterviewTime) ? WordGet("Y", lang) : WordGet("N", lang));
                retVal = retVal.Replace("%_InterviewDateTime_%", !string.IsNullOrEmpty(regInfo.InterviewTime) ? regInfo.InterviewDate.Value.ToString("dd MMM yyyy") + " " + regInfo.InterviewTime : "");
                retVal = retVal.Replace("%_InterviewResult_%", WordGet(regInfo.IsInterviewPass, lang));
                retVal = retVal.Replace("%_FinalCoursePayment_%", WordGet(regInfo.IsPaymentSuccess, lang));
                retVal = retVal.Replace("%_SeatAllocated_%", WordGet(regInfo.IsSeatConfirmed, lang));
                retVal = retVal.Replace("%_ProfilePicture_%", !string.IsNullOrEmpty(memberInfo.ProfilePhoto) ? WebConfigurationManager.AppSettings["WebUrl"].ToString() + "Media/ApplicantFile/" + memberInfo.ProfilePhoto : WebConfigurationManager.AppSettings["WebUrl"].ToString() + "ui/media/images/avater.jpg");

                byte[] retByte = PDFGenerate.GetPDFFromHtmlToByteArray(retVal);

                //return Convert.ToBase64String(retByte, 0, retByte.Length);
                return File(retByte, "application/octet-stream", memberInfo.EmailAddress + ".pdf");

            }

        }

        public string WordGet(string key, string lang)
        {
            string ret = "";
            if (!string.IsNullOrEmpty(key))
            {
                if (key == "Y")
                {
                    ret = "Yes";
                }
                else if (key == "N")
                {
                    ret = "No";
                }
            }
            return ret;
        }

    }
}