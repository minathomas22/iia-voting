﻿using System;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using IIA.DBContext;
using System.Linq.Dynamic;
using umbraco.cms.helpers;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using IIA.Models;
using CMS.Helpers;
using System.Data.Entity;
using Umbraco.Web;
using System.Web.Configuration;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using umbraco.editorControls.SettingControls.Pickers;
using Newtonsoft.Json;
using IIA.Helper;
using IIA.Helpers;
using umbraco;
using Umbraco.Core;

namespace IIA.Controllers
{
    public class FormProcessController : SurfaceController
    {
        IIAContext db = new IIAContext();
        //public string PaymentIntegrationMode = "Test";
        //public string PaymentIntegrationUrl = "https://test.oppwa.com/";
        public string PaymentIntegrationMode = WebConfigurationManager.AppSettings["PaymentIntegrationMode"].ToString();
        public string PaymentIntegrationUrl = WebConfigurationManager.AppSettings["PaymentIntegrationUrl"].ToString();
        public string Currency = "SAR";
        public double CurrencyRate = 1;
        public int VAT = 15;

        [HttpGet]
        public ActionResult NonMemberRegistration()
        {
            try
            {
                return PartialView("Registration/NonMemberRegistration");
            }
            catch (Exception ex)
            {
                
            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult MemberRegistration()
        {
            try
            {
                RegistrationViewModel model = new RegistrationViewModel();
                if (Utility.GetApplicationCookies("proId") != null)
                {
                    long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                    var ctRegistration = db.ctRegistrations.Where(x => x.ProfileId == ProfileId && x.ProfileType == "NonMember").FirstOrDefault();
                    if (ctRegistration != null)
                    {
                        model.FirstName = ctRegistration.FirstName;
                        model.MiddleName = ctRegistration.MiddleName;
                        model.LastName = ctRegistration.LastName;
                        model.FirstName_ar = ctRegistration.FirstName_ar;
                        model.MiddleName_ar = ctRegistration.MiddleName_ar;
                        model.LastName_ar = ctRegistration.LastName_ar;
                        model.NationalIDNumber = ctRegistration.NationalIDNumber;
                        model.Gender = ctRegistration.Gender;
                        model.DateOfBirth = ctRegistration.DateOfBirth;
                        model.Country = ctRegistration.Country;
                        model.City = ctRegistration.City;
                        model.City_ar = ctRegistration.City_ar;
                        model.Zip_Postal_Code = ctRegistration.Zip_Postal_Code;
                        model.MobileNumber = ctRegistration.MobileNumber;
                        model.PhoneNumber = ctRegistration.PhoneNumber;
                        model.FaxNumber = ctRegistration.FaxNumber;
                        if (!string.IsNullOrEmpty(ctRegistration.ProfilePhoto))
                        {
                            model.ProfilePhoto = "/Media/ApplicantFile/" + ctRegistration.ProfilePhoto;
                        }
                        else
                        {
                            model.ProfilePhoto = "/ui/media/images/avater.jpg";
                        }
                        model.PreferredLanguage = ctRegistration.lang;
                        model.EmailAddress = ctRegistration.EmailAddress;
                        model.Password = ctRegistration.Password;
                        model.ProfileType = ctRegistration.ProfileType;
                        model.JobTitle = ctRegistration.JobTitle;
                        model.JobTitle_ar = ctRegistration.JobTitle_ar;
                        model.Organization = ctRegistration.Organization;
                        model.Organization_ar = ctRegistration.Organization_ar;
                    }
                }
                else
                {
                    model.ProfilePhoto = "/ui/media/images/avater.jpg";
                }
                return PartialView("Registration/MemberRegistration", model);
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult ShippingBilling()
        {
            try
            {
                return PartialView("Registration/ShippingBilling");
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult RegistrationCheckout()
        {
            try
            {
                long profileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                RegistrationCheckoutViewModel model = new RegistrationCheckoutViewModel();
                var regData = db.ctRegistrations.Where(x => x.ProfileId == profileId).FirstOrDefault();
                var BecomeQualifiedPage = Umbraco.TypedContent(1810);
                var MembershipType = BecomeQualifiedPage.Children.Where(x => x.GetPropertyValue("typeOfMembership").ToString() == regData.MembershipType).FirstOrDefault();
                model.MembershipType = GetMultilanguageText(regData.MembershipType, Utility.GetLanguageFromUrl(Request.Url.ToString()));
                model.MembershipBadge = GetMultilanguageText(regData.MembershipBadge, Utility.GetLanguageFromUrl(Request.Url.ToString()));
                model.MembershipDiscount = regData.MembershipDiscount.Value;

                if (regData.MembershipType == "Corporate")
                {
                    if (regData.MembershipBadge == "Bronze")
                    {
                        model.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("bronzeFee").ToString());
                    }
                    else if (regData.MembershipBadge == "Silver")
                    {
                        model.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("silverFee").ToString());
                    }
                    else if (regData.MembershipBadge == "Gold")
                    {
                        model.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("goldFee").ToString());
                    }
                }
                else
                {
                    model.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("generalFee").ToString());
                }

                model.Shipping = 0;
                model.Discount = 0;
                model.VAT = ((model.SubTotal + model.Shipping - model.Discount) * VAT) / 100;
                model.GrandTotal = (model.SubTotal + model.VAT + model.Shipping - model.Discount);

                return PartialView("Registration/RegistrationCheckout", model);
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult RegistrationPayment()
        {
            try
            {
                string lang = Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString());
                PaymentGateayStep1Model model = new PaymentGateayStep1Model();

                long PaymentId = long.Parse(Utility.GetApplicationCookies("payId").Value);
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);

                var MemberInfo = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                var ShippingBilling = db.ctShippingBillings.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                var PaymentInfo = db.ctRegistrationPayments.Where(x => x.PaymentId == PaymentId).FirstOrDefault();
                var PaymentGatewayInfo = db.ctPaymentGatewayInfoes.Where(x => x.IntegrationMode == PaymentIntegrationMode).FirstOrDefault();
                var entityId = "";
                if (PaymentInfo.PaymentMethod == "VISA")
                {
                    entityId = PaymentGatewayInfo.VIsaEntityId;
                }
                else if (PaymentInfo.PaymentMethod == "MASTER")
                {
                    entityId = PaymentGatewayInfo.MasterEntityId;
                }
                else if (PaymentInfo.PaymentMethod == "MADA")
                {
                    entityId = PaymentGatewayInfo.MadaEntityId;
                }
                string CheckoutId = Utility.PaymentGatewayGetCheckoutId(entityId, PaymentInfo.TransactionAmount.Value, PaymentInfo.TransactionCurrency, "DB", "EXTERNAL", PaymentInfo.InvoiceNumber, ShippingBilling.BEmailAddress, ShippingBilling.BAddress1, ShippingBilling.BCity, ShippingBilling.BArea, ShippingBilling.BCountry, MemberInfo.Zip_Postal_Code, ShippingBilling.BFirstName, ShippingBilling.BLastName, PaymentGatewayInfo.BearerToken, PaymentInfo.PaymentMethod);

                model.CheckoutId = CheckoutId;
                model.TransactionAmount = PaymentInfo.TransactionCurrency + " " + string.Format("{0:n2}", PaymentInfo.TransactionAmount.Value);
                model.PaymentMethod = PaymentInfo.PaymentMethod;
                model.PaymentUrl = PaymentIntegrationUrl + "v1/paymentWidgets.js?checkoutId=" + model.CheckoutId;
                model.RedirectUrl = WebConfigurationManager.AppSettings["WebUrl"].ToString() + lang + "/registration/registration-payment-status/";

                return PartialView("Registration/RegistrationPayment", model);
            }
            catch (Exception ex)
            {

            }
                       
            return HttpNotFound();
        }
              
        [HttpGet]
        public ActionResult RegistrationPaymentSuccess()
        {
            try
            {
                RegistrationCheckoutViewModel model = new RegistrationCheckoutViewModel();
                long PaymentId = long.Parse(Utility.GetApplicationCookies("payId").Value);
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                ctRegistrationPayment objDbRegistrationPayment = db.ctRegistrationPayments.Where(x => x.PaymentId == PaymentId).FirstOrDefault();
                ctRegistration objDbRegistration = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                if (objDbRegistrationPayment.TransactionStatus == "Success")
                {
                    model.MembershipType = GetMultilanguageText(objDbRegistration.MembershipType, Utility.GetLanguageFromUrl(Request.Url.ToString()));
                    model.MembershipBadge = GetMultilanguageText(objDbRegistration.MembershipBadge, Utility.GetLanguageFromUrl(Request.Url.ToString()));
                    model.MembershipDiscount = objDbRegistration.MembershipDiscount.Value;
                    model.InvoiceNumber = objDbRegistrationPayment.InvoiceNumber;
                    model.SubTotal = objDbRegistrationPayment.SubTotal.Value;
                    model.Shipping = objDbRegistrationPayment.Shipping.Value;
                    model.VAT = objDbRegistrationPayment.VAT.Value;
                    model.Discount = objDbRegistrationPayment.Discount.Value;
                    model.GrandTotal = objDbRegistrationPayment.GrandTotal.Value;
                    model.TransactionAmount = objDbRegistrationPayment.TransactionAmount.Value;
                    model.TransactionCurrency = objDbRegistrationPayment.TransactionCurrency;

                    if (Request.Cookies["IsNMTM"] != null)
                    {
                        objDbRegistration.ProfileType = "Member";
                        objDbRegistration.IsActive = false;
                        db.Entry(objDbRegistration).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    EmailSettingModel eModel = Utility.GetEmailSetting("MemberRegistration", ProfileId.ToString(), Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString()));

                    string Message = EmailMessage.GetRegistrationMessage(objDbRegistration, "MemberRegistration", "", objDbRegistration.FirstName, Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString()));

                    Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);

                    Utility.ClearRegistrationCookies();
                    Utility.ClearLoginCookies();

                    return PartialView("Registration/RegistrationPaymentSuccess", model);
                }
                else
                {
                    return HttpNotFound();
                }
            }
            catch
            {
                return HttpNotFound();
            }
        }

        [HttpGet]
        public ActionResult RegistrationPaymentFailed()
        {
            return PartialView("Registration/RegistrationPaymentFailed");
        }
               
       
        [HttpPost]
        public ActionResult GetCountry()
        {
            var countryList = db.CountryLists.ToList().OrderBy(x=>x.CountryName);            

            return Json(countryList.ToArray(), JsonRequestBehavior.AllowGet);

        }

        [HttpPost]       
        public ActionResult NonMemberRegistration(RegistrationViewModel model, IEnumerable<HttpPostedFileBase> additionalDocument, HttpPostedFileBase imageUpload)
        {
            try
            {
                ctRegistration objDbModel = new ctRegistration();
                objDbModel.ProfileType = "NonMember";
                objDbModel.ParentProfileId = 0;
                objDbModel.MembershipType = model.MembershipType;
                objDbModel.IIAKSANumber = "";
                objDbModel.FirstName = model.FirstName;
                objDbModel.MiddleName = model.MiddleName;
                objDbModel.LastName = model.LastName;
                objDbModel.Gender = model.Gender;
                objDbModel.DateOfBirth = model.DateOfBirth;
                objDbModel.JobTitle = model.JobTitle;
                objDbModel.Organization = model.Organization;
                objDbModel.Country = model.Country;
                objDbModel.City = model.City;
                objDbModel.Zip_Postal_Code = model.Zip_Postal_Code;
                objDbModel.MobileNumber = model.MobileNumber;
                objDbModel.PhoneNumber = model.PhoneNumber;
                objDbModel.FaxNumber = model.FaxNumber;
                objDbModel.NationalIDNumber = model.NationalIDNumber;
                objDbModel.EmailAddress = model.EmailAddress;
                objDbModel.Password = model.Password;
                objDbModel.IsActive = false;
                objDbModel.IsEmailVerify = true;
                objDbModel.IsApplyForMentor = false;
                objDbModel.IsApproveMentor = false;
                objDbModel.MembershipDiscount = 0;
                objDbModel.LastUpdated = DateTime.Now;
                objDbModel.lang = model.PreferredLanguage;
                objDbModel.FirstName_ar = model.FirstName_ar;
                objDbModel.MiddleName_ar = model.MiddleName_ar;
                objDbModel.LastName_ar = model.LastName_ar;
                objDbModel.JobTitle_ar = model.JobTitle_ar;
                objDbModel.Organization_ar = model.Organization_ar;
                objDbModel.City_ar = model.City_ar;


                if (imageUpload != null)
                {
                    string FileName = Utility.UploadFileInFolder(imageUpload);
                    objDbModel.ProfilePhoto = FileName;
                }

                db.ctRegistrations.Add(objDbModel);
                db.SaveChanges();
                long profileId = objDbModel.ProfileId;

                if (additionalDocument != null)
                {
                    foreach (HttpPostedFileBase file in additionalDocument)
                    {
                        if (file != null)
                        {
                            string FileName = Utility.UploadFileInFolder(file);
                            ctRegistrationDocument objDocument = new ctRegistrationDocument();
                            objDocument.DocumentUrl = FileName;
                            objDocument.ProfileId = profileId;
                            objDocument.LastUpdated = DateTime.Now;

                            db.ctRegistrationDocuments.Add(objDocument);
                            db.SaveChanges();
                        }
                    }
                }

                EmailSettingModel eModel = Utility.GetEmailSetting("NonMemberRegistration", profileId.ToString(), model.lang);

                string Message = EmailMessage.GetRegistrationMessage(objDbModel, "NonMemberRegistration", "", objDbModel.FirstName, model.lang);

                Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);

                if (model.lang == "en")
                {
                    return RedirectToUmbracoPage(1891);
                }
                else
                {
                    return RedirectToUmbracoPage(2708);
                }
            }
            catch (Exception ex)
            {
                if (model.lang == "en")
                {
                    return RedirectToUmbracoPage(1866);
                }
                else
                {
                    return RedirectToUmbracoPage(2734);
                }
            }
        }

        [HttpPost]
        public ActionResult MemberRegistration(RegistrationViewModel model, IEnumerable<HttpPostedFileBase> additionalDocument, HttpPostedFileBase imageUpload)
        {

            try
            {
                if (db.ctRegistrations.Where(x => x.ProfileType == "NonMember" && x.EmailAddress == model.EmailAddress && x.IsActive==true).Count() <= 0)
                {
                    ctRegistration objDbModel = new ctRegistration();
                    objDbModel.ProfileType = "Member";
                    objDbModel.ParentProfileId = 0;
                    objDbModel.MembershipType = model.MembershipType;
                    objDbModel.MembershipBadge = model.MembershipType == "Corporate" ? model.MembershipBadge : "";
                    objDbModel.IIAKSANumber = Utility.GenerateIIAKSANumber("Member");
                    objDbModel.FirstName = model.FirstName;
                    objDbModel.MiddleName = model.MiddleName;
                    objDbModel.LastName = model.LastName;
                    objDbModel.Gender = model.Gender;
                    objDbModel.DateOfBirth = model.DateOfBirth;
                    objDbModel.JobTitle = model.JobTitle;
                    objDbModel.Organization = model.Organization;
                    objDbModel.Country = model.Country;
                    objDbModel.City = model.City;
                    objDbModel.Zip_Postal_Code = model.Zip_Postal_Code;
                    objDbModel.MobileNumber = model.MobileNumber;
                    objDbModel.PhoneNumber = model.PhoneNumber;
                    objDbModel.FaxNumber = model.FaxNumber;
                    objDbModel.NationalIDNumber = model.NationalIDNumber;
                    objDbModel.EmailAddress = model.EmailAddress;
                    objDbModel.Password = model.Password;
                    objDbModel.IsActive = false;
                    objDbModel.IsEmailVerify = true;
                    objDbModel.IsApplyForMentor = false;
                    objDbModel.IsApproveMentor = false;
                    objDbModel.LastUpdated = DateTime.Now;
                    objDbModel.lang = model.PreferredLanguage;
                    objDbModel.FirstName_ar = model.FirstName_ar;
                    objDbModel.MiddleName_ar = model.MiddleName_ar;
                    objDbModel.LastName_ar = model.LastName_ar;
                    objDbModel.JobTitle_ar = model.JobTitle_ar;
                    objDbModel.Organization_ar = model.Organization_ar;
                    objDbModel.City_ar = model.City_ar;
                    objDbModel.GraduationDate = model.GraduationDate;

                    var BecomeQualifiedPage = Umbraco.TypedContent(1810);
                    var MembershipType = BecomeQualifiedPage.Children.Where(x => x.GetPropertyValue("typeOfMembership").ToString() == model.MembershipType).FirstOrDefault();

                    if (model.MembershipType == "Corporate")
                    {
                        if (model.MembershipBadge == "Bronze")
                        {
                            objDbModel.MembershipDiscount = int.Parse(MembershipType.GetPropertyValue("bronzeDiscount").ToString());
                        }
                        else if (model.MembershipBadge == "Silver")
                        {
                            objDbModel.MembershipDiscount = int.Parse(MembershipType.GetPropertyValue("silverDiscount").ToString());
                        }
                        else if (model.MembershipBadge == "Gold")
                        {
                            objDbModel.MembershipDiscount = int.Parse(MembershipType.GetPropertyValue("goldDiscount").ToString());
                        }
                    }
                    else
                    {
                        objDbModel.MembershipDiscount = int.Parse(MembershipType.GetPropertyValue("generalDiscount").ToString());
                    }

                    objDbModel.LastUpdated = DateTime.Now;

                    if (imageUpload != null)
                    {
                        string FileName = Utility.UploadFileInFolder(imageUpload);
                        objDbModel.ProfilePhoto = FileName;
                    }

                    db.ctRegistrations.Add(objDbModel);
                    db.SaveChanges();
                    long profileId = objDbModel.ProfileId;

                    Utility.SetApplicationCookies("proId", profileId.ToString());
                    Utility.SetApplicationCookies("IsMR", "true");

                    if (additionalDocument != null)
                    {
                        foreach (HttpPostedFileBase file in additionalDocument)
                        {
                            if (file != null)
                            {
                                string FileName = Utility.UploadFileInFolder(file);
                                ctRegistrationDocument objDocument = new ctRegistrationDocument();
                                objDocument.DocumentUrl = FileName;
                                objDocument.ProfileId = profileId;
                                objDocument.LastUpdated = DateTime.Now;

                                db.ctRegistrationDocuments.Add(objDocument);
                                db.SaveChanges();
                            }
                        }
                    }

                }
                else
                {
                    ctRegistration objDbModel = db.ctRegistrations.Where(x => x.ProfileType == "NonMember" && x.EmailAddress == model.EmailAddress && x.IsActive == true).FirstOrDefault();
                    objDbModel.ParentProfileId = 0;
                    objDbModel.MembershipType = model.MembershipType;
                    objDbModel.MembershipBadge = model.MembershipType == "Corporate" ? model.MembershipBadge : "";
                    objDbModel.IIAKSANumber = Utility.GenerateIIAKSANumber("Member");
                    objDbModel.FirstName = model.FirstName;
                    objDbModel.MiddleName = model.MiddleName;
                    objDbModel.LastName = model.LastName;
                    objDbModel.Gender = model.Gender;
                    objDbModel.DateOfBirth = model.DateOfBirth;
                    objDbModel.JobTitle = model.JobTitle;
                    objDbModel.Organization = model.Organization;
                    objDbModel.Country = model.Country;
                    objDbModel.City = model.City;
                    objDbModel.Zip_Postal_Code = model.Zip_Postal_Code;
                    objDbModel.MobileNumber = model.MobileNumber;
                    objDbModel.PhoneNumber = model.PhoneNumber;
                    objDbModel.FaxNumber = model.FaxNumber;
                    objDbModel.NationalIDNumber = model.NationalIDNumber;                   
                    objDbModel.Password = model.Password;                    
                    objDbModel.IsEmailVerify = true;
                    objDbModel.IsApplyForMentor = false;
                    objDbModel.IsApproveMentor = false;
                    objDbModel.LastUpdated = DateTime.Now;
                    objDbModel.lang = model.PreferredLanguage;
                    objDbModel.FirstName_ar = model.FirstName_ar;
                    objDbModel.MiddleName_ar = model.MiddleName_ar;
                    objDbModel.LastName_ar = model.LastName_ar;
                    objDbModel.JobTitle_ar = model.JobTitle_ar;
                    objDbModel.Organization_ar = model.Organization_ar;
                    objDbModel.City_ar = model.City_ar;
                    objDbModel.GraduationDate = model.GraduationDate;

                    var BecomeQualifiedPage = Umbraco.TypedContent(1810);
                    var MembershipType = BecomeQualifiedPage.Children.Where(x => x.GetPropertyValue("typeOfMembership").ToString() == model.MembershipType).FirstOrDefault();

                    if (model.MembershipType == "Corporate")
                    {
                        if (model.MembershipBadge == "Bronze")
                        {
                            objDbModel.MembershipDiscount = int.Parse(MembershipType.GetPropertyValue("bronzeDiscount").ToString());
                        }
                        else if (model.MembershipBadge == "Silver")
                        {
                            objDbModel.MembershipDiscount = int.Parse(MembershipType.GetPropertyValue("silverDiscount").ToString());
                        }
                        else if (model.MembershipBadge == "Gold")
                        {
                            objDbModel.MembershipDiscount = int.Parse(MembershipType.GetPropertyValue("goldDiscount").ToString());
                        }
                    }
                    else
                    {
                        objDbModel.MembershipDiscount = int.Parse(MembershipType.GetPropertyValue("generalDiscount").ToString());
                    }

                    objDbModel.LastUpdated = DateTime.Now;

                    if (imageUpload != null)
                    {
                        string FileName = Utility.UploadFileInFolder(imageUpload);
                        objDbModel.ProfilePhoto = FileName;
                    }

                    db.Entry(objDbModel).State = EntityState.Modified;
                    db.SaveChanges();
                    long profileId = objDbModel.ProfileId;

                    Utility.SetApplicationCookies("proId", profileId.ToString());
                    Utility.SetApplicationCookies("IsMR", "true");
                    Utility.SetApplicationCookies("IsNMTM", "true");

                    if (additionalDocument != null)
                    {
                        foreach (HttpPostedFileBase file in additionalDocument)
                        {
                            if (file != null)
                            {
                                string FileName = Utility.UploadFileInFolder(file);
                                ctRegistrationDocument objDocument = new ctRegistrationDocument();
                                objDocument.DocumentUrl = FileName;
                                objDocument.ProfileId = profileId;
                                objDocument.LastUpdated = DateTime.Now;

                                db.ctRegistrationDocuments.Add(objDocument);
                                db.SaveChanges();
                            }
                        }
                    }

                }               

                if (model.lang == "en")
                {
                    return RedirectToUmbracoPage(1868);
                }
                else
                {
                    return RedirectToUmbracoPage(2713);
                }
            }
            catch (Exception ex)
            {
                if (model.lang == "en")
                {
                    return RedirectToUmbracoPage(1866);
                }
                else
                {
                    return RedirectToUmbracoPage(2734);
                }
            }

        }

        [HttpPost]
        public ActionResult ShippingBilling(ShippingBillingViewModel model)
        {
            try
            {
                long profileId = long.Parse(Utility.GetApplicationCookies("proId").Value);

                ctShippingBilling objExisting = db.ctShippingBillings.Where(x => x.ProfileId == profileId).FirstOrDefault();

                if (objExisting == null)
                {
                    ctShippingBilling objDbModel = new ctShippingBilling();
                    objDbModel.ProfileId = profileId;
                    objDbModel.BFirstName = model.BFirstName;
                    objDbModel.BMiddleName = model.BMiddleName;
                    objDbModel.BLastName = model.BLastName;
                    objDbModel.BEmailAddress = model.BEmailAddress;
                    objDbModel.BMobileNumber = model.BMobileNumber;
                    objDbModel.BPhoneNumber = model.BPhoneNumber;
                    objDbModel.BCountry = model.BCountry;
                    objDbModel.BCity = model.BCity;
                    objDbModel.BArea = model.BArea;
                    objDbModel.BAddress1 = model.BAddress1;
                    objDbModel.BAddress2 = model.BAddress2;
                    objDbModel.BAddress3 = model.BAddress3;
                    objDbModel.BillingType = model.BillingType;

                    objDbModel.SFirstName = model.SFirstName;
                    objDbModel.SMiddleName = model.SMiddleName;
                    objDbModel.SLastName = model.SLastName;
                    objDbModel.SEmailAddress = model.SEmailAddress;
                    objDbModel.SMobileNumber = model.SMobileNumber;
                    objDbModel.SPhoneNumber = model.SPhoneNumber;
                    objDbModel.SCountry = model.SCountry;
                    objDbModel.SCity = model.SCity;
                    objDbModel.SArea = model.SArea;
                    objDbModel.SAddress1 = model.SAddress1;
                    objDbModel.SAddress2 = model.SAddress2;
                    objDbModel.SAddress3 = model.SAddress3;
                    objDbModel.ShippingType = model.ShippingType;
                    objDbModel.LastUpdated = DateTime.Now;
                    db.ctShippingBillings.Add(objDbModel);
                    db.SaveChanges();
                }
                else
                {                    
                    objExisting.BFirstName = model.BFirstName;
                    objExisting.BMiddleName = model.BMiddleName;
                    objExisting.BLastName = model.BLastName;
                    objExisting.BEmailAddress = model.BEmailAddress;
                    objExisting.BMobileNumber = model.BMobileNumber;
                    objExisting.BPhoneNumber = model.BPhoneNumber;
                    objExisting.BCountry = model.BCountry;
                    objExisting.BCity = model.BCity;
                    objExisting.BArea = model.BArea;
                    objExisting.BAddress1 = model.BAddress1;
                    objExisting.BAddress2 = model.BAddress2;
                    objExisting.BAddress3 = model.BAddress3;
                    objExisting.BillingType = model.BillingType;

                    objExisting.SFirstName = model.SFirstName;
                    objExisting.SMiddleName = model.SMiddleName;
                    objExisting.SLastName = model.SLastName;
                    objExisting.SEmailAddress = model.SEmailAddress;
                    objExisting.SMobileNumber = model.SMobileNumber;
                    objExisting.SPhoneNumber = model.SPhoneNumber;
                    objExisting.SCountry = model.SCountry;
                    objExisting.SCity = model.SCity;
                    objExisting.SArea = model.SArea;
                    objExisting.SAddress1 = model.SAddress1;
                    objExisting.SAddress2 = model.SAddress2;
                    objExisting.SAddress3 = model.SAddress3;
                    objExisting.ShippingType = model.ShippingType;
                    objExisting.LastUpdated = DateTime.Now;
                    db.Entry(objExisting).State = EntityState.Modified;
                    db.SaveChanges();
                }

                if (model.lang == "en")
                {
                    if(Utility.GetApplicationCookies("IsMR")!=null)
                    {
                        return RedirectToUmbracoPage(1870);
                    }
                    else
                    {
                        return RedirectToUmbracoPage(2029);
                    }
                   
                }
                else
                {
                    if (Utility.GetApplicationCookies("IsMR") != null)
                    {
                        return RedirectToUmbracoPage(2703);
                    }
                    else
                    {
                        return RedirectToUmbracoPage(2714);
                    }
                }
            }
            catch (Exception ex)
            {
                if (model.lang == "en")
                {
                    return RedirectToUmbracoPage(1866);
                }
                else
                {
                    return RedirectToUmbracoPage(2734);
                }
            }

        }

        //[HttpPost]
        //public ActionResult RegistrationPayment(string lang)
        //{
        //    try
        //    {
        //        if (lang == "en")
        //        {
        //            return RedirectToUmbracoPage(1867);
        //        }
        //        else
        //        {
        //            return RedirectToUmbracoPage(1867);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (lang == "en")
        //        {
        //            return RedirectToUmbracoPage(1866);
        //        }
        //        else
        //        {
        //            return RedirectToUmbracoPage(1866);
        //        }
        //    }

        //}

        [HttpPost]
        public ActionResult RegistrationCheckout(RegistrationCheckoutViewModel model)
        {
            try
            {
                ctRegistrationPayment dbModel = new ctRegistrationPayment();

                long profileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                var regData = db.ctRegistrations.Where(x => x.ProfileId == profileId).FirstOrDefault();
                var BecomeQualifiedPage = Umbraco.TypedContent(1810);
                var MembershipType = BecomeQualifiedPage.Children.Where(x => x.GetPropertyValue("typeOfMembership").ToString() == regData.MembershipType).FirstOrDefault();
                if (regData.MembershipType == "Corporate")
                {
                    if (regData.MembershipBadge == "Bronze")
                    {
                        dbModel.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("bronzeFee").ToString());
                    }
                    else if (regData.MembershipBadge == "Silver")
                    {
                        dbModel.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("silverFee").ToString());
                    }
                    else if (regData.MembershipBadge == "Gold")
                    {
                        dbModel.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("goldFee").ToString());
                    }
                }
                else
                {
                    dbModel.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("generalFee").ToString());
                }


                PromoCodeValidation promoCodeValidation = NewRegistrationGetPromoValue(model.lang, model.PromoCode, dbModel.SubTotal.Value);

                dbModel.Shipping = 0;
                dbModel.Discount =  (string.IsNullOrEmpty(promoCodeValidation.Message) ? promoCodeValidation.Value : 0);
                dbModel.VAT = ((dbModel.SubTotal + dbModel.Shipping - dbModel.Discount) * VAT) / 100;
                dbModel.GrandTotal = (dbModel.SubTotal + dbModel.Shipping + dbModel.VAT - dbModel.Discount);
                dbModel.InvoiceNumber = Utility.GenerateMemberInvoiceNumber();
                dbModel.ProfileId = profileId;
                dbModel.TransactionAmount = decimal.Parse((double.Parse(dbModel.GrandTotal.ToString()) * CurrencyRate).ToString());
                dbModel.TransactionCurrency = Currency;
                dbModel.PaymentMethod = model.PaymentMethod;
                dbModel.TransactionDate = DateTime.Now;
                dbModel.PromoCode = (string.IsNullOrEmpty(promoCodeValidation.Message) ? model.PromoCode : "");
                db.ctRegistrationPayments.Add(dbModel);
                db.SaveChanges();
                Utility.SetApplicationCookies("payId", dbModel.PaymentId.ToString());

                if (model.lang == "en")
                {
                    return RedirectToUmbracoPage(1869);
                }
                else
                {
                    return RedirectToUmbracoPage(2704);
                }
            }
            catch (Exception ex)
            {
                if (model.lang == "en")
                {
                    return RedirectToUmbracoPage(1866);
                }
                else
                {
                    return RedirectToUmbracoPage(2734);
                }
            }

        }

        [HttpPost]
        public ActionResult Login(FormCollection form)
        {
            bool Status = true;
            string Message = "";
            var EmailAddress = form["EmailAddress"];
            var Password = form["Password"];
            var lang = form["lang"];
            var returnPageUrl = lang == "en" ? "/en/my-account/user-profile/" : "/ar/my-account/user-profile/";

            try
            {

                var AppLogin = db.ctRegistrations.Where(x => x.EmailAddress == EmailAddress && x.Password == Password).FirstOrDefault();

                if (AppLogin != null)
                {
                    ///if (AppLogin.IsEmailVerify == true)
                    //{
                    if (AppLogin.IsActive == true)
                    {
                        Utility.ClearRegistrationCookies();
                        Utility.ClearPurchaseCookies();
                        Utility.ClearLoginCookies();

                        Utility.SetApplicationCookies("proId", AppLogin.ProfileId.ToString());
                        Utility.SetApplicationCookies("IsLogin", "true");

                        if (Utility.GetApplicationCookies("backUrl") != null)
                        {
                            if (Utility.GetApplicationCookies("backUrl").Value != "")
                            {
                                returnPageUrl = Utility.GetApplicationCookies("backUrl").Value;
                            }
                        }
                    }
                    else
                    {
                        Status = true;
                        Message = "Account_Not_Activated";
                    }
                    //}
                    //else
                    //{
                    //    Status = false;
                    //    Message = "Sorry, your email address is not verify yet.";
                    //}
                }
                else
                {
                    Status = false;
                    Message = "Invalid_Credential";
                }
            }
            catch (Exception ex)
            {
                Status = false;
                Message = ex.Message;               
            }
            return Json(new
            {
                status = Status,
                message = Message,
                returnUrl = returnPageUrl
            }, JsonRequestBehavior.AllowGet);
        }
               
               
        [HttpPost]
        public ActionResult CheckExists(string email)
        {
            
            if (db.ctRegistrations.Where(x => x.EmailAddress == email).Count() > 0)
            {
                return Json(new
                {
                    status = true                   
                }, JsonRequestBehavior.AllowGet);
            }
            return Json(new
            {
                status = false,
                message = ""
            }, JsonRequestBehavior.AllowGet);
        }
              
        [HttpPost]
        public ActionResult GetShippingBilling(string lang)
        {
            if(Utility.GetApplicationCookies("proId")!=null)
            {
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                var ShippingBilling = db.ctShippingBillings.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                return Json(new { status = true, ShippingBilling }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = false}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ForgotPassword(string email, string lang)
        {                   
            try
            {
                var ctRegistration = db.ctRegistrations.Where(x => x.EmailAddress == email).FirstOrDefault();

                if (ctRegistration != null)
                {
                    var Key = Guid.NewGuid().ToString();
                    ctTempKey dbTempKey = new ctTempKey();
                    dbTempKey.ModuleType = "ForgotPassword";
                    dbTempKey.TempKey = Key;
                    dbTempKey.ModuleId = ctRegistration.ProfileId.ToString();
                    dbTempKey.GeneratedTime = DateTime.Now;
                    db.ctTempKeys.Add(dbTempKey);
                    db.SaveChanges();

                    string Url = WebConfigurationManager.AppSettings["WebUrl"].ToString() + lang + "/reset-password/?key=" + Key;

                    EmailSettingModel eModel = Utility.GetEmailSetting("ForgotPassword", ctRegistration.ProfileId.ToString(), lang);

                    string Message = EmailMessage.GetRegistrationMessage(ctRegistration, "ForgotPassword", Url, ctRegistration.FirstName, lang);

                    Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);
                    
                    return Json(new
                    {
                        status = true                       
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = false                       
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }

            return HttpNotFound();
        }


        [HttpPost]
        public ActionResult CheckResetPasswordLink(string key)
        {
            try
            {
                ctTempKey ctTempKey = db.ctTempKeys.Where(x => x.TempKey == key).FirstOrDefault();
                if (ctTempKey != null)
                {
                    return Json(new
                    {
                        status = true
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }            
        }


        [HttpPost]
        public ActionResult ResetPassword(string key, string newPassword)
        {
            try
            {
                ctTempKey ctTempKey = db.ctTempKeys.Where(x => x.TempKey == key).FirstOrDefault();
                if (ctTempKey != null)
                {
                    var ProfileId = long.Parse(ctTempKey.ModuleId);
                    ctRegistration ctRegistration = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                    ctRegistration.Password = newPassword;
                    db.Entry(ctRegistration).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json(new
                    {
                        status = true
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult NewsletterSubscribe(string email)
        {
            try
            {
                NewsletterSubscribe model = new NewsletterSubscribe();
                model.EmailAddress = email;
                model.SubscriptionDate = DateTime.Now;
                db.NewsletterSubscribes.Add(model);
                db.SaveChanges();

                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult MembershipRenewalCheckout()
        {
            try
            {
                long profileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                RegistrationCheckoutViewModel model = new RegistrationCheckoutViewModel();
                var regData = db.ctRegistrations.Where(x => x.ProfileId == profileId).FirstOrDefault();
                var BecomeQualifiedPage = Umbraco.TypedContent(1810);
                var MembershipType = BecomeQualifiedPage.Children.Where(x => x.GetPropertyValue("typeOfMembership").ToString() == regData.MembershipType).FirstOrDefault();
                model.MembershipType = GetMultilanguageText(regData.MembershipType, Utility.GetLanguageFromUrl(Request.Url.ToString()));
                model.MembershipBadge = GetMultilanguageText(regData.MembershipBadge, Utility.GetLanguageFromUrl(Request.Url.ToString()));
                model.MembershipDiscount = regData.MembershipDiscount.Value;

                if (regData.MembershipType == "Corporate")
                {
                    if (regData.MembershipBadge == "Bronze")
                    {
                        model.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("bronzeFee").ToString());
                    }
                    else if (regData.MembershipBadge == "Silver")
                    {
                        model.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("silverFee").ToString());
                    }
                    else if (regData.MembershipBadge == "Gold")
                    {
                        model.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("goldFee").ToString());
                    }
                }
                else
                {
                    model.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("generalFee").ToString());
                }

                model.Shipping = 0;
                model.Discount = 0;
                model.VAT = ((model.SubTotal + model.Shipping - model.Discount) * VAT) / 100;
                model.GrandTotal = (model.SubTotal + model.VAT + model.Shipping - model.Discount);                

                return PartialView("Registration/MembershipRenewalCheckout", model);
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }
        [HttpPost]
        public ActionResult MembershipRenewalCheckout(RegistrationCheckoutViewModel model)
        {
            try
            {
                ctRegistrationPayment dbModel = new ctRegistrationPayment();

                long profileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                var regData = db.ctRegistrations.Where(x => x.ProfileId == profileId).FirstOrDefault();
                var BecomeQualifiedPage = Umbraco.TypedContent(1810);
                var MembershipType = BecomeQualifiedPage.Children.Where(x => x.GetPropertyValue("typeOfMembership").ToString() == regData.MembershipType).FirstOrDefault();
                if (regData.MembershipType == "Corporate")
                {
                    if (regData.MembershipBadge == "Bronze")
                    {
                        dbModel.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("bronzeFee").ToString());
                    }
                    else if (regData.MembershipBadge == "Silver")
                    {
                        dbModel.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("silverFee").ToString());
                    }
                    else if (regData.MembershipBadge == "Gold")
                    {
                        dbModel.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("goldFee").ToString());
                    }
                }
                else
                {
                    dbModel.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("generalFee").ToString());
                }

                PromoCodeValidation promoCodeValidation = RenewalRegistrationGetPromoValue(model.lang, model.PromoCode, dbModel.SubTotal.Value);

                dbModel.Shipping = 0;
                dbModel.Discount = (string.IsNullOrEmpty(promoCodeValidation.Message) ? promoCodeValidation.Value : 0);
                dbModel.VAT = ((dbModel.SubTotal + dbModel.Shipping - dbModel.Discount) * VAT) / 100;
                dbModel.GrandTotal = (dbModel.SubTotal + dbModel.Shipping + dbModel.VAT - dbModel.Discount);
                dbModel.InvoiceNumber = Utility.GenerateMemberInvoiceNumber();
                dbModel.ProfileId = profileId;
                dbModel.TransactionAmount = decimal.Parse((double.Parse(dbModel.GrandTotal.ToString()) * CurrencyRate).ToString());
                dbModel.TransactionCurrency = Currency;
                dbModel.PaymentMethod = model.PaymentMethod;
                dbModel.TransactionDate = DateTime.Now;
                dbModel.PromoCode = (string.IsNullOrEmpty(promoCodeValidation.Message) ? model.PromoCode : "");
                db.ctRegistrationPayments.Add(dbModel);
                db.SaveChanges();
                Utility.SetApplicationCookies("payId", dbModel.PaymentId.ToString());

                if (model.lang == "en")
                {
                    return RedirectToUmbracoPage(PaymentIntegrationMode == "Test"? 3391 : 3425);
                }
                else
                {
                    return RedirectToUmbracoPage(PaymentIntegrationMode == "Test" ? 3399 : 3433);
                }
            }
            catch (Exception ex)
            {
                if (model.lang == "en")
                {
                    return RedirectToUmbracoPage(1866);
                }
                else
                {
                    return RedirectToUmbracoPage(2734);
                }
            }

        }
        [HttpGet]
        public ActionResult MembershipRenewalPayment()
        {
            try
            {
                string CheckoutId = "";
                string lang = Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString());
                PaymentGateayStep1Model model = new PaymentGateayStep1Model();

                long PaymentId = long.Parse(Utility.GetApplicationCookies("payId").Value);
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);

                var MemberInfo = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                var ShippingBilling = db.ctShippingBillings.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                var PaymentInfo = db.ctRegistrationPayments.Where(x => x.PaymentId == PaymentId).FirstOrDefault();
                var PaymentGatewayInfo = db.ctPaymentGatewayInfoes.Where(x => x.IntegrationMode == PaymentIntegrationMode).FirstOrDefault();
                var entityId = "";
                if (PaymentInfo.PaymentMethod == "VISA")
                {
                    entityId = PaymentGatewayInfo.VIsaEntityId;
                }
                else if (PaymentInfo.PaymentMethod == "MASTER")
                {
                    entityId = PaymentGatewayInfo.MasterEntityId;
                }
                else if (PaymentInfo.PaymentMethod == "MADA")
                {
                    entityId = PaymentGatewayInfo.MadaEntityId;
                }

                if (ShippingBilling != null)
                {
                    CheckoutId = Utility.PaymentGatewayGetCheckoutId(entityId, PaymentInfo.TransactionAmount.Value, PaymentInfo.TransactionCurrency, "DB", "EXTERNAL", PaymentInfo.InvoiceNumber, ShippingBilling.BEmailAddress, ShippingBilling.BAddress1, ShippingBilling.BCity, ShippingBilling.BArea, ShippingBilling.BCountry, MemberInfo.Zip_Postal_Code, ShippingBilling.BFirstName, ShippingBilling.BLastName, PaymentGatewayInfo.BearerToken, PaymentInfo.PaymentMethod);
                }
                else
                {
                    CheckoutId = Utility.PaymentGatewayGetCheckoutId(entityId, PaymentInfo.TransactionAmount.Value, PaymentInfo.TransactionCurrency, "DB", "EXTERNAL", PaymentInfo.InvoiceNumber, MemberInfo.EmailAddress, MemberInfo.City, MemberInfo.City, MemberInfo.City, MemberInfo.Country, MemberInfo.Zip_Postal_Code, MemberInfo.FirstName, MemberInfo.LastName, PaymentGatewayInfo.BearerToken, PaymentInfo.PaymentMethod);
                }
                model.CheckoutId = CheckoutId;
                model.TransactionAmount = PaymentInfo.TransactionCurrency + " " + string.Format("{0:n2}", PaymentInfo.TransactionAmount.Value);
                model.PaymentMethod = PaymentInfo.PaymentMethod;
                model.PaymentUrl = PaymentIntegrationUrl + "v1/paymentWidgets.js?checkoutId=" + model.CheckoutId;
                model.RedirectUrl = WebConfigurationManager.AppSettings["WebUrl"].ToString() + lang + "/registration/membership-renewal-payment-status/";

                return PartialView("Registration/MembershipRenewalPayment", model);
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult MembershipRenewalPaymentSuccess()
        {
            try
            {
                RegistrationCheckoutViewModel model = new RegistrationCheckoutViewModel();
                long PaymentId = long.Parse(Utility.GetApplicationCookies("payId").Value);
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                ctRegistrationPayment objDbRegistrationPayment = db.ctRegistrationPayments.Where(x => x.PaymentId == PaymentId).FirstOrDefault();
                ctRegistration objDbRegistration = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                if (objDbRegistrationPayment.TransactionStatus == "Success")
                {
                    model.MembershipType = GetMultilanguageText(objDbRegistration.MembershipType, Utility.GetLanguageFromUrl(Request.Url.ToString()));
                    model.MembershipBadge = GetMultilanguageText(objDbRegistration.MembershipBadge, Utility.GetLanguageFromUrl(Request.Url.ToString()));
                    model.MembershipDiscount = objDbRegistration.MembershipDiscount.Value;
                    model.InvoiceNumber = objDbRegistrationPayment.InvoiceNumber;
                    model.SubTotal = objDbRegistrationPayment.SubTotal.Value;
                    model.Shipping = objDbRegistrationPayment.Shipping.Value;
                    model.VAT = objDbRegistrationPayment.VAT.Value;
                    model.Discount = objDbRegistrationPayment.Discount.Value;
                    model.GrandTotal = objDbRegistrationPayment.GrandTotal.Value;
                    model.TransactionAmount = objDbRegistrationPayment.TransactionAmount.Value;
                    model.TransactionCurrency = objDbRegistrationPayment.TransactionCurrency;

                    UpdateMembershipDate(ProfileId);

                    EmailSettingModel eModel = Utility.GetEmailSetting("MembershipRenewal", ProfileId.ToString(), Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString()));

                    string Message = EmailMessage.GetRegistrationMessage(objDbRegistration, "MembershipRenewal", "", objDbRegistration.FirstName, Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString()));

                    Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);

                    Utility.ClearRenewalMembershipCookies();

                    return PartialView("Registration/MembershipRenewalSuccess", model);
                }
                else
                {
                    return HttpNotFound();
                }
            }
            catch
            {
                return HttpNotFound();
            }
        }

        private void UpdateMembershipDate(long ProfileId)
        {
            ctRegistration objDbRegistration = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
            DateTime MembershipEndDate;
            if(objDbRegistration.MembershipEndDate>DateTime.Now)
            {
                MembershipEndDate= objDbRegistration.MembershipEndDate.Value.AddYears(1);
            }
            else
            {
                MembershipEndDate = DateTime.Now.AddYears(1);
            }
            objDbRegistration.MembershipEndDate = MembershipEndDate;
            objDbRegistration.MembershipRenewalDate = DateTime.Now;
            db.Entry(objDbRegistration).State = EntityState.Modified;
            db.SaveChanges();

            try
            {
                var ListSubMember = db.ctRegistrations.Where(x => x.ParentProfileId == ProfileId && x.IsActive == true).ToList();
                if (ListSubMember != null)
                {
                    if (ListSubMember.Count > 0)
                    {
                        foreach (var member in ListSubMember)
                        {
                            member.MembershipEndDate = MembershipEndDate;
                            member.MembershipRenewalDate = DateTime.Now;
                            db.Entry(member).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch
            {

            }
        }

        [HttpGet]
        public ActionResult MembershipRenewalPaymentFailed()
        {
            return PartialView("Registration/MembershipRenewalFailed");
        }

        public  string GetMultilanguageText(string text, string lang)
        {
            string retText = text;

            if (text == "Students" && lang == "ar")
            {
                retText = "الطلاب";
            }
            else if (text == "Individuals" && lang == "ar")
            {
                retText = "الأفراد";
            }
            else if (text == "Corporate" && lang == "ar")
            {
                retText = "المؤسسة";
            }
            else if (text == "Gold" && lang == "ar")
            {
                retText = "الذهبية";
            }
            else if (text == "Silver" && lang == "ar")
            {
                retText = "الفضية";
            }
            else if (text == "Bronze" && lang == "ar")
            {
                retText = "البرونزية";
            }
            else if (text == "Bronze" && lang == "ar")
            {
                retText = "البرونزية";
            }
            else if (text == "Professional" && lang == "ar")
            {
                retText = "متخصص";
            }
            return retText;
        }

        [HttpGet]
        public ActionResult MembershipUpgradeCheckout()
        {
            try
            {
                long profileId = long.Parse(Utility.GetApplicationCookies("proId").Value);

                if(db.ctProfileUpgrades.Where(x=>x.ProfileId==profileId).Count()>0)
                {
                    var profileData = db.ctProfileUpgrades.Where(x => x.ProfileId == profileId).FirstOrDefault();
                    db.ctProfileUpgrades.Remove(profileData);
                    db.SaveChanges();                    
                }

                var BecomeQualifiedPage = Umbraco.TypedContent(1810);
                var MembershipType = BecomeQualifiedPage.Children.Where(x => x.GetPropertyValue("typeOfMembership").ToString() == "Individuals").FirstOrDefault();

                ctProfileUpgrade ctProfileUpgrade = new ctProfileUpgrade();
                ctProfileUpgrade.ProfileId = profileId;
                ctProfileUpgrade.MembershipType = "Individuals";
                ctProfileUpgrade.MembershipBadge = "";
                ctProfileUpgrade.MembershipDiscount= int.Parse(MembershipType.GetPropertyValue("generalDiscount").ToString());
                ctProfileUpgrade.LastUpdated = DateTime.Now;
                db.ctProfileUpgrades.Add(ctProfileUpgrade);
                db.SaveChanges();

                RegistrationCheckoutViewModel model = new RegistrationCheckoutViewModel();
                var regData = db.ctProfileUpgrades.Where(x => x.ProfileId == profileId).FirstOrDefault();
               
                model.MembershipType = GetMultilanguageText(regData.MembershipType, Utility.GetLanguageFromUrl(Request.Url.ToString()));
                model.MembershipBadge = GetMultilanguageText(regData.MembershipBadge, Utility.GetLanguageFromUrl(Request.Url.ToString()));
                
                if (regData.MembershipType == "Corporate")
                {
                    if (regData.MembershipBadge == "Bronze")
                    {
                        model.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("bronzeFee").ToString());                        
                    }
                    else if (regData.MembershipBadge == "Silver")
                    {
                        model.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("silverFee").ToString());
                    }
                    else if (regData.MembershipBadge == "Gold")
                    {
                        model.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("goldFee").ToString());
                    }
                }
                else
                {
                    model.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("generalFee").ToString());
                }

                model.Shipping = 0;
                model.Discount = 0;
                model.VAT = ((model.SubTotal + model.Shipping - model.Discount) * VAT) / 100;
                model.GrandTotal = (model.SubTotal + model.VAT + model.Shipping - model.Discount);

                return PartialView("Registration/MembershipUpgradeCheckout", model);
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }
        [HttpPost]
        public ActionResult MembershipUpgradeCheckout(RegistrationCheckoutViewModel model)
        {
            try
            {
                ctRegistrationPayment dbModel = new ctRegistrationPayment();

                long profileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                var regData = db.ctProfileUpgrades.Where(x => x.ProfileId == profileId).FirstOrDefault();
                var BecomeQualifiedPage = Umbraco.TypedContent(1810);
                var MembershipType = BecomeQualifiedPage.Children.Where(x => x.GetPropertyValue("typeOfMembership").ToString() == regData.MembershipType).FirstOrDefault();
                if (regData.MembershipType == "Corporate")
                {
                    if (regData.MembershipBadge == "Bronze")
                    {
                        dbModel.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("bronzeFee").ToString());
                    }
                    else if (regData.MembershipBadge == "Silver")
                    {
                        dbModel.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("silverFee").ToString());
                    }
                    else if (regData.MembershipBadge == "Gold")
                    {
                        dbModel.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("goldFee").ToString());
                    }
                }
                else
                {
                    dbModel.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("generalFee").ToString());
                }

                PromoCodeValidation promoCodeValidation = RenewalRegistrationGetPromoValue(model.lang, model.PromoCode, dbModel.SubTotal.Value);

                dbModel.Shipping = 0;
                dbModel.Discount = (string.IsNullOrEmpty(promoCodeValidation.Message) ? promoCodeValidation.Value : 0);
                dbModel.VAT = ((dbModel.SubTotal + dbModel.Shipping - dbModel.Discount) * VAT) / 100;
                dbModel.GrandTotal = (dbModel.SubTotal + dbModel.Shipping + dbModel.VAT - dbModel.Discount);
                dbModel.InvoiceNumber = Utility.GenerateMemberInvoiceNumber();
                dbModel.ProfileId = profileId;
                dbModel.TransactionAmount = decimal.Parse((double.Parse(dbModel.GrandTotal.ToString()) * CurrencyRate).ToString());
                dbModel.TransactionCurrency = Currency;
                dbModel.PaymentMethod = model.PaymentMethod;
                dbModel.TransactionDate = DateTime.Now;
                dbModel.PromoCode = (string.IsNullOrEmpty(promoCodeValidation.Message) ? model.PromoCode : "");
                db.ctRegistrationPayments.Add(dbModel);
                db.SaveChanges();
                Utility.SetApplicationCookies("payId", dbModel.PaymentId.ToString());

                if (model.lang == "en")
                {
                    return RedirectToUmbracoPage(PaymentIntegrationMode == "Test" ? 3425 : 3480);
                }
                else
                {
                    return RedirectToUmbracoPage(PaymentIntegrationMode == "Test" ? 3435 : 3489);
                }
            }
            catch (Exception ex)
            {
                if (model.lang == "en")
                {
                    return RedirectToUmbracoPage(1866);
                }
                else
                {
                    return RedirectToUmbracoPage(2734);
                }
            }

        }
        [HttpGet]
        public ActionResult MembershipUpgradePayment()
        {
            try
            {
                string CheckoutId = "";
                string lang = Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString());
                PaymentGateayStep1Model model = new PaymentGateayStep1Model();

                long PaymentId = long.Parse(Utility.GetApplicationCookies("payId").Value);
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);

                var MemberInfo = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                var ShippingBilling = db.ctShippingBillings.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                var PaymentInfo = db.ctRegistrationPayments.Where(x => x.PaymentId == PaymentId).FirstOrDefault();
                var PaymentGatewayInfo = db.ctPaymentGatewayInfoes.Where(x => x.IntegrationMode == PaymentIntegrationMode).FirstOrDefault();
                var entityId = "";
                if (PaymentInfo.PaymentMethod == "VISA")
                {
                    entityId = PaymentGatewayInfo.VIsaEntityId;
                }
                else if (PaymentInfo.PaymentMethod == "MASTER")
                {
                    entityId = PaymentGatewayInfo.MasterEntityId;
                }
                else if (PaymentInfo.PaymentMethod == "MADA")
                {
                    entityId = PaymentGatewayInfo.MadaEntityId;
                }

                if (ShippingBilling != null)
                {
                    CheckoutId = Utility.PaymentGatewayGetCheckoutId(entityId, PaymentInfo.TransactionAmount.Value, PaymentInfo.TransactionCurrency, "DB", "EXTERNAL", PaymentInfo.InvoiceNumber, ShippingBilling.BEmailAddress, ShippingBilling.BAddress1, ShippingBilling.BCity, ShippingBilling.BArea, ShippingBilling.BCountry, MemberInfo.Zip_Postal_Code, ShippingBilling.BFirstName, ShippingBilling.BLastName, PaymentGatewayInfo.BearerToken, PaymentInfo.PaymentMethod);
                }
                else
                {
                    CheckoutId = Utility.PaymentGatewayGetCheckoutId(entityId, PaymentInfo.TransactionAmount.Value, PaymentInfo.TransactionCurrency, "DB", "EXTERNAL", PaymentInfo.InvoiceNumber, MemberInfo.EmailAddress, MemberInfo.City, MemberInfo.City, MemberInfo.City, MemberInfo.Country, MemberInfo.Zip_Postal_Code, MemberInfo.FirstName, MemberInfo.LastName, PaymentGatewayInfo.BearerToken, PaymentInfo.PaymentMethod);
                }
                model.CheckoutId = CheckoutId;
                model.TransactionAmount = PaymentInfo.TransactionCurrency + " " + string.Format("{0:n2}", PaymentInfo.TransactionAmount.Value);
                model.PaymentMethod = PaymentInfo.PaymentMethod;
                model.PaymentUrl = PaymentIntegrationUrl + "v1/paymentWidgets.js?checkoutId=" + model.CheckoutId;
                model.RedirectUrl = WebConfigurationManager.AppSettings["WebUrl"].ToString() + lang + "/registration/membership-upgrade-payment-status/";

                return PartialView("Registration/MembershipUpgradePayment", model);
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult MembershipUpgradePaymentSuccess()
        {
            try
            {
                RegistrationCheckoutViewModel model = new RegistrationCheckoutViewModel();
                long PaymentId = long.Parse(Utility.GetApplicationCookies("payId").Value);
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);

                var ProfileUpgrade = db.ctProfileUpgrades.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                ctRegistrationPayment objDbRegistrationPayment = db.ctRegistrationPayments.Where(x => x.PaymentId == PaymentId).FirstOrDefault();
                ctRegistration objDbRegistration = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                if (objDbRegistrationPayment.TransactionStatus == "Success")
                {
                    objDbRegistration.MembershipType = ProfileUpgrade.MembershipType;
                    objDbRegistration.MembershipBadge = ProfileUpgrade.MembershipBadge;
                    objDbRegistration.MembershipDiscount = ProfileUpgrade.MembershipDiscount;
                    objDbRegistration.MembershipEndDate = DateTime.Now.AddYears(1);
                    db.Entry(objDbRegistration).State = EntityState.Modified;
                    db.SaveChanges();

                    model.MembershipType = GetMultilanguageText(objDbRegistration.MembershipType, Utility.GetLanguageFromUrl(Request.Url.ToString()));
                    model.MembershipBadge = GetMultilanguageText(objDbRegistration.MembershipBadge, Utility.GetLanguageFromUrl(Request.Url.ToString()));
                    model.MembershipDiscount = objDbRegistration.MembershipDiscount.Value;
                    model.InvoiceNumber = objDbRegistrationPayment.InvoiceNumber;
                    model.SubTotal = objDbRegistrationPayment.SubTotal.Value;
                    model.Shipping = objDbRegistrationPayment.Shipping.Value;
                    model.VAT = objDbRegistrationPayment.VAT.Value;
                    model.Discount = objDbRegistrationPayment.Discount.Value;
                    model.GrandTotal = objDbRegistrationPayment.GrandTotal.Value;
                    model.TransactionAmount = objDbRegistrationPayment.TransactionAmount.Value;
                    model.TransactionCurrency = objDbRegistrationPayment.TransactionCurrency;


                    EmailSettingModel eModel = Utility.GetEmailSetting("MembershipUpgrade", ProfileId.ToString(), Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString()));

                    string Message = EmailMessage.GetRegistrationMessage(objDbRegistration, "MembershipUpgrade", "", objDbRegistration.FirstName, Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString()));

                    Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);

                    Utility.ClearRenewalMembershipCookies();

                    return PartialView("Registration/MembershipUpgradeSuccess", model);
                }
                else
                {
                    return HttpNotFound();
                }
            }
            catch
            {
                return HttpNotFound();
            }
        }
               

        [HttpGet]
        public ActionResult MembershipUpgradePaymentFailed()
        {
            return PartialView("Registration/MembershipUpgradeFailed");
        }


        [HttpPost]
        public ActionResult GetRegistrationCheckoutPriceItem(string lang, string promoCode)
        {
            try
            {
                long profileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                RegistrationCheckoutViewModel model = new RegistrationCheckoutViewModel();
                var regData = db.ctRegistrations.Where(x => x.ProfileId == profileId).FirstOrDefault();
                var BecomeQualifiedPage = Umbraco.TypedContent(1810);
                var MembershipType = BecomeQualifiedPage.Children.Where(x => x.GetPropertyValue("typeOfMembership").ToString() == regData.MembershipType).FirstOrDefault();
                
                if (regData.MembershipType == "Corporate")
                {
                    if (regData.MembershipBadge == "Bronze")
                    {
                        model.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("bronzeFee").ToString());
                    }
                    else if (regData.MembershipBadge == "Silver")
                    {
                        model.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("silverFee").ToString());
                    }
                    else if (regData.MembershipBadge == "Gold")
                    {
                        model.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("goldFee").ToString());
                    }
                }
                else
                {
                    model.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("generalFee").ToString());
                }

                PromoCodeValidation promoCodeValidation = NewRegistrationGetPromoValue(lang, promoCode, model.SubTotal);

                model.Shipping = 0;
                model.Discount = (string.IsNullOrEmpty(promoCodeValidation.Message) ? promoCodeValidation.Value : 0);
                model.VAT = ((model.SubTotal + model.Shipping - model.Discount) * VAT) / 100;
                model.GrandTotal = (model.SubTotal + model.VAT + model.Shipping - model.Discount);

                return Json(new { status = true, CartItem = model }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetRenewalRegistrationCheckoutPriceItem(string lang, string promoCode)
        {
            try
            {
                long profileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                RegistrationCheckoutViewModel model = new RegistrationCheckoutViewModel();
                var regData = db.ctRegistrations.Where(x => x.ProfileId == profileId).FirstOrDefault();
                var BecomeQualifiedPage = Umbraco.TypedContent(1810);
                var MembershipType = BecomeQualifiedPage.Children.Where(x => x.GetPropertyValue("typeOfMembership").ToString() == regData.MembershipType).FirstOrDefault();

                if (regData.MembershipType == "Corporate")
                {
                    if (regData.MembershipBadge == "Bronze")
                    {
                        model.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("bronzeFee").ToString());
                    }
                    else if (regData.MembershipBadge == "Silver")
                    {
                        model.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("silverFee").ToString());
                    }
                    else if (regData.MembershipBadge == "Gold")
                    {
                        model.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("goldFee").ToString());
                    }
                }
                else
                {
                    model.SubTotal = decimal.Parse(MembershipType.GetPropertyValue("generalFee").ToString());
                }

                PromoCodeValidation promoCodeValidation = RenewalRegistrationGetPromoValue(lang, promoCode, model.SubTotal);

                model.Shipping = 0;
                model.Discount = (string.IsNullOrEmpty(promoCodeValidation.Message) ? promoCodeValidation.Value : 0);
                model.VAT = ((model.SubTotal + model.Shipping - model.Discount) * VAT) / 100;
                model.GrandTotal = (model.SubTotal + model.VAT + model.Shipping - model.Discount);

                return Json(new { status = true, CartItem = model }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult NewRegistrationValidatePromoCode(string lang, string promoCode)
        {
            PromoCodeValidation model = NewRegistrationGetPromoValue(lang, promoCode, 0);
            if (string.IsNullOrEmpty(model.Message))
            {

                return Json(new
                {
                    status = true,
                    message = "",
                    value = model.Value
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    status = false,
                    message = model.Message,
                    value = model.Value
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public PromoCodeValidation NewRegistrationGetPromoValue(string lang, string promoCode, decimal discountValue)
        {
            PromoCodeValidation model = new PromoCodeValidation();
            model.Message = "";
            model.Value = 0;
            bool IsMemberExists = false;
            var MembershipType = "";

            try
            {
                if (!string.IsNullOrEmpty(promoCode))
                {
                    long proId = long.Parse(Utility.GetApplicationCookies("proId").Value);

                    if (db.ctRegistrations.Where(x => x.ProfileId == proId).FirstOrDefault().MembershipType == "Professional")
                    {
                        MembershipType = "Corporate";
                    }
                    else
                    {
                        MembershipType = db.ctRegistrations.Where(x => x.ProfileId == proId).FirstOrDefault().MembershipType;
                    }


                    var rootPageId = lang == "en" ? 1131 : 2459;
                    var rootPage = Umbraco.TypedContent(rootPageId);
                    var promoCodePage = rootPage.Descendants(1).Where(x => x.DocumentTypeAlias == "promoCode").FirstOrDefault();
                    var promoCodeItem = promoCodePage.Children.Where(x => x.IsVisible() && x.GetPropertyValue<string>("promoCode") == promoCode).FirstOrDefault();
                    if (promoCodeItem != null)
                    {
                        var discountPercentage = promoCodeItem.GetPropertyValue("discountPercentage");
                        var flatDiscount = promoCodeItem.GetPropertyValue("flatDiscountAED");
                        if (discountPercentage != null)
                        {
                            if (decimal.Parse(discountPercentage.ToString()) > 0)
                            {
                                model.Value = (discountValue * decimal.Parse(discountPercentage.ToString())) / 100;
                            }
                            else
                            {
                                model.Value = decimal.Parse(flatDiscount.ToString());
                            }
                        }
                        else
                        {
                            model.Value = decimal.Parse(flatDiscount.ToString());
                        }

                        if (promoCodeItem.GetPropertyValue("startDate") != null && promoCodeItem.GetPropertyValue("endDate") != null)
                        {
                            if (promoCodeItem.GetPropertyValue<DateTime>("startDate") <= DateTime.Now.Date && promoCodeItem.GetPropertyValue<DateTime>("endDate") >= DateTime.Now.Date)
                            {

                            }
                            else
                            {
                                model.Message = "Invalid_Promo_Code";
                                return model;
                            }
                        }
                        if (promoCodeItem.GetPropertyValue("isApplyNewRegistration") != null)
                        {
                            if (promoCodeItem.GetPropertyValue<Boolean>("isApplyNewRegistration") == false)
                            {
                                model.Message = "Invalid_Promo_Code";
                                return model;
                            }
                        }
                        if (promoCodeItem.HasValue("applicableMember"))
                        {
                            foreach (var item in promoCodeItem.GetPropertyValue<IEnumerable<string>>("applicableMember"))
                            {
                                if (MembershipType == item)
                                {
                                    IsMemberExists = true;
                                }
                            }
                            if (IsMemberExists == false)
                            {
                                model.Message = "Invalid_Promo_Code";
                                return model;
                            }
                        }
                        else
                        {
                            model.Message = "Invalid_Promo_Code";
                            return model;
                        }                        

                    }
                    else
                    {
                        model.Message = "Invalid_Promo_Code";
                    }
                }
                else
                {
                    model.Message = "Invalid_Promo_Code";
                }
            }
            catch (Exception ex)
            {
                model.Message = ex.Message;
            }
            return model;
        }

        [HttpPost]
        public ActionResult RenewalRegistrationValidatePromoCode(string lang, string promoCode)
        {
            PromoCodeValidation model = RenewalRegistrationGetPromoValue(lang, promoCode, 0);
            if (string.IsNullOrEmpty(model.Message))
            {

                return Json(new
                {
                    status = true,
                    message = "",
                    value = model.Value
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    status = false,
                    message = model.Message,
                    value = model.Value
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public PromoCodeValidation RenewalRegistrationGetPromoValue(string lang, string promoCode, decimal discountValue)
        {
            PromoCodeValidation model = new PromoCodeValidation();
            model.Message = "";
            model.Value = 0;
            bool IsMemberExists = false;
            var MembershipType = "";

            try
            {
                if (!string.IsNullOrEmpty(promoCode))
                {
                    long proId = long.Parse(Utility.GetApplicationCookies("proId").Value);

                    if (db.ctRegistrations.Where(x => x.ProfileId == proId).FirstOrDefault().MembershipType == "Professional")
                    {
                        MembershipType = "Corporate";
                    }
                    else
                    {
                        MembershipType = db.ctRegistrations.Where(x => x.ProfileId == proId).FirstOrDefault().MembershipType;
                    }


                    var rootPageId = lang == "en" ? 1131 : 2459;
                    var rootPage = Umbraco.TypedContent(rootPageId);
                    var promoCodePage = rootPage.Descendants(1).Where(x => x.DocumentTypeAlias == "promoCode").FirstOrDefault();
                    var promoCodeItem = promoCodePage.Children.Where(x => x.IsVisible() && x.GetPropertyValue<string>("promoCode") == promoCode).FirstOrDefault();
                    if (promoCodeItem != null)
                    {
                        var discountPercentage = promoCodeItem.GetPropertyValue("discountPercentage");
                        var flatDiscount = promoCodeItem.GetPropertyValue("flatDiscountAED");
                        if (discountPercentage != null)
                        {
                            if (decimal.Parse(discountPercentage.ToString()) > 0)
                            {
                                model.Value = (discountValue * decimal.Parse(discountPercentage.ToString())) / 100;
                            }
                            else
                            {
                                model.Value = decimal.Parse(flatDiscount.ToString());
                            }
                        }
                        else
                        {
                            model.Value = decimal.Parse(flatDiscount.ToString());
                        }

                        if (promoCodeItem.GetPropertyValue("startDate") != null && promoCodeItem.GetPropertyValue("endDate") != null)
                        {
                            if (promoCodeItem.GetPropertyValue<DateTime>("startDate") <= DateTime.Now.Date && promoCodeItem.GetPropertyValue<DateTime>("endDate") >= DateTime.Now.Date)
                            {

                            }
                            else
                            {
                                model.Message = "Invalid_Promo_Code";
                                return model;
                            }
                        }
                        if (promoCodeItem.GetPropertyValue("isApplyRenewalRegistration") != null)
                        {
                            if (promoCodeItem.GetPropertyValue<Boolean>("isApplyRenewalRegistration") == false)
                            {
                                model.Message = "Invalid_Promo_Code";
                                return model;
                            }
                        }
                        if (promoCodeItem.HasValue("applicableMember"))
                        {
                            foreach (var item in promoCodeItem.GetPropertyValue<IEnumerable<string>>("applicableMember"))
                            {
                                if (MembershipType == item)
                                {
                                    IsMemberExists = true;
                                }
                            }
                            if (IsMemberExists == false)
                            {
                                model.Message = "Invalid_Promo_Code";
                                return model;
                            }
                        }
                        else
                        {
                            model.Message = "Invalid_Promo_Code";
                            return model;
                        }

                    }
                    else
                    {
                        model.Message = "Invalid_Promo_Code";
                    }
                }
                else
                {
                    model.Message = "Invalid_Promo_Code";
                }
            }
            catch (Exception ex)
            {
                model.Message = ex.Message;
            }
            return model;
        }
    }
}