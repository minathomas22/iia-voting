﻿using CMS.Helpers;
using IIA.DBContext;
using IIA.Helper;
using IIA.Models;
using Microsoft.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;
using umbraco.BasePages;
using umbraco.cms.businesslogic.member;
using Umbraco.Core.Security;
using Umbraco.Web.Mvc;

namespace IIA.Controllers
{
    public class CandidateController : SurfaceController
    {
        IIAContext db = new IIAContext();

        [HttpPost]
        public ActionResult CreateBaseCandidate(CandidateBaseViewModel model)
        {
            try
            {
                var ctDbCandidate = db.ctCandidates.FirstOrDefault(e => e.ElectionId == model.ElectionId && e.MemberId == model.MemberId);
                if (ctDbCandidate == null)
                {
                    var ctNewCandidate = new ctCandidate()
                    {
                        ElectionId = model.ElectionId,
                        MemberId = model.MemberId,
                        Status = (int)CandidateRequestStatus.PendingRequest,
                        CreatedOn = DateTime.Now,
                    };
                    db.ctCandidates.Add(ctNewCandidate);
                }
                else
                {
                    ctDbCandidate.Status = (int)CandidateRequestStatus.PendingRequest;
                    ctDbCandidate.UpdatedOn = DateTime.Now;
                    db.Entry(ctDbCandidate).State = EntityState.Modified;
                }
                db.SaveChanges();
                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult BackToProceedCandidate(CandidateBaseViewModel model)
        {
            try
            {
                var dbCandidate = db.ctCandidates.FirstOrDefault(e => e.ElectionId == model.ElectionId && e.MemberId == model.MemberId);
                if (dbCandidate != null)
                {
                    dbCandidate.Status = (int)CandidateRequestStatus.UnderRequest;
                    dbCandidate.UpdatedOn = DateTime.Now;
                    db.Entry(dbCandidate).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public ActionResult ApplyCandidate()
        {
            try
            {
                var lang = Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString());
                RegistrationWithCandidateViewModel RegisterInfo = new RegistrationWithCandidateViewModel();
                long memberId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                long electionId = long.Parse(Utility.GetApplicationCookies("Election").Value);
                ctRegistration objDbModel = db.ctRegistrations.FirstOrDefault(x => x.ProfileId == memberId);
                ctCandidate ctCandidateDb = db.ctCandidates.FirstOrDefault(x => x.MemberId == memberId && x.ElectionId == electionId);
                ctElection ctElectionDb = db.ctElections.FirstOrDefault(x => x.ElectionId == electionId);
                if (objDbModel != null)
                {
                    string middleName = lang == "en" ? objDbModel.MiddleName : objDbModel.MiddleName_ar;
                    RegisterInfo.FirstName = lang == "en" ? objDbModel.FirstName : objDbModel.FirstName_ar;
                    RegisterInfo.MiddleName = !string.IsNullOrEmpty(middleName) ? middleName : "";
                    RegisterInfo.LastName = lang == "en" ? objDbModel.LastName : objDbModel.LastName_ar;
                    RegisterInfo.ProfilePhoto = !string.IsNullOrEmpty(objDbModel.ProfilePhoto) ? "/Media/ApplicantFile/" + objDbModel.ProfilePhoto : "/ui/media/images/avater.jpg";
                    RegisterInfo.JobTitle = lang == "en" ? objDbModel.JobTitle : objDbModel.JobTitle_ar;
                    RegisterInfo.Organization = lang == "en" ? objDbModel.Organization : objDbModel.Organization_ar;
                    RegisterInfo.EmailAddress = objDbModel.EmailAddress;
                    RegisterInfo.Qualification = lang == "en" ? objDbModel.Qualification : objDbModel.Qualification_ar;
                    RegisterInfo.Bio = lang == "en" ? objDbModel.Bio : objDbModel.Bio_ar;
                    RegisterInfo.ProfileType = objDbModel.ProfileType;
                    RegisterInfo.ProfileId = objDbModel.ProfileId;
                    RegisterInfo.ParentProfileId = objDbModel.ParentProfileId;
                    RegisterInfo.IIAGlobalNumber = objDbModel.IIAGlobalNumber;
                    RegisterInfo.ElectionId = electionId;
                    RegisterInfo.Comment = ctCandidateDb == null ? "" : ctCandidateDb.Comment;
                    RegisterInfo.ElectionTitle = ctElectionDb == null ? "" : ctElectionDb.Title;
                    RegisterInfo.CandidateStatus = ctCandidateDb == null ? CandidateRequestStatus.UnderRequest : (CandidateRequestStatus)ctCandidateDb.Status;
                    if (ctCandidateDb != null)
                    {
                        var candidateInfo = db.ctCandidateInfoes.FirstOrDefault(a => a.CandidateId == ctCandidateDb.CandidateId);
                        if (candidateInfo != null)
                        {
                            RegisterInfo.IsAgree = false;
                            RegisterInfo.IsAcknowledge = false;
                            RegisterInfo.OtherDocumentFile = !string.IsNullOrEmpty(candidateInfo.OtherDocumentFile) ? "/Media/ApplicantFile/" + candidateInfo.OtherDocumentFile : ""; ;
                            RegisterInfo.Description = candidateInfo.Description;
                            RegisterInfo.Bio = candidateInfo.Bio;
                            RegisterInfo.ResumeFile = !string.IsNullOrEmpty(candidateInfo.ResumeFile) ? "/Media/ApplicantFile/" + candidateInfo.ResumeFile : ""; ;
                            RegisterInfo.ElectionProgramFile = !string.IsNullOrEmpty(candidateInfo.ElectionProgramFile) ? "/Media/ApplicantFile/" + candidateInfo.ElectionProgramFile : ""; ;
                            RegisterInfo.LetterOfInterestFile = !string.IsNullOrEmpty(candidateInfo.LetterOfInterestFiles) ? "/Media/ApplicantFile/" + candidateInfo.LetterOfInterestFiles : ""; ;
                            //RegisterInfo.OtherDocumentFile = !string.IsNullOrEmpty(candidateInfo.OtherDocumentFile) ? "/Media/ApplicantFile/" + candidateInfo.OtherDocumentFile : ""; ;
                            RegisterInfo.OtherDocumentFiles = !string.IsNullOrEmpty(candidateInfo.OtherDocumentFile) ? JsonConvert.DeserializeObject<List<string>>(candidateInfo.OtherDocumentFile) : new List<string>();
                        }
                    }
                }
                return PartialView("Dashboard/ApplyCandidate", RegisterInfo);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult DeleteFileFromCandidate(long candidateId, int fileIndex)
        {
            var candidateInfo = db.ctCandidateInfoes.FirstOrDefault(e => e.CandidateId == candidateId);
            if (candidateInfo == null)
            {
                return Json(new
                {
                    model = candidateId,
                    error = "No Candidate Info Have This Candidate Id",
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
            var documentFiles = string.IsNullOrEmpty(candidateInfo.OtherDocumentFile) ? new List<string>() : JsonConvert.DeserializeObject<List<string>>(candidateInfo.OtherDocumentFile);
            if (documentFiles.Any())
            {
                documentFiles.RemoveAt(fileIndex);
                candidateInfo.OtherDocumentFile = JsonConvert.SerializeObject(documentFiles);
                candidateInfo.UpdatedOn = DateTime.Now;
                db.Entry(candidateInfo).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new
                {
                    model = candidateId,
                    error = "File Deleted Sucessfully",
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            return Json(new
            {
                model = documentFiles,
                error = "The Candidate Info Not Have Any Other Document Files To Delete From It",
                status = false
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApplyCandidate(CandidateViewModel model)
        {
            try
            {
                var ResumeFile = Request.Files["ResumeFile"];
                var ElectionProgramFile = Request.Files["ElectionProgramFile"];
                var LetterOfInterestFile = Request.Files["LetterOfInterestFile"];
                if (ResumeFile == null || ElectionProgramFile == null || LetterOfInterestFile == null || model.OtherDocumentFiles.Count() == 0)
                {
                    var obj = new { ResumeFile, ElectionProgramFile, LetterOfInterestFile };
                    return Json(new
                    {
                        model = obj,
                        error = "File Required",
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
                var ctDbCandidate = db.ctCandidates.FirstOrDefault(e => e.MemberId == model.MemberId && e.ElectionId == model.ElectionId);
                if (ctDbCandidate != null)
                {
                    ctDbCandidate.Status = (int)CandidateRequestStatus.Pending;
                    ctDbCandidate.UpdatedOn = DateTime.Now;
                    db.Entry(ctDbCandidate).State = EntityState.Modified;
                    var candidateInfo = new ctCandidateInfo()
                    {
                        CandidateId = ctDbCandidate.CandidateId,
                        Bio = model.Bio,
                        Description = model.Description,
                        ResumeFile = Utility.UploadFileInFolder(ResumeFile),
                        ElectionProgramFile = Utility.UploadFileInFolder(ElectionProgramFile),
                        LetterOfInterestFiles = Utility.UploadFileInFolder(LetterOfInterestFile),
                        OtherDocumentFile = SerlizeDocumentFiles(new List<string>(), model.OtherDocumentFiles),
                        CreatedOn = DateTime.Now
                    };
                    db.ctCandidateInfoes.Add(candidateInfo);
                    var mailIsSent = SendMailForApplyElectionByCandidate(ctDbCandidate.MemberId , candidateInfo);
                    if (mailIsSent)
                    {
                        db.SaveChanges();
                    }
                    else
                    {
                        return Json(new
                        {
                            error = "Mail Send Process Is Failed",
                            status = false
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    model = model,
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string SerlizeDocumentFiles(List<string> otherDocumentFilesList, IEnumerable<HttpPostedFileBase> OtherDocumentFiles)
        {
            foreach (var file in OtherDocumentFiles)
            {
                otherDocumentFilesList.Add(Utility.UploadFileInFolder(file));
            }
            var otherDocumentFiles = JsonConvert.SerializeObject(otherDocumentFilesList);
            return otherDocumentFiles;

        }

        public bool SendMailForApplyElectionByCandidate(long profileId , ctCandidateInfo candidateInfo)
        {
            var member = db.ctRegistrations.AsNoTracking().FirstOrDefault(e => e.ProfileId == profileId);
            if (member != null)
            {
                EmailSettingModel eModelUser = Utility.GetEmailSetting("ApplyCandidate", member.ProfileId.ToString(), member.lang);
                string MessageUser = EmailMessage.GetMemberElectionForApplyCandidate(member.lang, candidateInfo, member.FirstName, member.FirstName_ar);
                Utility.SendMail(eModelUser.FromName, eModelUser.FromEmail, eModelUser.EmailTo, eModelUser.EmailCC, eModelUser.EmailBCC, eModelUser.Subject, MessageUser);
                return true;
            }
            return false;
        }

        [HttpPost]
        public ActionResult ApplyCandidateByAdmin(CandidateViewModel model)
        {
            try
            {
                #region Remove Old Candidate Data
                var ctDbCandidate = db.ctCandidates.FirstOrDefault(e => e.ElectionId == model.ElectionId && e.MemberId == model.MemberId);
                if (ctDbCandidate != null)
                {
                    if (ctDbCandidate.Status == (int)CandidateRequestStatus.Approve)
                    {
                        return Json(new
                        {
                            error = "Can Not Add New Approved Candidate",
                            status = false
                        }, JsonRequestBehavior.AllowGet);
                    }
                    var ctDbCandidateInfo = db.ctCandidateInfoes.FirstOrDefault(e => e.CandidateId == ctDbCandidate.CandidateId);
                    if (ctDbCandidateInfo != null)
                    {
                        db.ctCandidateInfoes.Remove(ctDbCandidateInfo);
                    }
                    db.ctCandidates.Remove(ctDbCandidate);
                }
                #endregion
                #region Create CandidateBase
                var ctNewCandidate = new ctCandidate()
                {
                    ElectionId = model.ElectionId,
                    MemberId = model.MemberId,
                    Status = (int)CandidateRequestStatus.Approve,
                    CreatedOn = DateTime.Now,
                };
                var newCandidate = db.ctCandidates.Add(ctNewCandidate);
                #endregion
                #region Create Candidate Info
                var ResumeFile = Request.Files["ResumeFile"];
                var ElectionProgramFile = Request.Files["ElectionProgramFile"];
                var LetterOfInterestFile = Request.Files["LetterOfInterestFile"];
                if (ResumeFile == null || ElectionProgramFile == null || LetterOfInterestFile == null || model.OtherDocumentFiles.Count() == 0)
                {
                    var obj = new { ResumeFile, ElectionProgramFile, LetterOfInterestFile, model.OtherDocumentFiles };
                    return Json(new
                    {
                        model = obj,
                        error = "File Required",
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
                db.ctCandidateInfoes.Add(new ctCandidateInfo()
                {
                    CandidateId = newCandidate.CandidateId,
                    Bio = model.Bio,
                    Description = model.Description,
                    ResumeFile = Utility.UploadFileInFolder(ResumeFile),
                    ElectionProgramFile = Utility.UploadFileInFolder(ElectionProgramFile),
                    LetterOfInterestFiles = Utility.UploadFileInFolder(LetterOfInterestFile),
                    OtherDocumentFile = SerlizeDocumentFiles(new List<string>(), model.OtherDocumentFiles),
                    CreatedOn = DateTime.Now
                });
                #endregion
                db.SaveChanges();
                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    model = model,
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateRejectedCandidate(CandidateViewModel model)
        {
            try
            {
                var ctDbCandidate = db.ctCandidates.FirstOrDefault(e => e.MemberId == model.MemberId && e.ElectionId == model.ElectionId);
                if (ctDbCandidate == null)
                {
                    return Json(new
                    {
                        error = "No candidate with these data",
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
                var ctDbCandidateInfo = db.ctCandidateInfoes.FirstOrDefault(e => e.CandidateId == ctDbCandidate.CandidateId);
                if (ctDbCandidateInfo == null)
                {
                    return Json(new
                    {
                        error = "no candidate info with theses data",
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
                #region  Update Candidate Info
                var ResumeFile = Request.Files["ResumeFile"];
                var ElectionProgramFile = Request.Files["ElectionProgramFile"];
                var LetterOfInterestFile = Request.Files["LetterOfInterestFile"];
                //var OtherDocumentFile = Request.Files["OtherDocumentFile"];
                if (ResumeFile != null)
                {
                    ctDbCandidateInfo.ResumeFile = Utility.UploadFileInFolder(ResumeFile);
                }
                if (ElectionProgramFile != null)
                {
                    ctDbCandidateInfo.ElectionProgramFile = Utility.UploadFileInFolder(ElectionProgramFile);
                }
                if (LetterOfInterestFile != null)
                {
                    ctDbCandidateInfo.LetterOfInterestFiles = Utility.UploadFileInFolder(LetterOfInterestFile);
                }
                if (model.OtherDocumentFiles.Count() != 0)
                {
                    var otherDocumentsDb = JsonConvert.DeserializeObject<List<string>>(ctDbCandidateInfo.OtherDocumentFile);
                    ctDbCandidateInfo.OtherDocumentFile = SerlizeDocumentFiles(otherDocumentsDb, model.OtherDocumentFiles);
                }
                ctDbCandidateInfo.Bio = model.Bio;
                ctDbCandidateInfo.Description = model.Description;
                ctDbCandidateInfo.UpdatedOn = DateTime.Now;
                db.Entry(ctDbCandidateInfo).State = EntityState.Modified;
                #endregion
                #region Update ctDbCandidate
                ctDbCandidate.Status = (int)CandidateRequestStatus.Pending;
                ctDbCandidate.UpdatedOn = DateTime.Now;
                db.Entry(ctDbCandidate).State = EntityState.Modified;
                #endregion
                db.SaveChanges();
                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    model = model,
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateCandidateStatus(CandidateUpdateStatusViewModel model)
        {
            try
            {
                var ctCandidate = db.ctCandidates.FirstOrDefault(e => e.CandidateId == model.CandidateId);
                if (ctCandidate == null)
                {
                    return Json(new
                    {
                        status = false,
                        error = "Candidate Id Is Not Exist In Db",
                    }, JsonRequestBehavior.AllowGet);
                }
                var member = db.ctRegistrations.AsNoTracking().FirstOrDefault(e => e.ProfileId == ctCandidate.MemberId);
                EmailSettingModel eModelUser = Utility.GetEmailSetting("ApplyCandidateStatusChange", member.ProfileId.ToString(), member.lang);
                if (model.IsApprove)
                {
                    string MessageUser = EmailMessage.GetMemberElectionForApplyCandidateStatusApproved(member.lang, member.FirstName, member.FirstName_ar);
                    Utility.SendMail(eModelUser.FromName, eModelUser.FromEmail, eModelUser.EmailTo, eModelUser.EmailCC, eModelUser.EmailBCC, eModelUser.Subject, MessageUser);
                }
                else
                {
                    string MessageUser = EmailMessage.GetMemberElectionForApplyCandidateStatusRejected(member.lang, member.FirstName, member.FirstName_ar);
                    Utility.SendMail(eModelUser.FromName, eModelUser.FromEmail, eModelUser.EmailTo, eModelUser.EmailCC, eModelUser.EmailBCC, eModelUser.Subject, MessageUser);
                }
                ctCandidate.Status = model.IsApprove ? (int)CandidateRequestStatus.Approve : (int)CandidateRequestStatus.Reject;
                ctCandidate.Comment = model.Comment;
                ctCandidate.UpdatedOn = DateTime.Now;
                db.Entry(ctCandidate).State = EntityState.Modified;
                db.SaveChanges();

                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GetAllActiveMembers(long electionId)
        {
            try
            {
                var ctCandidateElectionMember = db.ctCandidates.Where(e => e.ElectionId == electionId &&
                                                   e.Status == (int)CandidateRequestStatus.Reject)
                                            .Select(e => e.MemberId).ToList();
                var memberDb = db.ctRegistrations.Where(e => !ctCandidateElectionMember.Contains(e.ProfileId))
                    .Select(e => new {
                        MemberId = e.ProfileId,
                        MemberName = string.Concat(e.FirstName, " ", e.MiddleName, " ", e.LastName, " - ", e.EmailAddress)
                    }).ToList();
                return Json(new
                {
                    status = true,
                    MembersInfo = memberDb
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new
                {
                    status = false,
                    Members = new { },
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetCandidateInfo(long candidateId)
        {
            try
            {
                CandidateViewModel model = new CandidateViewModel();
                var dbModel = db.ctCandidates.FirstOrDefault(x => x.CandidateId == candidateId);
                var candidateInfo = dbModel.ctCandidateInfoes.FirstOrDefault();
                if (dbModel == null)
                {
                    return Json(new
                    {
                        status = false,
                        error = "Candidate Id Is Not Exist In Db",
                    }, JsonRequestBehavior.AllowGet);
                }
                model.CandidateId = dbModel.CandidateId;
                model.MemberId = dbModel.MemberId;
                model.ElectionId = dbModel.ElectionId;
                model.Status = (CandidateRequestStatus)dbModel.Status;
                model.StatusName = dbModel.Status == (int)CandidateRequestStatus.Pending ? "Pending" :
                                   dbModel.Status == (int)CandidateRequestStatus.Approve ? "Approved" :
                                   dbModel.Status == (int)CandidateRequestStatus.Reject ? "Rejected" : "";
                model.Comment = dbModel.Comment;
                if (candidateInfo != null)
                {
                    model.Bio = candidateInfo.Bio;
                    model.Description = candidateInfo.Description;
                    model.ResumeFile = !string.IsNullOrEmpty(candidateInfo.ResumeFile) ? "/Media/ApplicantFile/" + candidateInfo.ResumeFile : ""; ;
                    model.ElectionProgramFile = !string.IsNullOrEmpty(candidateInfo.ElectionProgramFile) ? "/Media/ApplicantFile/" + candidateInfo.ElectionProgramFile : ""; ;
                    model.LetterOfInterestFile = !string.IsNullOrEmpty(candidateInfo.LetterOfInterestFiles) ? "/Media/ApplicantFile/" + candidateInfo.LetterOfInterestFiles : ""; ;
                    var files = !string.IsNullOrEmpty(candidateInfo.OtherDocumentFile) ? JsonConvert.DeserializeObject<List<string>>(candidateInfo.OtherDocumentFile) : new List<string>();
                    var linksList = new List<string>();
                    foreach (var file in files)
                    {
                        linksList.Add("/Media/ApplicantFile/" + file);
                    }
                    model.OtherDocumentFilesList = linksList;
                }
                return Json(new
                {
                    status = true,
                    CandidateInfo = model
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult CheckCandidateAlreadyApplied(CandidateIsAppyStatusViewModel model)
        {
            try
            {
                var dbModel = db.ctCandidates.FirstOrDefault(x => x.MemberId == model.MemberId && x.ElectionId == model.ElectionId);
                if (dbModel == null)
                {
                    return Json(new
                    {
                        status = true,
                        isApply = false,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = true,
                        isApply = true,
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ApplyCandidateInCookie(int electionId)
        {
            try
            {
                Utility.SetApplicationCookies("Election", electionId.ToString());
                return Json(new
                {
                    status = true,
                    message = "",
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GetAssignedElectionList()
        {
            try
            {
                var userId = GetCurrentUserId();
                var electionCommiteAssigned = db.ctElectionCommittees.Where(e => e.UserAdminId == userId).Select(e => e.ElectionId).ToList();
                var elections = db.ctElections.Where(x => electionCommiteAssigned.Contains(x.ElectionId)).ToList();
                var model = elections.OrderByDescending(x => x.ElectionId).Select(e => new ElectionDropDownViewModel
                {
                    ElectionId = e.ElectionId,
                    Title = string.Concat(e.Title, " ", "(" + e.StartDate.ToString("MM/dd/yyyy") + ")")
                }).ToList();
                return Json(new
                {
                    status = true,
                    ElectionInfo = model
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    error = ex.Message,
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetAllElectionRequests(int electionId)
        {
            try
            {
                int UserId = GetCurrentUserId();
                var elections = db.ctCandidates.Where(x => x.Status != (int)CandidateRequestStatus.UnderRequest && x.ElectionId == electionId).ToList();
                var model = elections.OrderByDescending(x => x.ElectionId).Select(e => new
                {
                    Email = e.ctRegistration.EmailAddress,
                    ProfilePhoto = e.ctRegistration.ProfilePhoto,
                    Status = e.Status,
                    StatusName = e.Status == (int)CandidateRequestStatus.Pending ? "Pending" :
                                 e.Status == (int)CandidateRequestStatus.Approve ? "Approved" :
                                 e.Status == (int)CandidateRequestStatus.Reject ? "Rejected" : "",
                    CandidateId = e.CandidateId
                }).ToList();

                return Json(new
                {
                    status = true,
                    data = model
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public int GetCurrentUserId()
        {
            var userTicket = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current).GetUmbracoAuthTicket();
            var currentUser = ApplicationContext.Services.UserService.GetByUsername(userTicket.Name);
            return currentUser.Id;
        }
    }
}