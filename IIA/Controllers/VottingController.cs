﻿using System;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using IIA.DBContext;
using System.Linq.Dynamic;
using umbraco.cms.helpers;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using IIA.Models;
using CMS.Helpers;
using System.Data.Entity;
using Umbraco.Web;
using System.Web.Configuration;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using umbraco.editorControls.SettingControls.Pickers;
using Newtonsoft.Json;
using Umbraco.Core.Models;
using IIA.Helper;
using System.Data;
using System.Data.SqlClient;
using Umbraco.Web.Media.EmbedProviders;
using umbraco;
using System.Web.Mvc.Html;
using System.Data.Entity.Core.Objects;
using Umbraco.Core.Security;

namespace IIA.Controllers
{
    public class VottingController : SurfaceController
    {
        IIAContext db = new IIAContext();

        [HttpPost]
        public ActionResult GetElectionInfosForVoting(string lang)
        {
            try
            {
            long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
            long voteElection = long.Parse(Utility.GetApplicationCookies("VoteElection").Value);
            ctRegistration objDbModel = db.ctRegistrations.FirstOrDefault(x => x.ProfileId == ProfileId);
            var lst = new List<ElectionCandidateViewModel>();
            if (canMemberApplyForVote(objDbModel))
            {
                var election = db.ctElections.FirstOrDefault(x => x.ElectionId == voteElection);
                    if (election.EndDate.Date < DateTime.Now.Date)
                    {
                        return Json(new
                        {
                            status = false,
                            error = "This Election Is Closed By End Date"
                        }, JsonRequestBehavior.AllowGet);
                    }
                    if (election.ApplyEndDate.Date >= DateTime.Now.Date)
                    {
                        return Json(new
                        {
                            status = false,
                            error = "This Election Apply End Date Is Not Closed"
                        }, JsonRequestBehavior.AllowGet);
                    }
                var objectedCandidateIds = db.ctCandidateObjections.Where(e => e.Status == (int)CandidateObjectionStatus.Approved).Select(e => e.CandidateId).ToList();
                var candidates = db.ctCandidates.Where(e => e.ElectionId == election.ElectionId &&
                                                       !objectedCandidateIds.Contains(e.CandidateId) &&
                                                       e.Status == (int)CandidateRequestStatus.Approve).ToList();
                var candidateIds = candidates.Select(e => e.CandidateId).ToList();
                var candidateInfos = db.ctCandidateInfoes.Where(e => candidateIds.Contains(e.CandidateId)).ToList();

                foreach (var candidate in candidates)
                {
                    var member = db.ctRegistrations.FirstOrDefault(e => e.ProfileId == candidate.MemberId);
                    var candidateInfo = candidateInfos.FirstOrDefault(e => e.CandidateId == candidate.CandidateId);
                        if (candidateInfo != null && member != null)
                        {
                            lst.Add(new ElectionCandidateViewModel()
                            {
                                Bio = candidateInfo.Bio,
                                CandidateName = lang == "en" ?
                                                string.Concat(member.FirstName, " ", member.MiddleName, " ", member.LastName) :
                                                string.Concat(member.FirstName_ar, " ", member.MiddleName_ar, " ", member.LastName_ar),
                                CandidateMemberId = candidate.MemberId,
                                CandidateProfile = "/Media/ApplicantFile/" + member.ProfilePhoto,
                                ResumeFile = "/Media/ApplicantFile/" + candidateInfo.ResumeFile,
                                ElectionProgramFile = "/Media/ApplicantFile/" + candidateInfo.ElectionProgramFile,
                                JobTitle = lang == "en" ? member.JobTitle : member.JobTitle_ar,
                                Status = GetCandidateElectionStatus(candidate, election.StartDate, ProfileId, election.ElectionId),
                                CandidateId = candidate.CandidateId,
                                ElectionName = election.Title,
                                ElectionId = election.ElectionId,
                                CanObjectStatus = (int)GetCandidateElectioObjectionStatus(candidate, election.StartDate, ProfileId)
                            });
                        }
                }
                return Json(new
                {
                    status = true,
                    data = lst
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    data = new { },
                }, JsonRequestBehavior.AllowGet);
            }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    error = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult getElectionInfosForVotingByCandidateId(long candidateId, string lang)
        {
            long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
            ctRegistration objDbModel = db.ctRegistrations.FirstOrDefault(x => x.ProfileId == ProfileId);
            var data = new ElectionCandidateViewModel();
            if (canMemberApplyForVote(objDbModel))
            {
                var candidate = db.ctCandidates.FirstOrDefault(e => e.CandidateId == candidateId && e.Status == (int)CandidateRequestStatus.Approve);
                var election = db.ctElections.FirstOrDefault(x => x.ElectionId == candidate.ElectionId);
                var candidateInfo = db.ctCandidateInfoes.FirstOrDefault(e => e.CandidateId == candidateId);
                var member = db.ctRegistrations.FirstOrDefault(e => e.ProfileId == candidate.MemberId);
                data.Bio = candidateInfo.Bio;
                data.CandidateName = lang == "en" ?
                                        string.Concat(member.FirstName, " ", member.MiddleName, " ", member.LastName) :
                                        string.Concat(member.FirstName_ar, " ", member.MiddleName_ar, " ", member.LastName_ar);

                data.CandidateProfile = "/Media/ApplicantFile/" + member.ProfilePhoto;
                data.ResumeFile = "/Media/ApplicantFile/" + candidateInfo.ResumeFile;
                data.ElectionProgramFile = "/Media/ApplicantFile/" + candidateInfo.ElectionProgramFile;
                data.JobTitle = lang == "en" ? member.JobTitle : member.JobTitle_ar;
                data.CandidateMemberId = candidate.MemberId;
                data.CandidateDescription = candidateInfo.Description;
                data.Status = GetCandidateElectionStatus(candidate, election.StartDate, ProfileId , election.ElectionId);
                data.ElectionId = election.ElectionId;
                data.CandidateId = candidate.CandidateId;
                return Json(new
                {
                    status = true,
                    data = data,
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    data = new { },
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ObjectionForCandidate(long candidateId, string reason)
        {
            try
            {
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                var candidate = db.ctCandidates.FirstOrDefault(e => e.CandidateId == candidateId);
                if (candidate != null)
                {
                    var ctObjection = db.ctCandidateObjections.FirstOrDefault(e => e.MemberId == ProfileId && e.CandidateId == candidateId);
                    if (ctObjection != null)
                    {
                        return Json(new
                        {
                            error = "You Already Raise Objection Related To This Candidate In this Election",
                            status = false
                        }, JsonRequestBehavior.AllowGet);
                    }
                    ctCandidateObjection ctCandidateObjection = new ctCandidateObjection()
                    {
                        CandidateId = candidateId,
                        MemberId = ProfileId,
                        Reason = reason ,
                        Status = (int)CandidateObjectionStatus.Pending
                    };
                    db.ctCandidateObjections.Add(ctCandidateObjection);
                    var mailIsSent = SendMailForObjectionRequestByMember(ProfileId , ctCandidateObjection.CandidateId);
                    if (mailIsSent)
                    {
                        db.SaveChanges();
                        //send same email to committee user emails
                        SendMailForObjectionCommittee(ProfileId, ctCandidateObjection.CandidateId);
                    }
                    else
                    {
                        return Json(new
                        {
                            error = "Objection Request Mail Process Is Failed",
                            status = false
                        }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new
                    {
                        error = "",
                        status = true
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        error = "This Candidate Id Is Not Exsits In Database",
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    error = ex.Message,
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult VoteForCandidate(long candidateId , long electionId)
        {
            try
            {
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
                var candidate = db.ctCandidates.FirstOrDefault(e => e.CandidateId == candidateId);
                var election = db.ctElections.FirstOrDefault(e => e.ElectionId == electionId);
                if(election == null)
                {
                    return Json(new
                    {
                        status = false,
                        error = "This Election Id Is Not Exsist In Database"
                    }, JsonRequestBehavior.AllowGet);
                }
                if (election.EndDate.Date < DateTime.Now.Date)
                {
                    return Json(new
                    {
                        status = false,
                        error = "This Election Is Closed By End Date"
                    }, JsonRequestBehavior.AllowGet);
                }
                if (candidate != null)
                {
                    if (candidate.MemberId == ProfileId)
                    {
                        return Json(new
                        {
                            error = "You Can Not Vote To Your Self",
                            status = false
                        }, JsonRequestBehavior.AllowGet);
                    }
                    var ctVotting = db.ctVotings.FirstOrDefault(e => e.ElectionId == electionId && e.MemberVotterId == ProfileId);
                    if(ctVotting != null)
                    {
                        return Json(new
                        {
                            error = "You Already Votted In this Election",
                            status = false
                        }, JsonRequestBehavior.AllowGet);
                    }
                    ctVoting vote = new ctVoting()
                    {
                        CandidateId = candidateId,
                        MemberVotterId = ProfileId,
                        ElectionId = electionId,
                        CreationDate = DateTime.Now
                    };
                    db.ctVotings.Add(vote);
                    db.SaveChanges();
                    return Json(new
                    {
                        error = "",
                        status = true
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        error = "This Candidate Id Is Not Exsits In Database",
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    error = ex.Message,
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public int GetCandidateElectionStatus(ctCandidate candidate, DateTime startDate, long logedInMember , long electionId)
        {
            var memberVoted = db.ctVotings.FirstOrDefault(e => e.ElectionId == electionId && e.MemberVotterId == logedInMember);
            
            if (startDate.Date < DateTime.Now.Date && logedInMember != candidate.MemberId && memberVoted == null)
            {
                return (int)MemberCanVoteToElectionStatus.Opened;
            }
            else if (startDate.Date == DateTime.Now.Date && memberVoted == null)
            {
                return (int)MemberCanVoteToElectionStatus.VottingNotStarted;
            }
            else if (startDate.Date < DateTime.Now.Date && logedInMember != candidate.MemberId && memberVoted != null)
            {
                if(memberVoted.CandidateId == candidate.CandidateId)
                {
                    return (int)MemberCanVoteToElectionStatus.Confirmed;
                }
                else
                {
                    return (int)MemberCanVoteToElectionStatus.Closed;
                }
            }
            else
            {
                return (int)MemberCanVoteToElectionStatus.Closed;
            }
        }

        public CandidateCanObjectStatus GetCandidateElectioObjectionStatus(ctCandidate candidate, DateTime startDate, long logedInMember)
        {
            if(candidate.MemberId == logedInMember)
            {
                return CandidateCanObjectStatus.SameLoggedInUser;
            }
            else
            {
                var memberIsObjected = db.ctCandidateObjections.Any(e => e.CandidateId == candidate.CandidateId && e.MemberId == logedInMember);
                if (memberIsObjected)
                {
                    return CandidateCanObjectStatus.AlreadyObjected;
                }
                else if(startDate.Date == DateTime.Now.Date)
                {
                    return CandidateCanObjectStatus.CanObject;
                }
                else
                {
                    return CandidateCanObjectStatus.CloseObject;
                }
            }
        }

        private bool canMemberApplyForVote(ctRegistration objDbModel)
        {
            return objDbModel.MembershipType != "Students";
        }

        #region Results

        public int GetCurrentUserId()
        {
            var userTicket = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current).GetUmbracoAuthTicket();
            var currentUser = ApplicationContext.Services.UserService.GetByUsername(userTicket.Name);
            return currentUser.Id;
        }

        [HttpPost]
        public ActionResult GetElectionListForResults()
        {
            try
            {
                var model = new List<ElectionDropDownViewModel>();
                var userId = GetCurrentUserId();
                var electionCommiteAssigned = db.ctElectionCommittees.Where(e => e.UserAdminId == userId).Select(e => e.ElectionId).ToList();
                var elections = db.ctElections.Where(e => DbFunctions.TruncateTime(e.EndDate) < DbFunctions.TruncateTime(DateTime.Now) && electionCommiteAssigned.Contains(e.ElectionId)).ToList();
                if(elections.Any())
                {
                    model = elections.OrderByDescending(x => x.ElectionId).Select(e => new ElectionDropDownViewModel
                    {
                        ElectionId = e.ElectionId,
                        Title = string.Concat(e.Title, " ", "(" + e.StartDate.ToString("MM/dd/yyyy") + ")")
                    }).ToList();
                }
                return Json(new
                {
                    status = true,
                    ElectionInfo = model
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    error = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetVottingDetailsByElection(long electionId)
        {
            try
            {
                List<VottingResultsViewModel> model = new List<VottingResultsViewModel>();
                var election = db.ctElections.FirstOrDefault(e => e.ElectionId == electionId &&
                DbFunctions.TruncateTime(e.EndDate) < DbFunctions.TruncateTime(DateTime.Now));
                if (election == null)
                {
                    return Json(new
                    {
                        error = "This Election ID Not Exists In Database",
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
                var candidates = db.ctCandidates.Where(e => e.ElectionId == electionId &&
                                                       e.Status == (int)CandidateRequestStatus.Approve).ToList();
                if (candidates.Any())
                {
                    var candidateIds = candidates.Select(e => e.CandidateId).ToList();
                    var objectedCandidateIds = db.ctCandidateObjections.Where(e => candidateIds.Contains(e.CandidateId) 
                                          && e.Status == (int)CandidateObjectionStatus.Approved)
                                               .Select(e => e.CandidateId).ToList();
                    if(objectedCandidateIds.Any())
                    {
                        candidateIds = candidateIds.Where(e => !objectedCandidateIds.Contains(e)).ToList();
                        candidates = candidates.Where(e => candidateIds.Contains(e.CandidateId)).ToList();
                    }
                    var vottingInfoList = db.ctVotings.Where(e => candidateIds.Contains(e.CandidateId)).ToList();
                    foreach (var item in candidates)
                    {
                        var memberInfo = GetMemberInfoForVotting(item.MemberId);
                        if (memberInfo != null)
                        {
                            memberInfo.ElectionId = item.ElectionId;
                            memberInfo.CandidateId = item.CandidateId;
                            memberInfo.VottingCount = vottingInfoList.Where(e => e.CandidateId == item.CandidateId).Count();
                            model.Add(memberInfo);
                        }
                    }
                    // In case of tie breakers, we will check their membership duration with IIA. Old members will get preference. 
                    model = model.OrderByDescending(x => x.VottingCount).ThenBy(x => x.MembershipStartDate).ToList();
                    foreach (var (item, index) in model.WithIndex())
                    {
                        item.IsWinner = (index + 1) <= election.NumberOfWinners;
                    }
                }
                return Json(new
                {
                    status = true,
                    MemberInfo = model,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    error = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetCandidateVottingInfo(long candidateId ,long electionId)
        {
            try
            {
                var election = db.ctElections.FirstOrDefault(e => e.ElectionId == electionId);
                if(election == null)
                {
                    return Json(new
                    {
                        error = "This Election ID Not Exists In Database",
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
                var candidate = db.ctCandidates.FirstOrDefault(e => e.CandidateId == candidateId && e.ElectionId == electionId);
                if(candidate != null)
                {
                    var vottingInfo = db.ctVotings.Where(e => e.CandidateId == candidateId).ToList();
                    var vottingInfoDetials = vottingInfo.Select(e => new
                    {
                        MemberId = e.MemberVotterId , 
                        VottingDate = e.CreationDate.ToString("MM/dd/yyyy hh:mm:ss")
                    }).ToList();
                    var memberVotters = vottingInfoDetials.Select(e => e.MemberId).ToList();
                    if(memberVotters.Any())
                    {
                        var members = db.ctRegistrations.Where(e => memberVotters.Contains(e.ProfileId)).ToList();
                        if(members.Any())
                        {
                            var model = vottingInfoDetials.Select(e => new
                            {
                                MemberId = e.MemberId,
                                MemberName = GetMemberFullName(members.FirstOrDefault(a => a.ProfileId == e.MemberId)),
                                VottingDate = e.VottingDate
                            }).ToList();
                            return Json(new
                            {
                                MemberInfo = model,
                                status = true
                            }, JsonRequestBehavior.AllowGet);
                        }
                        return Json(new
                        {
                            status = false,
                            error = "There Are Members Votting For This Candidate",
                        }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new
                    {
                        status = false,
                        error = "There Are Members Votting For This Candidate",
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = false,
                    error = "There Is No Candidate Has This CandidateId In Database",
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    error = ex.Message,
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetMemberFullName(ctRegistration member)
        {
            return string.Concat(member.FirstName, " ", member.MiddleName, " ", member.LastName);
        }

        public VottingResultsViewModel GetMemberInfoForVotting(long memberId)
        {
            var item = db.ctRegistrations.FirstOrDefault(x => x.ProfileType == "Member" && x.ProfileId == memberId);
            if(item != null)
            {
                VottingResultsViewModel regItem = new VottingResultsViewModel();
                regItem.ProfileId = item.ProfileId;
                regItem.FirstName = item.FirstName;
                regItem.MiddleName = !string.IsNullOrEmpty(item.MiddleName) ? item.MiddleName : "";
                regItem.LastName = item.LastName;
                regItem.EmailAddress = item.EmailAddress;
                regItem.MembershipType = !string.IsNullOrEmpty(item.MembershipBadge) ? item.MembershipType + ", " + item.MembershipBadge : item.MembershipType;
                regItem.MobileNumber = !string.IsNullOrEmpty(item.MobileNumber) ? item.MobileNumber : "";
                regItem.Country = !string.IsNullOrEmpty(item.Country) ? db.CountryLists.Where(x => x.CountryCode == item.Country).FirstOrDefault().CountryName : "";
                regItem.City = !string.IsNullOrEmpty(item.City) ? item.City : "";
                regItem.MembershipStartDate = item.MembershipStartDate.HasValue ? item.MembershipStartDate.Value.ToString("dd MMM yyyy") : "";
                regItem.MembershipEndDate = item.MembershipEndDate.HasValue ? item.MembershipEndDate.Value.ToString("dd MMM yyyy") : "";
                regItem.IsActive = item.IsActive.Value == true ? "Yes" : "No";
                return regItem;
            }
            return null;
        }
        #endregion

        #region Board Voting Starts Mail For Members
        public ActionResult SendBoardVotingStartsMailForMembers(DateTime? currentDate)
        {
            try
            {
                var members = GetMembersCanVoteForElection();
                if (!members.Any())
                {
                    return Json(new
                    {
                        error = "No Members Founded Can Vote For Any Election",
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
                var electionsReadyForVote = GetActiveElectionsReadyForVoting(currentDate);
                if (electionsReadyForVote.Any())
                {
                    foreach (var member in members)
                    {
                        if (electionsReadyForVote.Any(e => member.MembershipStartDate <= e.StartDate))
                        {
                            EmailSettingModel eModelUser = Utility.GetEmailSetting("ElectionVotingStarts", member.ProfileId.ToString(), member.lang);
                            string MessageUser = EmailMessage.GetVotingMailMeesageBasedOnType(member.lang, VotingMailType.ElectionVotingStarts);
                            Utility.SendMail(eModelUser.FromName, eModelUser.FromEmail, eModelUser.EmailTo, eModelUser.EmailCC, eModelUser.EmailBCC, eModelUser.Subject, MessageUser);
                        }
                    }
                    return Json(new
                    {
                        error = "",
                        status = true
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        error = "No Elections In The System Is Ready For Voting",
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    error = ex.Message,
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public List<ctRegistration> GetMembersCanVoteForElection()
        {
            var membersList = db.ctRegistrations.Where(e => e.MembershipType != "Students" && e.MembershipStartDate != null).ToList();
            return membersList;
        }

        public List<ctElection> GetActiveElectionsReadyForVoting(DateTime? currentDateTime)
        {
            if (currentDateTime == null)
            {
                currentDateTime = DateTime.Today;
            }
            var elections = db.ctElections.Where(x => (DbFunctions.TruncateTime(currentDateTime) == DbFunctions.TruncateTime(x.StartDate) &&
                                                        x.Status == (int)ElectionStatus.Working)).ToList();
            return elections;
        }
        #endregion

        #region ObjectionRequestSubmission
        public bool SendMailForObjectionRequestByMember(long profileId , long candidateId)
        {
            var member = db.ctRegistrations.FirstOrDefault(e => e.ProfileId == profileId);
            var candidate = db.ctCandidates.FirstOrDefault(e => e.CandidateId == candidateId);
            if (member != null && candidate != null)
            {
                var candidateInfo = db.ctCandidateInfoes.FirstOrDefault(e => e.CandidateId == candidateId);
                if (candidateInfo != null)
                {
                    EmailSettingModel eModelUser = Utility.GetEmailSetting("ObjectionForCandidate", member.ProfileId.ToString(), member.lang);
                    string MessageUser = EmailMessage.GetMemberElectionObjectionForCandidate(member.lang, candidateInfo, member.FirstName, member.FirstName_ar);
                    Utility.SendMail(eModelUser?.FromName, eModelUser?.FromEmail, eModelUser?.EmailTo, eModelUser?.EmailCC, eModelUser?.EmailBCC, eModelUser?.Subject, MessageUser);
                    return true;
                }
                return false;
            }
            return false;
        }
        public bool SendMailForObjectionCommittee(long profileId, long candidateId)
        {
            var member = db.ctRegistrations.FirstOrDefault(e => e.ProfileId == profileId);
            var candidate = db.ctCandidates.FirstOrDefault(e => e.CandidateId == candidateId);
            List<ctElectionUserMail> committee = db.ctElectionUserMails.Where(e => e.ElectionId == candidate.ElectionId).ToList();
            if (member != null && candidate != null && committee.Count > 0)
            {
                var candidateInfo = db.ctCandidateInfoes.FirstOrDefault(e => e.CandidateId == candidateId);
                if (candidateInfo != null)
                {
                    foreach (ctElectionUserMail committeeMember in committee)
                    {
                        EmailSettingModel eModelUser = Utility.GetEmailSetting("ObjectionForCandidateToCommittee", member.ProfileId.ToString(), member.lang);
                        string MessageUser = EmailMessage.GetMemberElectionObjectionForCandidateToCommittee(member.lang, candidateInfo, committeeMember.UserEmail, committeeMember.UserEmail);
                        Utility.SendMail(eModelUser?.FromName, eModelUser?.FromEmail, committeeMember.UserEmail, eModelUser?.EmailCC, eModelUser?.EmailBCC, eModelUser?.Subject, MessageUser);
                        
                    }
                    return true;
                }
                return false;
            }
            return false;
        }

        #endregion
    }
}