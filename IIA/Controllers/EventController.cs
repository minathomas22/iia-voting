﻿using System;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using IIA.DBContext;
using System.Linq.Dynamic;
using umbraco.cms.helpers;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using IIA.Models;
using CMS.Helpers;
using System.Data.Entity;
using Umbraco.Web;
using System.Web.Configuration;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using umbraco.editorControls.SettingControls.Pickers;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using Umbraco.Core.Models;
using IIA.Helper;

namespace IIA.Controllers
{
    public class EventController : SurfaceController
    {
        IIAContext db = new IIAContext();
        [HttpPost]
        public JsonResult GetEvents(string lang, string date, string pastEvent, string eventTypeId, string page)
        {
            try
            {
                int pageIndex = !string.IsNullOrEmpty(page) ? int.Parse(page) : 1;
                int pageSize = 8;
                int totalPage = 0;

                List<EventsViewModelcs> listItem = new List<EventsViewModelcs>();

                int EventsID = lang == "en" ? 2384 : 2516;
                var Events = Umbraco.TypedContent(EventsID);

                var EventList = Events.Descendants().Where(x => x.DocumentTypeAlias == "eventsItem" && x.IsVisible() && x.GetPropertyValue<DateTime>("date") >= DateTime.Now.Date).OrderByDescending(x => x.GetPropertyValue<DateTime>("date")).ToList();

                if(!string.IsNullOrEmpty(pastEvent))
                {
                    EventList= Events.Descendants().Where(x => x.DocumentTypeAlias == "eventsItem" && x.IsVisible() && x.GetPropertyValue<DateTime>("date") <= DateTime.Now.Date).OrderByDescending(x => x.GetPropertyValue<DateTime>("date")).ToList();
                }

                if (!string.IsNullOrEmpty(eventTypeId))
                {
                    EventList = EventList.Where(x => x.Parent.Id == int.Parse(eventTypeId)).OrderByDescending(x => x.GetPropertyValue<DateTime>("date")).ToList();
                }

                if (!string.IsNullOrEmpty(date))
                {
                    date = date.Replace("to", "#");
                    var dateList = date.Split('#');

                    if (dateList.Length > 1)
                    {
                        EventList = EventList.Where(x => x.GetPropertyValue<DateTime>("date") >= DateTime.Parse(dateList[0].ToString().Trim()) && x.GetPropertyValue<DateTime>("date") <= DateTime.Parse(dateList[1].ToString().Trim())).OrderByDescending(x => x.GetPropertyValue<DateTime>("date")).ToList();
                    }
                    else
                    {
                        EventList = EventList.Where(x => x.GetPropertyValue<DateTime>("date") >= DateTime.Parse(dateList[0].ToString().Trim())).OrderByDescending(x => x.GetPropertyValue<DateTime>("date")).ToList();
                    }


                }
                foreach (var item in EventList)
                {
                    EventsViewModelcs lItem = new EventsViewModelcs();
                    lItem.EventId = item.Id;
                    lItem.EventCmsName = item.Name;
                    lItem.Title = item.GetPropertyValue("title").ToString();
                    lItem.Introduction = item.GetPropertyValue("introduction").ToString();
                    lItem.City = item.GetPropertyValue("city").ToString();
                    lItem.Type = item.GetPropertyValue("type").ToString();
                    lItem.Date = item.GetPropertyValue("displayDate").ToString();
                    lItem.Time = item.GetPropertyValue("time").ToString();
                    lItem.Image = item.GetPropertyValue<IPublishedContent>("thumbnail").Url.ToString();
                    lItem.Url = item.Url;
                    listItem.Add(lItem);
                }

                totalPage = (int)Math.Ceiling((double)listItem.Count() / pageSize);

                return Json(new { status = true, EventList = listItem, totalPage = totalPage }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult EventRegister(EventRegisterModel model)
        {
            try
            {
                EventRegister dbModel = new EventRegister()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    EmailAddress = model.EmailAddress,
                    ContactNo = model.ContactNo,
                    Country = db.CountryLists.Where(x => x.CountryCode == model.Country).FirstOrDefault().CountryName,
                    City = model.City,
                    ZipCode = model.ZipCode,
                    EventId=model.EventId,
                    EventCmsName=model.EventCmsName,
                    RegistrationDate=DateTime.Now                  
                };

                db.EventRegisters.Add(dbModel);
                db.SaveChanges();

                var eventContent = Umbraco.TypedContent(model.EventId);

                EmailSettingModel eModel = Utility.GetEmailSettingEvent("EventRegister", "", "", model.lang);

                string Message = EmailMessage.GetEventRegistrationMessage(dbModel, model.lang);

                Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);

                EmailSettingModel eModelUser = Utility.GetEmailSettingEvent("EventRegisterUser", model.EmailAddress, eventContent.GetPropertyValue("emailSubject").ToString(), model.lang);

                string MessageUser = EmailMessage.GetEventRegistrationMessageUser(dbModel.FirstName, eventContent.GetPropertyValue("emailMessage").ToString(), eventContent.GetPropertyValue("displayDate").ToString(), eventContent.GetPropertyValue("time").ToString(), eventContent.GetPropertyValue("place").ToString(), eventContent.GetPropertyValue("googleMapLink").ToString(), model.lang);

                Utility.SendMail(eModelUser.FromName, eModelUser.FromEmail, eModelUser.EmailTo, eModelUser.EmailCC, eModelUser.EmailBCC, eModelUser.Subject, MessageUser);

                return Json(new
                {
                    status = true,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }


        }
    }
}