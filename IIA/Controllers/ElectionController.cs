﻿using System;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using IIA.DBContext;
using System.Linq.Dynamic;
using umbraco.cms.helpers;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using IIA.Models;
using CMS.Helpers;
using System.Data.Entity;
using Umbraco.Web;
using System.Web.Configuration;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using umbraco.editorControls.SettingControls.Pickers;
using Newtonsoft.Json;
using Umbraco.Core.Models;
using IIA.Helper;
using System.Data;
using System.Data.SqlClient;
using Umbraco.Web.Media.EmbedProviders;
using umbraco;
using System.Web.Mvc.Html;

namespace IIA.Controllers
{
    public class ElectionController : SurfaceController
    {
        IIAContext db = new IIAContext();

        [HttpPost]
        public ActionResult GetElectionList()
        {
            try
            {
                var elections = db.ctElections.ToList();
                var model = elections.OrderByDescending(x => x.ElectionId).Select(e => new ElectionDropDownViewModel
                {
                    ElectionId = e.ElectionId,
                    Title = string.Concat(e.Title, " ", "(" + e.StartDate.ToString("MM/dd/yyyy") + ")")
                }).ToList();

                return Json(new
                {
                    status = true,
                    ElectionInfo = model
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetElectionStatusList()
        {
            try
            {
                var model = EnumHelper.GetSelectList(typeof(ElectionStatus)).Select(e => new ElectionStatusViewModel()
                {
                    Id = int.Parse(e.Value),
                    Text = e.Text.ToString()
                }).ToList();
                return Json(new
                {
                    status = true,
                    ElectionStatusInfo = model
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetElectionInfo(long electionId)
        {
            try
            {
                var viewModel = new ElectionInfoViewModel();
                var dbModel = db.ctElections.FirstOrDefault(x => x.ElectionId == electionId);
                if (dbModel != null)
                {
                    viewModel.ElectionId = dbModel.ElectionId;
                    viewModel.Title = dbModel.Title;
                    viewModel.TitleAr = dbModel.TitleAr;
                    viewModel.ApplyTitle = dbModel.ApplyTitle;
                    viewModel.ApplyTitleAr = dbModel.ApplyTitleAr;
                    viewModel.Description = dbModel.Description;
                    viewModel.StartDate = dbModel.StartDate.ToString("MM/dd/yyyy");
                    viewModel.EndDate = dbModel.EndDate.ToString("MM/dd/yyyy");
                    viewModel.ApplyStartDate = dbModel.ApplyStartDate.ToString("MM/dd/yyyy");
                    viewModel.ApplyEndDate = dbModel.ApplyEndDate.ToString("MM/dd/yyyy");
                    viewModel.NumberOfWinners = dbModel.NumberOfWinners;

                    viewModel.WinningAnnouncementDate = dbModel.WinningAnnouncementDate.ToString("yyyy-MM-ddThh:mm");
                    viewModel.Status = (ElectionStatus)dbModel.Status;
                }
                return Json(new
                {
                    status = true,
                    ElectionInfo = viewModel
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult RedoElection(long electionId)
        {
            try
            {
                var electionDb = db.ctElections.FirstOrDefault(x => x.ElectionId == electionId);
                if (electionDb != null)
                {
                    var newElectionDb = new ctElection()
                    {
                        Title = electionDb.Title,
                        Description = electionDb.Description,
                        StartDate = electionDb.StartDate,
                        EndDate = electionDb.EndDate,
                        ApplyStartDate = electionDb.ApplyStartDate,
                        ApplyEndDate = electionDb.ApplyEndDate,
                        NumberOfWinners = electionDb.NumberOfWinners,
                        WinningAnnouncementDate = electionDb.WinningAnnouncementDate,
                        CreatedByRedo = true,
                        Status = (int)ElectionStatus.Working,
                        CreatedOn = DateTime.Now
                    };
                    db.ctElections.Add(newElectionDb);
                    db.SaveChanges();
                    return Json(new
                    {
                        status = true
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveElection(ElectionViewModel model)
        {
            try
            {
                var ctElection = new ctElection();
                var isValid = ValidateElectionModel(model);
                if (!isValid)
                {
                    return Json(new
                    {
                        data = model,
                        error = "Date Conflict",
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
                // Create Mode
                if (model.ElectionId == 0)
                {
                    ctElection.Title = model.Title;
                    ctElection.TitleAr = model.TitleAr;
                    ctElection.ApplyTitle = model.ApplyTitle;
                    ctElection.ApplyTitleAr = model.ApplyTitleAr;
                    ctElection.Description = model.Description;
                    ctElection.StartDate = model.StartDate;
                    ctElection.EndDate = model.EndDate;
                    ctElection.ApplyStartDate = model.ApplyStartDate;
                    ctElection.ApplyEndDate = model.ApplyEndDate;
                    ctElection.NumberOfWinners = model.NumberOfWinners;
                    ctElection.WinningAnnouncementDate = model.WinningAnnouncementDate;
                    ctElection.Status = (int)ElectionStatus.Working;
                    ctElection.CreatedOn = DateTime.Now;
                    ctElection.CreatedByRedo = model.IsRedo;
                    var newElection = db.ctElections.Add(ctElection);
                    db.SaveChanges();
                    if (model.IsRedo)
                    {
                        var candidates = db.ctCandidates.Where(e => e.ElectionId == model.PastElectionId && e.Status == (int)CandidateRequestStatus.Approve).ToList();
                        foreach (var candidateItem in candidates)
                        {
                            var ctNewCandidate = new ctCandidate();
                            ctNewCandidate.ElectionId = newElection.ElectionId;
                            ctNewCandidate.MemberId = candidateItem.MemberId;
                            ctNewCandidate.Status = candidateItem.Status;
                            ctNewCandidate.CreatedOn = DateTime.Now;
                            var newCandidate = db.ctCandidates.Add(ctNewCandidate);
                            db.SaveChanges();
                            var candidatesInfo = db.ctCandidateInfoes.FirstOrDefault(e => e.CandidateId == candidateItem.CandidateId);
                            if (candidatesInfo != null)
                            {
                                db.ctCandidateInfoes.Add(new ctCandidateInfo()
                                {
                                    CandidateId = newCandidate.CandidateId,
                                    Bio = candidatesInfo.Bio,
                                    Description = candidatesInfo.Description,
                                    ResumeFile = candidatesInfo.ResumeFile,
                                    ElectionProgramFile = candidatesInfo.ElectionProgramFile,
                                    LetterOfInterestFiles = candidatesInfo.LetterOfInterestFiles,
                                    OtherDocumentFile = candidatesInfo.OtherDocumentFile,
                                    CreatedOn = DateTime.Now
                                });
                            }
                        }
                        db.SaveChanges();
                    }
                }
                // Update Mode
                else
                {
                    var electionDb = db.ctElections.FirstOrDefault(x => x.ElectionId == model.ElectionId);
                    if (electionDb != null)
                    {
                        electionDb.Title = model.Title;
                        electionDb.TitleAr = model.TitleAr;
                        electionDb.ApplyTitle = model.ApplyTitle;
                        electionDb.ApplyTitleAr = model.ApplyTitleAr;
                        electionDb.Description = model.Description;
                        electionDb.StartDate = model.StartDate;
                        electionDb.EndDate = model.EndDate;
                        electionDb.ApplyStartDate = model.ApplyStartDate;
                        electionDb.ApplyEndDate = model.ApplyEndDate;
                        electionDb.NumberOfWinners = model.NumberOfWinners;
                        electionDb.WinningAnnouncementDate = model.WinningAnnouncementDate;
                        electionDb.Status = (int)model.Status;
                        electionDb.UpdatedOn = DateTime.Now;
                        db.Entry(electionDb).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                return Json(new
                {
                    data = model,
                    error = "",
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    data = model,
                    error = ex.Message,
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ClearVotes(long electionId)
        {
            try
            {
                var election = db.ctElections.FirstOrDefault(e => e.ElectionId == electionId);
                if (election != null)
                {
                    var candidates = db.ctCandidates.Where(e => e.ElectionId == election.ElectionId).ToList();
                    var candidateIds = candidates.Select(e => e.CandidateId).ToList();
                    if (candidateIds.Any())
                    {
                        var votes = db.ctVotings.Where(e => candidateIds.Contains(e.CandidateId)).ToList();
                        if (votes.Any())
                        {
                            db.ctVotings.RemoveRange(votes);
                            db.SaveChanges();
                            return Json(new
                            {
                                error = "",
                                status = true
                            }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new
                            {
                                error = "No Saved Votes Related To This Election",
                                status = false
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            error = "No Candidates Assigned To This Election",
                            status = false
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new
                    {
                        error = "No Election Has This Election Id",
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    error = ex.Message,
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ApplyVottingElectionInCookie(int electionId)
        {
            try
            {
                Utility.SetApplicationCookies("VoteElection", electionId.ToString());
                return Json(new
                {
                    status = true,
                    message = "",
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }

        }

        private bool canMemberApplyForVote(ctRegistration objDbModel)
        {
            return objDbModel.MembershipType != "Students";
        }

        #region HelperMethod
        public bool ValidateElectionModel(ElectionViewModel model)
        {
            // Check If End Date Must Be Greater Than Start Date
            if ((model.EndDate < model.StartDate) || (model.ApplyEndDate < model.ApplyStartDate))
            {
                return false;
            }
            if( (model.StartDate < model.ApplyStartDate) || (model.EndDate < model.ApplyStartDate))
            {
                return false;
            }
            else if ((model.WinningAnnouncementDate < model.EndDate) || (model.WinningAnnouncementDate < model.ApplyEndDate))
            {
                return false;
            }

            return true;
        }
        #endregion

        #region Election Mail User

        public ActionResult GetAssignedMailUsersToElection(long electionId)
        {
            try
            {
                var election = db.ctElections.FirstOrDefault(e => e.ElectionId == electionId);
                if(election != null)
                {
                    var model = db.ctElectionUserMails.Where(e => e.ElectionId == electionId).ToList();
                    var result = model.Select(a => new
                    {
                        a.ElectionUserMailId , 
                        a.ElectionId ,
                        a.UserEmail,
                        a.UserName
                    }).ToList();
                    return Json(new
                    {
                        MailUserInfo = result,
                        error = "",
                        status = true,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        error = "The Selected Election Not Exsits In Database",
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
                
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    error = ex.Message,
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RemoveMailUserAssignedToElection(long electionUserMailId)
        {
            try
            {
                var electionUserMail = db.ctElectionUserMails.FirstOrDefault(e => e.ElectionUserMailId == electionUserMailId);
                if(electionUserMail != null)
                {
                    db.ctElectionUserMails.Remove(electionUserMail);
                    db.SaveChanges();
                    return Json(new
                    {
                        error = "",
                        status = true
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        error = "This Election User Mail Id Not Exists In Database",
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    error = ex.Message,
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AssignUserMailToElection(ElectionMailUserViewModel model)
        {
            try
            {
                var isValid =  ValidateMailIsExistsInSameElection(model);
                if(isValid)
                {
                    ctElectionUserMail newObj = new ctElectionUserMail()
                    {
                        ElectionId = model.ElectionId,
                        UserName = model.UserName.Trim(),
                        UserEmail = model.UserMail.Trim(),
                        CreationDate = DateTime.Now
                    };
                    db.ctElectionUserMails.Add(newObj);
                    db.SaveChanges();
                    return Json(new
                    {
                        error = "",
                        status = true
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        error = "This Mail Is Already Exists In The Same Election With Different User Name",
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
                
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    error = ex.Message,
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }


        }

        public bool ValidateMailIsExistsInSameElection(ElectionMailUserViewModel model)
        {
            var isExists = db.ctElectionUserMails.Any(e => e.UserEmail.ToLower() == model.UserMail.ToLower().Trim() && e.ElectionId == model.ElectionId);
            if (isExists)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion

        #region Nomination Applications Mail
        public ActionResult SendNominationApplicationsMailForMembers(DateTime? currentDate)
        {
            try
            {
                var candidates = GetCandidatesCanApplyForElection(currentDate);
                if(!candidates.Any())
                {
                    return Json(new
                    {
                        error = "No Candidates Can Apply For Election In The System",
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
                var readyElections = GetActiveElectionsReadyForApplyCandidate(currentDate);
                if (!readyElections.Any())
                {
                    return Json(new
                    {
                        error = "No Elections In The System Is Ready For Applied",
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
                foreach (var candidate in candidates)
                {
                        EmailSettingModel eModelUser = Utility.GetEmailSetting("NominationApplications", candidate.ProfileId.ToString(), candidate.lang);
                        string MessageUser = EmailMessage.GetVotingMailMeesageBasedOnType(candidate.lang, VotingMailType.NominationApplications);
                        Utility.SendMail(eModelUser.FromName, eModelUser.FromEmail, eModelUser.EmailTo, eModelUser.EmailCC, eModelUser.EmailBCC, eModelUser.Subject, MessageUser);
                }
                return Json(new
                {
                    error = "",
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    error = ex.Message,
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public List<ctRegistration> GetCandidatesCanApplyForElection(DateTime? currentDate)
        {
            if(currentDate == null)
            {
                currentDate = DateTime.Today;
            }
            DateTime monthAgoDate = currentDate.Value.AddMonths(-4);
            var candidatesList = db.ctRegistrations.Where(e => e.MembershipStartDate != null &&
                                                           (e.BanDate == null || DateTime.Today > e.BanDate) &&
                                                            e.MembershipStartDate <= monthAgoDate &&
                                                            e.MembershipType != "Students").ToList();
            return candidatesList;
        }

        public List<ctElection> GetActiveElectionsReadyForApplyCandidate(DateTime? currentDateTime)
        {
            if (currentDateTime == null)
            {
                currentDateTime = DateTime.Today;
            }
            var elections = db.ctElections.Where(x => (DbFunctions.TruncateTime(currentDateTime) == DbFunctions.TruncateTime(x.ApplyStartDate) &&
                                                        x.Status == (int)ElectionStatus.Working)).ToList();
            return elections;
        }
        #endregion

        #region Election Candidate Objection Requests
        [HttpPost]
        public ActionResult GetAllCandidateObjectionRequests(long electionId , long? candidateObjectionId = null)
        {
            try
            {
                var objectionRequestsList = GetObjectionRequestInfo(electionId);
                if (candidateObjectionId != null)
                {
                    var model = objectionRequestsList.FirstOrDefault();
                    if (model != null)
                    {
                        return Json(new
                        {
                            data = model,
                            status = true,
                            message = "",
                        }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new
                        {
                            data = new { },
                            status = false,
                            error = "No Objection Exists Related To This Info",
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new
                    {
                        data = objectionRequestsList,
                        status = true,
                        message = "",
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    error = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ChangeObjectionCandidateStatus(long candidateObjectionId, bool isApprove, DateTime? banDate)
        {
            try
            {
                var candidateObjection = db.ctCandidateObjections.FirstOrDefault(e => e.CandidateObjectionId == candidateObjectionId);
                if (candidateObjection != null)
                {
                    if (isApprove)
                    {
                        if(banDate != null)
                        {
                            candidateObjection.BanDate = banDate;
                        }
                        var candidate = db.ctCandidates.FirstOrDefault(e => e.CandidateId == candidateObjection.CandidateId);
                        if (candidate != null)
                        {
                            var member = db.ctRegistrations.FirstOrDefault(e => e.ProfileId == candidate.MemberId);
                            if (member != null)
                            {
                                member.BanDate = banDate;
                                db.Entry(member).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                    }
                    candidateObjection.Status = isApprove ? (int)CandidateObjectionStatus.Approved : (int)CandidateObjectionStatus.Rejected;
                    db.Entry(candidateObjection).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json(new
                    {
                        error = "",
                        status = true
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        error = "This Candidate Objection Id Is Not Exsits In Database",
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    error = ex.Message,
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }
         
        public List<ElectionObjectionRequestsViewModel> GetObjectionRequestInfo(long electionId , long? objectionRequestId = null)
        {
            var election = db.ctElections.FirstOrDefault(e => e.ElectionId == electionId);
            var objectionRequestList = new List<ElectionObjectionRequestsViewModel>();
            if (election != null)
            {
                var candidates = db.ctCandidates.Where(e => e.ElectionId == electionId).ToList();
                var candidateIds = candidates.Select(e => e.CandidateId).ToList();
                var candidatesMemberIds = candidates.Select(e => e.MemberId).ToList();
                if (candidateIds.Any())
                {
                    var objectionRequests = new List<ctCandidateObjection>();
                    if (objectionRequestId != null)
                    {
                         objectionRequests = db.ctCandidateObjections.Where(e => e.CandidateObjectionId == objectionRequestId).ToList();
                    }
                    else
                    {
                         objectionRequests = db.ctCandidateObjections.Where(e => candidateIds.Contains(e.CandidateId)).ToList();
                    }
                    var objectedMemberIds = objectionRequests.Select(e => e.MemberId).ToList();
                    if (objectedMemberIds.Any())
                    {
                        candidatesMemberIds.AddRange(objectedMemberIds);
                        candidatesMemberIds = candidatesMemberIds.Distinct().ToList();
                    }
                    var members = db.ctRegistrations.Where(e => candidatesMemberIds.Contains(e.ProfileId)).ToList();
                    foreach (var request in objectionRequests)
                    {
                        var candidate = candidates.FirstOrDefault(e => e.CandidateId == request.CandidateId);
                        var candidateMember = members.FirstOrDefault(e => e.ProfileId == candidate.MemberId);
                        var objectedMember = members.FirstOrDefault(e => e.ProfileId == request.MemberId);
                        if (candidateMember != null && objectedMember != null)
                        {
                            objectionRequestList.Add(new ElectionObjectionRequestsViewModel()
                            {
                                CandidateName = string.Concat(candidateMember?.FirstName, " ", candidateMember?.MiddleName, " ", candidateMember?.LastName),
                                MemberName = string.Concat(objectedMember?.FirstName, " ", objectedMember?.MiddleName, " ", objectedMember?.LastName),
                                ElectionName = election.Title,
                                StatusName = request.Status == (int)CandidateObjectionStatus.Pending ? "Pending" :
                                             request.Status == (int)CandidateObjectionStatus.Approved ? "Approved" :
                                             request.Status == (int)CandidateObjectionStatus.Rejected ? "Rejected" : "",

                                ObjectionRequestId = request.CandidateObjectionId,
                                Reason = request.Reason,
                                BanDate = request.BanDate == null ? "" : ((DateTime)request.BanDate).ToString("MM/dd/yyyy")
                            });
                        }
                    }
                }
            }
            return objectionRequestList;
        }
        #endregion
    }
}