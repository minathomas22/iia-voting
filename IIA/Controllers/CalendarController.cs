﻿using System;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using IIA.DBContext;
using System.Linq.Dynamic;
using umbraco.cms.helpers;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using IIA.Models;
using CMS.Helpers;
using System.Data.Entity;
using Umbraco.Web;
using System.Web.Configuration;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using umbraco.editorControls.SettingControls.Pickers;
using Newtonsoft.Json;
using System.Globalization;
using Umbraco.Core.Models;
using Umbraco.Core;
using nuPickers;
using umbraco.editorControls.tinyMCE3.webcontrol.plugin;

namespace IIA.Controllers
{    
    public class CalendarController : SurfaceController
    {
        IIAContext db = new IIAContext();
        public int VAT = 15;

        [HttpPost]
        public JsonResult GetCourseCategory(string lang)
        {           
            List<SelectListItem> listItem = new List<SelectListItem>();

            int trainingCalendarId = lang == "en" ? 1924 : 3240;
            var trainingCalendar = Umbraco.TypedContent(trainingCalendarId);
            var CourseCategory = trainingCalendar.Children.Where(x => x.DocumentTypeAlias == "trainingCalendarCourseCategory" && x.IsVisible()).ToList();
            foreach (var item in CourseCategory)
            {
                SelectListItem lItem = new SelectListItem();
                lItem.Text = item.GetPropertyValue("courseCategoryTitle").ToString();
                lItem.Value = item.Id.ToString();
                listItem.Add(lItem);
            }
            return Json(new { CourseCategory = listItem }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetCourse(string lang, int courseCategoryId)
        {           
            List<SelectListItem> listItem = new List<SelectListItem>();

            int trainingCalendarId = lang == "en" ? 1924 : 3240;
            var trainingCalendar = Umbraco.TypedContent(trainingCalendarId);
            var Course = trainingCalendar.Descendants("trainingCalendarCourse").Where(x=>x.Parent.Id== courseCategoryId); 
            foreach (var item in Course)
            {
                SelectListItem lItem = new SelectListItem();
                lItem.Text = item.GetPropertyValue("courseTitle").ToString();
                lItem.Value = item.Id.ToString();
                listItem.Add(lItem);
            }
            return Json(new { Courses = listItem }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetLevel(string lang, int courseId)
        {           
            List<SelectListItem> listItem = new List<SelectListItem>();

            int trainingCalendarId = lang == "en" ? 1924 : 3240;
            var trainingCalendar = Umbraco.TypedContent(trainingCalendarId);
            var Level = trainingCalendar.Descendants("trainingCalendarCourseLevel").Where(x => x.Parent.Id == courseId);
            foreach (var item in Level)
            {
                SelectListItem lItem = new SelectListItem();
                lItem.Text = item.GetPropertyValue("courseLevel").ToString();
                lItem.Value = item.Id.ToString();
                listItem.Add(lItem);
            }
            return Json(new { Level = listItem }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult StudyMethods(string lang)
        {            
            List<SelectListItem> listItem = new List<SelectListItem>();

            int home = lang == "en" ? 1131 : 2459;
            var homePage = Umbraco.TypedContent(home);
            var StudyMethods = homePage.Descendants("trainingMethod");
            foreach (var item in StudyMethods)
            {
                SelectListItem lItem = new SelectListItem();
                lItem.Text = item.GetPropertyValue("title").ToString();
                lItem.Value = item.Id.ToString();
                listItem.Add(lItem);
            }
            return Json(new { StudyMethods = listItem }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetCalendarList(string lang, string courseCategoryId, string courseId, string levelId, string studyMethodId, string month, string coursePage)
        {
            TrainingCalendarViewModel model = new TrainingCalendarViewModel();
            List<TrainingCalendarCourse> trainingCalendarCourses = new List<TrainingCalendarCourse>();
            

            string strMainCourse = "";
            string cmsMainCourseName = "";
            int cmsMainCourseId = 0;

            string strCouseLevel = "";
            string cmsLevelName = "";
            int cmsLevelId = 0;
                       


            string courseName = "";
            int trainingCalendarId = lang == "en" ? 1924 : 3240;
            var trainingCalendar = Umbraco.TypedContent(trainingCalendarId);

            int trainingCourseId = lang == "en" ? 1304 : 2525;
            var trainingCourse = Umbraco.TypedContent(trainingCourseId);

            var TrainingandCourses = Umbraco.TypedContent(lang=="en"? "1304" : "2525");

            if (!string.IsNullOrEmpty(courseId))
            {
                if(coursePage=="tc")
                {
                    courseName = trainingCalendar.Descendants("trainingCalendarCourse").Where(x => x.Id == int.Parse(courseId)).FirstOrDefault().Name;
                }
                else
                {
                    courseName = trainingCourse.Descendants("trainingCourseItem").Where(x => x.Id == int.Parse(courseId)).FirstOrDefault().Name;
                }                
            }

            var Course = string.IsNullOrEmpty(courseName) ?trainingCalendar.Descendants("trainingCalendarCourse"): trainingCalendar.Descendants("trainingCalendarCourse").Where(x=>x.Name== courseName);

            foreach (var itemCourse in Course)
            {
                strMainCourse = itemCourse.GetPropertyValue("courseTitle").ToString();
                cmsMainCourseId = itemCourse.Id;
                cmsMainCourseName = itemCourse.GetPropertyValue("courseTitle").ToString();

                var Level = itemCourse.Descendants("trainingCalendarCourseLevel");

                if (!string.IsNullOrEmpty(levelId))
                {
                    var allCategory = levelId.Split(',');
                    for(int i=0;i<allCategory.Length;i++)
                    {
                        Level = Level.Where(x => x.Id == int.Parse(allCategory[i]));
                        foreach (var levelItem in Level)
                        {
                            strCouseLevel = levelItem.GetPropertyValue("courseLevel").ToString();
                            cmsLevelName = levelItem.GetPropertyValue("courseLevel").ToString();
                            cmsLevelId = levelItem.Id;

                            var TrainingMonth = levelItem.Children.Where(x => x.IsVisible()).ToList();

                            foreach (var itemTrainingMonth in TrainingMonth)
                            {
                                var TrainingDates = itemTrainingMonth.GetPropertyValue<IEnumerable<IPublishedContent>>("availableDates").Where(x => x.GetPropertyValue<DateTime>("startDate") >= DateTime.Now.Date && x.IsVisible()==true);
                                if (!string.IsNullOrEmpty(studyMethodId))
                                {
                                    TrainingDates = TrainingDates.Where(x => x.GetPropertyValue("studyMethod").ToString() == studyMethodId);
                                }
                                if (!string.IsNullOrEmpty(month))
                                {
                                    TrainingDates = TrainingDates.Where(x => x.GetPropertyValue<DateTime>("startDate").Month == int.Parse(month));
                                }
                                if (TrainingDates.Count() > 0)
                                {
                                    TrainingCalendarCourse trainingCalendarCourse = new TrainingCalendarCourse();
                                    trainingCalendarCourse.MainCourse = strMainCourse;
                                    trainingCalendarCourse.cmsMainCourseName = cmsMainCourseName;
                                    trainingCalendarCourse.cmsMainCourseId = cmsMainCourseId;

                                    trainingCalendarCourse.Level = strCouseLevel;
                                    trainingCalendarCourse.cmsLevelName = cmsLevelName;
                                    trainingCalendarCourse.cmsLevelId = cmsLevelId;

                                    List<TrainingCalendarDate> trainingCalendarDates = new List<TrainingCalendarDate>();
                                    foreach (var item in TrainingDates)
                                    {
                                        TrainingCalendarDate trainingCalendarDate = new TrainingCalendarDate();
                                        trainingCalendarDate.StartDate = item.GetPropertyValue<DateTime>("startDate").ToString("dd MMM yyyy");
                                        trainingCalendarDate.EndDate = item.GetPropertyValue<DateTime>("endDate").ToString("dd MMM yyyy");
                                        trainingCalendarDate.EndDate = item.GetPropertyValue<DateTime>("endDate").ToString("dd MMM yyyy");
                                        trainingCalendarDate.Price = decimal.Parse(item.GetPropertyValue("price").ToString());
                                        trainingCalendarDate.DisplayPrice = trainingCalendarDate.Price + (decimal.Parse(item.GetPropertyValue("price").ToString())*VAT)/100;

                                        trainingCalendarDate.Duration = !string.IsNullOrEmpty(item.GetPropertyValue<string>("duration")) ? item.GetPropertyValue<string>("duration") : "";

                                        //TimeSpan span = item.GetPropertyValue<DateTime>("endDate").Subtract(item.GetPropertyValue<DateTime>("startDate"));
                                        //if (!string.IsNullOrEmpty(item.GetPropertyValue<string>("duration")))
                                        //{
                                        //    if (item.GetPropertyValue<int>("duration")>0)
                                        //    {
                                        //        trainingCalendarDate.Duration = item.GetPropertyValue<string>("duration");
                                        //    }
                                        //    else
                                        //    {
                                        //        trainingCalendarDate.Duration = (span.Days + 1).ToString();
                                        //    }
                                        //}
                                        //else
                                        //{
                                        //    trainingCalendarDate.Duration = (span.Days + 1).ToString();
                                        //}
                                        trainingCalendarDate.TotalHours = !string.IsNullOrEmpty(item.GetPropertyValue<string>("totalHours"))? item.GetPropertyValue<string>("totalHours"):"";

                                        var itemStudyMethod = Umbraco.TypedContent(int.Parse(item.GetPropertyValue("studyMethod").ToString()));

                                        trainingCalendarDate.StudyMethods = itemStudyMethod.GetPropertyValue("title").ToString();
                                        trainingCalendarDate.cmsStudyMethodName = itemStudyMethod.GetPropertyValue("title").ToString();
                                        trainingCalendarDate.cmsStudyMethodsId = itemStudyMethod.Id;

                                        List<OurTrainerModel> ourTrainerModels = new List<OurTrainerModel>();
                                        var OurTrainer = item.GetPropertyValue<Picker>("trainers");
                                        if (OurTrainer != null)
                                        {
                                            if (OurTrainer.AsPublishedContent().Count() > 0)
                                            {
                                                foreach (var trainerItem in OurTrainer.AsPublishedContent())
                                                {
                                                    OurTrainerModel modelItem = new OurTrainerModel();
                                                    modelItem.Title = trainerItem.GetPropertyValue("title").ToString();
                                                    modelItem.Url = trainerItem.Url;
                                                    ourTrainerModels.Add(modelItem);
                                                }
                                            }
                                        }

                                        trainingCalendarDate.OurTrainer = ourTrainerModels;

                                        trainingCalendarDates.Add(trainingCalendarDate);
                                    }
                                    trainingCalendarCourse.trainingCalendarDates = trainingCalendarDates;
                                    

                                    trainingCalendarCourses.Add(trainingCalendarCourse);
                                }
                            }
                        }                        
                    }                    
                }  
                else
                {
                    foreach (var levelItem in Level)
                    {
                        strCouseLevel = levelItem.GetPropertyValue("courseLevel").ToString();
                        cmsLevelName = levelItem.GetPropertyValue("courseLevel").ToString();
                        cmsLevelId = levelItem.Id;

                        var TrainingMonth = levelItem.Children.Where(x => x.IsVisible()).ToList();

                        foreach (var itemTrainingMonth in TrainingMonth)
                        {
                            var TrainingDates = itemTrainingMonth.GetPropertyValue<IEnumerable<IPublishedContent>>("availableDates").Where(x => x.GetPropertyValue<DateTime>("startDate") >= DateTime.Now.Date && x.IsVisible() == true);
                            if (!string.IsNullOrEmpty(studyMethodId))
                            {
                                TrainingDates = TrainingDates.Where(x => x.GetPropertyValue("studyMethod").ToString() == studyMethodId);
                            }
                            if (!string.IsNullOrEmpty(month))
                            {
                                TrainingDates = TrainingDates.Where(x => x.GetPropertyValue<DateTime>("startDate").Month == int.Parse(month));
                            }
                            if (TrainingDates.Count() > 0)
                            {
                                TrainingCalendarCourse trainingCalendarCourse = new TrainingCalendarCourse();
                                trainingCalendarCourse.MainCourse = strMainCourse;
                                trainingCalendarCourse.cmsMainCourseName = cmsMainCourseName;
                                trainingCalendarCourse.cmsMainCourseId = cmsMainCourseId;

                                trainingCalendarCourse.Level = strCouseLevel;
                                trainingCalendarCourse.cmsLevelName = cmsLevelName;
                                trainingCalendarCourse.cmsLevelId = cmsLevelId;

                                List<TrainingCalendarDate> trainingCalendarDates = new List<TrainingCalendarDate>();
                                foreach (var item in TrainingDates)
                                {
                                    TrainingCalendarDate trainingCalendarDate = new TrainingCalendarDate();
                                    trainingCalendarDate.StartDate = item.GetPropertyValue<DateTime>("startDate").ToString("dd MMM yyyy");
                                    trainingCalendarDate.EndDate = item.GetPropertyValue<DateTime>("endDate").ToString("dd MMM yyyy");
                                    trainingCalendarDate.EndDate = item.GetPropertyValue<DateTime>("endDate").ToString("dd MMM yyyy");
                                    trainingCalendarDate.Price = decimal.Parse(item.GetPropertyValue("price").ToString());
                                    trainingCalendarDate.DisplayPrice = trainingCalendarDate.Price + (decimal.Parse(item.GetPropertyValue("price").ToString()) * VAT) / 100;
                                    //TimeSpan span = item.GetPropertyValue<DateTime>("endDate").Subtract(item.GetPropertyValue<DateTime>("startDate"));

                                    //if (!string.IsNullOrEmpty(item.GetPropertyValue<string>("duration")))
                                    //{
                                    //    if (item.GetPropertyValue<int>("duration") > 0)
                                    //    {
                                    //        trainingCalendarDate.Duration = item.GetPropertyValue<string>("duration");
                                    //    }
                                    //    else
                                    //    {
                                    //        trainingCalendarDate.Duration = (span.Days + 1).ToString();
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    trainingCalendarDate.Duration = (span.Days + 1).ToString();
                                    //}
                                    trainingCalendarDate.Duration = !string.IsNullOrEmpty(item.GetPropertyValue<string>("duration")) ? item.GetPropertyValue<string>("duration") : "";
                                    trainingCalendarDate.TotalHours = !string.IsNullOrEmpty(item.GetPropertyValue<string>("totalHours")) ? item.GetPropertyValue<string>("totalHours") : "";

                                    var itemStudyMethod = Umbraco.TypedContent(int.Parse(item.GetPropertyValue("studyMethod").ToString()));

                                    trainingCalendarDate.StudyMethods = itemStudyMethod.GetPropertyValue("title").ToString();
                                    trainingCalendarDate.cmsStudyMethodName = itemStudyMethod.GetPropertyValue("title").ToString();
                                    trainingCalendarDate.cmsStudyMethodsId = itemStudyMethod.Id;

                                    List<OurTrainerModel> ourTrainerModels = new List<OurTrainerModel>();
                                    var OurTrainer = item.GetPropertyValue<Picker>("trainers");
                                    if (OurTrainer != null)
                                    {
                                        if (OurTrainer.AsPublishedContent().Count() > 0)
                                        {
                                            foreach (var trainerItem in OurTrainer.AsPublishedContent())
                                            {
                                                OurTrainerModel modelItem = new OurTrainerModel();
                                                modelItem.Title = trainerItem.GetPropertyValue("title").ToString();
                                                modelItem.Url = trainerItem.Url;
                                                ourTrainerModels.Add(modelItem);
                                            }
                                        }
                                    }

                                    trainingCalendarDate.OurTrainer = ourTrainerModels;

                                    trainingCalendarDates.Add(trainingCalendarDate);
                                }
                                trainingCalendarCourse.trainingCalendarDates = trainingCalendarDates;

                                //List<OurTrainerModel> ourTrainerModels = new List<OurTrainerModel>();
                                //var CourseNode = TrainingandCourses.Descendants(2).Where(x => x.Name == cmsMainCourseName).FirstOrDefault();
                                //var OurTrainer = CourseNode.GetPropertyValue<Picker>("selectTrainer");
                                //foreach (var item in OurTrainer.AsPublishedContent())
                                //{
                                //    OurTrainerModel modelItem = new OurTrainerModel();
                                //    modelItem.Title = item.GetPropertyValue("title").ToString();
                                //    modelItem.Url = item.Url;
                                //    ourTrainerModels.Add(modelItem);
                                //}

                                //trainingCalendarCourse.OurTrainer = ourTrainerModels;

                                trainingCalendarCourses.Add(trainingCalendarCourse);
                            }
                        }
                    }
                }

                
            }

            model.trainingCalendarCourses = trainingCalendarCourses;
            return Json(new { AvailableDates = model }, JsonRequestBehavior.AllowGet);
        }


    }
}