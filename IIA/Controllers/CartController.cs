﻿using System;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using IIA.DBContext;
using System.Linq.Dynamic;
using umbraco.cms.helpers;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using IIA.Models;
using CMS.Helpers;
using System.Data.Entity;
using Umbraco.Web;
using System.Web.Configuration;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using umbraco.editorControls.SettingControls.Pickers;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using Umbraco.Core.Models;
using umbraco.cms.businesslogic;
using IIA.Helper;
using IIA.Helpers;
using System.Globalization;

namespace IIA.Controllers
{
    public class CartController : SurfaceController
    {
        IIAContext db = new IIAContext();
        //public string PaymentIntegrationMode = "Test";
        //public string PaymentIntegrationUrl = "https://test.oppwa.com/";
        public string PaymentIntegrationMode = WebConfigurationManager.AppSettings["PaymentIntegrationMode"].ToString();
        public string PaymentIntegrationUrl = WebConfigurationManager.AppSettings["PaymentIntegrationUrl"].ToString();
        public string Currency = "SAR";
        public double CurrencyRate = 1;
        public int VAT = 15;

        [HttpGet]
        public ActionResult MyCart()
        {
            try
            {

                return PartialView("Shop/MyCart");
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult CartCheckout()
        {
            try
            {

                return PartialView("Shop/CartCheckout");
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult CartPayment()
        {
            try
            {
                string lang = Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString());

                PaymentGateayStep1Model model = new PaymentGateayStep1Model();

                long InvoiceId = long.Parse(Utility.GetApplicationCookies("invId").Value);
                long ProfileId = long.Parse(Utility.GetApplicationCookies("proId").Value);

                var MemberInfo = db.ctRegistrations.Where(x => x.ProfileId == ProfileId).FirstOrDefault();
                var ShippingBilling = db.ctShippingBillings.Where(x => x.ProfileId == ProfileId).FirstOrDefault();

                var PaymentInfo = db.ctInvoices.Where(x => x.InvoiceId == InvoiceId).FirstOrDefault();
                var PaymentGatewayInfo = db.ctPaymentGatewayInfoes.Where(x => x.IntegrationMode == PaymentIntegrationMode).FirstOrDefault();
                var entityId = "";
                if (PaymentInfo.PaymentMethod == "VISA")
                {
                    entityId = PaymentGatewayInfo.VIsaEntityId;
                }
                else if (PaymentInfo.PaymentMethod == "MASTER")
                {
                    entityId = PaymentGatewayInfo.MasterEntityId;
                }
                else if (PaymentInfo.PaymentMethod == "MADA")
                {
                    entityId = PaymentGatewayInfo.MadaEntityId;
                }
                string CheckoutId = Utility.PaymentGatewayGetCheckoutId(entityId, PaymentInfo.TransactionAmount.Value, PaymentInfo.TransactionCurrency, "DB", "EXTERNAL", PaymentInfo.InvoiceNumber, ShippingBilling.BEmailAddress, ShippingBilling.BAddress1, ShippingBilling.BCity, ShippingBilling.BArea, ShippingBilling.BCountry, MemberInfo.Zip_Postal_Code, ShippingBilling.BFirstName, ShippingBilling.BLastName, PaymentGatewayInfo.BearerToken, PaymentInfo.PaymentMethod);

                model.CheckoutId = CheckoutId;
                model.TransactionAmount = PaymentInfo.TransactionCurrency + " " + String.Format("{0:n2}", Convert.ToDouble(PaymentInfo.TransactionAmount.ToString()));
                model.PaymentMethod = PaymentInfo.PaymentMethod;
                model.PaymentUrl = PaymentIntegrationUrl + "v1/paymentWidgets.js?checkoutId=" + model.CheckoutId;
                model.RedirectUrl = WebConfigurationManager.AppSettings["WebUrl"].ToString() + lang + "/shop/cart-payment-status/";

                return PartialView("Shop/CartPayment", model);
            }
            catch (Exception ex)
            {

            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult CartPaymentSuccess()
        {

            InvoiceViewModel model = new InvoiceViewModel();
            InvoiceModel InvoiceInfo = new InvoiceModel();
            List<InvoiceDetailsModel> ListInvoiceItem = new List<InvoiceDetailsModel>();

            try
            {
                long InvoiceId = long.Parse(Utility.GetApplicationCookies("invId").Value);
                ctInvoice dbInvoice = db.ctInvoices.Where(x => x.InvoiceId == InvoiceId).FirstOrDefault();

                if (dbInvoice.TransactionStatus == "Success")
                {
                    var dbInvoiceItem = db.ctInvoiceDetails.Where(x => x.InvoiceId == InvoiceId).ToList();

                    foreach (var item in dbInvoiceItem)
                    {
                        InvoiceDetailsModel invoiceModel = new InvoiceDetailsModel();
                        if (item.ProductType == "C")
                        {
                            invoiceModel.CourseCmsName = Umbraco.TypedContent(item.CourseCmsId).GetPropertyValue("courseTitle").ToString();
                            invoiceModel.LevelCmsName = Umbraco.TypedContent(item.LevelCmsId).GetPropertyValue("courseLevel").ToString();
                            invoiceModel.StudyMethodCmsName = Umbraco.TypedContent(item.StudyMethodCmsId).GetPropertyValue("title").ToString();
                            invoiceModel.StartDate = item.StartDate;
                            invoiceModel.EndDate = item.EndDate;
                            invoiceModel.ShowHideCounter = "none;";
                            invoiceModel.CourseCmsId = item.CourseCmsId.Value;
                            invoiceModel.LevelCmsId = item.LevelCmsId.Value;
                            invoiceModel.StudyMethodCmsId = item.StudyMethodCmsId.Value;
                        }
                        else
                        {
                            invoiceModel.ProductCmsName = Umbraco.TypedContent(item.ProductCmsId).GetPropertyValue("title").ToString();
                            invoiceModel.ProductPicture = Umbraco.TypedContent(item.ProductCmsId).GetPropertyValue<IPublishedContent>("image").Url.ToString();
                            invoiceModel.ShowHideCounter = "none;";
                            invoiceModel.ProductCmsId = item.ProductCmsId.Value;

                            UpdateStokQty(item.ProductCmsId.Value, item.ProductCmsName, item.Quantity.Value);
                        }

                        invoiceModel.Price = item.Price.Value;
                        invoiceModel.Discount = item.Discount.Value;
                        invoiceModel.Quantity = item.Quantity.Value;
                        invoiceModel.ProductType = item.ProductType;

                        ListInvoiceItem.Add(invoiceModel);
                    }

                    InvoiceInfo.SubTotal = dbInvoice.SubTotal.Value;
                    InvoiceInfo.Shipping = dbInvoice.Shipping.Value;
                    InvoiceInfo.Discount = dbInvoice.Discount.Value;
                    InvoiceInfo.VAT = dbInvoice.VAT.Value;
                    InvoiceInfo.GrandTotal = dbInvoice.GrandTotal.Value;
                    InvoiceInfo.TransactionAmount = dbInvoice.TransactionAmount.Value;
                    InvoiceInfo.TransactionCurrency = dbInvoice.TransactionCurrency;
                    InvoiceInfo.InvoiceNumber = dbInvoice.InvoiceNumber;
                    InvoiceInfo.ShippingReference = dbInvoice.ShippingReference;
                    InvoiceInfo.ShippingReferencePDF = dbInvoice.ShippingReferencePDF;
                    InvoiceInfo.ProfileId = dbInvoice.ProfileId.Value;

                    model.InvoiceInfo = InvoiceInfo;
                    model.ListInvoiceItem = ListInvoiceItem;


                    EmailSettingModel eModel = Utility.GetEmailSetting("CartPayment", dbInvoice.ProfileId.ToString(), Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString()));

                    string CartItem = GetCartItemEmailTemplate(model);

                    string Message = EmailMessage.GetInvoiceMessage(model, CartItem, Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString()));

                    Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);


                    Utility.ClearCartCookies();
                    Utility.ClearPurchaseCookies();

                    return PartialView("Shop/CartPaymentSuccess", model);
                }
                else
                {
                    return HttpNotFound();
                }
            }
            catch
            {
                return HttpNotFound();
            }
        }

        private void UpdateStokQty(int productCmsId, string productCmsName, int Quantity)
        {
            var lang = Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString());
            var EngBookMaterial = Umbraco.TypedContent(2060);
            var ArgBookMaterial = Umbraco.TypedContent(2614);

            var ProductName = Umbraco.TypedContent(productCmsId).Name;

            try
            {
                var EnglisNode = EngBookMaterial.Descendants(3).Where(x => x.DocumentTypeAlias == "booksAndMaterials" && x.Name == ProductName).FirstOrDefault();
                var ItemCcontent_en = Umbraco.TypedContent(EnglisNode.Id);
                int CurrentStock_en = ItemCcontent_en.GetPropertyValue<int>("currentStockQty");
                int Limit_en = ItemCcontent_en.GetPropertyValue<int>("stockLimitForDisplay");
                int NewStock_en = CurrentStock_en - Quantity;

                var Service_en = Services.ContentService.GetById(EnglisNode.Id);
                Service_en.SetValue("currentStockQty", NewStock_en.ToString());
                Services.ContentService.SaveAndPublishWithStatus(Service_en);
            }
            catch
            {

            }

            try
            {
                var ArabicNode = ArgBookMaterial.Descendants(3).Where(x => x.DocumentTypeAlias == "booksAndMaterials" && x.Name == ProductName).FirstOrDefault();
                var ItemCcontent_ar = Umbraco.TypedContent(ArabicNode.Id);
                int CurrentStock_ar = ItemCcontent_ar.GetPropertyValue<int>("currentStockQty");
                int Limit_ar = ItemCcontent_ar.GetPropertyValue<int>("stockLimitForDisplay");
                int NewStock_ar = CurrentStock_ar - Quantity;

                var Service_ar = Services.ContentService.GetById(ArabicNode.Id);
                Service_ar.SetValue("currentStockQty", NewStock_ar.ToString());
                Services.ContentService.SaveAndPublishWithStatus(Service_ar);
            }
            catch
            {

            }


            try
            {
                var ItemCcontent = Umbraco.TypedContent(productCmsId);
                int CurrentStock = ItemCcontent.GetPropertyValue<int>("currentStockQty");
                int Limit = ItemCcontent.GetPropertyValue<int>("stockLimitForDisplay");
                int NewStock = CurrentStock - Quantity;

                if (NewStock <= Limit)
                {
                    //var ProductFullName = ItemCcontent_en.GetPropertyValue("title") + " / " + ItemCcontent_ar.GetPropertyValue("title");
                    var ProductFullName = ItemCcontent.GetPropertyValue("title").ToString();
                    EmailSettingModel eModel = Utility.GetEmailSetting("StockNotification", "", lang);
                    string Message = EmailMessage.GetStockNotificationMessage(ProductFullName, NewStock, Limit, lang);
                    Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);
                }
            }
            catch
            {

            }
            
        }

        [HttpGet]
        public ActionResult CartPaymentFailed()
        {
            return PartialView("Shop/CartPaymentFailed");
        }

        [HttpPost]
        public ActionResult AddToCart(CartModel model)
        {
            try
            {

                ctCart objDbModel = new ctCart();
                string CartKey = "";
                string ProductType = !string.IsNullOrEmpty(model.CourseCmsName) ? "C" : "P";

                if (Utility.GetApplicationCookies("CartKey") != null)
                {
                    CartKey = Utility.GetApplicationCookies("CartKey").Value;
                }
                else
                {
                    CartKey = Guid.NewGuid().ToString();
                    Utility.SetCartCookies("CartKey", CartKey);
                }

                if (CartAlreadyAdded(ProductType, model, CartKey) == false)
                {
                    objDbModel.CartKey = CartKey;
                    objDbModel.ProductType = ProductType;
                    if (ProductType == "C")
                    {
                        objDbModel.CourseCmsId = model.CourseCmsId;
                        objDbModel.CourseCmsName = model.CourseCmsName;
                        objDbModel.LevelCmsId = model.LevelCmsId;
                        objDbModel.LevelCmsName = model.LevelCmsName;
                        objDbModel.StudyMethodCmsId = model.StudyMethodCmsId;
                        objDbModel.StudyMethodCmsName = model.StudyMethodCmsName;
                        objDbModel.StartDate = model.StartDate;
                        objDbModel.EndDate = model.EndDate;
                        objDbModel.Duration = model.Duration;
                    }
                    else
                    {
                        objDbModel.ProductCmsId = model.ProductCmsId;
                        objDbModel.ProductCmsName = model.ProductCmsName;
                    }

                    objDbModel.Price = model.Price;
                    objDbModel.Quantity = model.Quantity;
                    objDbModel.Discount = 0;
                    objDbModel.LastUpdated = DateTime.Now;

                    db.ctCarts.Add(objDbModel);
                    db.SaveChanges();

                    return Json(new
                    {
                        status = true
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new
                {
                    status = false,
                    message = "Sorry, added in cart failed, please try again!"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public Boolean CartAlreadyAdded(string ProductType, CartModel model, string CartKey)
        {
            Boolean IsAdded = false;
            if (ProductType == "C")
            {
                if (db.ctCarts.Where(x => x.CartKey == CartKey && x.CourseCmsName == model.CourseCmsName && x.LevelCmsName == model.LevelCmsName).Count() > 0)
                {
                    IsAdded = true;
                }
            }
            else if (ProductType == "P")
            {
                if (db.ctCarts.Where(x => x.CartKey == CartKey && x.ProductCmsId == model.ProductCmsId).Count() > 0)
                {
                    IsAdded = true;
                }
            }
            return IsAdded;
        }

        [HttpPost]
        public ActionResult GetCartItem(string lang)
        {
            string CartKey = "";
            if (Utility.GetApplicationCookies("CartKey") != null)
            {
                CartKey = Utility.GetApplicationCookies("CartKey").Value;
            }
            if (!string.IsNullOrEmpty(CartKey))
            {
                if (db.ctCarts.Where(x => x.CartKey == CartKey).Count() > 0)
                {
                    CartViewModel model = new CartViewModel();
                    List<CartModel> CartList = new List<CartModel>();

                    decimal SubTotal = 0;
                    decimal Discount = 0;

                    try
                    {
                        var dbCartList = db.ctCarts.Where(x => x.CartKey == CartKey).ToList();
                        foreach (var item in dbCartList)
                        {
                            CartModel cartModel = new CartModel();
                            if (item.ProductType == "C")
                            {
                                cartModel.CourseCmsName = Umbraco.TypedContent(item.CourseCmsId).GetPropertyValue("courseTitle").ToString();
                                cartModel.LevelCmsName = Umbraco.TypedContent(item.LevelCmsId).GetPropertyValue("courseLevel").ToString();
                                cartModel.StudyMethodCmsName = Umbraco.TypedContent(item.StudyMethodCmsId).GetPropertyValue("title").ToString();
                                cartModel.StartDate = item.StartDate;
                                cartModel.EndDate = item.EndDate;
                                cartModel.ShowHideCounter = "none;";
                            }
                            else
                            {
                                cartModel.ProductCmsName = Umbraco.TypedContent(item.ProductCmsId).GetPropertyValue("title").ToString();
                                cartModel.ProductPicture = Umbraco.TypedContent(item.ProductCmsId).GetPropertyValue<IPublishedContent>("image").Url.ToString();
                                cartModel.ShowHideCounter = "flex;";
                            }

                            cartModel.Price = item.Price.Value;
                            cartModel.Discount = Utility.GetMemberDiscount(item.Price.Value, item.Quantity.Value);
                            cartModel.Quantity = item.Quantity.Value;
                            cartModel.CartId = item.CartId;
                            cartModel.ProductType = item.ProductType;

                            SubTotal = SubTotal + item.Quantity.Value * item.Price.Value;
                            Discount = Discount + cartModel.Discount;

                            CartList.Add(cartModel);
                        }
                                                

                        model.SubTotal = SubTotal;
                        model.Shipping = 0;
                        model.Discount = Discount;
                        model.VAT = ((model.SubTotal + model.Shipping - model.Discount) * VAT) / 100;
                        model.GrandTotal = (model.SubTotal + model.VAT + model.Shipping - model.Discount);
                        model.CartList = CartList;

                        return Json(new { status = true, CartItem = model }, JsonRequestBehavior.AllowGet);
                    }
                    catch
                    {
                        return Json(new
                        {
                            status = false
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new
                    {
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteItem(string lang, string CartId)
        {
            try
            {
                long cartId = long.Parse(CartId);
                var CartItem = db.ctCarts.Where(x => x.CartId == cartId).FirstOrDefault();
                db.ctCarts.Remove(CartItem);
                db.SaveChanges();
                return Json(new
                {
                    status = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult MinusItem(string lang, string CartId)
        {
            try
            {
                long cartId = long.Parse(CartId);
                var CartItem = db.ctCarts.Where(x => x.CartId == cartId).FirstOrDefault();
                if (CartItem.Quantity > 1)
                {
                    CartItem.Quantity = CartItem.Quantity - 1;
                    db.Entry(CartItem).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return Json(new
                {
                    status = true,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult PlusItem(string lang, string CartId)
        {
            try
            {
                long cartId = long.Parse(CartId);
                var CartItem = db.ctCarts.Where(x => x.CartId == cartId).FirstOrDefault();

                CartItem.Quantity = CartItem.Quantity + 1;
                db.Entry(CartItem).State = EntityState.Modified;
                db.SaveChanges();

                return Json(new
                {
                    status = true,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult ProceedToCheckout(string lang)
        {
            Utility.SetApplicationCookies("backUrl", "/" + lang + "/shop/shipping-billing-address/");
            string nextUrl = "/" + lang + "/login/";

            if (Utility.GetApplicationCookies("proId") != null)
            {
                if (Utility.GetApplicationCookies("proId").Value != "")
                {
                    nextUrl = "/" + lang + "/shop/shipping-billing-address/";
                }
            }
            try
            {
                return Json(new
                {
                    status = true,
                    url = nextUrl
                }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new
                {
                    status = false,
                    message = "Sorry, some internal server error occured, please try again later!"
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GetCartCheckoutItem(string lang, string promoCode)
        {
            long profileId = long.Parse(Utility.GetApplicationCookies("proId").Value);
            string CartKey = "";
            if (Utility.GetApplicationCookies("CartKey") != null)
            {
                CartKey = Utility.GetApplicationCookies("CartKey").Value;
            }
            if (!string.IsNullOrEmpty(CartKey))
            {
                if (db.ctCarts.Where(x => x.CartKey == CartKey).Count() > 0)
                {
                    CartViewModel model = new CartViewModel();
                    List<CartModel> CartList = new List<CartModel>();

                    decimal SubTotal = 0;
                    decimal Discount = 0;
                    bool IsProductExists = false;

                    try
                    {
                        var dbCartList = db.ctCarts.Where(x => x.CartKey == CartKey).ToList();
                        foreach (var item in dbCartList)
                        {
                            CartModel cartModel = new CartModel();
                            if (item.ProductType == "C")
                            {
                                cartModel.CourseCmsName = Umbraco.TypedContent(item.CourseCmsId).GetPropertyValue("courseTitle").ToString();
                                cartModel.LevelCmsName = Umbraco.TypedContent(item.LevelCmsId).GetPropertyValue("courseLevel").ToString();
                                cartModel.StudyMethodCmsName = Umbraco.TypedContent(item.StudyMethodCmsId).GetPropertyValue("title").ToString();
                                cartModel.StartDate = item.StartDate;
                                cartModel.EndDate = item.EndDate;
                                cartModel.ShowHideCounter = "none;";
                            }
                            else
                            {
                                cartModel.ProductCmsName = Umbraco.TypedContent(item.ProductCmsId).GetPropertyValue("title").ToString();
                                cartModel.ProductPicture = Umbraco.TypedContent(item.ProductCmsId).GetPropertyValue<IPublishedContent>("image").Url.ToString();
                                cartModel.ShowHideCounter = "flex;";
                                IsProductExists = true;
                            }

                            cartModel.Price = item.Price.Value;
                            cartModel.Discount = Utility.GetMemberDiscount(item.Price.Value, item.Quantity.Value);
                            cartModel.Quantity = item.Quantity.Value;
                            cartModel.CartId = item.CartId;
                            cartModel.ProductType = item.ProductType;

                            SubTotal = SubTotal + item.Quantity.Value * item.Price.Value;
                            Discount = Discount + cartModel.Discount;

                            CartList.Add(cartModel);
                        }

                        PromoCodeValidation promoCodeValidation = GetPromoValue(lang, promoCode, SubTotal);

                        model.SubTotal = SubTotal;
                        model.Shipping = Aramex.GetShippingRate(CartKey);
                        model.Discount = Discount + (string.IsNullOrEmpty(promoCodeValidation.Message) ? promoCodeValidation.Value : 0);
                        model.VAT = ((model.SubTotal + model.Shipping - model.Discount) * VAT) / 100;
                        model.GrandTotal = (model.SubTotal + model.VAT + model.Shipping - model.Discount);
                        model.CartList = CartList;

                        return Json(new { status = true, CartItem = model }, JsonRequestBehavior.AllowGet);
                    }
                    catch
                    {
                        return Json(new
                        {
                            status = false
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new
                    {
                        status = false
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ProceedToPayment(string lang, string PaymentMethod, string sc, string promoCode)
        {

            try
            {
                decimal SubTotal = 0;
                decimal Discount = 0;
                string CartKey = "";
                long profileId = long.Parse(Utility.GetApplicationCookies("proId").Value);

                if (Utility.GetApplicationCookies("CartKey") != null)
                {
                    CartKey = Utility.GetApplicationCookies("CartKey").Value;
                }

                ctInvoice ctInvoice = new ctInvoice();
                ctInvoice.ProfileId = profileId;
                ctInvoice.InvoiceNumber = Utility.GenerateShopInvoiceNumber();
                db.ctInvoices.Add(ctInvoice);
                db.SaveChanges();
                var InvoiceId = ctInvoice.InvoiceId;

                bool IsProductExists = false;

                var CartList = db.ctCarts.Where(x => x.CartKey == CartKey).ToList();
                foreach (var item in CartList)
                {
                    ctInvoiceDetail ctInvoiceDetail = new ctInvoiceDetail()
                    {
                        InvoiceId = InvoiceId,
                        ProductType = item.ProductType,
                        ProductCmsId = item.ProductCmsId,
                        ProductCmsName = item.ProductCmsName,
                        CourseCmsId = item.CourseCmsId,
                        CourseCmsName = item.CourseCmsName,
                        LevelCmsId = item.LevelCmsId,
                        LevelCmsName = item.LevelCmsName,
                        StudyMethodCmsId = item.StudyMethodCmsId,
                        StudyMethodCmsName = item.StudyMethodCmsName,
                        StartDate = item.StartDate,
                        EndDate = item.EndDate,
                        Price = item.Price,
                        Duration = item.Duration,
                        Quantity = item.Quantity,
                        Discount = Utility.GetMemberDiscount(item.Price.Value, item.Quantity.Value),
                        LastUpdated = item.LastUpdated
                    };

                    SubTotal = SubTotal + item.Quantity.Value * item.Price.Value;
                    Discount = Discount + Utility.GetMemberDiscount(item.Price.Value, item.Quantity.Value);
                    db.ctInvoiceDetails.Add(ctInvoiceDetail);
                    db.SaveChanges();

                    if (item.ProductType == "P")
                    {
                        IsProductExists = true;
                    }
                }

                PromoCodeValidation promoCodeValidation = GetPromoValue(lang, promoCode, SubTotal);

                var ctUpdateInvoice = db.ctInvoices.Where(x => x.InvoiceId == InvoiceId).FirstOrDefault();
                ctUpdateInvoice.SubTotal = SubTotal;
                ctUpdateInvoice.Shipping = Aramex.GetShippingRate(CartKey);
                ctUpdateInvoice.Discount = Discount + (string.IsNullOrEmpty(promoCodeValidation.Message) ? promoCodeValidation.Value : 0);
                ctUpdateInvoice.VAT = ((ctUpdateInvoice.SubTotal + ctUpdateInvoice.Shipping - ctUpdateInvoice.Discount) * VAT) / 100;
                ctUpdateInvoice.GrandTotal = (ctUpdateInvoice.SubTotal + ctUpdateInvoice.VAT + ctUpdateInvoice.Shipping - ctUpdateInvoice.Discount);
                ctUpdateInvoice.TransactionAmount = ctUpdateInvoice.GrandTotal * decimal.Parse(CurrencyRate.ToString());
                ctUpdateInvoice.TransactionCurrency = Currency;
                ctUpdateInvoice.PaymentMethod = PaymentMethod;
                ctUpdateInvoice.TransactionDate = DateTime.Now;
                ctUpdateInvoice.PromoCode = (string.IsNullOrEmpty(promoCodeValidation.Message) ? promoCode : "");

                db.Entry(ctUpdateInvoice).State = EntityState.Modified;
                db.SaveChanges();

                Utility.SetApplicationCookies("invId", InvoiceId.ToString());

                return Json(new
                {
                    status = true,
                    url = "/" + lang + "/shop/cart-payment/"
                }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new
                {
                    status = false,
                    message = "Sorry, some internal server error occured, please try again later!"
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public string GetCartItemEmailTemplate(InvoiceViewModel model)
        {
            string lang = Utility.GetLanguageFromUrl(HttpContext.Request.Url.ToString());

            string CartItem = "";
            string QuantityLevel = lang == "en" ? "Quantity" : "الكمية";
            foreach (var item in model.ListInvoiceItem)
            {
                CartItem += "<tr>";

                if (item.ProductType == "P")
                {
                    var product = Umbraco.TypedContent(item.ProductCmsId);
                    CartItem += "<td style='background-color:#ffffff; width:120px; max-width:120px; min-width:120px;' width='120'>";
                    CartItem += "<img src='" + WebConfigurationManager.AppSettings["WebUrl"].ToString() + Umbraco.TypedContent(item.ProductCmsId).GetPropertyValue<IPublishedContent>("image").Url.ToString() + "' alt='product' style='display:block; line-height:0; border:0;  width:120px; max-width:120px; min-width:120px' width='120'/>";
                    CartItem += "</td>";

                    CartItem += "<td style='background-color: #ffffff'>";
                    CartItem += "<h2 style='margin: 0;font-size:21px;line-height: normal; color: #000000;'>" + Umbraco.TypedContent(item.ProductCmsId).GetPropertyValue("title").ToString() + "</h2>";
                    CartItem += "<p style='margin: 0;font-size:16px;line-height: normal; color: #4F4F4F;'>";
                    CartItem += QuantityLevel + ": " + item.Quantity.ToString();
                    CartItem += "</p>";
                    CartItem += "</td>";

                }
                else
                {
                    CartItem += "<td style='background-color:#ffffff; width:120px; max-width:120px; min-width:120px;' width='120'>";
                    CartItem += "</td>";
                    CartItem += "<td style='background-color: #ffffff;'>";
                    CartItem += "<h2 style='margin: 0;font-size:21px;line-height: normal; color: #000000;'>" + Umbraco.TypedContent(item.CourseCmsId).GetPropertyValue("courseTitle").ToString() + "</h2>";
                    CartItem += "<p style='margin: 0;font-size:16px;line-height: normal; color: #4F4F4F;'>";
                    CartItem += Umbraco.TypedContent(item.LevelCmsId).GetPropertyValue("courseLevel").ToString() + ", " + Umbraco.TypedContent(item.StudyMethodCmsId).GetPropertyValue("title").ToString() + ", " + item.StartDate + " - " + item.EndDate;
                    CartItem += "</p>";
                    CartItem += "</td>";

                }

                CartItem += "<td align='right' style='background-color: #ffffff;font-size:14px;line-height: normal; color: #000000;  width:60px; max-width:120px; min-width:60px;' width='60'>";
                CartItem += "SAR " + string.Format("{0:0.00}", item.Price);
                CartItem += "</td>";

                CartItem += "</tr>";
            }
            return CartItem;
        }

        [HttpPost]
        public ActionResult GetCity(string CountryCode, string NameStartsWith)
        {
            try
            {
                List<CityModel> cityModels = new List<CityModel>();
                cityModels = Aramex.GetCity("SA", "");

                return Json(cityModels.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new
                {
                    status = false,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult CheckStockQty(int productid, int quantity, string lang)
        {
            try
            {
                var ItemCcontent = Umbraco.TypedContent(productid);
                int CurrentStock = ItemCcontent.GetPropertyValue<int>("currentStockQty");
                //int Limit = ItemCcontent.GetPropertyValue<int>("stockLimitForDisplay");
                //int NewStock = CurrentStock - quantity;
                if (CurrentStock >= quantity)
                {
                    return Json(new
                    {
                        status = true,
                        message = ""
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = false,
                        message = "Product_Quantity_Greater_Than_Stock_Qty",
                        availableStock = CurrentStock
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new
                {
                    status = false,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult CheckCartStockQty(int CartId, string lang)
        {
            try
            {
                var ctCart = db.ctCarts.Where(x => x.CartId == CartId).FirstOrDefault();
                var ItemCcontent = Umbraco.TypedContent(ctCart.ProductCmsId);
                int CurrentStock = ItemCcontent.GetPropertyValue<int>("currentStockQty");
                //int Limit = ItemCcontent.GetPropertyValue<int>("stockLimitForDisplay");
                //int NewStock = CurrentStock - quantity;
                int quantity = ctCart.Quantity.Value + 1;
                if (CurrentStock >= quantity)
                {
                    return Json(new
                    {
                        status = true,
                        message = ""
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = false,
                        message = "Product_Quantity_Greater_Than_Stock_Qty",
                        availableStock = CurrentStock
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new
                {
                    status = false,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult UpdateCounter()
        {
            string CartKey = "";
            int qtyCount = 0;
            if (Utility.GetApplicationCookies("CartKey") != null)
            {
                CartKey = Utility.GetApplicationCookies("CartKey").Value;
            }
            if (!string.IsNullOrEmpty(CartKey))
            {
                if (db.ctCarts.Where(x => x.CartKey == CartKey).Count() > 0)
                {
                    var CartItem = db.ctCarts.Where(x => x.CartKey == CartKey).ToList();
                    foreach(var item in CartItem)
                    {
                        qtyCount += item.Quantity.Value;
                    }
                    
                    return Json(new { status = true, counter = qtyCount.ToString() }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = false,
                        counter = 0
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new
                {
                    status = false,
                    counter = 0
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ValidatePromoCode(string lang, string promoCode)
        {
            PromoCodeValidation model = GetPromoValue(lang, promoCode, 0);
            if (string.IsNullOrEmpty(model.Message))
            {

                return Json(new
                {
                    status = true,
                    message = "",
                    value = model.Value
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    status = false,
                    message = model.Message,
                    value = model.Value
                }, JsonRequestBehavior.AllowGet);
            }            
        }

        public PromoCodeValidation GetPromoValue(string lang, string promoCode, decimal discountValue)
        {
            PromoCodeValidation model = new PromoCodeValidation();
            model.Message = "";
            model.Value = 0;
            bool IsMemberExists = false;
            var MembershipType = "";

            try
            {
                if (!string.IsNullOrEmpty(promoCode))
                {
                    long proId = long.Parse(Utility.GetApplicationCookies("proId").Value);

                    if (db.ctRegistrations.Where(x => x.ProfileId == proId).FirstOrDefault().MembershipType == "Professional")
                    {
                        MembershipType = "Corporate";
                    }
                    else
                    {
                        MembershipType = db.ctRegistrations.Where(x => x.ProfileId == proId).FirstOrDefault().MembershipType;
                    }


                    var rootPageId = lang == "en" ? 1131 : 2459;
                    var rootPage = Umbraco.TypedContent(rootPageId);
                    var promoCodePage = rootPage.Descendants(1).Where(x => x.DocumentTypeAlias == "promoCode").FirstOrDefault();
                    var promoCodeItem = promoCodePage.Children.Where(x => x.IsVisible() && x.GetPropertyValue<string>("promoCode") == promoCode).FirstOrDefault();
                    if (promoCodeItem != null)
                    {
                        var discountPercentage = promoCodeItem.GetPropertyValue("discountPercentage");
                        var flatDiscount = promoCodeItem.GetPropertyValue("flatDiscountAED");
                        if (discountPercentage != null)
                        {
                            if (decimal.Parse(discountPercentage.ToString()) > 0)
                            {
                                model.Value = (discountValue * decimal.Parse(discountPercentage.ToString())) / 100;
                            }
                            else
                            {
                                model.Value = decimal.Parse(flatDiscount.ToString());
                            }
                        }
                        else
                        {
                            model.Value = decimal.Parse(flatDiscount.ToString());
                        }

                        if (promoCodeItem.GetPropertyValue("startDate") != null && promoCodeItem.GetPropertyValue("endDate") != null)
                        {
                            if (promoCodeItem.GetPropertyValue<DateTime>("startDate") <= DateTime.Now.Date && promoCodeItem.GetPropertyValue<DateTime>("endDate") >= DateTime.Now.Date)
                            {

                            }
                            else
                            {
                                model.Message = "Invalid_Promo_Code";
                                return model;
                            }
                        }
                        if (promoCodeItem.GetPropertyValue("isApplyTrainingAndBooks") != null)
                        {
                            if (promoCodeItem.GetPropertyValue<Boolean>("isApplyTrainingAndBooks") == false)
                            {
                                model.Message = "Invalid_Promo_Code";
                                return model;
                            }
                        }
                        if (promoCodeItem.HasValue("applicableMember"))
                        {
                            foreach (var item in promoCodeItem.GetPropertyValue<IEnumerable<string>>("applicableMember"))
                            {
                                if (MembershipType == item)
                                {
                                    IsMemberExists = true;
                                }
                            }
                            if (IsMemberExists == false)
                            {
                                model.Message = "Invalid_Promo_Code";
                                return model;
                            }
                        }
                        else
                        {
                            model.Message = "Invalid_Promo_Code";
                            return model;
                        }
                        if (promoCodeItem.GetPropertyValue("noOfUsePerUser") != null)
                        {
                            if (promoCodeItem.GetPropertyValue<int>("noOfUsePerUser") > 0)
                            {
                                if (db.ctInvoices.Where(x => x.PromoCode == promoCode && x.ProfileId == proId && x.TransactionStatus == "Success").Count() == promoCodeItem.GetPropertyValue<int>("noOfUsePerUser"))
                                {
                                    model.Message = "Maximum_Use_Promo_Code";
                                    return model;
                                }                                
                            }
                        }
                       
                    }
                    else
                    {
                        model.Message = "Invalid_Promo_Code";                        
                    }
                }
                else
                {
                    model.Message = "Invalid_Promo_Code";                   
                }
            }
            catch (Exception ex)
            {
                model.Message = ex.Message;                
            }
            return model;
        }

    }
}