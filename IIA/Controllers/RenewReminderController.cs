﻿using System;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using IIA.DBContext;
using System.Linq.Dynamic;
using umbraco.cms.helpers;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using IIA.Models;
using CMS.Helpers;
using System.Data.Entity;
using Umbraco.Web;
using System.Web.Configuration;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using umbraco.editorControls.SettingControls.Pickers;
using Newtonsoft.Json;
using Umbraco.Core.Models;
using IIA.Helper;
using System.Data;
using System.Data.SqlClient;
using Umbraco.Web.Media.EmbedProviders;
using System.Configuration;

namespace IIA.Controllers
{
    public class RenewReminderController : SurfaceController
    {
        IIAContext db = new IIAContext();
        public int insertedCount = 0;
        // GET: RenewReminder
        [HttpGet]
        public ActionResult Index()
        {
            var ctRegistration = db.ctRegistrations.Where(x => x.ProfileType == "Member" && x.MembershipEndDate > DateTime.Now && x.IsActive == true && x.MembershipType!= "Professional").ToList();
            TimeSpan ts=new TimeSpan();
            TimeSpan send = new TimeSpan();
            DateTime lastUpdate = new DateTime();
            foreach (var item in ctRegistration)
            {
                ts = item.MembershipEndDate.Value - DateTime.Parse(DateTime.Now.ToShortDateString());
                if (db.ctRenewReminders.Where(x => x.EmailAddress == item.EmailAddress).Count() > 0)
                {
                    lastUpdate = db.ctRenewReminders.Where(x => x.EmailAddress == item.EmailAddress).OrderByDescending(x=>x.LastUpdate).FirstOrDefault().LastUpdate.Value;
                    send = DateTime.Now - lastUpdate;
                    if (ts.Days == 60 && send.Days >= 60)
                    {
                        SendEmail(item);
                        InsertRecord(60, item.EmailAddress);                        
                    }
                    else if (ts.Days == 30 && send.Days >= 30)
                    {
                        SendEmail(item);
                        InsertRecord(30, item.EmailAddress);                        
                    }
                    else if (ts.Days == 15 && send.Days >= 15)
                    {
                        SendEmail(item);
                        InsertRecord(15, item.EmailAddress);                        
                    }
                    else if (ts.Days == 7 && send.Days >= 7)
                    {
                        SendEmail(item);
                        InsertRecord(7, item.EmailAddress);                        
                    }
                    else if (ts.Days == 1 && send.Days >= 1)
                    {
                        SendEmail(item);
                        InsertRecord(1, item.EmailAddress);
                    }
                }
                else
                {
                    if (ts.Days == 60)
                    {
                        SendEmail(item);
                        InsertRecord(60, item.EmailAddress);
                    }
                }
                
            }
            return Json(new { status = "ok", message = "Total (" + insertedCount.ToString() + ") Email Has Been Sent" }, JsonRequestBehavior.AllowGet);
        }

        private void SendEmail(ctRegistration item)
        {
            EmailSettingModel eModel = Utility.GetEmailSetting("RenewalReminder", item.ProfileId.ToString(), item.lang);

            string Message = EmailMessage.GetRegistrationMessage(item, "RenewalReminder", "", item.FirstName, item.lang);

            Utility.SendMail(eModel.FromName, eModel.FromEmail, eModel.EmailTo, eModel.EmailCC, eModel.EmailBCC, eModel.Subject, Message);
            insertedCount += 1;
        }

        private void InsertRecord(int SendBefore, string emailAddress)
        {
            ctRenewReminder ctRenewReminder = new ctRenewReminder();
            ctRenewReminder.EmailAddress = emailAddress;
            ctRenewReminder.SendBefore = SendBefore;
            ctRenewReminder.LastUpdate = DateTime.Now;
            db.ctRenewReminders.Add(ctRenewReminder);
            db.SaveChanges();
        }
    }
}