﻿using System.Web.Mvc;
using Umbraco.Web.Mvc;
using System;
using System.Text;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Linq.Dynamic;
using IIA.DBContext;
using System.Linq;
using Umbraco.Web;

namespace IIA.Controllers
{
    public class ExportController : SurfaceController
    {
        IIAContext db = new IIAContext();
        [HttpPost]
        public ActionResult MemberReportDownload(string fromDate, string toDate)
        {
            string conn = WebConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;
            DataTable dtTable = new DataTable();
            SqlConnection sqlConn = new SqlConnection(conn);
            sqlConn.Open();
            string sqlString = "spMemberRegistration";           
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConn;
            cmd.CommandText = sqlString;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", DateTime.Parse(fromDate));
            cmd.Parameters.AddWithValue("toDate", DateTime.Parse(toDate));
            cmd.Parameters.AddWithValue("fileUrl", WebConfigurationManager.AppSettings["WebUrl"].ToString()+ "Media/ApplicantFile/");

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dtTable);
            sqlConn.Close();

            string exportStr = GenerateExportData(dtTable);

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(exportStr.Trim());
            return File(buffer, "application/vnd.ms-excel;charset=utf-8");
        }

        [HttpPost]
        public ActionResult NonMemberReportDownload(string fromDate, string toDate)
        {
            string conn = WebConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;
            DataTable dtTable = new DataTable();
            SqlConnection sqlConn = new SqlConnection(conn);
            sqlConn.Open();
            string sqlString = "spNonMemberRegistration";
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConn;
            cmd.CommandText = sqlString;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", DateTime.Parse(fromDate));
            cmd.Parameters.AddWithValue("toDate", DateTime.Parse(toDate));
            cmd.Parameters.AddWithValue("fileUrl", WebConfigurationManager.AppSettings["WebUrl"].ToString() + "Media/ApplicantFile/");

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dtTable);
            sqlConn.Close();

            string exportStr = GenerateExportData(dtTable);

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(exportStr.Trim());
            return File(buffer, "application/vnd.ms-excel;charset=utf-8");
        }

        [HttpPost]
        public ActionResult ContactReportDownload(string fromDate, string toDate)
        {
            string conn = WebConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;
            DataTable dtTable = new DataTable();
            SqlConnection sqlConn = new SqlConnection(conn);
            sqlConn.Open();
            string sqlString = "spGetContactReport";
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConn;
            cmd.CommandText = sqlString;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", DateTime.Parse(fromDate));
            cmd.Parameters.AddWithValue("toDate", DateTime.Parse(toDate));            

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dtTable);
            sqlConn.Close();

            string exportStr = GenerateExportData(dtTable);

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(exportStr.Trim());
            return File(buffer, "application/vnd.ms-excel;charset=utf-8");
        }

        [HttpPost]
        public ActionResult ContactAdminReportDownload(string fromDate, string toDate)
        {
            string conn = WebConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;
            DataTable dtTable = new DataTable();
            SqlConnection sqlConn = new SqlConnection(conn);
            sqlConn.Open();
            string sqlString = "spGetContactAdminReport";
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConn;
            cmd.CommandText = sqlString;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", DateTime.Parse(fromDate));
            cmd.Parameters.AddWithValue("toDate", DateTime.Parse(toDate));

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dtTable);
            sqlConn.Close();

            string exportStr = GenerateExportData(dtTable);

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(exportStr.Trim());
            return File(buffer, "application/vnd.ms-excel;charset=utf-8");
        }

        [HttpPost]
        public ActionResult JobReportDownload(string fromDate, string toDate)
        {
            string conn = WebConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;
            DataTable dtTable = new DataTable();
            SqlConnection sqlConn = new SqlConnection(conn);
            sqlConn.Open();
            string sqlString = "spGetJobReport";
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConn;
            cmd.CommandText = sqlString;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", DateTime.Parse(fromDate));
            cmd.Parameters.AddWithValue("toDate", DateTime.Parse(toDate));
            cmd.Parameters.AddWithValue("fileUrl", WebConfigurationManager.AppSettings["WebUrl"].ToString() + "Media/ApplicantFile/");

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dtTable);
            sqlConn.Close();

            string exportStr = GenerateExportData(dtTable);

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(exportStr.Trim());
            return File(buffer, "application/vnd.ms-excel;charset=utf-8");
        }

        [HttpPost]
        public ActionResult InternshipReportDownload(string fromDate, string toDate)
        {
            string conn = WebConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;
            DataTable dtTable = new DataTable();
            SqlConnection sqlConn = new SqlConnection(conn);
            sqlConn.Open();
            string sqlString = "spGetInternshipReport";
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConn;
            cmd.CommandText = sqlString;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", DateTime.Parse(fromDate));
            cmd.Parameters.AddWithValue("toDate", DateTime.Parse(toDate));
            cmd.Parameters.AddWithValue("fileUrl", WebConfigurationManager.AppSettings["WebUrl"].ToString() + "Media/ApplicantFile/");

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dtTable);
            sqlConn.Close();

            string exportStr = GenerateExportData(dtTable);

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(exportStr.Trim());
            return File(buffer, "application/vnd.ms-excel;charset=utf-8");
        }

        [HttpPost]
        public ActionResult EventReportDownload(string fromDate, string toDate)
        {
            string conn = WebConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;
            DataTable dtTable = new DataTable();
            SqlConnection sqlConn = new SqlConnection(conn);
            sqlConn.Open();
            string sqlString = "spGetEventReport";
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConn;
            cmd.CommandText = sqlString;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", DateTime.Parse(fromDate));
            cmd.Parameters.AddWithValue("toDate", DateTime.Parse(toDate));           

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dtTable);
            sqlConn.Close();

            string exportStr = GenerateExportData(dtTable);

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(exportStr.Trim());
            return File(buffer, "application/vnd.ms-excel;charset=utf-8");
        }

        [HttpPost]
        public ActionResult CareerReportDownload(string fromDate, string toDate)
        {
            string conn = WebConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;
            DataTable dtTable = new DataTable();
            SqlConnection sqlConn = new SqlConnection(conn);
            sqlConn.Open();
            string sqlString = "spGetCareerReport";
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConn;
            cmd.CommandText = sqlString;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", DateTime.Parse(fromDate));
            cmd.Parameters.AddWithValue("toDate", DateTime.Parse(toDate));
            cmd.Parameters.AddWithValue("fileUrl", WebConfigurationManager.AppSettings["WebUrl"].ToString() + "Media/ApplicantFile/");

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dtTable);
            sqlConn.Close();

            string exportStr = GenerateExportData(dtTable);

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(exportStr.Trim());
            return File(buffer, "application/vnd.ms-excel;charset=utf-8");
        }


        [HttpPost]
        public ActionResult NewsletterReportDownload(string fromDate, string toDate)
        {
            string conn = WebConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;
            DataTable dtTable = new DataTable();
            SqlConnection sqlConn = new SqlConnection(conn);
            sqlConn.Open();
            string sqlString = "spGeNewsletterReport";
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConn;
            cmd.CommandText = sqlString;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", DateTime.Parse(fromDate));
            cmd.Parameters.AddWithValue("toDate", DateTime.Parse(toDate));            

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dtTable);
            sqlConn.Close();

            string exportStr = GenerateExportData(dtTable);

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(exportStr.Trim());
            return File(buffer, "application/vnd.ms-excel;charset=utf-8");
        }

        [HttpPost]
        public ActionResult ContactMemberReportDownload(string fromDate, string toDate)
        {
            string conn = WebConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;
            DataTable dtTable = new DataTable();
            SqlConnection sqlConn = new SqlConnection(conn);
            sqlConn.Open();
            string sqlString = "spGeContactMemberReport";
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConn;
            cmd.CommandText = sqlString;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", DateTime.Parse(fromDate));
            cmd.Parameters.AddWithValue("toDate", DateTime.Parse(toDate));

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dtTable);
            sqlConn.Close();

            string exportStr = GenerateExportData(dtTable);

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(exportStr.Trim());
            return File(buffer, "application/vnd.ms-excel;charset=utf-8");
        }

        [HttpPost]
        public ActionResult RequestCallBackReportDownload(string fromDate, string toDate)
        {
            string conn = WebConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;
            DataTable dtTable = new DataTable();
            SqlConnection sqlConn = new SqlConnection(conn);
            sqlConn.Open();
            string sqlString = "spGeRequestCallBackReport";
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConn;
            cmd.CommandText = sqlString;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", DateTime.Parse(fromDate));
            cmd.Parameters.AddWithValue("toDate", DateTime.Parse(toDate));

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dtTable);
            sqlConn.Close();
            string exportStr = GenerateExportData(dtTable);

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(exportStr.Trim());
            return File(buffer, "application/vnd.ms-excel;charset=utf-8");
        }

        [HttpPost]
        public ActionResult AboutInternalAuditingReportDownload(string fromDate, string toDate)
        {
            string conn = WebConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;
            DataTable dtTable = new DataTable();
            SqlConnection sqlConn = new SqlConnection(conn);
            sqlConn.Open();
            string sqlString = "spGeAboutInternalAuditingReport";
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConn;
            cmd.CommandText = sqlString;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", DateTime.Parse(fromDate));
            cmd.Parameters.AddWithValue("toDate", DateTime.Parse(toDate));

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dtTable);
            sqlConn.Close();
            string exportStr = GenerateExportData(dtTable);

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(exportStr.Trim());
            return File(buffer, "application/vnd.ms-excel;charset=utf-8");
        }

        [HttpPost]
        public ActionResult MemberPaymentReportDownload(string fromDate, string toDate)
        {
            string conn = WebConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;
            DataTable dtTable = new DataTable();
            SqlConnection sqlConn = new SqlConnection(conn);
            sqlConn.Open();
            string sqlString = "spGetMemberPaymentReport";
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConn;
            cmd.CommandText = sqlString;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", DateTime.Parse(fromDate));
            cmd.Parameters.AddWithValue("toDate", DateTime.Parse(toDate));

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dtTable);
            sqlConn.Close();
            string exportStr = GenerateExportData(dtTable);

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(exportStr.Trim());
            return File(buffer, "application/vnd.ms-excel;charset=utf-8");
        }

        [HttpPost]
        public ActionResult InvoicePaymentReportDownload(string fromDate, string toDate)
        {
            DataTable dt = new DataTable();
            dt.Clear();
            dt.Columns.Add("FirstName");
            dt.Columns.Add("MiddleName");
            dt.Columns.Add("LastName");
            dt.Columns.Add("EmailAddress");
            dt.Columns.Add("MobileNumber");
            dt.Columns.Add("MembershipType");
            dt.Columns.Add("MembershipBadge");
            dt.Columns.Add("InvoiceNumber");
            dt.Columns.Add("SubTotal");
            dt.Columns.Add("Discount");
            dt.Columns.Add("Shipping");
            dt.Columns.Add("VAT");
            dt.Columns.Add("GrandTotal");
            dt.Columns.Add("Bank_Transaction_ID");
            dt.Columns.Add("PaymentMethod");
            dt.Columns.Add("AWBNo");
            dt.Columns.Add("AWBPDF");
            dt.Columns.Add("ShippingAddress");
            dt.Columns.Add("Purchased_Item");
            dt.Columns.Add("TransactionDate");

            string conn = WebConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;            
            SqlConnection sqlConn = new SqlConnection(conn);
            sqlConn.Open();
            string sqlString = "spGetInvoicePaymentReport";
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConn;
            cmd.CommandText = sqlString;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", DateTime.Parse(fromDate));
            cmd.Parameters.AddWithValue("toDate", DateTime.Parse(toDate));
            SqlDataReader reader = cmd.ExecuteReader();
            while(reader.Read())
            {
                DataRow row = dt.NewRow();
                row["FirstName"] = reader["FirstName"].ToString();
                row["MiddleName"] = reader["MiddleName"].ToString();
                row["LastName"] = reader["LastName"].ToString();
                row["EmailAddress"] = reader["EmailAddress"].ToString();
                row["MobileNumber"] = reader["MobileNumber"].ToString();
                row["MembershipType"] = reader["MembershipType"].ToString();
                row["MembershipBadge"] = reader["MembershipBadge"].ToString();
                row["InvoiceNumber"] = reader["InvoiceNumber"].ToString();
                row["SubTotal"] = reader["SubTotal"].ToString();
                row["Discount"] = reader["Discount"].ToString();
                row["Shipping"] = reader["Shipping"].ToString();
                row["VAT"] = reader["VAT"].ToString();
                row["GrandTotal"] = reader["GrandTotal"].ToString();
                row["Bank_Transaction_ID"] = reader["Bank_Transaction_ID"].ToString();
                row["PaymentMethod"] = reader["PaymentMethod"].ToString();
                row["AWBNo"] = reader["AWBNo"].ToString();
                row["AWBPDF"] = reader["AWBPDF"].ToString();
                row["ShippingAddress"] = getShipping(long.Parse(reader["InvoiceId"].ToString()));
                row["Purchased_Item"] = getInvoiceItem(long.Parse(reader["InvoiceId"].ToString()));
                row["TransactionDate"] = reader["TransactionDate"].ToString();
                dt.Rows.Add(row);
            }
            reader.Close();
            sqlConn.Close();

            string exportStr = GenerateExportData(dt);

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(exportStr.Trim());
            return File(buffer, "application/vnd.ms-excel;charset=utf-8");
        }

        private object getShipping(long InvoiceId)
        {
            string retStr = "";
            var profileid = db.ctInvoices.Where(x => x.InvoiceId == InvoiceId).FirstOrDefault().ProfileId;
            var shipBilling = db.ctShippingBillings.Where(x => x.ProfileId == profileid).FirstOrDefault();
            if(shipBilling!=null)
            {
                retStr += "Name: "+ shipBilling.SFirstName + " " + shipBilling.SMiddleName + " " + shipBilling.SLastName;
                retStr += ", City/Country: " + shipBilling.SCity + ", " + db.CountryLists.Where(x => x.CountryCode == shipBilling.SCountry).FirstOrDefault().CountryName;
                retStr += ", Area: " + shipBilling.SArea;
                retStr += ", Address: " + shipBilling.SAddress1 + "," + shipBilling.SAddress2 + "," + shipBilling.SAddress3;
                retStr += ", Mobile: " + shipBilling.SMobileNumber;
            }
            return retStr;
        }

        private string getInvoiceItem(long InvoiceId)
        {
            string retStr = "";

            var InvoiceItem = db.ctInvoiceDetails.Where(x => x.InvoiceId == InvoiceId).ToList();
            foreach (var item in InvoiceItem)
            {

                if (item.ProductType == "P")
                {
                    try
                    {
                        retStr += Umbraco.TypedContent(item.ProductCmsId).GetPropertyValue("title").ToString() + "_Qty: " + item.Quantity.ToString() + "_Price:SAR " + string.Format("{0:n}", item.Price) + "/";
                    }
                    catch
                    {

                    }

                }
                else
                {
                    try
                    {
                        retStr += Umbraco.TypedContent(item.CourseCmsId).GetPropertyValue("courseTitle").ToString() + "-" + Umbraco.TypedContent(item.LevelCmsId).GetPropertyValue("courseLevel").ToString() + "-" + Umbraco.TypedContent(item.StudyMethodCmsId).GetPropertyValue("title").ToString() + "-" + item.StartDate + "-" + item.EndDate + "_Price: SAR " + string.Format("{0:n}", item.Price) + "/";
                    }
                    catch
                    {

                    }
                }

            }
            if(retStr!="")
            {
                retStr = retStr.Remove(retStr.Length - 1, 1);
            }
            return retStr;
        }

        [HttpPost]
        public ActionResult ExternalCourseReportDownload(string fromDate, string toDate, string prefix)
        {
            string conn = WebConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;
            DataTable dtTable = new DataTable();
            SqlConnection sqlConn = new SqlConnection(conn);
            sqlConn.Open();
            string sqlString = "spGetExternalCourseReport";
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConn;
            cmd.CommandText = sqlString;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", DateTime.Parse(fromDate));
            cmd.Parameters.AddWithValue("toDate", DateTime.Parse(toDate));
            cmd.Parameters.AddWithValue("prefix", prefix);
            cmd.Parameters.AddWithValue("fileUrl", WebConfigurationManager.AppSettings["WebUrl"].ToString() + "Media/ApplicantFile/");

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dtTable);
            sqlConn.Close();

            string exportStr = GenerateExportData(dtTable);

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(exportStr.Trim());
            return File(buffer, "application/vnd.ms-excel;charset=utf-8");
        }

        [HttpPost]
        public ActionResult ExternalCourseInterviewTimeDownload(string fromDate, string toDate, string prefix)
        {
            string conn = WebConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;
            DataTable dtTable = new DataTable();
            SqlConnection sqlConn = new SqlConnection(conn);
            sqlConn.Open();
            string sqlString = "spGetExternalCourseInterviewTime";
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConn;
            cmd.CommandText = sqlString;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", DateTime.Parse(fromDate));
            cmd.Parameters.AddWithValue("toDate", DateTime.Parse(toDate));
            cmd.Parameters.AddWithValue("prefix", prefix);
            cmd.Parameters.AddWithValue("fileUrl", WebConfigurationManager.AppSettings["WebUrl"].ToString() + "Media/ApplicantFile/");

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dtTable);
            sqlConn.Close();

            string exportStr = GenerateExportData(dtTable);

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(exportStr.Trim());
            return File(buffer, "application/vnd.ms-excel;charset=utf-8");
        }

        [HttpPost]
        public ActionResult HarvardCourseAllApplicant(string fromDate, string toDate, string prefix, string courseCode)
        {
            string conn = WebConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;
            DataTable dtTable = new DataTable();
            SqlConnection sqlConn = new SqlConnection(conn);
            sqlConn.Open();
            string sqlString = "spGetExternalCourseAllApplicant";
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConn;
            cmd.CommandText = sqlString;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", DateTime.Parse(fromDate));
            cmd.Parameters.AddWithValue("toDate", DateTime.Parse(toDate));
            cmd.Parameters.AddWithValue("prefix", prefix);
            cmd.Parameters.AddWithValue("fileUrl", WebConfigurationManager.AppSettings["WebUrl"].ToString() + "Media/ApplicantFile/");
            cmd.Parameters.AddWithValue("courseCode", courseCode);

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dtTable);
            sqlConn.Close();

            string exportStr = GenerateExportData(dtTable);

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(exportStr.Trim());
            return File(buffer, "application/vnd.ms-excel;charset=utf-8");
        }

        protected string GenerateExportData(DataTable dt)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<head><meta charset='UTF-8'></head><table style='1px solid black; font-size:12px;' border='1'>");
            sb.Append("<tr>");
            foreach (DataColumn dtcol in dt.Columns)
            {
                sb.Append("<td><b>" + dtcol.ColumnName + "</b></td>");
            }
            sb.Append("</tr>");

            foreach (DataRow dtrow in dt.Rows)
            {
                sb.Append("<tr>");
                foreach (DataColumn column in dt.Columns)
                {
                    sb.Append("<td>" + dtrow[column].ToString() + "</td>");
                }
                sb.Append("</tr>");
            }

            sb.Append("</table>");

            return sb.ToString();
        }

    }
}