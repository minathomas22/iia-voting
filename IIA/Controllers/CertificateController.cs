﻿using System;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using IIA.DBContext;
using System.Linq.Dynamic;
using umbraco.cms.helpers;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using IIA.Models;
using CMS.Helpers;
using System.Data.Entity;
using Umbraco.Web;
using System.Web.Configuration;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using umbraco.editorControls.SettingControls.Pickers;
using Newtonsoft.Json;
using Umbraco.Core.Models;
using IIA.Helper;
using System.Data;
using System.Data.SqlClient;
using Umbraco.Web.Media.EmbedProviders;
using umbraco;


namespace IIA.Controllers
{
    public class CertificateController : SurfaceController
    {        
        IIAContext db = new IIAContext();
        public string PaymentIntegrationMode = WebConfigurationManager.AppSettings["PaymentIntegrationMode"].ToString();
        [HttpPost]
        public ActionResult GetMemberNonMemberList()
        {

            try
            {
                List<RegistrationViewModel> model = new List<RegistrationViewModel>();
                var dbModel = db.ctRegistrations.Where(x => x.IsActive == true && x.MembershipType!="Corporate").OrderByDescending(x => x.ProfileId).ToList();
                foreach (var item in dbModel)
                {
                    RegistrationViewModel regItem = new RegistrationViewModel();
                    regItem.ProfileId = item.ProfileId;
                    regItem.FirstName = item.FirstName;
                    regItem.MiddleName = !string.IsNullOrEmpty(item.MiddleName) ? item.MiddleName : "";
                    regItem.LastName = item.LastName;
                    regItem.EmailAddress = item.EmailAddress;
                    model.Add(regItem);
                }

                return Json(new
                {
                    status = true,
                    MemberInfo = model
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpPost]
        public ActionResult GetMemberByID(long profileId)
        {
            try
            {
                RegistrationViewModel model = new RegistrationViewModel();
                var objDbModel = db.ctRegistrations.Where(x => x.ProfileId == profileId).FirstOrDefault();                
                model.IIAGlobalNumber = objDbModel.IIAGlobalNumber;
                model.MobileNumber = objDbModel.MobileNumber;
                if (!string.IsNullOrEmpty(objDbModel.MembershipBadge))
                {
                    model.MembershipType = objDbModel.MembershipType + ", " + objDbModel.MembershipBadge;
                }
                else
                {
                    model.MembershipType = objDbModel.MembershipType;
                }                
                model.JobTitle = objDbModel.JobTitle;
                model.Organization = objDbModel.Organization;
                model.FirstName = objDbModel.FirstName;
                model.MiddleName = !string.IsNullOrEmpty(objDbModel.MiddleName) ? objDbModel.MiddleName : "";
                model.LastName = objDbModel.LastName;
                model.EmailAddress = objDbModel.EmailAddress;
                model.NationalIDNumber = objDbModel.NationalIDNumber;

                return Json(new
                {
                    status = true,
                    MemberInfo = model
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetCertificateTemplateList()
        {

            try
            {
                var ParentNode = Umbraco.TypedContent(PaymentIntegrationMode == "Test" ? 3539 : 3539);
                var template = ParentNode.Children.Where(x => x.IsVisible() == true).ToList();
                List<SelectListItem> listTemplate = new List<SelectListItem>();
                foreach (var item in template)
                {
                    SelectListItem listItem = new SelectListItem();
                    listItem.Text = item.GetPropertyValue("certificateTitleEnglish").ToString();
                    listItem.Value = item.Id.ToString();
                    listTemplate.Add(listItem);
                }

                return Json(new
                {
                    status = true,
                    template = listTemplate
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpGet]
        public ActionResult GetPDF()
        {
            string retVal = "";

            using (System.IO.StreamReader reader = new System.IO.StreamReader(Server.MapPath("~/EmailTemplate/en/Contact_Admin.html")))
            {
                retVal = reader.ReadToEnd();               

            }

            PDFGenerate.GetPDFFromHtmlToFile(retVal);
            return Content("test");
        }
    }
}