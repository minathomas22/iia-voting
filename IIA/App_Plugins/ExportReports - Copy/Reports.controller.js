﻿angular.module("umbraco").controller("Dashboard.ExportReportsController", function ($scope, $http, notificationsService)
{
    $scope.actionURL = "";
    $scope.fromDate = "";
    $scope.toDate = "";
    $scope.fileName = "";
    $scope.CourseCode = "";

	$scope.clickMe = function()
    {
        $scope.fromDate = $('#fromMonth option:selected').val() + '/' + $('#fromDay option:selected').val() + '/' + $('#fromYear option:selected').val();
        $scope.toDate = $('#toMonth option:selected').val() + '/' + $('#toDay option:selected').val() + '/' + $('#toYear option:selected').val();
        if (typeof $scope.reportType == "undefined" || $scope.reportType == "") {
            notificationsService.error("Error", "Please Choose Report Type and Date");
        }
        else if ($scope.reportType != "HarvardCourseAllApplicant") {
            if ($('#fromMonth option:selected').val() == "") {
                notificationsService.error("Error", "Please Select Date Range");
            }
        }        
        //else if ($scope.reportType == "HarvardCourseAllApplicant") {
        //    if ($('#fromMonth option:selected').val() == "") {
        //        notificationsService.error("Error", "Please Select Date Range");
        //    }
        //    if ($('#CourseCode option:selected').val() == "") {
        //        notificationsService.error("Error", "Please Select Course Code");
        //    }
        //}
        else {
            if ($scope.reportType == "Member") {
                $scope.actionURL = "/umbraco/Surface/Export/MemberReportDownload";
                $scope.fileName = "Member_" + getDateString() + ".xls";
            }
            else if ($scope.reportType == "NonMember") {
                $scope.actionURL = "/umbraco/Surface/Export/NonMemberReportDownload";
                $scope.fileName = "NonMember_" + getDateString() + ".xls";
            }           
            else if ($scope.reportType == "ContactUs") {
                $scope.actionURL = "/umbraco/Surface/Export/ContactReportDownload";
                $scope.fileName = "ContactUs_" + getDateString() + ".xls";
            } 
            else if ($scope.reportType == "ContactAdmin") {
                $scope.actionURL = "/umbraco/Surface/Export/ContactAdminReportDownload";
                $scope.fileName = "ContactAdmin_" + getDateString() + ".xls";
            } 
            else if ($scope.reportType == "CorporateJobPost") {
                $scope.actionURL = "/umbraco/Surface/Export/JobReportDownload";
                $scope.fileName = "CorporateJobPost_" + getDateString() + ".xls";
            } 
            else if ($scope.reportType == "CorporateInternshipPost") {
                $scope.actionURL = "/umbraco/Surface/Export/InternshipReportDownload";
                $scope.fileName = "CorporateInternshipPost_" + getDateString() + ".xls";
            } 
            else if ($scope.reportType == "EventRegistration") {
                $scope.actionURL = "/umbraco/Surface/Export/EventReportDownload";
                $scope.fileName = "EventRegistration_" + getDateString() + ".xls";
            } 
            else if ($scope.reportType == "CareerApply") {
                $scope.actionURL = "/umbraco/Surface/Export/CareerReportDownload";
                $scope.fileName = "CareerApply_" + getDateString() + ".xls";
            } 
            else if ($scope.reportType == "NewsletterSubscription") {
                $scope.actionURL = "/umbraco/Surface/Export/NewsletterReportDownload";
                $scope.fileName = "NewsletterSubscription_" + getDateString() + ".xls";
            } 
            else if ($scope.reportType == "ContactMember") {
                $scope.actionURL = "/umbraco/Surface/Export/ContactMemberReportDownload";
                $scope.fileName = "ContactMember_" + getDateString() + ".xls";
            } 
            else if ($scope.reportType == "RequestCallBack") {
                $scope.actionURL = "/umbraco/Surface/Export/RequestCallBackReportDownload";
                $scope.fileName = "RequestCallBack_" + getDateString() + ".xls";
            } 
            else if ($scope.reportType == "AboutInternalAuditing") {
                $scope.actionURL = "/umbraco/Surface/Export/AboutInternalAuditingReportDownload";
                $scope.fileName = "AboutInternalAuditing_" + getDateString() + ".xls";
            } 
            else if ($scope.reportType == "MemberPayment") {
                $scope.actionURL = "/umbraco/Surface/Export/MemberPaymentReportDownload";
                $scope.fileName = "Member_Registration_Payment_" + getDateString() + ".xls";
            } 
            else if ($scope.reportType == "InvoicePayment") {
                $scope.actionURL = "/umbraco/Surface/Export/InvoicePaymentReportDownload";
                $scope.fileName = "Sales_Order_Payment_" + getDateString() + ".xls";
            } 
            else if ($scope.reportType == "HarvardCourse") {
                $scope.actionURL = "/umbraco/Surface/Export/ExternalCourseReportDownload";
                $scope.fileName = "Harvard_Course_Payment_" + getDateString() + ".xls";
                $scope.prefix = "Harvard";
            } 
            else if ($scope.reportType == "HarvardCourseInterviewSchedule") {
                $scope.actionURL = "/umbraco/Surface/Export/ExternalCourseInterviewTimeDownload";
                $scope.fileName = "Harvard_Course_Interview_Schedule_" + getDateString() + ".xls";
                $scope.prefix = "Harvard";
            } 
            else if ($scope.reportType == "HarvardCourseAllApplicant") {
                $scope.actionURL = "/umbraco/Surface/Export/HarvardCourseAllApplicant";
                $scope.fileName = "Harvard_Course_All_Applicant_" + getDateString() + ".xls";
                $scope.prefix = "Harvard";    
               // $scope.CourseCode = $('#CourseCode option:selected').val();
            } 
            var data = { fromDate: $scope.fromDate, toDate: $scope.toDate, prefix: $scope.prefix};
            $http({
                method: "post",
                url: $scope.actionURL,
                datatype: "json",
                params: data
            }).then(function (response) {
                alert(response);
                var headers = response.headers();
                var blob = new Blob([response.data], { type: headers['content-type'] });
                var link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = $scope.fileName;
                link.click();
            })
        }

    }

    //$scope.getCourseCode = function () {
    //    $scope.ListCode = [];
    //    $scope.prefix = "Harvard";
    //    $scope.actionURL = "/umbraco/Surface/AdminExternalCourse/GetCourseCode";
    //    var data = { prefix: $scope.prefix };
    //    $http({
    //        method: "post",
    //        url: $scope.actionURL,
    //        datatype: "json",
    //        params: data
    //    }).then(function (response) {                
    //        $scope.ListCode = response.data.CourseInfo;
    //    })        
    //}

    //$scope.showhideCourseCode = function () {        
    //    if ($scope.reportType == "HarvardCourseAllApplicant") {
    //        $("#CourseCode").css("display", "block");
    //    }
    //    else {
    //        $("#CourseCode").css("display", "none");
    //    }
    //}
});

function getDateString() {
    const date = new Date();
    const year = date.getFullYear();
    const month = `${date.getMonth() + 1}`.padStart(2, '0');
    const day = `${date.getDate()}`.padStart(2, '0');
    return `${year}${month}${day}`
}