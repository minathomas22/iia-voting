﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class CartModel
    {
        public long CartId { get; set; }
        public string CartKey { get; set; }
        public string ProductType { get; set; }
        public int ProductCmsId { get; set; }
        public string ProductCmsName { get; set; }
        public string ProductPicture { get; set; }
        public int CourseCmsId { get; set; }
        public string CourseCmsName { get; set; }
        public int LevelCmsId { get; set; }
        public string LevelCmsName { get; set; }
        public int StudyMethodCmsId { get; set; }
        public string StudyMethodCmsName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public decimal Price { get; set; }
        public int Duration { get; set; }
        public int Quantity { get; set; }
        public decimal Discount { get; set; }
        public System.DateTime LastUpdated { get; set; }
        public string lang { get; set; }

        public string ShowHideCounter { get; set; }
    }
}