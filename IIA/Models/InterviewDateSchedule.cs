﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class InterviewDateSchedule
    {
        public string InterviewDate { get; set; }
        public string InterviewDateMonth { get; set; }
        public string InterviewDateDay { get; set; }
        public string InterviewDateYear { get; set; }
        public string InterviewDateDayName { get; set; }
        public int TotalAvailableSlot { get; set; }
        public string SerialNo { get; set; }

        public string IsDefaultChecked { get; set; }
    }
}