﻿namespace IIA.Models
{
    public class AdminUserViewModel
    {
        public int AdminId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}