﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class InvoiceViewModel
    {
        public InvoiceModel InvoiceInfo { get; set; }
        public List<InvoiceDetailsModel> ListInvoiceItem { get; set; }
    }
}