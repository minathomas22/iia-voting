﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class TrainingCalendarCourse
    {
        public string MainCourse { get; set; }
        public string cmsMainCourseName { get; set; }
        public int cmsMainCourseId { get; set; }
       
        public string Level { get; set; }
        public string cmsLevelName { get; set; }
        public int cmsLevelId { get; set; }        
        
        public List<TrainingCalendarDate> trainingCalendarDates { get; set; }
       
    }
}