﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class QuickbookInvoiceModel
    {
        public Line[] Line { get; set; }
               
        public CustomerRef CustomerRef { get; set; }

        public ShipAddr ShipAddr { get; set; }

        public BillAddr BillAddr { get; set; }
    }

    public partial class CustomerRef
    {        
        public string value { get; set; }
    }

    public partial class Line
    {
        public string DetailType { get; set; }        
        public decimal Amount { get; set; }       
        public SalesItemLineDetail SalesItemLineDetail { get; set; }
    }

    public partial class SalesItemLineDetail
    {        
        public ItemRef ItemRef { get; set; }
        public decimal Qty { get; set; }
        public decimal UnitPrice { get; set; }
        public TaxCodeRef TaxCodeRef { get; set; }

    }
    public partial class ItemRef
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public partial class TaxCodeRef
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public partial class ShipAddr
    {
        public string Line1 { get; set; }

        public string Line2 { get; set; }
        public string Line3 { get; set; }

        public string Line4 { get; set; }

        public string Line5 { get; set; }

        public string City { get; set; }

        public string Country { get; set; }
    }

    public partial class BillAddr
    {
        public string Line1 { get; set; }

        public string Line2 { get; set; }
        public string Line3 { get; set; }

        public string Line4 { get; set; }

        public string Line5 { get; set; }

        public string City { get; set; }

        public string Country { get; set; }
    }
}

