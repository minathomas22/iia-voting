﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;

namespace IIA.Models
{
    public class ShopCheckoutViewModel
    {
        public InvoiceModel Invoice { get; set; }
        public List<InvoiceDetailsModel> InvoiceDetails { get; set; }
    }
}