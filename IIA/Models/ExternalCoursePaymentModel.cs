﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class ExternalCoursePaymentModel
    {
        [Key]
        public long ExternalPaymentId { get; set; }
        public Nullable<long> RegId { get; set; }
        public Nullable<long> ProfileId { get; set; }
        public string InvoiceNumber { get; set; }
        public Nullable<decimal> SubTotal { get; set; }
        public Nullable<decimal> Shipping { get; set; }
        public Nullable<decimal> VAT { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public Nullable<decimal> GrandTotal { get; set; }
        public Nullable<decimal> TransactionAmount { get; set; }
        public string TransactionCurrency { get; set; }
        public string TransactionReference { get; set; }
        public string TransactionStatus { get; set; }
        public string PaymentMethod { get; set; }
        public string ShippingReference { get; set; }
        public string ShippingReferencePDF { get; set; }
        public Nullable<System.DateTime> TransactionDate { get; set; }
        public string PromoCode { get; set; }

        public string CourseName { get; set; }

        public string lang { get; set; }

        public string SeatConfirmMessage { get; set; }

        public string PaymentType { get; set; }

    }
}