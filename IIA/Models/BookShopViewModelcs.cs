﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Web;

namespace IIA.Models
{
    public class BookShopViewModelcs
    {
        public int ProductId { get; set; }
        public string ProductCmsName { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public decimal Price { get; set; }

        public decimal DisplayPrice { get; set; }

        public string Url { get; set; }
        
    }
}