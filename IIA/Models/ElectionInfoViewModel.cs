﻿using IIA.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class ElectionInfoViewModel
    {
        [Key]
        public long ElectionId { get; set; }
        public string Title { get; set; }
        public string TitleAr { get; set; }
        public string ApplyTitle { get; set; }
        public string ApplyTitleAr { get; set; }
        public string Description { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string ApplyStartDate { get; set; }
        public string ApplyEndDate { get; set; }
        public int NumberOfWinners { get; set; }
        public string WinningAnnouncementDate { get; set; }
        public ElectionStatus Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool CreatedByRedo { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}