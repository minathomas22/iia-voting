﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class EventRegisterModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string ContactNo { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public int EventId { get; set; }
        public string EventCmsName { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string lang { get; set; }
    }
}