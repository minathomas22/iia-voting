﻿using IIA.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class CandidateBaseViewModel
    {
        public long MemberId { get; set; }
        public long ElectionId { get; set; }
        public CandidateRequestStatus Status { get; set; } = CandidateRequestStatus.UnderRequest;
    }
}