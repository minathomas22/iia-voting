﻿using IIA.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class CandidateIsAppyStatusViewModel
    {
        [Required]
        public long MemberId { get; set; }

        [Required]
        public long ElectionId { get; set; }
    }
}