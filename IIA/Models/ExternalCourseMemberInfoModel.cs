﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class ExternalCourseMemberInfoModel
    {
        [Key]
        public long Id { get; set; }
        public Nullable<long> ProfileId { get; set; }
        public string Education { get; set; }
        public string EducationCertificate { get; set; }
        public string CIAHolder { get; set; }
        public string CIAHolderCertificate { get; set; }
        public string EnglishProficiency { get; set; }
        public string HoldEnglishProficiency { get; set; }
        public string EnglishProficiencyCertificate { get; set; }
        public string OtherProfessionalCertificates { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }

    }
}