﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class InvoiceModel
    {
        public long InvoiceId { get; set; }
        public long  ProfileId { get; set; }
        public string InvoiceNumber { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Discount { get; set; }
        public decimal Shipping { get; set; }
        public decimal VAT { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal TransactionAmount { get; set; }
        public string TransactionCurrency { get; set; }
        public string TransactionReference { get; set; }
        public string TransactionStatus { get; set; }
        public string PaymentMethod { get; set; }
        public string ShippingReference { get; set; }
        public string ShippingReferencePDF { get; set; }
        public System.DateTime TransactionDate { get; set; }
        public string lang { get; set; }
        public string PromoCode { get; set; }
    }
}