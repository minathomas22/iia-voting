﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class InterviewTimeSchedule
    {
        public string InterviewDisplayTime { get; set; }
        public string InterviewTime { get; set; }

        public string SerialNo { get; set; }

        public string IsDisable { get; set; }
    }
}