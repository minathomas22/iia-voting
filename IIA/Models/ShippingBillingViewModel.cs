﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class ShippingBillingViewModel
    {
        [Key]
        public long ShippingId { get; set; }
        public long ProfileId { get; set; }
        public string BFirstName { get; set; }
        public string BMiddleName { get; set; }
        public string BLastName { get; set; }
        public string BEmailAddress { get; set; }
        public string BMobileNumber { get; set; }
        public string BPhoneNumber { get; set; }
        public string BCountry { get; set; }
        public string BCity { get; set; }
        public string BArea { get; set; }
        public string BAddress1 { get; set; }
        public string BAddress2 { get; set; }
        public string BAddress3 { get; set; }
        public string SFirstName { get; set; }
        public string SMiddleName { get; set; }
        public string SLastName { get; set; }
        public string SEmailAddress { get; set; }
        public string SMobileNumber { get; set; }
        public string SPhoneNumber { get; set; }
        public string SCountry { get; set; }
        public string SCity { get; set; }
        public string SArea { get; set; }
        public string SAddress1 { get; set; }
        public string SAddress2 { get; set; }
        public string SAddress3 { get; set; }
        public string BillingType { get; set; }
        public string ShippingType { get; set; }
        public System.DateTime LastUpdated { get; set; }
        public string lang { get; set; }
    }
}