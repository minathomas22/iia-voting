﻿using IIA.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class ElectionViewModel
    {
        [Key]
        public long ElectionId { get; set; }
        public string Title { get; set; }
        public string TitleAr { get; set; }
        public string ApplyTitle { get; set; }
        public string ApplyTitleAr { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime ApplyStartDate { get; set; }
        public DateTime ApplyEndDate { get; set; }
        public int NumberOfWinners { get; set; }
        public DateTime WinningAnnouncementDate { get; set; }
        public ElectionStatus Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool CreatedByRedo { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public bool IsRedo { get; set; } = false;
        public int PastElectionId { get; set; }
    }
}