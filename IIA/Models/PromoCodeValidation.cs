﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class PromoCodeValidation
    {
        public string Message { get; set; }
        public decimal Value { get; set; }
    }
}