﻿namespace IIA.Models
{
    public class AdminMemberSearchModel
    {
        public long ProfileId { get; set; }
        public string MembershipType { get; set; }
        public string IIAGlobalNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string MobileNumber { get; set; }
        public string NationalIDNumber { get; set; }
        public string EmailAddress { get; set; }
        public string IsActive { get; set; }
        public string MembershipStartDate { get; set; }
        public string MembershipEndDate { get; set; }
        public string FirstName_ar { get; set; }
        public string MiddleName_ar { get; set; }
        public string LastName_ar { get; set; }
        public string JobTitle_ar { get; set; }
        public string Organization_ar { get; set; }
        public string City_ar { get; set; }
        public string PreferredLanguage { get; set; }

    }
}