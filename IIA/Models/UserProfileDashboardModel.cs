﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class UserProfileDashboardModel
    {
        public RegistrationViewModel RegisterInfo { get; set; }
        public List<InvoiceDetailsModel> PurchaseList { get; set; }
    }
}