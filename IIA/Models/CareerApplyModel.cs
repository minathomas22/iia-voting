﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class CareerApplyModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string ContactNo { get; set; }
        public string Message { get; set; }
        public string CV { get; set; }
        public string ApplyFor { get; set; }
        public long PostId { get; set; }
        public string PostTitle { get; set; }
        public string PostType { get; set; }
        public System.DateTime ApplyDate { get; set; }
        public string lang { get; set; }

    }
}