﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class ExternalCourseRegistrationModel
    {
        [Key]
        public long RegId { get; set; }
        public Nullable<long> ProfileId { get; set; }
        public Nullable<int> CourseCmsId { get; set; }
        public string CourseCmsTitle { get; set; }
        public string CourseCode { get; set; }
        public string EnglishSpokenPaymentReceipt { get; set; }
        public string EnglishSpokenResult { get; set; }
        public string EnglishSpokenResultScore { get; set; }
        public string IsEnglishSpokenPass { get; set; }
        public Nullable<System.DateTime> InterviewDate { get; set; }
        public string InterviewTime { get; set; }
        public string IsInterviewPass { get; set; }
        public string IsSeatConfirmed { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }

        public string IsPaymentSuccess { get; set; }

        public string strInterviewDate { get; set; }

        public string IsApplicationFeePaid { get; set; }


    }
}