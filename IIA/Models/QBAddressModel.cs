﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class QBAddressModel
    {
        public PhysicalAddress PhysicalAddress { get; set; }
    }

    public partial class PhysicalAddress
    {
        public string Line1 { get; set; }

        public string Line2 { get; set; }
        public string Line3 { get; set; }

        public string Line4 { get; set; }

        public string Line5 { get; set; }

        public string City { get; set; }

        public string Country { get; set; }
    }
}