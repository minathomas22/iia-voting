﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class QuickBookItem
    {       
        public string Name { get; set; }       
        public IncomeAccountRef IncomeAccountRef { get; set; }        
        public string Type { get; set; }
    }
    public partial class IncomeAccountRef
    {        
        public string name { get; set; }
                
        public string value { get; set; }
    }
}