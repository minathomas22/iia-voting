﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class JobInternshipViewModel
    {
        [Key]
        public long PostId { get; set; }
        public long ProfileId { get; set; }
        public string PostType { get; set; }
        public string JobTitle { get; set; }
        public string Company { get; set; }
        public string Location { get; set; }
        public string Tenure { get; set; }
        public string JobDescription { get; set; }
        public string JobResponsibilities { get; set; }
        public string BannerImage { get; set; }
        public string BackgroundImage { get; set; }
        public System.DateTime DatePosted { get; set; }
        public System.DateTime LastUpdated { get; set; }
        public bool IsApprove { get; set; }

        public bool IsActive { get; set; }

        public string strDatePosted { get; set; }

        public string lang { get; set; }
                
    }
}