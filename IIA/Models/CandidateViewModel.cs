﻿using IIA.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class CandidateViewModel
    {
        public long CandidateId { get; set; }
        [Required]
        public long MemberId { get; set; }
        [Required]
        public long ElectionId { get; set; }

        [Required]
        public string Bio { get; set; }
        [Required]
        public string Description { get; set; }

        public int PageId { get; set; }

        public CandidateRequestStatus Status { get; set; }
        public string ResumeFile { get; set; }
        public string ElectionProgramFile { get; set; }
        public string LetterOfInterestFile { get; set; }
        public string OtherDocumentFile { get; set; }
        public IEnumerable<HttpPostedFileBase> OtherDocumentFiles { get; set; }
        public List<string> OtherDocumentFilesList { get; set; }
        public string Comment { get;  set; }
        public string StatusName { get; set; }
    }
}