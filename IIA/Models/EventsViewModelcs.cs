﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Web;

namespace IIA.Models
{
    public class EventsViewModelcs
    {
        public int EventId { get; set; }
        public string EventCmsName { get; set; }
        public string Title { get; set; }
        public string Introduction { get; set; }
        public string City { get; set; }
        public string Type { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Image { get; set; }
        public string Url { get; set; }

    }
}