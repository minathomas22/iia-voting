﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class ElectionStatusViewModel
    {
        public int Id { get; set; }

        public string Text { get; set; }

    }
}