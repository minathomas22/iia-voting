﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class RegistrationViewModel
    {
        [Key]
        public long ProfileId { get; set; }
        public long ParentProfileId { get; set; }
        public string ProfileType { get; set; }
        public string MembershipType { get; set; }
        public string MembershipBadge { get; set; }
        public string IIAKSANumber { get; set; }
        public string IIAGlobalNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string DateOfBirth { get; set; }
        public string JobTitle { get; set; }
        public string Organization { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Zip_Postal_Code { get; set; }
        public string MobileNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string NationalIDNumber { get; set; }
        public string ProfilePhoto { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public System.DateTime LastUpdated { get; set; }
        public bool IsActive { get; set; }
        public string lang { get; set; }
        public bool IsEmailVerify { get; set; }
        public int MembershipDiscount { get; set; }
        public System.DateTime? MemberActivationDate { get; set; }

        public string Experience { get; set; }
        public string Qualification { get; set; }
        public string Bio { get; set; }

        public string IsEnrolledValid { get; set; }

        public bool IsApplyForMentor { get; set; }
        public bool IsApproveMentor { get; set; }

        public System.DateTime? MembershipStartDate { get; set; }
        public System.DateTime? MembershipEndDate { get; set; }
        public System.DateTime? MembershipRenewalDate { get; set; }
        public System.DateTime? MembershipCancelDate { get; set; }
        public System.DateTime? BanDate { get; set; }

        public string FirstName_ar { get; set; }
        public string MiddleName_ar { get; set; }
        public string LastName_ar { get; set; }
        public string JobTitle_ar { get; set; }
        public string Organization_ar { get; set; }
        public string City_ar { get; set; }
        public string Experience_ar { get; set; }
        public string Qualification_ar { get; set; }
        public string Bio_ar { get; set; }

        public string PreferredLanguage { get; set; }

        public string strMembershipStartDate { get; set; }
        public string strMembershipEndDate { get; set; }

        public System.DateTime? GraduationDate { get; set; }
        public string strBanDate { get; set; }
    }
}