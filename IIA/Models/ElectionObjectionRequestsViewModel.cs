﻿using IIA.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class ElectionObjectionRequestsViewModel
    {
        public long ObjectionRequestId { get; set; }
        public string ElectionName { get; set; }
        public string CandidateName { get; set; }
        public string MemberName { get; set; }
        public string Reason { get; set; }
        public string StatusName { get; set; }
        public string BanDate { get; set; }
    }
}