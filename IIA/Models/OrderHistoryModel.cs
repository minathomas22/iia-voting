﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class OrderHistoryModel
    {
        public long InvoiceId { get; set; }
        public string strInvoiceDate { get; set; }

        public DateTime InvoiceDate { get; set; }

        public string GrandTotal { get; set; }

        public string InvoiceType { get; set; }
    }
}