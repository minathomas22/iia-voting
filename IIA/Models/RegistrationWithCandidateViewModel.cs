﻿using IIA.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class RegistrationWithCandidateViewModel : RegistrationViewModel 
    {
        public long ElectionId { get; set; }

        public CandidateRequestStatus CandidateStatus { get; set; }

        public string ElectionTitle { get; set; }

        public long CandidateId { get; set; }

        public long MemberId { get; set; }

        public string Description { get; set; }

        public string ResumeFile { get; set; }

        public string ElectionProgramFile { get; set; }

        public string LetterOfInterestFile { get; set; }

        public string OtherDocumentFile { get; set; }

        public List<string> OtherDocumentFiles { get; set; }

        public bool IsAgree { get; set; } = false;

        public bool IsAcknowledge { get; set; } = false;

        public string Comment { get; set; }
    }
}