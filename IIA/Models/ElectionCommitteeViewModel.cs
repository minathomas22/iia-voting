﻿using IIA.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class ElectionCommitteeViewModel
    {
        public long ElectionId { get; set; }
        public string MemberIds { get; set; }
    }
}