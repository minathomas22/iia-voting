﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class ContactFormModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string ContactNo { get; set; }
        public string Company { get; set; }
        public string Designation { get; set; }
        public string Message { get; set; }
        public string ContactType { get; set; }
        public string PageId { get; set; }        
        public DateTime ContactDate { get; set; }
        public string lang { get; set; }

    }
}