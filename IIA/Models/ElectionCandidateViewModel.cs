﻿using IIA.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class ElectionCandidateViewModel
    {
        public string CandidateProfile { get; set; }
        public string CandidateName { get; set; }
        public string JobTitle { get; set; }
        public string Bio { get; set; }
        public int Status { get; set; }
        public long CandidateId { get; set; }
        public string ResumeFile { get; set; }
        public string ElectionProgramFile { get; set; }
        public string CandidateDescription { get;  set; }
        public long CandidateMemberId { get;  set; }
        public string ElectionName { get; set; }
        public int CanObjectStatus { get; set; }
        public long ElectionId { get; set; }
    }
}