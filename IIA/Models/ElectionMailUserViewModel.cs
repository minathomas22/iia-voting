﻿using IIA.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class ElectionMailUserViewModel
    {
        [Key]
        public long ElectionId { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string UserMail { get; set; }
    }
}