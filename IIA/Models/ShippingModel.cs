﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class ShippingModel
    {
        public string AWBNo { get; set; }
        public string AWBPDF { get; set; }
    }
}