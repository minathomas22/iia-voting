﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class TrainingCalendarDate
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }      

        public decimal Price { get; set; }
        public decimal DisplayPrice { get; set; }
        public string Duration { get; set; }

        public string TotalHours { get; set; }
        public string StudyMethods { get; set; }
        public string cmsStudyMethodName { get; set; }
        public int cmsStudyMethodsId { get; set; }

        public List<OurTrainerModel> OurTrainer { get; set; }
    }
}