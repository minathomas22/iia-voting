﻿using IIA.DBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class LeftMenuModel
    {
        public RegistrationViewModel RegistrationViewModel { get; set; }
        public List<ctElection> Elections { get; set; } = new List<ctElection>();
    }
}