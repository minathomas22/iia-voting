﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class MemberCourseRegistrationModel
    {
        public string EmailAddress { get; set; }
        public string IIAGlobalNumber { get; set; }
        public string FirstName { get; set; }
        public string FirstName_ar { get; set; }
        public string MiddleName { get; set; }
        public string MiddleName_ar { get; set; }
        public string LastName { get; set; }
        public string LastName_ar { get; set; }
        public string JobTitle { get; set; }
        public string JobTitle_ar { get; set; }
        public string Organization { get; set; }
        public string Organization_ar { get; set; }
        public string MobileNumber { get; set; }
        public string NationalIDNumber { get; set; }
        public string Experience { get; set; }
        public string Experience_ar { get; set; }
        public string lang { get; set; }
        public string Education { get; set; }
        public string EducationCertificate { get; set; }
        public string CIAHolder { get; set; }
        public string CIAHolderCertificate { get; set; }
        public string EnglishProficiency { get; set; }
        public string HoldEnglishProficiency { get; set; }
        public string EnglishProficiencyCertificate { get; set; }

        public string OtherProfessionalCertificates { get; set; }

        public string EnglishSpokenPaymentReceipt { get; set; }
        public string EnglishSpokenResult { get; set; }
        public string InterviewDate { get; set; }
        public string InterviewTime { get; set; }
        public string EnglishSpokenResultScore { get; set; }
        public string IsEnglishSpokenPass { get; set; }
        public string IsInterviewPass { get; set; }

    }
}