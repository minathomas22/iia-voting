﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class QuickBookCustomer
    {
        public string FullyQualifiedName { get; set; }       
        public PrimaryEmailAddr PrimaryEmailAddr { get; set; }
        
        public string DisplayName { get; set; }
        
        public string MiddleName { get; set; }
       
        public string FamilyName { get; set; }
       
        public PrimaryPhone PrimaryPhone { get; set; }
        
        public string CompanyName { get; set; }
        
        public string GivenName { get; set; }
    }

    public partial class PrimaryEmailAddr
    {        
        public string Address { get; set; }
    }

    public partial class PrimaryPhone
    {       
        public string FreeFormNumber { get; set; }
    }
}
