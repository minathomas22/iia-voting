﻿using IIA.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class CandidateUpdateStatusViewModel
    {
        public long CandidateId { get; set; }
        public bool IsApprove { get; set; }
        public string Comment { get; set; }

    }
}