﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class PaymentGateayStep1Model
    {
        public string CheckoutId { get; set; }
        public string PaymentMethod { get; set; }
        public string TransactionAmount { get; set; }

        public string PaymentUrl { get; set; }

        public string RedirectUrl { get; set; }
    }
}