﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IIA.Models
{
    public class CartViewModel
    {
        public decimal SubTotal { get; set; }
        public decimal Discount { get; set; }
        public decimal Shipping { get; set; }
        public decimal VAT { get; set; }
        public decimal GrandTotal { get; set; }
        public string lang { get; set; }
        public List<CartModel> CartList { get; set; }
    }
}